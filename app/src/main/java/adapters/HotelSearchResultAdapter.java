package adapters;

import android.app.Activity;
import android.util.Log;
import android.util.Pair;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import singleton.AppController;
import utils.Util;

/**
 * Created by STEFAN on 12/25/2015.
 */
public class HotelSearchResultAdapter extends ArrayAdapter<String> implements Filterable {
    ArrayList<String> resultList;
    Activity act;
    int partialResourceID;
    ArrayList<String> data;
    String token;

    boolean dataChanging=false;
    public HotelSearchResultAdapter(Activity act, int partialResourceID, ArrayList<String> data){
        super(act,partialResourceID,data);
        this.act = act;
        this.partialResourceID = partialResourceID;
        this.data = data;
        token = "";
    }

    @Override
    public Filter getFilter() {
        Log.v("setres", "foo");
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if(constraint!=null){
                    updateSuggestions(constraint.toString());
                }
                return null;
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            }
        };
    }

    public void setToken(String token){
        this.token = token;
    }

    protected void updateSuggestions(String keyword){
        String url = Util.BASE_URL + Util.API_GATEWAY + Util.BOOKING_URL + Util.HOTEL_AUTOCOMPLETE_URL;
        url += "?token=" + token + "&q=" + keyword;
        Log.v("queryhotel",keyword);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(!dataChanging) {
                    Log.v("hotelsearchresultadapter","inside datachanging");
                    dataChanging = true;
                    try {
                        JSONArray results = response.getJSONObject("results").getJSONArray("result");
                        data.clear();
                        int sz = results.length();
                        Log.v("resultsize",String.valueOf(sz));
                        for (int i = 0; i < sz; i++) {
                            data.add(results.getJSONObject(i).getString("value"));
                            notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    notifyDataSetChanged();
                    dataChanging = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().cancelPendingRequests("HotelSearchAdapter");
        AppController.getInstance().addToRequestQueue(jor,"HotelSearchAdapter");
    }

    /*
    protected ArrayList<String> updateSuggestions(String keyword){
        ArrayList<String> resultList = new ArrayList<>();

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(Util.BASE_URL + Util.API_GATEWAY + Util
                    .BOOKING_URL + Util.HOTEL_AUTOCOMPLETE_URL);
            sb.append("?token="+token);
            sb.append("&q="+keyword);
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("HotelAdapter", "Url Error", e);
            return resultList;
        } catch (IOException e) {
            Log.e("HotelAdapter", "Error No Internet", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Log.d(TAG, jsonResults.toString());

            // Create a JSON object hierarchy from the results
            JSONObject result = new JSONObject(jsonResults.toString());
            JSONArray suggestions = result.getJSONObject("results").getJSONArray("result");

            for (int i = 0; i < suggestions.length(); i++) {
                resultList.add(suggestions.getJSONObject(i).getString("value"));
            }
        } catch (JSONException e) {
            Log.e("HotelAdapter", "Cannot process JSON results", e);
        }

        return resultList;
    }*/

}
