package adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.AgendaTrip.R;

import java.util.ArrayList;

/**
 * Created by STEFAN on 12/27/2015.
 */
public class IconTextViewAdapter extends ArrayAdapter<String> {
    LayoutInflater inflater;
    Activity act;
    int partialResourceId,iconResourceId;
    ArrayList<String> data;
    public IconTextViewAdapter(Activity act,int partialResourceId, int
            iconResourceId, ArrayList<String> data){
        super(act,partialResourceId,data);
        this.act = act;
        this.partialResourceId = partialResourceId;
        this.iconResourceId = iconResourceId;
        this.data = data;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        IconTextViewHolder itvh = null;

        if(row ==null || row.getTag() == null){
            Log.v("photoadap", "if");
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(this.partialResourceId, parent, false);
            itvh = new IconTextViewHolder(row);

            row.setTag(itvh);
        }
        else{
            Log.v("photoadap","else");
            itvh = (IconTextViewHolder) row.getTag();
        }

        itvh.setData(getItem(position),iconResourceId);

        return row;
    }

    public class IconTextViewHolder{
        TextView textView;
        ImageView icon;

        public IconTextViewHolder(View src){
            textView = (TextView) src.findViewById(R.id.iconTextViewTextView);
            icon = (ImageView) src.findViewById(R.id.iconTextViewIcon);
        }

        public void setData(String text, int iconId){
            textView.setText(text);
            icon.setImageResource(iconId);
        }
    }
}
