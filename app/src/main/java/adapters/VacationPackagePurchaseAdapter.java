package adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.android.AgendaTrip.R;

import java.util.ArrayList;

import model.VacationPackage;
import model.VacationPackagePurchase;
import utils.Util;

/**
 * Created by STEFAN on 1/4/2016.
 */
public class VacationPackagePurchaseAdapter extends ArrayAdapter<VacationPackagePurchase> {
    LayoutInflater inflater;
    Activity act;
    int partialResourceId;
    ArrayList<VacationPackagePurchase> data;

    public VacationPackagePurchaseAdapter(Activity act, int partialResourceId, ArrayList<VacationPackagePurchase> data){
        super(act,partialResourceId,data);
        this.act = act;
        this.partialResourceId = partialResourceId;
        this.data = data;
    }

    @Override
    public VacationPackagePurchase getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        VacationPackagePurchaseViewHolder fh = null;

        if(row ==null || row.getTag() == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(this.partialResourceId, parent, false);
            fh = new VacationPackagePurchaseViewHolder(row);
            row.setTag(fh);
        }
        else{
            fh = (VacationPackagePurchaseViewHolder) row.getTag();
        }

        fh.setData(getItem(position));

        return row;

    }

    class VacationPackagePurchaseViewHolder{
        VacationPackagePurchase vpp;
        TextView name,price,purchaseDate,departureDate;

        public VacationPackagePurchaseViewHolder(View source){
            name = (TextView) source.findViewById(R.id.vacationPackagePurchaseVacationPackageName);
            price = (TextView) source.findViewById(R.id.vacationPackagePurchasePrice);
            purchaseDate = (TextView) source.findViewById(R.id.vacationPackagePurchaseDate);
            departureDate = (TextView) source.findViewById(R.id.vacationPackageDepartureDate);
        }

        public void setData(VacationPackagePurchase vpp){
            this.vpp=vpp;
            this.name.setText(vpp.getVp().getName());
            this.price.setText(Util.convertDoubleToIDRString(vpp.getPrice()));
            this.purchaseDate.setText("Purchase Date : " + vpp.getPurchaseDateAsString());
            this.departureDate.setText("Departure Date : " + vpp.getVp().getDepartureDateAsString());
        }
    }
}
