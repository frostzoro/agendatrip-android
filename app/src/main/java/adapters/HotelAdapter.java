package adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.android.AgendaTrip.FlightSearchResult;
import com.example.android.AgendaTrip.HotelDetail;
import com.example.android.AgendaTrip.HotelSearchResult;
import com.example.android.AgendaTrip.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import model.Flight;
import model.Hotel;
import utils.Util;

/**
 * Created by STEFAN on 12/25/2015.
 */
public class HotelAdapter extends ArrayAdapter<Hotel> {
    LayoutInflater inflater;
    Activity act;
    int partialResourceId;
    ArrayList<Hotel> data;

    public HotelAdapter(Activity act, int partialResourceId, ArrayList<Hotel> data){
        super(act,partialResourceId,data);
        this.act = act;
        this.partialResourceId = partialResourceId;
        this.data = data;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Hotel getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {View row = convertView;
        HotelViewHolder fh = null;

        if(row ==null || row.getTag() == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(this.partialResourceId, parent, false);
            fh = new HotelViewHolder(row);
            row.setTag(fh);
        }
        else{
            fh = (HotelViewHolder) row.getTag();
        }

        fh.setData(getItem(position));

        return row;
    }

    class HotelViewHolder{
        TextView name,price,oldPrice,promoName;
        ImageView image;
        RatingBar stars;
        Hotel h;

        public HotelViewHolder(View src){
            name = (TextView) src.findViewById(R.id.partialHotelName);
            price = (TextView) src.findViewById(R.id.partialHotelPrice);
            oldPrice = (TextView) src.findViewById(R.id.partialHotelOldPrice);
            promoName = (TextView) src.findViewById(R.id.partialHotelPromoName);
            image = (ImageView) src.findViewById(R.id.partialHotelImage);
            stars = (RatingBar) src.findViewById(R.id.partialHotelStars);
        }

        public void setData(Hotel h){
            this.h = h;
            name.setText(h.breadCrumb.getName());
            price.setText(Util.convertDoubleToIDRString(Double.valueOf(h.breadCrumb.getPrice())));
            if(h.breadCrumb.getOldprice() != null){
                oldPrice.setText(Util.convertDoubleToIDRString(Double.valueOf(h.breadCrumb.getOldprice
                        ())));
                oldPrice.setVisibility(View.VISIBLE);
                oldPrice.setPaintFlags(oldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);}
            else
                oldPrice.setVisibility(View.GONE);
            if(h.breadCrumb.getPromo_name().length()>0){
                promoName.setVisibility(View.VISIBLE);
                promoName.setText(h.breadCrumb.getPromo_name());}
            else
                promoName.setVisibility(View.GONE);
            Picasso.with(act).load(h.breadCrumb.getPhoto()).placeholder(R.drawable.hotel_icon).into
                    (this
                    .image);
            int star = Integer.valueOf(h.breadCrumb.getStar());
            stars.setRating(star);
        }
    }
}
