package adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.UserIdentityDetail;

import java.util.ArrayList;

import model.UserIdentity;
import model.VacationSiteTag;

/**
 * Created by STEFAN on 12/22/2015.
 */
public class UserIdentityAdapter extends ArrayAdapter<UserIdentity> {
    private LayoutInflater inflater;
    Activity act;
    int partial_resource_id;
    ArrayList<UserIdentity> data;

    public UserIdentityAdapter(Activity act, int resource, ArrayList<UserIdentity> objects){
        super(act,resource,objects);
        this.act = act;
        this.partial_resource_id = resource;
        this.data = objects;
    }

    @Override
    public int getPosition(UserIdentity item) {
        return super.getPosition(item);
    }

    @Override
    public UserIdentity getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent){
        View row = convertView;
        UserIdentityViewHolder uivh = null;

        if( row == null || row.getTag() == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(partial_resource_id, parent, false);

            uivh = new UserIdentityViewHolder(row);
            row.setTag(uivh);
        }
        else {
            uivh = (UserIdentityViewHolder) row.getTag();
        }
        uivh.setData(getItem(position));
        final UserIdentity finalui = uivh.userIdentity;
        uivh.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(act, UserIdentityDetail.class);
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("user_identity",finalui);
                mBundle.putString("mode","edit");
                it.putExtras(mBundle);
                act.getFragmentManager().findFragmentById(R.id.content_frame).startActivityForResult(it,0);
            }
        });
        return row;
    }

     class UserIdentityViewHolder{
        public UserIdentity userIdentity;
        TextView name;
        Button edit;

        public UserIdentityViewHolder(View rowSource){
            this.name = (TextView) rowSource.findViewById(R.id.partialMyIdentityName);
            this.edit = (Button) rowSource.findViewById(R.id.partialMyIdentityEdit);
        }

        public void setData(UserIdentity ui){
            this.userIdentity = ui;
            this.name.setText(ui.getFirst_name() + " " + ui.getLast_name());
        }
    }
}
