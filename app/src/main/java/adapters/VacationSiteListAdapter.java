package adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.VacationSiteDetail;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;

import model.VacationSite;
import singleton.AppController;
import utils.Util;

/**
 * Created by STEFAN on 2015/10/17.
 */
public class VacationSiteListAdapter extends ArrayAdapter<VacationSite> {
    private LayoutInflater inflater;
    private ArrayList<VacationSite> data;
    private Activity act;
    private int partialResourceID;
    ImageLoader imgLoader = AppController.getInstance().getImageLoader();


    //contants
    public static final int SORT_BY_PREFERENCE = 0;
    public static final int SORT_BY_POPULARITY = 1;
    public static final int SORT_BY_RELEVANCE = 2;

    public VacationSiteListAdapter(Activity act, int resource, ArrayList<VacationSite> objects) {
        super(act, resource, objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public VacationSite getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(VacationSite item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        VacationSiteViewHolder vs = null;

        if(row == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(partialResourceID, parent, false);

            vs = new VacationSiteViewHolder(row);

            row.setTag(vs);
        }
        else{
            vs = (VacationSiteViewHolder) row.getTag();
        }

        try{
            vs.setData(getItem(position));
            Log.v("topvcidoutclick", String.valueOf(vs.vs.getID()));
            final VacationSite finalvs = vs.vs;

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(act, VacationSiteDetail.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("vacation_site",String.valueOf(finalvs.getID()));
                    mBundle.putString("vacation_site_name",String.valueOf(finalvs.getName()));
                    mBundle.putString("name",getItem(position).getName());
                    mBundle.putDouble("longitude", getItem(position).getLongitude());
                    mBundle.putDouble("latitude",getItem(position).getLatitude());
                    it.putExtras(mBundle);
                    act.startActivity(it);
                }
            });
        }
        catch(Exception e){
            Log.v("Adapter getView", e.getStackTrace().toString());
        };

        return row;
    }

    public void sortItem(int type){
        switch(type){
            case SORT_BY_PREFERENCE :
                sort (new Comparator<VacationSite>() {
                    @Override
                    public int compare(VacationSite lhs, VacationSite rhs) {
                        return (lhs.getHypotheses()>rhs.getHypotheses()) ? 1 : 0;
                    }
                });
                break;
            case SORT_BY_POPULARITY :
                sort(new Comparator<VacationSite>(){
                    @Override
                    public int compare(VacationSite lhs, VacationSite rhs) {
                        return (lhs.getRating()>rhs.getRating()) ? 1 : 0;
                    }
                });
                break;
            case SORT_BY_RELEVANCE :
                sort(new Comparator<VacationSite>(){
                    @Override
                    public int compare(VacationSite lhs, VacationSite rhs) {
                        return (lhs.getRelevance()>rhs.getRelevance()) ? 1 : 0;
                    }
                });
                break;
        }
        notifyDataSetChanged();
    }

    public class VacationSiteViewHolder{
        public VacationSite vs;
        TextView name,address,tags;
        ImageView siteImage;

        public VacationSiteViewHolder(View rowSource){

            this.name = (TextView) rowSource.findViewById(R.id.vacationSitePartialName);
            this.address = (TextView) rowSource.findViewById(R.id.vacationSitePartialAddress);
            this.tags = (TextView) rowSource.findViewById(R.id.vacationSitePartialTag);
            this.siteImage = (ImageView) rowSource.findViewById(R.id
                    .vacationSitePartialThumb);
        }

        public void setData(VacationSite vs){
            this.vs=vs;
            this.name.setText(vs.getName());
            this.address.setText(vs.getAddress());
            this.tags.setText(vs.getTagsAsString());
            try {
                Picasso.with(act).load(Util.BASE_URL + Util.THUMB_URL + vs.getImage()).fit()
                        .centerCrop()
                        .placeholder(R.drawable.agenda_placeholder_square_1).into(this.siteImage);
                Log.v("vacsitesearchok", vs.getImage());
                Log.v("vacsitesearchok",Util.BASE_URL + Util.THUMB_URL + vs.getImage());
            }
            catch(Exception e){
                e.printStackTrace();
                Log.v("vacsitesearch","image is empty");
            }
        }
    }
}
