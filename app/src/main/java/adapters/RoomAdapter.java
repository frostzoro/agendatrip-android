package adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.AgendaTrip.HotelRoomList;
import com.example.android.AgendaTrip.R;

import java.util.ArrayList;

import model.Hotel;
import utils.Util;

/**
 * Created by STEFAN on 12/27/2015.
 */
public class RoomAdapter extends ArrayAdapter<Hotel.Room> {
    LayoutInflater inflater;
    Activity act;
    int partialResourceId;
    ArrayList<Hotel.Room> data;

    public RoomAdapter(Activity act, int partialResourceId, ArrayList<Hotel.Room> data){
        super(act,partialResourceId,data);
        this.act = act;
        this.partialResourceId = partialResourceId;
        this.data = data;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Hotel.Room getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {View row = convertView;
        RoomViewHolder rvh = null;

        if(row ==null || row.getTag() == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(this.partialResourceId, parent, false);
            rvh = new RoomViewHolder(row);

            row.setTag(rvh);
        }
        else{
            rvh = (RoomViewHolder) row.getTag();
        }

        rvh.setData(getItem(position));

        final Hotel.Room finalroom = getItem(position);

        row.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                HotelRoomList hrl = (HotelRoomList) act;
                hrl.showRoomData(finalroom);
            }
        });

        return row;
    }

    public class RoomViewHolder{
        Hotel.Room r;
        TextView roomName,roomPrice,roomBreakfast,roomDetail;
        Button bookButton;

        public RoomViewHolder(View src){
            roomName = (TextView) src.findViewById(R.id.partialRoomName);
            roomPrice = (TextView) src.findViewById(R.id.partialRoomPrice);
            roomBreakfast = (TextView) src.findViewById(R.id.partialRoomBreakfast);
            roomDetail = (TextView) src.findViewById(R.id.partialRoomRoomDetail);
            bookButton = (Button) src.findViewById(R.id.partialRoomBookButton);
        }

        public void setData(Hotel.Room r){
            this.r = r;
            final Hotel.Room fr = this.r;
            roomName.setText(this.r.getName());
            roomPrice.setText(Util.convertDoubleToIDRString(this.r.getPrice()) + "/night");
            if(this.r.getWith_breakfast()==false){
                roomBreakfast.setVisibility(View.GONE);
            }
            else{
                roomBreakfast.setVisibility(View.VISIBLE);
            }
            bookButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    HotelRoomList hrl = (HotelRoomList) act;
                    hrl.goToCheckOut(fr);
                }
            });
        }
    }

}
