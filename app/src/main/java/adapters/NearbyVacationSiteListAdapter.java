package adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.VacationSiteDetail;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;

import model.VacationSite;
import singleton.AppController;
import utils.Util;

/**
 * Created by STEFAN on 2015/10/20.
 */
public class NearbyVacationSiteListAdapter extends ArrayAdapter<VacationSite> {
    private LayoutInflater inflater;
    private ArrayList<VacationSite> data;
    private Activity act;
    private int partialResourceID;
    ImageLoader imgLoader = AppController.getInstance().getImageLoader();

    //constants
    public static final int SORT_BY_PREFERENCE = 1;
    public static final int SORT_BY_POPULARITY = 2;
    public static final int SORT_BY_DISTANCE = 3;

    public NearbyVacationSiteListAdapter(Activity act, int resource, ArrayList<VacationSite>
            objects) {
        super(act, resource, objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public VacationSite getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(VacationSite item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        NearbyVacationSiteViewHolder vs = null;

        if(row == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(partialResourceID, parent, false);

            vs = new NearbyVacationSiteViewHolder(row);

            row.setTag(vs);
        }
        else{
            vs = (NearbyVacationSiteViewHolder) row.getTag();
        }

        try{
            vs.setData(getItem(position));
            Log.v("topvcidoutclick",String.valueOf(vs.vs.getID()));
            final VacationSite finalvs = vs.vs;
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(act, VacationSiteDetail.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("vacation_site",String.valueOf(finalvs.getID()));
                    mBundle.putString("vacation_site_name",String.valueOf(finalvs.getName()));
                    mBundle.putString("name",getItem(position).getName());
                    mBundle.putDouble("longitude", getItem(position).getLongitude());
                    mBundle.putDouble("latitude", getItem(position).getLatitude());
                    Log.v("topvcinclick",String.valueOf(finalvs));
                    it.putExtras(mBundle);
                    act.startActivity(it);
                }
            });
        }
        catch(Exception e){
            Log.v("Adapter getView", e.getStackTrace().toString());
        };

        return row;
    }

    public void sortItem(int type){
        switch(type){
            case SORT_BY_PREFERENCE : sort(new Comparator<VacationSite>() {
                @Override
                public int compare(VacationSite lhs, VacationSite rhs) {
                    return (lhs.getHypotheses() > rhs.getHypotheses()) ? -1 : 1;
                }
            }); break;
            case SORT_BY_DISTANCE : sort(new Comparator<VacationSite>() {
                @Override
                public int compare(VacationSite lhs, VacationSite rhs) {
                    return (lhs.getDistance() > rhs.getDistance()) ? 1 : -1;
                }
            }); break;
            case SORT_BY_POPULARITY : sort(new Comparator<VacationSite>() {
                @Override
                public int compare(VacationSite lhs, VacationSite rhs) {
                    return (lhs.getRating() > rhs.getRating()) ? -1 : 1;
                }
            }); break;
        }
        notifyDataSetChanged();
    }

    public class NearbyVacationSiteViewHolder{
        public VacationSite vs;
        TextView name,distance;
        ImageView siteImage;
        RatingBar rating;

        public NearbyVacationSiteViewHolder(View rowSource){
            this.name = (TextView) rowSource.findViewById(R.id.nearbyVacationSiteName);
            this.distance = (TextView) rowSource.findViewById(R.id.nearbyVacationSiteDistance);
            this.rating = (RatingBar) rowSource.findViewById(R.id.nearbyVacationSiteRating);
            this.siteImage = (ImageView) rowSource.findViewById(R.id
                    .nearbyVacationSiteImageView);
        }

        public void setData(VacationSite vs){
            this.vs=vs;
            this.name.setText(vs.getName());
            this.distance.setText(Util.distanceToWord(vs.getDistance()) + " from current location");
            this.rating.setRating((float)vs.getRating());
            try {
                Picasso.with(act).load(Util.BASE_URL + Util.COVER_URL + vs.getImage()).fit()
                        .centerCrop()
                        .placeholder(R.drawable.agenda_placeholder_square_1).into(this.siteImage);
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }

    }
}
