package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.android.AgendaTrip.AgendaEventDetails;
import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.TagRegister;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;

import model.Agenda;
import model.VacationSiteTag;
import singleton.AppController;

public class TagsAdapter extends ArrayAdapter<VacationSiteTag> implements Serializable {

    private LayoutInflater inflater;
    private ArrayList<VacationSiteTag> data;
    private Activity act;
    private int partialResourceID;
    ImageLoader imgLoader = AppController.getInstance().getImageLoader();
    ArrayList<String> selectedStrings = new ArrayList<String>();
    CheckBox cb;
    TextView idtv;
    public TagsAdapter(Activity act, int resource, ArrayList<VacationSiteTag> objects) {
        super(act,resource,objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public VacationSiteTag getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(VacationSiteTag item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        View row = convertView;
        TagViewHolder tagViewHolder = null;

        if(row == null){

            inflater = LayoutInflater.from(act);
            row = inflater.inflate(partialResourceID, parent, false);
            tagViewHolder = new TagViewHolder(row);
            tagViewHolder.id=(TextView) row.findViewById(R.id.tagsid);
            tagViewHolder.name=(TextView) row.findViewById(R.id.tagsname);
            tagViewHolder.tagcbox=(CheckBox) row.findViewById(R.id.tagsbox);

            row.setTag(tagViewHolder);
        }


        else{
            tagViewHolder = (TagViewHolder) row.getTag();
        }
        tagViewHolder.name.setText(data.get(position).getName());
        tagViewHolder.id.setText(data.get(position).getId()+"");
        tagViewHolder.tagcbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    TagRegister.selectedTags.add(data.get(position).getId()+"");
                }else{
                    TagRegister.selectedTags.remove(data.get(position).getId()+"");
                }
                data.get(position).setChecked(b);
                notifyDataSetChanged();
                Log.v("ST",TagRegister.selectedTags.size()+"");
            }
        });

        try{

            tagViewHolder.setData(data.get(position));

        }

        catch(Exception e){
            Log.v("Adapter getView", e.getStackTrace().toString());
        };
       /* cb= (CheckBox) row.findViewById(R.id.tagsbox);
        idtv=(TextView)row.findViewById(R.id.tagsid);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    TagRegister.selectedTags.add(idtv.getText().toString());
                }else{
                    TagRegister.selectedTags.remove(idtv.getText().toString());
                }

            }
        });*/

        tagViewHolder.tagcbox.setChecked(data.get(position).isChecked());
        return row;
    }



    public class TagViewHolder{
        public VacationSiteTag tag;
        TextView name,id;
        CheckBox tagcbox;
        NetworkImageView siteImage;

        public TagViewHolder(View rowSource){
            this.tagcbox=(CheckBox) rowSource.findViewById(R.id.tagsbox);
            this.name = (TextView) rowSource.findViewById(R.id.tagsname);
            this.id = (TextView) rowSource.findViewById(R.id.tagsid);

        }
        ArrayList<String> getSelectedString(){
            return selectedStrings;
        }

        public void setData(VacationSiteTag tag){
            Log.v("a", tag.getName());
            //this.name.setText(tag.getName());
            //this.id.setText(tag.getId()+"");

        }
    }
}
