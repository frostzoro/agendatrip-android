package adapters;

import android.app.Activity;
import android.content.Context;
import android.location.Geocoder;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import org.osmdroid.util.GeoPoint;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by STEFAN on 12/1/2015.
 */
public class SearchMapTextAdapter extends ArrayAdapter<Pair<String,String>> implements Filterable {
    ArrayList<Pair<String,String>> data;
    ArrayList<Pair<String,String>> dataAll;
    ArrayList<Pair<String,String>> suggestions;
    Activity act;
    int resource;
    public SearchMapTextAdapter(Activity act, int resource, ArrayList<Pair<String, String>> data){
        super(act,resource,data);
        this.data =data;
        this.suggestions= new ArrayList<Pair<String,String>>();
        this.resource=resource;
        this.act = act;

        Log.v("datasize",String.valueOf(data.size()));
        Log.v("datsz2",String.valueOf(data.size()));
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent){
        View view = convertView;
        if(view == null){
            view = new TextView(getContext());
        }
        ((TextView)view).setText(data.get(pos).second);
        return view;
    }

    public Pair<String,String> getItem(int position){
        return data.get(position);
    }

    public int getPosition(Pair<String,String> item){
        return data.indexOf(item);
    }

    public int getCount(){
        return data.size();
    }

    public void notifyDataLoaded() {
        this.dataAll= (ArrayList<Pair<String,String>>) data.clone();
    }

    @Override
    public Filter getFilter() {
        Log.v("setres", "foo");
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                String str = ((Pair<String,String>)(resultValue)).second;
                return str;
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if(constraint != null) {

                    double lat=0.0,lng=0.0;
                    Geocoder gc=new Geocoder(getContext(), Locale.getDefault());
                    try{
                            List addresses=gc.getFromLocationName(constraint.toString(),5);

                        if(addresses.size()>0){
                            Log.v("SIZE ADDRESS",addresses.size()+" ");
                            for(int i=0;i<addresses.size();i++){
                                addresses.get(i);
                            }


                        }

                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }


                    Log.v("filres", constraint.toString());
                    suggestions.clear();
                    Log.v("sugsize",String.valueOf(suggestions));
                    Log.v("datasize",String.valueOf(data.size()));
                    Log.v("dataAllsize",String.valueOf(dataAll.size()));
                    for (Pair<String,String> place : dataAll) {
                        if(place.second.toLowerCase().startsWith(constraint.toString().toLowerCase())){
                            Log.v("word","word = " + place.second.toLowerCase() + " filter = " +
                                    constraint
                                            .toString().toLowerCase() + " res = " + place.second.toLowerCase()
                                    .startsWith
                                            (constraint.toString().toLowerCase()));

                            suggestions.add(place);
                            Log.v("done!",place.second);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = suggestions;
                    filterResults.count = suggestions.size();
                    return filterResults;
                } else {
                    Log.v("null!","null!");
                    return new FilterResults();
                }
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                Log.v("publishres", "foo");
                if(results.count > 0) {
                    clear();
                    data = (ArrayList<Pair<String,String>>) results
                            .values;
                    for(Pair<String,String> a : data){
                        add(a);
                    }
                    notifyDataSetChanged();
                    Log.v("publishres1", "foo sz "+data.size());
                }
                else{
                    Log.v("publishres2", "foo");
                    notifyDataSetInvalidated();
                }
            }
        };
    }

}
