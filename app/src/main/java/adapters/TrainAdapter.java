package adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.AgendaTrip.FlightSearchResult;
import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.TrainSearchResult;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

import helper.TrainFilter;
import model.Flight;
import model.Train;

/**
 * Created by STEFAN on 12/3/2015.
 */
public class TrainAdapter extends ArrayAdapter<Train> {
    private Activity act;
    private int partialResourceID;
    private LayoutInflater inflater;
    private TrainFilter trainFilter;
    private TrainSearchResult fsr;
    private ArrayList<Train> data, allData;
    // CONSTANTS
    public static final int SORT_BY_PRICE = 0;
    public static final int SORT_BY_DEPARTURE = 1;

    public TrainAdapter(Activity act, int resource, ArrayList<Train> objects){
        super(act,resource,objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
        this.trainFilter = new TrainFilter();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Train getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(Train item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        TrainViewHolder fh = null;

        if(row ==null || row.getTag() == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(this.partialResourceID, parent, false);
            fh = new TrainViewHolder(row);
            row.setTag(fh);
        }
        else{
            fh = (TrainViewHolder) row.getTag();

        }

            fh.setData(getItem(position));
            final Train ff = fh.getF();
            row.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    fsr = (TrainSearchResult) act;
                    fsr.goToNextStep(ff);
                }
            });

        return row;
    }

    public void sortItem(int method){
        switch(method){
            case TrainAdapter.SORT_BY_PRICE : sort(new Comparator<Train>(){
                @Override
                public int compare(Train lhs, Train rhs) {
                    return lhs.getPrice_total().compareTo(rhs.getPrice_total());
                }
            }); break;
            case TrainAdapter.SORT_BY_DEPARTURE : sort(new Comparator<Train>() {
                @Override
                public int compare(Train lhs, Train rhs) {
                    return lhs.getDeparture_time().compareTo(rhs.getDeparture_time());
                }
            });
                break;

        }
        notifyDataSetChanged();
        Log.v("flightadap", "SORTED ITMS");
    }

    public void notifyDataLoaded(){
        notifyDataSetChanged();
        this.allData = (ArrayList<Train>) this.data.clone();
    }

    public void filter(){
        data.clear();
        int sz = allData.size();
        for(int i=0; i<sz; i++){
            if(trainFilter.filterClass(allData.get(i))){
                data.add(allData.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setTrainClassFilter(ArrayList<String> classes){
        trainFilter.setClasses(classes);
    }

    class TrainViewHolder{
        TextView airlinename,flightnumber,flighttime,flightduration,flightstops,flightprice;
        TextView ispromo,isbaggage,isfood,istax;

        public Train getF() {
            return f;
        }

        public void setF(Train f) {
            this.f = f;
        }

        Train f;
        ImageView airlinelogo;

        public TrainViewHolder(View src){
            //=====================================================
            //binds all flight_partial elements to this view holder
            //=====================================================
            airlinename = (TextView) src.findViewById(R.id.flightPartialAirlineName);
            flightnumber = (TextView) src.findViewById(R.id.flightPartialAirlineFlightNumber);
            flighttime = (TextView) src.findViewById(R.id.flightPartialTime);
            flightduration = (TextView) src.findViewById(R.id.flightPartialETA);
            flightprice = (TextView) src.findViewById(R.id.flightPartialPrice);
            ispromo = (TextView) src.findViewById(R.id.flightPartialPromoSticker);
            isbaggage = (TextView) src.findViewById(R.id.flightPartialBaggageIcon);
            isfood = (TextView) src.findViewById(R.id.flightPartialFoodIcon);
            istax = (TextView) src.findViewById(R.id.flightPartialTaxIcon);
            airlinelogo = (ImageView) src.findViewById(R.id.flightPartialAirlineLogo);
        }

        public void setData(Train f){
            //=====================================================
            //binds all Flight object f's data to this view holder
            //=====================================================
            this.f = f;
            airlinename.setText(f.getTrain_name());
            flightnumber.setText(f.getTrain_id());
            flighttime.setText(f.getDeparture_time() + " - " + f.getArrival_time());
            flightduration.setText(f.getDuration()) ;
//                String crc = NumberFormat.getCurrencyInstance(new Locale("EN", "ID")).format
  //                      (f.getPrice_total());
    //            flightprice.setText(crc.substring(0, 3) + " " + crc.substring(3, crc.length()));
            flightprice.setText(f.getPrice_total());


        }

    }

}
