package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.android.AgendaTrip.AgendaEventDetails;
import com.example.android.AgendaTrip.DeleteAgenda;
import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.RegisterAgendaDetail;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.Agenda;
import singleton.AppController;

/**
 * Created by STEFAN on 2015/10/17.
 */
public class AgendaFromVSDetailsAdapter extends ArrayAdapter<Agenda> implements View.OnClickListener, Serializable {
    public static int sAgendaID;
    private LayoutInflater inflater;
    private ArrayList<Agenda> data;
    private Activity act;
    private int vsid;
    private String vsname;
    private int partialResourceID;
    ImageLoader imgLoader = AppController.getInstance().getImageLoader();

    public AgendaFromVSDetailsAdapter(Activity act, int resource, ArrayList<Agenda> objects,int vsid,String vsname) {
        super(act, resource, objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
        this.vsid=vsid;
        this.vsname=vsname;
        Log.v("vsnameadapter",vsname);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Agenda getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(Agenda item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View row = convertView;
        AgendaViewHolder ag = null;

        if(row == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(partialResourceID, parent, false);

            ag = new AgendaViewHolder(row);

            row.setTag(ag);
        }
        else{
            ag = (AgendaViewHolder) row.getTag();
        }

        try{

            ag.setData(data.get(position));
            final String agname=getItem(position).getName();
            final int agid=getItem(position).getId();
            Log.v("taraaa", agname+agid);

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Context mCon = getContext();
                    Agenda sentAgenda = new Agenda();
                    sentAgenda.setUserid(data.get(position).getUserid());
                    sentAgenda.setStartDate(data.get(position).getStartDate());
                    sentAgenda.setEndDate(data.get(position).getEndDate());
                    sentAgenda.setId(data.get(position).getId());
                    sentAgenda.setName(data.get(position).getName());
                    sAgendaID = data.get(position).getId();
                    AgendaAdapter.sAgendaID=data.get(position).getId();
                    Intent mIntent = new Intent(mCon, RegisterAgendaDetail.class);
                    Bundle mBundle = new Bundle();

                    mIntent.putExtra("namaagenda", vsname);
                    mIntent.putExtra("vacation_site", vsid + "");
                    mBundle.putString("userid", sentAgenda.getUserid());
                    mBundle.putInt("id", sentAgenda.getId());
                    mBundle.putString("startdate", sentAgenda.getStartDate());
                    mBundle.putString("enddate", sentAgenda.getEndDate());
                    mIntent.putExtra("agendaStart", sentAgenda.getStartDate());
                    mIntent.putExtra("agendaEnd", sentAgenda.getEndDate());
                    mBundle.putString("name", sentAgenda.getName());

                    mIntent.putExtras(mBundle);

                    mCon.startActivity(mIntent);


                    // DI SINI HARUSNYA ACTION DILANJUTKAN, YATTAAA!
                    //Log.v("penandaonItemClick", "BERHASILLLLLLLLL" + data.get(position).getId());

                }
            });
            ag.delicon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(getContext(), DeleteAgenda.class);
                    i.putExtra("agName",agname);
                    i.putExtra("agID",agid);
                    act.startActivity(i);
                }
            });
        }
        catch(Exception e){
            Log.v("Adapter getView", e.getStackTrace().toString());
        };

        return row;
    }

    @Override
    public void onClick(View v) {

        // Intent it = new Intent(act, ActivityEventDetails.class);
        // Bundle mBundle = new Bundle();
        // mBundle.putSerializable("eventObj", finalViewHolder.event);
        // it.putExtras(mBundle);
        // mContext.startActivity(it);
    }

    public class AgendaViewHolder{
        public Agenda ag;
        TextView name,date,delicon;
        NetworkImageView siteImage;

        public AgendaViewHolder(View rowSource){
            this.name = (TextView) rowSource.findViewById(R.id.agendaPartialName);
            this.date = (TextView) rowSource.findViewById(R.id.agendaPartialDate );
            this.delicon=(TextView) rowSource.findViewById(R.id.delicon);
        }

        public void setData(Agenda ag){
            String time=ag.getStartDate();
            String time2=ag.getEndDate();
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convdate=new Date();
            Date convdate2=new Date();
            String trydate,trydate2;

            try{
                convdate=sdf.parse(time);
                convdate2=sdf.parse(time2);
            }
            catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            DateFormat destFormat=new SimpleDateFormat("MMM dd, yyyy hh:mm aa");
            trydate=destFormat.format(convdate);
            trydate2=destFormat.format(convdate2);
            this.name.setText(ag.getName());
            this.date.setText(trydate+" - "+trydate2);

        }
    }
}
