package adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.AgendaTrip.FlightSearchResult;
import com.example.android.AgendaTrip.R;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

import helper.FlightFilter;
import model.Agenda;
import model.Flight;
import model.FlightSearch;

/**
 * Created by STEFAN on 12/3/2015.
 */
public class FlightAdapter extends ArrayAdapter<Flight> {
    private LayoutInflater inflater;
    private Activity act;
    private FlightSearchResult fsr;
    private int partialResourceID;
    private ArrayList<Flight> data, allData;
    private FlightFilter flightFilter;
    // CONSTANTS
    public static final int SORT_BY_PRICE = 0;
    public static final int SORT_BY_DEPARTURE = 1;

    public FlightAdapter(Activity act, int resource, ArrayList<Flight> objects){
        super(act,resource,objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
        this.allData = (ArrayList<Flight>) this.data.clone();
        this.flightFilter = new FlightFilter();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Flight getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(Flight item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        FlightViewHolder fh = null;

        if(row ==null || row.getTag() == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(this.partialResourceID, parent, false);
            fh = new FlightViewHolder(row);
            row.setTag(fh);
        }
        else{
            fh = (FlightViewHolder) row.getTag();

        }

            fh.setData(getItem(position));
            final Flight ff = fh.getF();
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fsr = (FlightSearchResult) act;
                    fsr.goToNextStep(ff);
                }
            });

        return row;
    }

    public void sortItem(int method){
        switch(method){
            case FlightAdapter.SORT_BY_PRICE : sort(new Comparator<Flight>(){
                @Override
                public int compare(Flight lhs, Flight rhs) {
                    return lhs.getPrice_value().compareTo(rhs.getPrice_value());
                }
            }); break;
            case FlightAdapter.SORT_BY_DEPARTURE : sort(new Comparator<Flight>() {
                @Override
                public int compare(Flight lhs, Flight rhs) {
                    return lhs.getDeparture_time().compareTo(rhs.getDeparture_time());
                }
            }); break;

        }
        notifyDataSetChanged();
        Log.v("flightadap", "SORTED ITMS");
    }

    public void filter(){
        this.data.clear();
        int sz = this.allData.size();
        for(int i=0; i<sz; i++){
            if(flightFilter.filterAirline(allData.get(i))){
                data.add(allData.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void notifyDataLoaded(){
        notifyDataSetChanged();
        this.allData = (ArrayList<Flight>) this.data.clone();
    }

    public void setAirlineFilter(ArrayList<String> airlines){
        flightFilter.setAirlines(airlines);
    }

    class FlightViewHolder{
        TextView airlinename,flightnumber,flighttime,flightduration,flightstops,flightprice;
        TextView ispromo,isbaggage,isfood,istax;

        public Flight getF() {
            return f;
        }

        public void setF(Flight f) {
            this.f = f;
        }

        Flight f;
        ImageView airlinelogo;

        public FlightViewHolder(View src){
            //=====================================================
            //binds all flight_partial elements to this view holder
            //=====================================================
            airlinename = (TextView) src.findViewById(R.id.flightPartialAirlineName);
            flightnumber = (TextView) src.findViewById(R.id.flightPartialAirlineFlightNumber);
            flighttime = (TextView) src.findViewById(R.id.flightPartialTime);
            flightduration = (TextView) src.findViewById(R.id.flightPartialETA);
            flightprice = (TextView) src.findViewById(R.id.flightPartialPrice);
            ispromo = (TextView) src.findViewById(R.id.flightPartialPromoSticker);
            isbaggage = (TextView) src.findViewById(R.id.flightPartialBaggageIcon);
            isfood = (TextView) src.findViewById(R.id.flightPartialFoodIcon);
            istax = (TextView) src.findViewById(R.id.flightPartialTaxIcon);
            airlinelogo = (ImageView) src.findViewById(R.id.flightPartialAirlineLogo);
        }

        public void setData(Flight f){
            //=====================================================
            //binds all Flight object f's data to this view holder
            //=====================================================
            this.f = f;
            airlinename.setText(f.getAirline_name());
            flightnumber.setText(f.getFlight_number());
            flighttime.setText(f.getDeparture_time() + " - " + f.getArrival_time());
            flightduration.setText(f.getDuration() + "(" + f.getStop() + ")") ;
                String crc = NumberFormat.getCurrencyInstance(new Locale("EN", "ID")).format
                        (f.getPrice_value());
                flightprice.setText(crc.substring(0,3)+" "+crc.substring(3,crc.length()));
            if(!f.getIs_promo()) ispromo.setVisibility(View.INVISIBLE);
            if(!f.getNeed_baggage()) isbaggage.setVisibility(View.GONE);
            if(!f.getHas_food()) isfood.setVisibility(View.GONE);
            if(!f.getAirport_tax()) istax.setVisibility(View.GONE);
            Picasso.with(act).load(f.getImage()).placeholder(R.drawable.description).into(this
                    .airlinelogo);
        }

    }

}
