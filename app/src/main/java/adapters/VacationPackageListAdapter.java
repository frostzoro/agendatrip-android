package adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.VacationPackageDetail;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;

import model.VacationPackage;
import utils.Util;

/**
 * Created by STEFAN on 2015/10/31.
 */
public class VacationPackageListAdapter extends ArrayAdapter<VacationPackage> {
    private LayoutInflater inflater;
    private ArrayList<VacationPackage> data;
    private Activity act;
    private int partialResourceID;
    ImageLoader imgLoader;

    // constants
    public static final int SORT_BY_PRICE = 0;
    public static final int SORT_BY_PURCHASE = 1;
    public static final int SORT_BY_PREFERENCE = 2;

    public VacationPackageListAdapter(Activity act, int resource,ArrayList<VacationPackage> objects) {
        super(act, resource, objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
    }

    @Override
    public VacationPackage getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(VacationPackage item) {
        return super.getPosition(item);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        VacationPackageViewHolder vp = null;

        if (row == null || row.getTag() == null) {
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(partialResourceID, parent, false);

            vp = new VacationPackageViewHolder(row);
            row.setTag(vp);
        }
        else {
            vp = (VacationPackageViewHolder) row.getTag();
        }
            vp.setData(getItem(position));
            final VacationPackage finalvp = vp.vp;
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(act, VacationPackageDetail.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("vacation_package", String.valueOf(finalvp.getId()));
                    mBundle.putString("vacation_package_name",String.valueOf(finalvp.getName()));
                    it.putExtras(mBundle);
                    act.startActivity(it);
                }
            });
        return row;
    }

    public void sortItem(int method){
        switch(method){
            case VacationPackageListAdapter.SORT_BY_PURCHASE : sort(new Comparator<VacationPackage>(){
                @Override
                public int compare(VacationPackage lhs, VacationPackage rhs) {
                    Log.v("sorting",lhs.getPurchaseCount() + " " + rhs.getPurchaseCount());
                    return (lhs.getPurchaseCount()>rhs.getPurchaseCount()) ? -1 : 1;
                }
            });
                break;
            case VacationPackageListAdapter.SORT_BY_PRICE : sort(new Comparator<VacationPackage>(){
                @Override
                public int compare(VacationPackage lhs, VacationPackage rhs) {
                    return (lhs.getPrice()<rhs.getPrice()) ? -1 : 1;
                }
            });
                break;
            case VacationPackageListAdapter.SORT_BY_PREFERENCE : sort(new Comparator<VacationPackage>(){
                @Override
                public int compare(VacationPackage lhs, VacationPackage rhs) {
                    return (lhs.getHypotheses()>rhs.getHypotheses()) ? -1 : 1;
                }
            });Log.v("vacpack","sortbypref");
                break;
        }
        notifyDataSetChanged();
    }

    public class VacationPackageViewHolder{
        VacationPackage vp;
        TextView name,date,price;
        ImageView image;
        public VacationPackageViewHolder(View src) {
            name = (TextView) src.findViewById(R.id.vacationPackagePartialName);
            date = (TextView) src.findViewById(R.id.vacationPackagePartialDate);
            price = (TextView) src.findViewById(R.id.vacationPackagePartialPrice);
            image = (ImageView) src.findViewById(R.id.vacationPackagePartialImage);
        }

        public void setData(VacationPackage vp){
            this.vp=vp;
            name.setText(vp.getTitle());
            date.setText(vp.getCompleteDateAsString());
            price.setText(Util.convertDoubleToIDRString(vp.getPrice()));
            try {
                Picasso.with(act).load(Util.BASE_URL + Util.VACATION_PACKAGE_IMAGE_URL + vp.getImage())
                        .placeholder(R.drawable.agenda_placeholder_1).into(this.image);
            }
            catch(Exception e){
                e.printStackTrace();
                Log.v("vp","image is empty");
            }
        }
    }
}

