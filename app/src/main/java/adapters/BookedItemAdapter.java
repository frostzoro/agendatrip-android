package adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.AgendaTrip.MyOrderDetail;
import com.example.android.AgendaTrip.R;

import java.util.ArrayList;

import model.BookedItem;
import utils.Util;

/**
 * Created by STEFAN on 12/17/2015.
 */
public class BookedItemAdapter extends ArrayAdapter<BookedItem> {
    private LayoutInflater inflater;
    private Activity act;
    private int partialResourceID;
    private ArrayList<BookedItem> data;

    public BookedItemAdapter(Activity act, int resource, ArrayList<BookedItem> objects){
        super(act,resource,objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public BookedItem getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(BookedItem item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        BookedItemViewHolder bh = null;

        if(row ==null || row.getTag() == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(this.partialResourceID, parent, false);
            bh = new BookedItemViewHolder(row);
            row.setTag(bh);
        }
        else{
            bh = (BookedItemViewHolder) row.getTag();

        }
        final BookedItem finalbi = getItem(position);
        bh.setData(getItem(position));
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(act,MyOrderDetail.class);
                Bundle b = new Bundle();
                b.putSerializable("booked_item", finalbi);
                i.putExtras(b);
                act.startActivity(i);
            }
        });

        return row;
    }


    class BookedItemViewHolder {
        ImageView bookingIcon;
        TextView orderAmount,orderName,orderDate;
        BookedItem bi;

        public BookedItemViewHolder(View src){
            bookingIcon = (ImageView) src.findViewById(R.id.bookedItemPartialImageView);
            orderAmount = (TextView) src.findViewById(R.id.bookedItemPartialOrderAmount);
            orderName = (TextView) src.findViewById(R.id.bookedItemPartialName);
            orderDate = (TextView) src.findViewById(R.id.bookedItemPartialDate);
        }

        public void setData(BookedItem bi){
            Drawable d;
            this.bi = bi;
            switch(bi.getOrder_type()){
                case "flight" : d = act.getResources().getDrawable(R.drawable.flight_icon);
                    break;
                case "hotel" : d= act.getResources().getDrawable(R.drawable.hotel_icon);
                    break;
                case "train" : d= act.getResources().getDrawable(R.drawable.train_icon);
                    break;
                default : d = act.getResources().getDrawable(R.drawable.flight_icon);
                    break;
            }
            bookingIcon.setImageDrawable(d);
            orderAmount.setText(Util.convertDoubleToIDRString(bi.getOrder_amount()));
            orderName.setText(bi.getBid().getOrder_name() + " " + bi.getBid().getOrder_name_detail
                    ());
            orderDate.setText(bi.getOrder_date());
        }

    }
}
