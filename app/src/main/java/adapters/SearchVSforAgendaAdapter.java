package adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.RegisterAgendaDetail;
import com.example.android.AgendaTrip.VacationSiteDetail;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import model.VacationSite;
import singleton.AppController;
import utils.Util;

/**
 * Created by STEFAN on 2015/10/17.
 */
public class SearchVSforAgendaAdapter extends ArrayAdapter<VacationSite> {
    private LayoutInflater inflater;
    private ArrayList<VacationSite> data;
    private Activity act;
    private int agenda_id;
    private int partialResourceID;
    ImageLoader imgLoader = AppController.getInstance().getImageLoader();
    TextView addBtn;
    String agendaStart,agendaEnd;

    public SearchVSforAgendaAdapter(Activity act, int resource, ArrayList<VacationSite> objects) {
        super(act, resource, objects);

        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;

    }

    public SearchVSforAgendaAdapter(Activity act, int resource, ArrayList<VacationSite> objects, String agendaStart, String agendaEnd) {
        super(act, resource, objects);

        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
        this.agendaStart=agendaStart;
        this.agendaEnd=agendaEnd;
    }
    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public VacationSite getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(VacationSite item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        VacationSiteViewHolder vs = null;

        if(row == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(partialResourceID, parent, false);

            vs = new VacationSiteViewHolder(row);
            row.setTag(vs);
        }
        else{
            vs = (VacationSiteViewHolder) row.getTag();
        }


        try{

            vs.setData(getItem(position));


            Log.v("topvcidoutclick", String.valueOf(vs.vs.getID()));
            final int finalvs = vs.vs.getID();
            final String namavs=vs.vs.getName();
            final double latitude=vs.vs.getLatitude();
            final double longitude=vs.vs.getLongitude();

            final int finalvs2=finalvs;
            final String namavs2=namavs;


//INI LISTENER TOMBOL YANG BERMASALAH
            vs.addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(act.getApplicationContext(),"#"+position,Toast.LENGTH_LONG).show();

                    Intent i=new Intent(act, RegisterAgendaDetail.class);

                    i.putExtra("namaagenda", namavs2);
                    i.putExtra("agendaid", AgendaAdapter.sAgendaID);
                    i.putExtra("vacation_site",String.valueOf(finalvs));
                    i.putExtra("agendaStart",agendaStart);
                    i.putExtra("agendaEnd",agendaEnd);
                    act.startActivity(i);
                    act.finish();
                    Log.v("BUTTONCLICK", String.valueOf(finalvs));
                }
            });

            //addAgendatoIntent(position,namavs,finalvs);

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent it = new Intent(act, VacationSiteDetail.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("vacation_site",String.valueOf(finalvs));
                    mBundle.putString("vs_name", namavs);
                    mBundle.putString("vacation_site_name", namavs);
                    mBundle.putString("name",getItem(position).getName());
                    mBundle.putDouble("latitude", latitude);
                    mBundle.putDouble("longitude",longitude);
                    Log.v("topvcinclick", String.valueOf(finalvs));
                    it.putExtras(mBundle);
                    act.startActivity(it);

                }
            });
        }
        catch(Exception e){
            Log.v("Adapter getView", e.getStackTrace().toString());
        };



        return row;
    }




    public class VacationSiteViewHolder{
        TextView addBtn;
        public VacationSite vs;
        TextView name,address,tags;
        ImageView siteImage;

        public VacationSiteViewHolder(View rowSource){
            this.addBtn= (TextView) rowSource.findViewById(R.id.addAgendaDetailsBtn);
            this.name = (TextView) rowSource.findViewById(R.id.vacationSitePartialName);
            this.address = (TextView) rowSource.findViewById(R.id.vacationSitePartialAddress);
            this.tags = (TextView) rowSource.findViewById(R.id.vacationSitePartialTag);
            this.siteImage = (ImageView) rowSource.findViewById(R.id
                    .vacationSitePartialThumb);
        }

        public void setData(VacationSite vs){
            this.vs=vs;
            this.name.setText(vs.getName());
            this.address.setText(vs.getAddress());
            this.tags.setText(vs.getTagsAsString());
            Picasso.with(act).load(Util.BASE_URL + Util.THUMB_URL + vs.getImage()).fit()
                    .centerCrop()
                    .placeholder(R.drawable.agenda_placeholder_square_1).into(this.siteImage);
        }

    }

}
