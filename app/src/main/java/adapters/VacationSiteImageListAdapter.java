package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.example.android.AgendaTrip.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import singleton.AppController;
import utils.Util;

/**
 * Created by STEFAN on 2015/10/24.
 */
public class VacationSiteImageListAdapter extends RecyclerView
        .Adapter<VacationSiteImageListAdapter.VSImageHolder>{
        Activity act;
        ArrayList<String> images;
        ImageLoader imgLoader = AppController.getInstance().getImageLoader();
        onItemClickListener ocl;

    public VacationSiteImageListAdapter(ArrayList<String> images,Activity act){
        this.images=images;
        this.act= act;
    }

    public interface onItemClickListener {
        // you can define any parameter as per your requirement
        public void onItemClick(int position);
    }

    public void setOnItemClickListener(onItemClickListener ocl){
        this.ocl = ocl;
    }

    public String getItem(int position){
        return images.get(position);
    }

    public VacationSiteImageListAdapter(){
        images = new ArrayList<>();
    }

    public void setImages(ArrayList<String> images){
        this.images=images;
        notifyDataSetChanged();
        Log.v("vsil imgsiz",String.valueOf(this.images.size()));
    }

    @Override
    public VSImageHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                .vacation_site_photo_partial,viewGroup,false);
        VSImageHolder vsi = new VSImageHolder(v);
        return vsi;
    }

    @Override
    public void onBindViewHolder(VSImageHolder vsImageHolder, int i) {
        Picasso.with(act).load(Util.BASE_URL + Util.THUMB_URL + images.get(i)).into(vsImageHolder
                .niv);
        final int si = i;
        vsImageHolder.niv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ocl != null) {
                    ocl.onItemClick(si);
                }
            }
        });
        //Log.v("imageadapter", images.get(i));
    }

    @Override
    public int getItemCount() {
        //Log.v("adaptersize",""+ images.size());
        return images.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class VSImageHolder extends RecyclerView.ViewHolder{
        ImageView niv;
        VSImageHolder(View item){
            super(item);
            niv  = (ImageView)item.findViewById(R.id.VSPhotoPartialImage);
        }
    }

}
