package adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.example.android.AgendaTrip.DeleteAgendaDetails;
import com.example.android.AgendaTrip.EditAgendaDetails;
import com.example.android.AgendaTrip.MapAgendaDetails;
import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.VacationSiteDetail;
import com.squareup.picasso.Picasso;


import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import model.AgendaDetails;
import utils.Util;


/**
 * Created by Vin on 10/25/2015.
 */
public class AgendaDetailsAdapter extends ArrayAdapter<AgendaDetails> implements Serializable {
    private LayoutInflater inflater;
    private ArrayList<AgendaDetails> data;
    private Activity act;
    private int partialResourceID;
    private String MainAgendaStart, MainAgendaEnd;



    public AgendaDetailsAdapter(Activity act, int resource, ArrayList<AgendaDetails> objects) {
        super(act, resource, objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
    }

    public AgendaDetailsAdapter(Activity act, int resource, ArrayList<AgendaDetails> objects, String AgendaStart, String AgendaEnd) {
        super(act, resource, objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
        MainAgendaStart=AgendaStart;
        MainAgendaEnd=AgendaEnd;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public AgendaDetails getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(AgendaDetails item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        View row = convertView;

        AgendaDetailsViewHolder ag=null;
        if(row == null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(partialResourceID, parent, false);

            ag = new AgendaDetailsViewHolder(row);
            row.setTag(ag);
        }
        else{
            ag = (AgendaDetailsViewHolder) row.getTag();
        }

        try{
            Log.v("tescount", getCount() + "");
            ag.setData(getItem(position));


            Log.v("Pos:", String.valueOf(position));



            final String start=ag.ag.getStartTime();
            final String end=ag.ag.getEndTime();
            final String adName=ag.name.getText().toString();
            final int adID = ag.ag.getAgenda_id();
            final int vsID= ag.ag.getVacation_site_id();
            final int lemparposition=position;
            if (position+1==getCount()){
                ag.pathBtn.setVisibility(View.GONE);

            }
            else {

                ag.pathBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String sourcename=getItem(lemparposition).getVacation_site_name();
                        String destname=getItem(lemparposition+1).getVacation_site_name();
                        int sourcevsid=vsID;
                        int destvsid=getItem(lemparposition+1).getVacation_site_id();

                        Intent i=new Intent(getContext(),MapAgendaDetails.class);
                        i.putExtra("sourcevsid",sourcevsid);
                        i.putExtra("destvsid", destvsid);
                        i.putExtra("sourcename",sourcename);
                        i.putExtra("destname",destname);

                        Log.v("coba ya", sourcevsid+" | "+destvsid);
                        getContext().startActivity(i);
                    }
                });
            }
            final AgendaDetailsViewHolder ag2=ag;
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent it = new Intent(act, VacationSiteDetail.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("vacation_site",String.valueOf(ag2.ag.getVacation_site_id()));
                    mBundle.putString("vacation_site_name",String.valueOf(ag2.ag.getVacation_site_name()));
                    mBundle.putString("name",ag2.ag.getVacation_site_name());
                    mBundle.putDouble("longitude", ag2.ag.getLng());
                    mBundle.putDouble("latitude",ag2.ag.getLat());
                    it.putExtras(mBundle);
                    act.startActivity(it);
                }
            });
            ag.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.v("cobamasukga nih", "masuk ga ya");

                    Intent i=new Intent(act.getApplicationContext() ,EditAgendaDetails.class);
                    i.putExtra("start",start);
                    i.putExtra("end",end);
                    Log.v("pas di detailsadapter", MainAgendaStart);
                    i.putExtra("AgendaStart", MainAgendaStart);
                    i.putExtra("AgendaEnd", MainAgendaEnd);
                    i.putExtra("adName",adName);
                    i.putExtra("adID",adID);
                    i.putExtra("vsID",vsID);
                    act.startActivity(i);

                }

            });

            ag.delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(act.getApplicationContext(),DeleteAgendaDetails.class);
                    i.putExtra("adName",adName);
                    i.putExtra("adID",adID);
                    act.startActivity(i);
                }
            });

        }
        catch(Exception e){
            Log.v("Adapter getView", e.getStackTrace().toString());
        };

        return row;
    }


    public class AgendaDetailsViewHolder{
        Button  pathBtn;
        public AgendaDetails ag;
        TextView name,date,id,editBtn, delBtn;
        ImageView siteImage;

        public AgendaDetailsViewHolder(View rowSource){
            this.name = (TextView) rowSource.findViewById(R.id.agendaDetailsName);
            this.date = (TextView) rowSource.findViewById(R.id.agendaDetailsDate );
            this.editBtn= (TextView) rowSource.findViewById(R.id.editAgendaDetailsBtn);
            this.delBtn=(TextView) rowSource.findViewById(R.id.delAgendaDetailsBtn);
            this.pathBtn=(Button) rowSource.findViewById(R.id.pathbtn);
            this.siteImage = (ImageView) rowSource.findViewById(R.id.agendaDetailImage);
        }

        public void setData(AgendaDetails ag){
            this.ag=ag;
            this.name.setText(ag.getVacation_site_name());
            String time=ag.getStartTime();
            String time2=ag.getEndTime();
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convdate=new Date();
            Date convdate2=new Date();
            String trydate,trydate2;
            try{
                convdate=sdf.parse(time);
                convdate2=sdf.parse(time2);
            }
            catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            DateFormat destFormat=new SimpleDateFormat("MMM dd, yyyy hh:mm aa");
            trydate=destFormat.format(convdate);
            trydate2=destFormat.format(convdate2);
            this.date.setText(trydate + " - " + trydate2);
            Picasso.with(act).load(Util.BASE_URL + Util.COVER_URL + this.ag.getVacationSiteImage()).placeholder(R.drawable.agenda_placeholder_square_1).fit().centerCrop().into(siteImage);
        }
    }
}
