package adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by STEFAN on 12/1/2015.
 */
public class HashMapTextAdapter extends ArrayAdapter<Pair<String,String>> implements Filterable {
    ArrayList<Pair<String,String>> data;
    ArrayList<Pair<String,String>> dataAll;
    ArrayList<Pair<String,String>> suggestions;
    ArrayList<Pair<String,String>> dataTemp;
    Boolean changing;
    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public int selectedIndex = -1;
    Activity act;
    int resource;
    public HashMapTextAdapter(Activity act,int resource, ArrayList<Pair<String,String > > data){
        super(act,resource,data);
        this.data =data;
        this.dataAll = (ArrayList<Pair<String,String> >)data.clone();
        this.suggestions= new ArrayList<Pair<String,String>>();
        this.resource=resource;
        this.act = act;
        Log.v("datasize",String.valueOf(data.size()));
        Log.v("datsz2",String.valueOf(data.size()));
        changing = false;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent){
        View view = convertView;
        if(view == null){
            view = new TextView(getContext());
        }
        ((TextView)view).setText(data.get(pos).second);
        return view;
    }

    public Pair<String,String> getItem(int position){
        return data.get(position);
    }

    public int getPosition(Pair<String,String> item){
        return data.indexOf(item);
    }

    public int getCount(){
        return data.size();
    }

    public void notifyDataLoaded() {
        this.dataAll= (ArrayList<Pair<String,String>>) data.clone();
    }

    @Override
    public Filter getFilter() {
        Log.v("setres", "foo");
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                String str = ((Pair<String,String>)(resultValue)).second;
                return str;
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if(constraint != null) {
                    //Log.v("filres", constraint.toString());
                    suggestions.clear();
                    for (Pair<String,String> airport : dataAll) {
                        if(airport.second.toLowerCase().startsWith(constraint.toString().toLowerCase())){
                            //Log.v("word","word = " + airport.second.toLowerCase() + " filter = " +
                            //constraint.toString().toLowerCase() + " res = " + airport.second.toLowerCase().startsWith(constraint                      //.toString().toLowerCase()));
                            suggestions.add(airport);
                            //Log.v("done!",airport.second);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = suggestions;
                    filterResults.count = suggestions.size();
                    return filterResults;
                } else {
                    Log.v("null!","null!");
                    return new FilterResults();
                }
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results.count > 0 && !changing) {
                    changing=true;
                    clear();
                    dataTemp = (ArrayList<Pair<String,String>>) results
                            .values;
                    addAll(dataTemp);
                    changing=false;
                    notifyDataSetChanged();
                }
                else{
                    notifyDataSetInvalidated();
                    notifyDataSetChanged();
                }
            }
        };
    }

}
