package adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.VacationSiteDetail;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;

import model.VacationSite;
import singleton.AppController;
import utils.Util;

/**
 * Created by STEFAN on 2015/10/22.
 */
public class TopVacationSiteListAdapter extends ArrayAdapter<VacationSite>{
    private LayoutInflater inflater;
    private ArrayList<VacationSite> data;
    private Activity act;
    private int partialResourceID;
    TopVacationSiteViewHolder vs;
    ImageLoader imgLoader;

    //constants

    public static final int SORT_BY_PREFERENCE=0;
    public static final int SORT_BY_POPULARITY=1;


    public TopVacationSiteListAdapter(Activity act, int resource, ArrayList<VacationSite>
            objects) {
        super(act, resource, objects);
        this.act = act;
        this.partialResourceID = resource;
        this.data = objects;
        this.imgLoader =  AppController.getInstance().getImageLoader();
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public VacationSite getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(VacationSite item) {
        return super.getPosition(item);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        vs = null;

        Log.v("topvacadap",getItem(position).getImage());
        if(row == null || row.getTag()==null){
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(partialResourceID, parent, false);

            vs = new TopVacationSiteViewHolder(row);

            row.setTag(vs);
        }
        else{
            vs = (TopVacationSiteViewHolder) row.getTag();
        }

        //try{
            vs.setData(getItem(position));
            final VacationSite finalvs = vs.vs;
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(act, VacationSiteDetail.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("vacation_site",String.valueOf(finalvs.getID()));
                    mBundle.putString("vacation_site_name",String.valueOf(finalvs.getName()));
                    mBundle.putDouble("longitude", getItem(position).getLongitude());
                    mBundle.putDouble("latitude",getItem(position).getLatitude());
                    mBundle.putString("name",getItem(position).getName());
                    it.putExtras(mBundle);
                    act.startActivity(it);
                }
            });
        //}
        //catch(Exception e){
            //e.printStackTrace();
        //};

        return row;
    }

    public void sortItem(int type){
        switch(type){
            case SORT_BY_PREFERENCE :
                sort (new Comparator<VacationSite>() {
                    @Override
                    public int compare(VacationSite lhs, VacationSite rhs) {
                        return (lhs.getHypotheses()>rhs.getHypotheses()) ? -1 : 1;
                    }
                });
                break;
            case SORT_BY_POPULARITY :
                sort(new Comparator<VacationSite>(){
                    @Override
                    public int compare(VacationSite lhs, VacationSite rhs) {
                        return (lhs.getRating()>rhs.getRating()) ? -1 : 1;
                    }
                });
                break;
        }
        notifyDataSetChanged();
    }

    public class TopVacationSiteViewHolder{
        public VacationSite vs;
        TextView name,rating,place;
        ImageView siteImage;

        public TopVacationSiteViewHolder(View rowSource){
            this.name = (TextView) rowSource.findViewById(R.id.topVacationSiteName);
            this.place = (TextView) rowSource.findViewById(R.id.topVacationSitePlace);
            this.rating = (TextView) rowSource.findViewById(R.id.topVacationSiteRating);
            this.siteImage = (ImageView) rowSource.findViewById(R.id
                    .topVacationSiteImage);
            siteImage.getLayoutParams().height = Math.round(siteImage.getLayoutParams().width
                    * 0.75f);
        }

        public void setData(VacationSite vs){
            this.vs=vs;
            this.name.setText(vs.getName());
            this.place.setText(vs.getAddress());
            this.rating.setText(String.format("%.1f", vs.getRating()) + " / 5");
            this.rating.setTextColor(Util.getRatingColor(vs.getRating()));
            try {
                Log.v("topvacsite",vs.getImage());
                Picasso.with(act).load(Util.BASE_URL + Util.COVER_URL + vs.getImage()).fit()
                        .centerCrop()
                        .placeholder(R.drawable.agenda_placeholder_1).into(this.siteImage);
            }
            catch(Exception e){
                e.printStackTrace();
                Log.v("topvacsite",vs.getName());
                Log.v("topvacsite","image is empty");
            }
        }
    }
}
