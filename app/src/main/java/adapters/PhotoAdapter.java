package adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.android.AgendaTrip.FlightSearchResult;
import com.example.android.AgendaTrip.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import model.Flight;

/**
 * Created by STEFAN on 12/26/2015.
 */
public class PhotoAdapter extends ArrayAdapter<String> {
    LayoutInflater inflater;
    Activity act;
    int partialResourceId;
    ArrayList<String> data;

    public PhotoAdapter(Activity act, int partialResourceId, ArrayList<String> data){
        super(act,partialResourceId,data);
        this.act = act;
        this.partialResourceId = partialResourceId;
        this.data = data;
    }

    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PhotoViewHolder pvh = null;

        if(row ==null || row.getTag() == null){
            Log.v("photoadap","if");
            inflater = LayoutInflater.from(act);
            row = inflater.inflate(this.partialResourceId, parent, false);
            pvh = new PhotoViewHolder(row);

            row.setTag(pvh);
        }
        else{
            Log.v("photoadap","else");
            pvh = (PhotoViewHolder) row.getTag();
        }
        Log.v("photoadap",getItem(position));
        Log.v("photoadap",pvh.toString());
        pvh.setData(getItem(position));

        return row;
    }

    public class PhotoViewHolder{
        ImageView photo;
        String src;
        public PhotoViewHolder(View source){
            photo = (ImageView) source.findViewById(R.id.partialPhotoImageView);
        }

        public void setData(String src){
            this.src= src;
            Picasso.with(act).load(this.src).placeholder(R.drawable.hotel_icon).fit().centerCrop
                    ().into(this.photo);
        }

    }
}
