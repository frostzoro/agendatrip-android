package data;

/**
 * Created by Vin on 11/28/2015.
 */
public class ConstantsAgendaDetails {

    public static final String DATABASE_NAME="LocalAgendaDetails";
    public static final int DATABASE_VERSION=4;
    public static final String TABLE_NAME="LocalAgendaDetails";
    public static final String StartDate="StartDate";
    public static final String EndDate="EndDate";
    public static final String AgendaDetails_ID="_AgendaDetailsID";
    public static final String AgendaID="_AgendaID";
    public static final String Server_AgendaDetails_ID="_ServerAgendaDetailsID";
    public static final String Vacation_Site_ID="_VacationSiteID";
    public static final String Vacation_Site_Name="_VacationSiteName";
    public static final String vacationSiteImage="_vacationSiteImage";
    public static final String vacationSiteLat="_vacationSiteLat";
    public static final String vacationSiteLng="_vacationSiteLng";
}
