package data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import model.Agenda;

/**
 * Created by Vin on 11/26/2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

private final ArrayList<Agenda> agendas=new ArrayList<>();

    public DatabaseHandler(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    String CREATE_AGENDA_TABLE="CREATE TABLE " + Constants.TABLE_NAME+"("+ Constants.Agenda_ID+" INTEGER PRIMARY KEY,"+
    Constants.Server_Agenda_ID+" INTEGER,"+ Constants.AgendaName+" TEXT,"+ Constants.StartDate+" LONG,"+ Constants.EndDate+" LONG);";

    db.execSQL(CREATE_AGENDA_TABLE);
        Log.v("SQLAGENDACREATED", "TRUE");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS "+ Constants.TABLE_NAME);
        //create a new one
        onCreate(db);
    }

    public void addAgenda(Agenda agenda){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constants.Server_Agenda_ID, agenda.getId());
        values.put(Constants.AgendaName,agenda.getName());
        values.put(Constants.StartDate,agenda.getStartDate());
        values.put(Constants.EndDate,agenda.getEndDate());
        db.insert(Constants.TABLE_NAME, null, values);
        db.close();

    }

    public void clearDB(){
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(Constants.TABLE_NAME, null, null);
    db.close();
    }

    public ArrayList<Agenda> getAgenda(){
String selectQuery= "SELECT * FROM "+Constants.TABLE_NAME;
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor =db.query(Constants.TABLE_NAME, new String[]{Constants.Server_Agenda_ID,Constants.AgendaName,
                        Constants.StartDate, Constants.EndDate },null,null,null,null,Constants.StartDate+ " ASC");

if(cursor.moveToFirst()){
    do{
        Agenda singleAgenda=new Agenda();
        singleAgenda.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.Server_Agenda_ID))));
        singleAgenda.setName(cursor.getString(cursor.getColumnIndex(Constants.AgendaName)));
        singleAgenda.setStartDate(cursor.getString(cursor.getColumnIndex(Constants.StartDate)));
        singleAgenda.setEndDate(cursor.getString(cursor.getColumnIndex(Constants.EndDate)));
        agendas.add(singleAgenda);
    }
    while(cursor.moveToNext());

}

        return agendas;

    }

}
