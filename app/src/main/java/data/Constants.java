package data;

/**
 * Created by Vin on 11/26/2015.
 */
public class Constants {
    public static final String DATABASE_NAME="LocalAgenda";
    public static final int DATABASE_VERSION=3;
    public static final String TABLE_NAME="LocalAgenda";
    public static final String StartDate="StartDate";
    public static final String EndDate="EndDate";
    public static final String Agenda_ID="_AgendaID";
    public static final String Server_Agenda_ID="_ServerAgendaID";
    public static final String AgendaName="AgendaName";


}
