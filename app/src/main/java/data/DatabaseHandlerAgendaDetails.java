package data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import model.AgendaDetails;

/**
 * Created by Vin on 11/26/2015.
 */
public class DatabaseHandlerAgendaDetails extends SQLiteOpenHelper {

private final ArrayList<AgendaDetails> ads=new ArrayList<>();

    public DatabaseHandlerAgendaDetails(Context context) {
        super(context, ConstantsAgendaDetails.DATABASE_NAME, null, ConstantsAgendaDetails.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    String CREATE_AGENDADETAILS_TABLE="CREATE TABLE " + ConstantsAgendaDetails.TABLE_NAME+"("+ ConstantsAgendaDetails.AgendaDetails_ID+" INTEGER PRIMARY KEY,"+
    ConstantsAgendaDetails.Server_AgendaDetails_ID+" INTEGER,"+ ConstantsAgendaDetails.AgendaID+" INTEGER,"+ConstantsAgendaDetails.Vacation_Site_ID+" INTEGER,"+ ConstantsAgendaDetails.StartDate+" LONG,"+ ConstantsAgendaDetails.EndDate+" LONG,"+ ConstantsAgendaDetails.Vacation_Site_Name+" VARCHAR(50),"+ConstantsAgendaDetails.vacationSiteImage+" VARCHAR(50),"+ ConstantsAgendaDetails.vacationSiteLat+" DOUBLE,"+ConstantsAgendaDetails.vacationSiteLng+" DOUBLE);";

    db.execSQL(CREATE_AGENDADETAILS_TABLE);
        Log.v("isi string", CREATE_AGENDADETAILS_TABLE);
        Log.v("SQLAGENDADETAILSCREATED","TRUE");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + ConstantsAgendaDetails.TABLE_NAME);
        //create a new one
        Log.v("agdetdb","updated");
        onCreate(db);
    }

    public void addAgendaDetails(AgendaDetails ad, int AgendaID){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ConstantsAgendaDetails.Server_AgendaDetails_ID, ad.getAgenda_id());
        values.put(ConstantsAgendaDetails.AgendaID, AgendaID);
        values.put(ConstantsAgendaDetails.Vacation_Site_ID,ad.getVacation_site_id());
        values.put(ConstantsAgendaDetails.StartDate,ad.getStartTime());
        values.put(ConstantsAgendaDetails.EndDate,ad.getEndTime());
        values.put(ConstantsAgendaDetails.Vacation_Site_Name,ad.getVacation_site_name());
        values.put(ConstantsAgendaDetails.vacationSiteImage,ad.getVacationSiteImage());
        values.put(ConstantsAgendaDetails.vacationSiteLat,ad.getLat());
        values.put(ConstantsAgendaDetails.vacationSiteLng,ad.getLng());
        db.insert(ConstantsAgendaDetails.TABLE_NAME, null, values);
        db.close();

    }

    public void clearDB(){
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(ConstantsAgendaDetails.TABLE_NAME, null, null);
        db.close();
    }

    public ArrayList<AgendaDetails> getAgendaDetails(int AgendaID){
        String selectQuery= "SELECT * FROM "+ConstantsAgendaDetails.TABLE_NAME+" WHERE "+ConstantsAgendaDetails.AgendaID+"="+AgendaID+" ORDER BY "+ConstantsAgendaDetails.StartDate+" ASC";
        SQLiteDatabase db=this.getReadableDatabase();

        Cursor cursor =db.query(ConstantsAgendaDetails.TABLE_NAME, new String[]{ConstantsAgendaDetails.Server_AgendaDetails_ID,ConstantsAgendaDetails.Vacation_Site_ID,
                ConstantsAgendaDetails.StartDate, ConstantsAgendaDetails.EndDate, ConstantsAgendaDetails.Vacation_Site_Name , ConstantsAgendaDetails.vacationSiteImage,ConstantsAgendaDetails.vacationSiteLat,ConstantsAgendaDetails.vacationSiteLng},null,null,null,null,ConstantsAgendaDetails.StartDate+ " ASC");

if(cursor.moveToFirst()){
    do{
        for (String s: cursor.getColumnNames()
             ) {
            Log.d("column name",s);
        }
        AgendaDetails singleAgendaDetail=new AgendaDetails();
        singleAgendaDetail.setVacation_site_name(cursor.getString(cursor.getColumnIndex(ConstantsAgendaDetails.Vacation_Site_Name)));
        singleAgendaDetail.setAgenda_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ConstantsAgendaDetails.Server_AgendaDetails_ID))));
        singleAgendaDetail.setVacation_site_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ConstantsAgendaDetails.Vacation_Site_ID))));
        singleAgendaDetail.setStartTime(cursor.getString(cursor.getColumnIndex(ConstantsAgendaDetails.StartDate)));
        singleAgendaDetail.setEndTime(cursor.getString(cursor.getColumnIndex(ConstantsAgendaDetails.EndDate)));
        singleAgendaDetail.setVacationSiteImage(cursor.getString(cursor.getColumnIndex(ConstantsAgendaDetails.vacationSiteImage)));
        singleAgendaDetail.setLat(cursor.getDouble(cursor.getColumnIndex(ConstantsAgendaDetails.vacationSiteLat)));
        singleAgendaDetail.setLng(cursor.getDouble(cursor.getColumnIndex(ConstantsAgendaDetails.vacationSiteLng)));
        ads.add(singleAgendaDetail);
    }
    while(cursor.moveToNext());

}

        return ads;

    }

}
