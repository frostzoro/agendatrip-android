package helper;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.example.android.AgendaTrip.R;

import org.florescu.android.rangeseekbar.RangeSeekBar;

/**
 * Created by STEFAN on 12/26/2015.
 */
public class RangeSeekBarDialogFragment extends DialogFragment {
    private RangeSeekBar rangeSeekBar;
    private Button doneButton;
    Number selectedMinValue,selectedMaxValue;
    Number minValue,maxValue;
    OnDialogDismissalListener mOnDialogDismissalListener;

    public interface OnDialogDismissalListener{
        //@param min            selected min value
        //@param max            selected max value
        void  onDialogDismissal(Number min, Number max);
    }

    public RangeSeekBarDialogFragment(){
        // Empty constructor required for dialog fragment.
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRange(0,10000000);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_range_seek_bar,container);
        rangeSeekBar = (RangeSeekBar) v.findViewById(R.id.dialogRangeSeekBar);
        rangeSeekBar.setTextAboveThumbsColor(R.color.black);
        rangeSeekBar.setRangeValues(minValue,maxValue);
        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar
                .OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object
                    maxValue) {
                selectedMinValue = (Number) minValue;
                selectedMaxValue = (Number) maxValue;
            }
        });
        doneButton = (Button) v.findViewById(R.id.dialogRangeSeekBarDoneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("inside range","onclickdone");
                if (mOnDialogDismissalListener != null) {
                    mOnDialogDismissalListener.onDialogDismissal(selectedMinValue, selectedMaxValue);
                    Log.v("values", selectedMinValue + " " + selectedMaxValue);
                }
                dismiss();
            }
        });
        return v;
    }

    public void setRange(Number A, Number B){
        minValue = A;
        maxValue = B;
    }

    public void setOnDialogDismissalListener(OnDialogDismissalListener listener){
        mOnDialogDismissalListener = listener;
    }
}
