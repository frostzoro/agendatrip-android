package helper;

import android.util.Log;

import org.joda.time.LocalTime;

import java.util.ArrayList;

import model.Flight;

/**
 * Created by STEFAN on 1/8/2016.
 */
public class FlightFilter {
    LocalTime departureTimeMin,departureTimeMax;
    ArrayList<String> airlines;
    Boolean usesTimeMin,usesTimeMax,usesAirlines;
    public FlightFilter(){
        airlines = new ArrayList<>();
    }

    public boolean filterAll(Flight f){
        boolean result = false;
        LocalTime departureTime, arrivalTime;
        departureTime = LocalTime.parse(f.getDeparture_time());
        arrivalTime = LocalTime.parse(f.getArrival_time());
        if(!usesTimeMin || (departureTime.isAfter(departureTimeMin) || departureTime.equals(departureTimeMin))){
            if(!usesTimeMax || (departureTime.isBefore(departureTimeMax) || departureTime.equals(departureTimeMax))){
                result = result || true;
            }
        }

        if(!usesAirlines || airlines.indexOf(f.getAirline_name())!=-1){
            result = result || true;
        }

        return result;
    }

    public boolean filterAirline(Flight f){
        Log.v("usesairline", usesAirlines.toString());
        Log.v("airlines",airlines.toString());
        Log.v("currairline",f.getAirline_name());
        if(!usesAirlines || airlines.indexOf(f.getAirline_name())!=-1){
            return true;
        }
        return false;
    }

    public void setDepartureTimeMin(LocalTime departureTimeMin){
        usesTimeMin = true;
        this.departureTimeMin = departureTimeMin;
    }

    public void setDepartureTimeMax(LocalTime departureTimeMax){
        usesTimeMax = true;
        this.departureTimeMax = departureTimeMax;
    }

    public void setAirlines(ArrayList<String> flightProviders){
        usesAirlines = true;
        this.airlines = flightProviders;
    }
}
