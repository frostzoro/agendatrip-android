package helper.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by STEFAN on 2/28/2016.
 */
public class GoogleLocation {
    String name ;
    LatLng latlon ;

    public GoogleLocation(){

    }

    public GoogleLocation(String name, LatLng latlon){
        this.name = name;
        this.latlon = latlon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLatlon() {
        return latlon;
    }

    public void setLatlon(LatLng latlon) {
        this.latlon = latlon;
    }
}
