package helper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.android.AgendaTrip.R;
import com.example.android.AgendaTrip.TrainPassengerInformation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import model.UserIdentity;

/**
 * Created by Vin on 12/27/2015.
 */
public class TrainPassengerInformationHolder {


    int partial_resource_id,number;
    LayoutInflater inflater;
    Activity act;
    Context ctx;
    HashMap<String,Boolean> h;
    String type;
    String errorMsg;
    View v;
    LinearLayout idLayout, fullnameLayout,birthDateLayout,identityLayout;
    TextView caption,birthDate;
    ArrayList<UserIdentity> userIdentityArrayList;
    EditText firstName, lastName, idNumber,phoneNumber;
    Spinner identity, title;
    ArrayList<String> titleAdult, titleChild,userIdentityNameList;;
    ArrayAdapter<String> titleAdapter,identityAdapter;
    TrainPassengerInformationHolder _self;

    public TrainPassengerInformationHolder(HashMap<String,Boolean> items, Activity act, Context ctx,String type,int number,ArrayList<UserIdentity>
            userIdentityArrayList){
        this.act=act;
        this.ctx=ctx;
        inflater=LayoutInflater.from(act);
        this.h=items;
        this.type=type;
        this.partial_resource_id= R.layout.train_passenger_information_partial;
        this.number=number;
        this.userIdentityArrayList = userIdentityArrayList;
        this.userIdentityNameList = new ArrayList<>();
        this.titleAdult = new ArrayList<>(Arrays.asList("Mr", "Mrs", "Ms"));
        this.titleChild = new ArrayList<>(Arrays.asList("Mstr","Miss"));
        _self = this;
    }

    public View getView(){
    if (v==null){
        v=inflater.inflate(partial_resource_id,null);
        identityLayout = (LinearLayout) v.findViewById(R.id
                .passengerInformationPartialIdentityLayout);
        if(userIdentityArrayList.size()>0) {
            identity = (Spinner) v.findViewById(R.id.passengerInformationPartialIdentity);
            int sz = userIdentityArrayList.size();
            for (int i = 0; i < sz; i++) {
                userIdentityNameList.add(userIdentityArrayList.get(i).getFirst_name() + " " +
                        userIdentityArrayList.get(i).getLast_name());
            }
            identityAdapter = new ArrayAdapter<String>(act, R.layout
                    .support_simple_spinner_dropdown_item, userIdentityNameList);
            identity.setAdapter(identityAdapter);
            identity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    UserIdentity selected = userIdentityArrayList.get(position);
                    switch (type) {
                        case "Adult":
                            title.setSelection(titleAdapter.getPosition(selected.getTitle()));
                            break;
                        default:
                            if (selected.getTitle().equals("Mr"))
                                title.setSelection(titleAdapter.getPosition("Mstr"));
                            else
                                title.setSelection(titleAdapter.getPosition("Miss"));
                            break;
                    }
                    firstName.setText(selected.getFirst_name());
                    lastName.setText(selected.getLast_name());
                    idNumber.setText(selected.getId_number().equals("null") ? "" : selected.getId_number());
                    phoneNumber.setText(selected.getPhone_number().equals("null") ? "" : selected.getPhone_number());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    birthDate.setText(sdf.format(selected.getBirthdate()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        else{
            identityLayout.setVisibility(View.GONE);
        }
        title=(Spinner) v.findViewById(R.id.passengerInformationPartialTitle);
        if(this.type.equals("Adult"))
            titleAdapter = new ArrayAdapter<String>(act,android.R.layout
                    .simple_spinner_dropdown_item,titleAdult);
        else titleAdapter = new ArrayAdapter<String>(act,android.R.layout
                .simple_spinner_dropdown_item,titleChild);
        title.setAdapter(titleAdapter);
        idLayout = (LinearLayout) v.findViewById(R.id.passengerInformationPartialIdLayout);
        birthDateLayout = (LinearLayout) v.findViewById(R.id
                .passengerInformationPartialBirthDateLayout);
    }
        caption = (TextView) v.findViewById(R.id.passengerInformationPartialCaption);
        caption.setText(type + " Passenger " + number);
        firstName = (EditText) v.findViewById(R.id.passengerInformationPartialFirstName);
        lastName = (EditText) v.findViewById(R.id.passengerInformationPartialLastName);
        idNumber = (EditText) v.findViewById(R.id.passengerInformationPartialId);
        phoneNumber=(EditText) v.findViewById(R.id.passengerInformationPartialPhoneNum);
       /* if(!type.equals("adult")){
            idLayout.setVisibility(View.GONE);
        }*/
        birthDate = (TextView) v.findViewById(R.id.passengerInformationPartialBirthDate);
      /*  if(h.get(type+"_birthdate")==null) {
            birthDateLayout.setVisibility(View.GONE);
        }*/
        birthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrainPassengerInformation f = (TrainPassengerInformation) act;
                f.showCalendarDatePicker(_self);
            }
        });
        return v;
    }

    public void setBirthDate(String birthDate){
        this.birthDate.setText(birthDate);
    }

    public Boolean checkData(){
        Boolean can = true;
        this.errorMsg = "";

        if(title.getSelectedItemPosition()<0){
            title.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("PIH", "title is empty");
            can = false;
        }

        if(type.equals("Adult") && idNumber.getText().length()<=0){
            idNumber.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
        }

        if(firstName.getText().length()<=0){
            firstName.setBackground(act.getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("PIH", "firstname is empty");
            can = false;
        }

        if(lastName.getText().length()<=0){
            lastName.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("PIH", "lastname is empty");
            can = false;
        }

        if(h.get(type+"_birthdate")!=null && birthDate.getText().length()<=0){
            birthDate.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("PTH","birthdate is empty");
            can=false;
        }
        else if(h.get(type+"_birthdate")!=null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Calendar c  = Calendar.getInstance();
                c.setTime(sdf.parse(birthDate.getText().toString()));
                int age = Calendar.getInstance().get(Calendar.YEAR) - c.get(Calendar.YEAR);

                Log.v("passage",String.valueOf(age));
                if(age<12 && type.equals("Adult")){
                    birthDate.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
                    errorMsg = "Adult " + number + " age must be more than 12";
                    can=false;
                }
                else if((age>12 || age <2) && type.equals("Child")){
                    birthDate.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
                    errorMsg = "Child " + number + " age must be between 2 and 12";
                    can=false;
                }
                else if(age>2 && type.equals("Infant")){
                    birthDate.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
                    errorMsg = "Infant " + number + " age must be be lower than 2";
                    can=false;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        if(can) {
            errorMsg = "";
        }
        return can;
    }

    public String attributesToParam(){
        String param = "";
        param += "name" + type + number + "="  + firstName
                .getText();

        param += "&salutation" + type + number + "=" +  title
                .getSelectedItem();
        if(type.equals("Adult") ){
            param += "&IdCard" +type + number + "=" + idNumber
                    .getText();
        }

            param += "&birthDate" + type + number + "=" +
                    birthDate.getText();
            param+= "&noHp"+type+number+"="+phoneNumber.getText();

        Log.v("pihinfo", type + number + param);
        return param;
    }

    public int getPartial_resource_id() {
        return partial_resource_id;
    }

    public void setPartial_resource_id(int partial_resource_id) {
        this.partial_resource_id = partial_resource_id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public void setInflater(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public Activity getAct() {
        return act;
    }

    public void setAct(Activity act) {
        this.act = act;
    }

    public Context getCtx() {
        return ctx;
    }

    public void setCtx(Context ctx) {
        this.ctx = ctx;
    }

    public HashMap<String, Boolean> getH() {
        return h;
    }

    public void setH(HashMap<String, Boolean> h) {
        this.h = h;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public View getV() {
        return v;
    }

    public void setV(View v) {
        this.v = v;
    }

    public LinearLayout getIdLayout() {
        return idLayout;
    }

    public void setIdLayout(LinearLayout idLayout) {
        this.idLayout = idLayout;
    }

    public LinearLayout getFullnameLayout() {
        return fullnameLayout;
    }

    public void setFullnameLayout(LinearLayout fullnameLayout) {
        this.fullnameLayout = fullnameLayout;
    }

    public LinearLayout getBirthDateLayout() {
        return birthDateLayout;
    }

    public void setBirthDateLayout(LinearLayout birthDateLayout) {
        this.birthDateLayout = birthDateLayout;
    }

    public TextView getCaption() {
        return caption;
    }

    public void setCaption(TextView caption) {
        this.caption = caption;
    }

    public TextView getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(TextView birthDate) {
        this.birthDate = birthDate;
    }

    public EditText getFirstName() {
        return firstName;
    }

    public void setFirstName(EditText firstName) {
        this.firstName = firstName;
    }

    public EditText getLastName() {
        return lastName;
    }

    public void setLastName(EditText lastName) {
        this.lastName = lastName;
    }

    public EditText getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(EditText idNumber) {
        this.idNumber = idNumber;
    }

    public Spinner getTitle() {
        return title;
    }

    public void setTitle(Spinner title) {
        this.title = title;
    }

    public ArrayList<String> getTitleAdult() {
        return titleAdult;
    }

    public void setTitleAdult(ArrayList<String> titleAdult) {
        this.titleAdult = titleAdult;
    }

    public ArrayList<String> getTitleChild() {
        return titleChild;
    }

    public void setTitleChild(ArrayList<String> titleChild) {
        this.titleChild = titleChild;
    }

    public TrainPassengerInformationHolder get_self() {
        return _self;
    }

    public void set_self(TrainPassengerInformationHolder _self) {
        this._self = _self;
    }
}
