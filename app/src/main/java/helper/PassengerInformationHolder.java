package helper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.android.AgendaTrip.FlightPassengerInformation;
import com.example.android.AgendaTrip.R;

import java.util.Date;
import java.util.Calendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import model.UserIdentity;

/**
 * Created by STEFAN on 12/10/2015.
 */
public class PassengerInformationHolder {
    int partial_resource_id,number;
    LayoutInflater inflater;
    Activity act;
    Context ctx;
    HashMap<String,Boolean> h;
    String type,errorMsg;
    View v;
    LinearLayout idLayout,birthDateLayout, nationalityLayout,
            departureBaggageLayout, returnBaggageLayout,identityLayout;
    TextView caption,birthDate;
    Calendar birthDateCalendar;
    EditText firstName, lastName, idNumber;
    Spinner identity, nationality, departureBaggage, returnBaggage,title;
    ArrayList<String> nationalityList,nationalityCode,baggageList,titleAdult,titleChild,
            userIdentityNameList;
    ArrayList<UserIdentity> userIdentityArrayList;
    ArrayAdapter<String> identityAdapter,nationalityAdapter, departureBaggageAdapter,
            returnBaggageAdapter,
            titleAdapter;
    PassengerInformationHolder _self;

    public PassengerInformationHolder(HashMap<String,Boolean> items,Activity act,Context ctx,String
            type,int
        number,ArrayList<String> nationalityList,ArrayList<String>
                                              nationalityCode,ArrayList<UserIdentity>
            userIdentityArrayList){
        this.birthDateCalendar = Calendar.getInstance();
        this.act = act;
        this.ctx = ctx;
        inflater = LayoutInflater.from(act);
        this.h = items;
        this.type = type;
        this.partial_resource_id = R.layout.passenger_information_partial;
        this.nationalityList = nationalityList;
        this.nationalityCode = nationalityCode;
        this.userIdentityArrayList = userIdentityArrayList;
        this.userIdentityNameList = new ArrayList<>();
        this.baggageList = new ArrayList<String>(Arrays.asList("15", "20", "25", "30", "40"));
        this.number = number;
        this.titleAdult = new ArrayList<>(Arrays.asList("Mr","Mrs","Ms"));
        this.titleChild = new ArrayList<>(Arrays.asList("Mstr","Miss"));
        _self = this;
        Log.v("hashbool",this.h.toString());
    }

    public View getView(){
        if(v == null){
            v = inflater.inflate(partial_resource_id,null);
            identityLayout = (LinearLayout) v.findViewById(R.id
                    .passengerInformationPartialIdentityLayout);

            if(userIdentityArrayList.size()>0) {
                identity = (Spinner) v.findViewById(R.id.passengerInformationPartialIdentity);
                int sz = userIdentityArrayList.size();
                for (int i = 0; i < sz; i++) {
                    userIdentityNameList.add(userIdentityArrayList.get(i).getFirst_name() + " " +
                            userIdentityArrayList.get(i).getLast_name());
                }
                identityAdapter = new ArrayAdapter<String>(act, R.layout
                        .support_simple_spinner_dropdown_item, userIdentityNameList);
                identity.setAdapter(identityAdapter);
                identity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        UserIdentity selected = userIdentityArrayList.get(position);
                        switch (type) {
                            case "Adult":
                                title.setSelection(titleAdapter.getPosition(selected.getTitle()));
                                break;
                            default:
                                if (selected.getTitle().equals("Mr"))
                                    title.setSelection(titleAdapter.getPosition("Mstr"));
                                else
                                    title.setSelection(titleAdapter.getPosition("Miss"));
                                break;
                        }
                        firstName.setText(selected.getFirst_name());
                        lastName.setText(selected.getLast_name());
                        idNumber.setText(selected.getId_number().equals("null") ? "" : selected.getId_number());
                        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                        birthDate.setText(sdf.format(selected.getBirthdate()));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            else{
                identityLayout.setVisibility(View.GONE);
            }

            title = (Spinner) v.findViewById(R.id.passengerInformationPartialTitle);
            if(this.type.equals("Adult"))
            titleAdapter = new ArrayAdapter<String>(act,android.R.layout
                    .simple_spinner_dropdown_item,titleAdult);
            else titleAdapter = new ArrayAdapter<String>(act,android.R.layout
                    .simple_spinner_dropdown_item,titleChild);
            title.setAdapter(titleAdapter);
            idLayout = (LinearLayout) v.findViewById(R.id.passengerInformationPartialIdLayout);
            birthDateLayout = (LinearLayout) v.findViewById(R.id
                    .passengerInformationPartialBirthDateLayout);
            nationalityLayout = (LinearLayout) v.findViewById(
                    R.id.passengerInformationPartialNationalityLayout
            );
            departureBaggageLayout = (LinearLayout) v.findViewById(R.id
                    .passengerInformationPartialDepartureBaggageLayout);
            returnBaggageLayout = (LinearLayout) v.findViewById(R.id
                    .passengerInformationPartialReturnBaggageLayout);
            caption = (TextView) v.findViewById(R.id.passengerInformationPartialCaption);
            caption.setText(type + " Passenger " + number);
            firstName = (EditText) v.findViewById(R.id.passengerInformationPartialFirstName);
            lastName = (EditText) v.findViewById(R.id.passengerInformationPartialLastName);
            idNumber = (EditText) v.findViewById(R.id.passengerInformationPartialId);
            if(!type.equals("adult")){
                idLayout.setVisibility(View.GONE);
            }
            birthDate = (TextView) v.findViewById(R.id.passengerInformationPartialBirthDate);
            if(h.get(type+"_birthdate")==null) {
                birthDateLayout.setVisibility(View.GONE);
            }
            birthDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FlightPassengerInformation f = (FlightPassengerInformation) act;
                    f.showCalendarDatePicker(_self);
                }
            });
            nationality = (Spinner) v.findViewById(R.id.passengerInformationPartialNationality);
            departureBaggage = (Spinner) v.findViewById(R.id
                    .passengerInformationPartialDepartureBaggage);
            returnBaggage = (Spinner) v.findViewById(R.id.passengerInformationPartialReturnBaggage);
            if(h.get(type+"_nationality") == null){
                nationalityLayout.setVisibility(View.GONE);
            }
            else {
                nationalityAdapter = new ArrayAdapter(act, android.R
                        .layout
                        .simple_spinner_dropdown_item, nationalityList);
                Log.v("nationality",nationalityList.toString());
                nationality.setAdapter(nationalityAdapter);
                nationality.setSelection(nationalityAdapter.getPosition("Indonesia"));
            }
            if(this.type.equals("Infant") || h.get(type+"_baggage_d") == null){
                departureBaggageLayout.setVisibility(View.GONE);
            }
            else{
                departureBaggageAdapter = new ArrayAdapter(act,android.R.layout
                        .simple_spinner_dropdown_item,
                        baggageList);
                departureBaggage.setAdapter(departureBaggageAdapter);
            }
            if(this.type.equals("Infant") || h.get("round_trip") == false || h.get
                    (type+"_baggage_r") == null){
                returnBaggageLayout.setVisibility(View.GONE);
            }
            else{
                returnBaggageAdapter = new ArrayAdapter(act,android.R.layout
                        .simple_spinner_dropdown_item,baggageList);
                returnBaggage.setAdapter(returnBaggageAdapter);
            }
        }
        return v;
    }

    public void setBirthDate(Date birthDate){
        this.birthDateCalendar.setTime(birthDate);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        this.birthDate.setText(sdf.format(birthDate));
    }

    public Boolean checkData(){
        Boolean can = true;
        this.errorMsg = "";

        if(title.getSelectedItemPosition()<0){
            title.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("PIH", "title is empty");
            can = false;
        }

        if(type.equals("Adult") && h.get("Adult_id") != null &&idNumber.getText().length()<=0){
            idNumber.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
        }

        if(type.equals("Adult") && h.get("Adult_id") !=null && idNumber.getText().length()>20){
            idNumber.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            errorMsg = act.getResources().getString(R.string.error_identity_number_length);
        }

        if(firstName.getText().length()<=0){
            firstName.setBackground(act.getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("PIH", "firstname is empty");
            can = false;
        }

        if(lastName.getText().length()<=0){
            lastName.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("PIH", "lastname is empty");
            can = false;
        }

        if(h.get(type+"_birthdate")!=null && birthDate.getText().length()<=0){
            birthDate.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("PTH","birthdate is empty");
            can=false;
        }
        else if(h.get(type+"_birthdate")!=null){
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
            try {
                Calendar c  = Calendar.getInstance();
                c.setTime(sdf.parse(birthDate.getText().toString()));
                int age = Calendar.getInstance().get(Calendar.YEAR) - c.get(Calendar.YEAR);
                Log.v("age of",age + " asda");
                Log.v("passage",String.valueOf(age));
                if(age<12 && type.equals("Adult")){
                    birthDate.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
                    errorMsg = "Adult " + number + " age must be more than 12";
                    can=false;
                }
                else if((age>12 || age <2) && type.equals("Child")){
                    birthDate.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
                    errorMsg = "Child " + number + " age must be between 2 and 12";
                    can=false;
                }
                else if(age>2 && type.equals("Infant")){
                    birthDate.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
                    errorMsg = "Infant " + number + " age must be be lower than 2";
                    can=false;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(h.get(type+"_nationality")!=null && nationality.getSelectedItemPosition()<0){
            nationality.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("PTH","birthdate is empty");
            can = false;
        }

        if(h.get(type+"_baggage_d") != null && departureBaggage.getSelectedItemPosition()<0){
            departureBaggage.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("PTH","baggage_d is empty");
            can = false;
        }

        if(h.get(type+"_baggage_r") != null && returnBaggage.getSelectedItemPosition()<0){
            returnBaggage.setBackground(act.getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("PTH","baggage_r is empty");
            can = false;
        }
        if(can) {
            errorMsg = "";
        }
        return can;
    }

    public String attributesToParam(){
        String param = "";
        param += "firstname" + Character.toLowerCase(type.charAt(0)) + number + "="  + firstName
                .getText();
        param += "&lastname" + Character.toLowerCase(type.charAt(0)) + number + "=" + lastName
                .getText();
        param += "&title" + Character.toLowerCase(type.charAt(0)) + number + "=" +  title
                .getSelectedItem();
        if(type.equals("Adult") && h.get("Adult_id") != null ){
            param += "&id" +Character.toLowerCase(type.charAt(0)) + number + "=" + idNumber
                    .getText();
        }
        if(h.get(type + "_birthdate") != null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String birthDateFormatted = sdf.format(birthDateCalendar.getTime());
            param += "&birthdate" + Character.toLowerCase(type.charAt(0)) + number + "=" +
                   birthDateFormatted;
        }
        if(h.get(type + "_baggage_d") != null){
            param += ("&dcheckinbaggage" + Character.toLowerCase(type.charAt(0)) + number +"1"+ "=" +
                    departureBaggage.getSelectedItem());
        }
        if(h.get(type+ "_baggage_r") != null){
            param += ("&rcheckinbaggage" + Character.toLowerCase(type.charAt(0)) + number +"1"+
                    "=" +
                    returnBaggage.getSelectedItem());
        }
        if(h.get(type+ "_nationality") != null){
            param += ("&passportnationality" + Character.toLowerCase(type.charAt(0)) + number + "" +
                    "=" + nationalityCode.get(nationality.getSelectedItemPosition()));
        }
        Log.v("pihinfo", type + number + param);
        return param;
    }


    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }


}
