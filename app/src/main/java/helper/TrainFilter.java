package helper;

import java.util.ArrayList;

import model.Train;

/**
 * Created by STEFAN on 1/9/2016.
 */
public class TrainFilter {
    ArrayList<String> classes;
    Boolean useClass;


    public TrainFilter(){
        useClass=false;
    }


    public void setClasses(ArrayList<String> classes){
        useClass=true;
        this.classes = classes;
    }

    public boolean filterClass(Train t){
        boolean result = false;
        String trainC = t.getClass_name();
        if(!useClass || classes.indexOf(trainC) != -1){
            result = result || true;
        }
        return result;
    }
}
