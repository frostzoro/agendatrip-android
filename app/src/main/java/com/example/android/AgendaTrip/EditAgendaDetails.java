package com.example.android.AgendaTrip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import adapters.AgendaAdapter;
import utils.AgendaTimePickerDialog;
import utils.Constants;
import utils.Util;

/**
 * Created by Vin on 10/29/2015.
 */
public class EditAgendaDetails extends AppCompatActivity implements CalendarDatePickerDialogFragment.OnDateSetListener,android.app.TimePickerDialog.OnTimeSetListener {
    String dateTemp,timeTemp,sentTemp,sentTimeTemp;
    boolean startFlag=true;
    TextView namavs;
    private String jsonResponse;
     TextView startDate, endDate,hiddenStart,hiddenEnd,agendaPeriodText;
    Button btnFinalizeAgendaDetail;
    private ProgressDialog pDialog;
    CalendarDatePickerDialogFragment dpb;
    String agendaStartS="",agendaEndS="";
    Date agendaStart=null,agendaEnd=null;
    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

        Calendar temp = Calendar.getInstance();
        dateTemp="";

        temp.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        temp.set(Calendar.MONTH,monthOfYear);
        temp.set(Calendar.YEAR, year);

        SimpleDateFormat destFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());


        sentTemp=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;

        dateTemp=destFormat.format(temp.getTime());


    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute){
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        Calendar temp=Calendar.getInstance();
        temp.set(Calendar.HOUR_OF_DAY, hour);
        temp.set(Calendar.MINUTE,minute);
        timeTemp="";
        sentTimeTemp=hour+":"+minute+":00";
        timeTemp=sdf.format(temp.getTime());

        if(startFlag){
            startDate.setText(dateTemp+" "+timeTemp);
            hiddenStart.setText(sentTemp+" "+sentTimeTemp);

        }
        else{endDate.setText(dateTemp+" "+timeTemp);
            hiddenEnd.setText(sentTemp+" "+sentTimeTemp);
        }


    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_agenda_detail);
        pDialog = new ProgressDialog(EditAgendaDetails.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        Intent i=getIntent();
        final String start=i.getStringExtra("start");
        final String end=i.getStringExtra("end");
        String adName=i.getStringExtra("adName");
        final int adID=i.getIntExtra("adID", 9999);
        final int vsID=i.getIntExtra("vsID",9999);

        agendaStartS=i.getStringExtra("AgendaStart");
        agendaEndS=i.getStringExtra("AgendaEnd");
        Log.v("coba ass ya", agendaStartS);
       final SimpleDateFormat sdfin=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        final SimpleDateFormat sdfout=new SimpleDateFormat("MMM dd, yyyy hh:mm aa");


        try{
           agendaStart= sdfin.parse(agendaStartS);
           agendaEnd=sdfin.parse(agendaEndS);
        }
        catch (Exception e){
            e.printStackTrace();

        }
        String outStart="",outEnd="";
        agendaPeriodText=(TextView) findViewById(R.id.agendaPeriodText);
        startDate=(TextView) findViewById(R.id.selectedAgendaStart);
        endDate=(TextView) findViewById(R.id.selectedAgendaEnd);
        namavs=(TextView) findViewById(R.id.selectedAgendaName);
        hiddenStart=(TextView) findViewById(R.id.hiddenStart);
        hiddenEnd=(TextView) findViewById(R.id.hiddenEnd);
        String outAgendaStart="", outAgendaEnd="";
        try {
            Date instartDate = sdfin.parse(start);
            Date inendDate=sdfin.parse(end);
            outStart=sdfout.format(instartDate);
            outEnd=sdfout.format(inendDate);
            outAgendaStart=sdfout.format(agendaStart);
            outAgendaEnd=sdfout.format(agendaEnd);
        }
        catch(Exception e){
            e.printStackTrace();

        }

        namavs.setText(adName);
        startDate.setText(outStart);
        endDate.setText(outEnd);
        hiddenStart.setText(start);
        hiddenEnd.setText(end);
        //String namaVS=getIntent().getExtras().getString("vs_name");

        final String idVS = getIntent().getExtras().getString("vacation_site");

        Log.v("Edited name: ", adName);


        btnFinalizeAgendaDetail=(Button) findViewById(R.id.finalizeAddAgenda);


        btnFinalizeAgendaDetail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pDialog.show();
Calendar tempcall1=Calendar.getInstance(), tempcall2=Calendar.getInstance();
                if(!hiddenStart.getText().toString().equals("") && !hiddenEnd.getText().toString().equals("")){
                    Date tempcal1=null, tempcal2=null;


                    try {
                      tempcal1 = sdfin.parse(hiddenStart.getText().toString());
                        tempcal2=sdfin.parse(hiddenEnd.getText().toString());

                    }
                    catch (Exception e){
                        e.printStackTrace();

                    }


                    tempcall1.setTime(tempcal1);
                    tempcall2.setTime(tempcal2);
                }
                if(tempcall2.after(tempcall1)) {


                    EditAgendaDetails(String.valueOf(adID), String.valueOf(vsID), hiddenStart.getText().toString(), hiddenEnd.getText().toString());
                }
                else{
                    Toast.makeText(EditAgendaDetails.this, "Your End Date can't be earlier than your Start Date", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Trip Details");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void EditAgendaDetails(String adID, String vsID, String start, String end) {

        Context context = getApplicationContext();
        String url = "http://agendatrip.com/api/v1/agenda/detail/update";
        Map<String, String> params = new HashMap<String, String>();


        params.put("userID", Util.loggedID);
        params.put("key", Util.loggedKey);
        params.put("agenda_id", String.valueOf(AgendaAdapter.sAgendaID));
        params.put("agenda_detail_id", adID);
        params.put("vacation_site_id", vsID);
        params.put("start", start);
        params.put("end", end);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                pDialog.hide();
                Log.v("dataresp", response.toString());


                Log.v("asd", response.toString());
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder(EditAgendaDetails.this);
                    adb.setMessage(getString(R.string.timeout_error));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                    pDialog.hide();

                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_LONG).show();
                    pDialog.hide();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    pDialog.hide();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    pDialog.hide();
                    //TODO
                }
            }
        });
        jsObjRequest.setRetryPolicy(Constants.RETRY_POLICY);
        requestQueue.add(jsObjRequest);
    }

    public void setStartDate(View v){
        Calendar cal1=Calendar.getInstance();
        Calendar cal2=Calendar.getInstance();
        cal1.setTime(agendaStart);
        cal2.setTime(agendaEnd);
        dpb = CalendarDatePickerDialogFragment
                .newInstance(EditAgendaDetails.this, cal1.get(Calendar
                                .YEAR),
                        cal1.get(Calendar.MONTH),cal1.get
                                (Calendar
                                        .DAY_OF_MONTH));

        MonthAdapter.CalendarDay cstart = new MonthAdapter.CalendarDay(cal1);
        MonthAdapter.CalendarDay cend = new MonthAdapter.CalendarDay(cal2);
        dpb.setDateRange(cstart, cend);

        dpb.show(getSupportFragmentManager(), "title");


    }

    public void setStartTime(View v){
        startFlag=true;
        AgendaTimePickerDialog agendaTimePickerDialog=new AgendaTimePickerDialog();
        agendaTimePickerDialog.show(getFragmentManager(), "tagtime");
        setStartDate(v);
    }

    public void setEndDate(View v){
        Calendar cal1=Calendar.getInstance();
        Calendar cal2=Calendar.getInstance();
        cal1.setTime(agendaStart);
        cal2.setTime(agendaEnd);
        dpb = CalendarDatePickerDialogFragment
                .newInstance(EditAgendaDetails.this, cal1.get(Calendar
                                .YEAR),
                        cal1.get(Calendar.MONTH),cal1.get
                                (Calendar
                                        .DAY_OF_MONTH));

        MonthAdapter.CalendarDay cstart = new MonthAdapter.CalendarDay(cal1);
        MonthAdapter.CalendarDay cend = new MonthAdapter.CalendarDay(cal2);
        dpb.setDateRange(cstart, cend);


        dpb.show(getSupportFragmentManager(), "title");


    }
    public void setEndTime(View v){
        startFlag=false;
        AgendaTimePickerDialog agendaTimePickerDialog=new AgendaTimePickerDialog();
        agendaTimePickerDialog.show(getFragmentManager(), "tagtime");
        setEndDate(v);
    }

}
