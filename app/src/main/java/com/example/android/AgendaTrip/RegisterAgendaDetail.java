package com.example.android.AgendaTrip;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import adapters.AgendaAdapter;
import helper.TrainPassengerInformationHolder;
//import utils.RegAgendaDatePickerDialog;
import utils.Constants;
import utils.RegAgendaTimePickerDialog;
import utils.Util;

/**
 * Created by Vin on 10/29/2015.
 */
public class RegisterAgendaDetail extends AppCompatActivity implements CalendarDatePickerDialogFragment.OnDateSetListener,android.app.TimePickerDialog.OnTimeSetListener{
    boolean startFlag=false;
    TextView namavs;
    String dateTemp, timeTemp, sentTemp,sentTimeTemp;
    private String jsonResponse;
    TextView startDate, endDate, hiddenStart, hiddenEnd,agendaPeriodText;
    Button btnFinalizeAgendaDetail;
    CalendarDatePickerDialogFragment dpb;
    String agendaStartS="",agendaEndS="";
    Date agendaStart=null,agendaEnd=null;
    Calendar cal1,cal2;
    private ProgressDialog pDialog;


    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

        Calendar temp = Calendar.getInstance();
        dateTemp="";
        temp.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        temp.set(Calendar.MONTH,monthOfYear);
        temp.set(Calendar.YEAR, year);

        SimpleDateFormat destFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());


        sentTemp=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        Log.v("tesbulan",sentTemp+" - "+monthOfYear);
        dateTemp=destFormat.format(temp.getTime());
        Log.v("formattedmonth",dateTemp);


    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute){
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");

        Calendar temp=Calendar.getInstance();
        temp.set(Calendar.HOUR_OF_DAY, hour);
        temp.set(Calendar.MINUTE,minute);
        timeTemp="";
        SimpleDateFormat sentsdf=new SimpleDateFormat("hh:mm:ss");
        sentTimeTemp=sentsdf.format(temp.getTime());
        timeTemp=sdf.format(temp.getTime());

        if(startFlag){
            startDate.setText(dateTemp+" "+timeTemp);
            hiddenStart.setText(sentTemp+" "+sentTimeTemp);

        }
        else{endDate.setText(dateTemp+" "+timeTemp);
        hiddenEnd.setText(sentTemp+" "+sentTimeTemp);
        }


    }

    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_agenda_detail);
        cal1=Calendar.getInstance();
        cal2=Calendar.getInstance();
        Intent i=getIntent();
        String namaVS=i.getStringExtra("namaagenda");

        pDialog = new ProgressDialog(RegisterAgendaDetail.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        namavs=(TextView) findViewById(R.id.selectedAgendaName);
        namavs.setText(namaVS);
        //String namaVS=getIntent().getExtras().getString("vs_name");

        final String idVS = getIntent().getStringExtra("vacation_site");
        agendaStartS=getIntent().getStringExtra("agendaStart");
        agendaEndS=getIntent().getStringExtra("agendaEnd");
        Log.v("COBAYA", agendaStartS);

        final SimpleDateFormat sdfin=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        final SimpleDateFormat sdfout=new SimpleDateFormat("MMM dd, yyyy hh:mm aa");
        startDate=(TextView) findViewById(R.id.selectedAgendaStart);
        endDate=(TextView) findViewById(R.id.selectedAgendaEnd);
        hiddenStart=(TextView) findViewById(R.id.hiddenStart);
        hiddenEnd=(TextView)findViewById(R.id.hiddenEnd);
        btnFinalizeAgendaDetail=(Button) findViewById(R.id.finalizeAddAgenda);
        agendaPeriodText=(TextView) findViewById(R.id.agendaPeriodText);

        try{
            agendaStart= sdfin.parse(agendaStartS);
            agendaEnd=sdfin.parse(agendaEndS);
        }
        catch (Exception e){
            e.printStackTrace();

        }
        String outAgendaStart="",outAgendaEnd="";

        try {

            outAgendaStart=sdfout.format(agendaStart);
            outAgendaEnd=sdfout.format(agendaEnd);
        }
        catch(Exception e){
            e.printStackTrace();

        }

        agendaPeriodText.setText(agendaPeriodText.getText()+ outAgendaStart + " Until "+outAgendaEnd);
        cal1.setTime(agendaStart);
        cal2.setTime(agendaEnd);
        startDate=(TextView) findViewById(R.id.selectedAgendaStart);
        endDate=(TextView) findViewById(R.id.selectedAgendaEnd);
        hiddenStart=(TextView) findViewById(R.id.hiddenStart);
        hiddenEnd=(TextView)findViewById(R.id.hiddenEnd);
        btnFinalizeAgendaDetail=(Button) findViewById(R.id.finalizeAddAgenda);




        btnFinalizeAgendaDetail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar tempcall1=Calendar.getInstance(),tempcall2=Calendar.getInstance();
                if(!hiddenStart.getText().toString().equals("") && !hiddenEnd.getText().toString().equals("")){
                    Date tempcal1=null, tempcal2=null;


                    try {
                        tempcal1 = sdfin.parse(hiddenStart.getText().toString());
                        tempcal2=sdfin.parse(hiddenEnd.getText().toString());

                    }
                    catch (Exception e){
                        e.printStackTrace();

                    }


                    tempcall1.setTime(tempcal1);
                    tempcall2.setTime(tempcal2);
                }

                if(tempcall2.after(tempcall1)) {
                    AddAgendaDetails(AgendaAdapter.sAgendaID, idVS, hiddenStart.getText().toString(), hiddenEnd.getText().toString());
                }
                else{Toast.makeText(RegisterAgendaDetail.this, "Your End Date can't be earlier than your Start Date", Toast.LENGTH_SHORT).show();}

            }
        });


    }
    private void AddAgendaDetails(int agendaid ,String vacationid, String start, String end) {
        pDialog.show();
        Context context = getApplicationContext();
        String url = "http://agendatrip.com/api/v1/agenda/detail/create";
        Map<String, String> params = new HashMap<String, String>();

        Log.v("userID",Util.loggedID);
        Log.v("key", Util.loggedKey);
        Log.v("agenda_id", String.valueOf(agendaid));
        Log.v("vacation_site_id", vacationid);
        Log.v("start", start);
        Log.v("end", end);
        params.put("userID", Util.loggedID);
        params.put("key", Util.loggedKey);
        params.put("agenda_id", String.valueOf(agendaid));
        params.put("vacation_site_id", vacationid);
        params.put("start", start);
        params.put("end", end);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
            pDialog.hide();
                Log.v("dataresp", response.toString());


                Log.v("asd", response.toString());
                Toast.makeText(RegisterAgendaDetail.this, "Agenda Details created successfully", Toast.LENGTH_SHORT).show();
                Intent i= new Intent(RegisterAgendaDetail.this, MainActivity.class);
                startActivity(i);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (RegisterAgendaDetail.this);
                    adb.setMessage(getString(R.string.timeout_error));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(),"Session is over, please log in again",Toast.LENGTH_LONG).show();
                    Util.LogOut();
                    Util.forceLogInFragment(RegisterAgendaDetail.this);
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });
        jsObjRequest.setRetryPolicy(Constants.RETRY_POLICY);
        jsObjRequest.setTag("RegisterAgendaDEtails");
        requestQueue.add(jsObjRequest);
    }


    public void setStartDate(View v){

       /* RegAgendaDatePickerDialog agendaDatePickerDialog =new RegAgendaDatePickerDialog();
        agendaDatePickerDialog.show(getFragmentManager(), "tag");*/
        dpb = CalendarDatePickerDialogFragment
                .newInstance(RegisterAgendaDetail.this, cal1.get(Calendar
                                .YEAR),
                        cal1.get(Calendar.MONTH), cal1.get
                                (Calendar
                                        .DAY_OF_MONTH));
        MonthAdapter.CalendarDay c1 = new MonthAdapter.CalendarDay(cal1);
        MonthAdapter.CalendarDay c2 = new MonthAdapter.CalendarDay(cal2);
        dpb.setDateRange(c1, c2);
        dpb.show(getSupportFragmentManager(), "title");

    }

    public void setStartTime(View v){
        startFlag=true;
        RegAgendaTimePickerDialog agendaTimePickerDialog=new RegAgendaTimePickerDialog();
        agendaTimePickerDialog.show(getFragmentManager(), "tagtime");
        setStartDate(v);
    }

    public void setEndDate(View v){

        //RegAgendaDatePickerDialog agendaDatePickerDialog =new RegAgendaDatePickerDialog();
        //agendaDatePickerDialog.show(getFragmentManager(), "tag");
        dpb = CalendarDatePickerDialogFragment
                .newInstance(RegisterAgendaDetail.this, cal1.get(Calendar
                                .YEAR),
                        cal1.get(Calendar.MONTH),cal1.get
                                (Calendar
                                        .DAY_OF_MONTH));
        MonthAdapter.CalendarDay c1 = new MonthAdapter.CalendarDay(cal1);
        MonthAdapter.CalendarDay c2 = new MonthAdapter.CalendarDay(cal2);
        dpb.setDateRange(c1, c2);
        dpb.show(getSupportFragmentManager(), "title");

    }
    public void setEndTime(View v){
        startFlag=false;
        RegAgendaTimePickerDialog agendaTimePickerDialog=new RegAgendaTimePickerDialog();
        agendaTimePickerDialog.show(getFragmentManager(), "tagtime");
        setEndDate(v);
    }

    public void showCalendarDatePicker(TrainPassengerInformationHolder p){
        dpb = CalendarDatePickerDialogFragment
                .newInstance(RegisterAgendaDetail.this, Calendar.getInstance().get(Calendar
                                .YEAR),
                        Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get
                                (Calendar
                                        .DAY_OF_MONTH));
        MonthAdapter.CalendarDay c = new MonthAdapter.CalendarDay(Calendar.getInstance());
        dpb.setDateRange(c, null);

        dpb.show(getSupportFragmentManager(), "title");

    }

}
