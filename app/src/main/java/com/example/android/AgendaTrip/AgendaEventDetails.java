package com.example.android.AgendaTrip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import adapters.AgendaDetailsAdapter;
import data.DatabaseHandlerAgendaDetails;
import model.AgendaDetails;
import model.VacationSite;
import utils.Constants;
import utils.Util;

/**
 * Created by Vin on 10/25/2015.
 */
public class AgendaEventDetails extends AppCompatActivity {
    final Activity acti=this;
    String userid,name,startdate,enddate;
    String jsonResponse,tgl;
    int id;
    Activity act;
    ListView listView;
    ProgressDialog pDialog;
    AgendaDetailsAdapter agendaDetailsAdapter;
    ArrayList<AgendaDetails> agendaDetailsList;
    private DatabaseHandlerAgendaDetails dba;
    ArrayList<AgendaDetails> adlocal=new ArrayList<>();

    Intent showBookingsIntent;

    public void onCreate (Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

        setContentView(R.layout.agenda_event_details);
         Intent i = getIntent();
        Bundle bundle=i.getExtras();
        userid=bundle.getString("userid");
        id=bundle.getInt("id");
        name=bundle.getString("name");
        startdate=bundle.getString("startdate");
        enddate=bundle.getString("enddate");
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        showBookingsIntent = new Intent(AgendaEventDetails.this,AgendaBookingDetail.class);

        agendaDetailsRequest(id);


    }

    public void onResume(){
        super.onResume();
        Intent i = getIntent();
        Bundle bundle=i.getExtras();
        userid=bundle.getString("userid");
        id=bundle.getInt("id");
        name=bundle.getString("name");
        startdate=bundle.getString("startdate");
        enddate=bundle.getString("enddate");
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        Log.v("aidis", String.valueOf(id));

        agendaDetailsRequest(id);
        Log.v("aidis", String.valueOf(id));
        showpDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_add_agenda_detail,menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menu_id = item.getItemId();
        if(menu_id== android.R.id.home){
            finish();
        }
        else if(menu_id == R.id.menu_add_agenda_detail){
            addAgendaDetails();
        }
        else if( menu_id == R.id.menu_refresh_agenda_detail){
            agendaDetailsRequest(id);
        }
        else if( menu_id == R.id.menu_bookings_agenda_detail){
            showBookingsIntent.putExtra("agenda_id",id);
            Log.v("aidis",String.valueOf(id));
            startActivity(showBookingsIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void agendaDetailsRequest(final int id) {

        final Context context=getApplicationContext();

        String url="http://agendatrip.com/api/v1/agenda/show?userID="+ Util.loggedID+"&key="+ Util.loggedKey+"&agenda_id="+id;

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                //Log.v("dataresp", response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object


                    agendaDetailsList=new ArrayList<>();
                    dba=new DatabaseHandlerAgendaDetails(getApplicationContext());
                    dba.clearDB();
                    dba.close();

                    jsonResponse="";
                        JSONObject payload = new JSONObject(response.getString("payload"));
                        JSONArray agendaDetails=payload.getJSONArray("agenda_details");
                        for(int i=0;i<agendaDetails.length();i++) {
                        JSONObject satuanAgendaDetails= agendaDetails.getJSONObject(i);
                        JSONObject jsonvs= satuanAgendaDetails.getJSONObject("vacation_site");
                        String namavs=jsonvs.getString("name");
                        Log.v("namavs",namavs);

                        int agendaid = satuanAgendaDetails.getInt("id");
                        int vacationsiteid = satuanAgendaDetails.getInt("vacation_site_id");
                        String detailsStartTime = satuanAgendaDetails.getString("start");
                        String detailsEndTime = satuanAgendaDetails.getString("end");

                        AgendaDetails a = new AgendaDetails(satuanAgendaDetails);
                        VacationSite vs=new VacationSite(jsonvs);
                        a.setVacation_site_name(namavs);
                        a.setAgenda_id(agendaid);
                        a.setVacation_site_id(vacationsiteid);
                        a.setStartTime(detailsStartTime);
                        a.setEndTime(detailsEndTime);
                        a.setLat(vs.getLatitude());
                        a.setLng(vs.getLongitude());

                        //adlocal.add(a);

                            dba=new DatabaseHandlerAgendaDetails(AgendaEventDetails.this);
                            dba.addAgendaDetails(a,id);
                            dba.close();
                            Log.v("isi agenda id", i+" | "+id );

                    }


                       /* jsonResponse += "AgendaID: " + agendaid + "\n\n";
                        jsonResponse += "AgendaName: " + agendaname + "\n\n";
                        jsonResponse += "UserID: " + userid + "\n\n";
                        jsonResponse += "Start Time: " + startTime + "\n\n";
                        jsonResponse += "End Time: " + endTime + "\n\n\n";*/

                    // Activity act=(Activity) context;
                    tgl= DateFormat.getDateTimeInstance().format(new Date());
                    dba=new DatabaseHandlerAgendaDetails(getApplicationContext());
                    adlocal=dba.getAgendaDetails(id);

                    SharedPreferences sharedPreference=getApplicationContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreference.edit();
                    editor.putString("agendaDetailsDBSyncDate", tgl);
                    editor.apply();

                    listView=(ListView) findViewById(R.id.AgendaDetailsListView);

                   agendaDetailsAdapter=new AgendaDetailsAdapter(AgendaEventDetails.this,R.layout.agenda_details_adapter,adlocal,startdate,enddate);
                   listView.setAdapter(agendaDetailsAdapter);
                   jsonResponse+="payload";

                    agendaDetailsAdapter.notifyDataSetChanged();


                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Insert Right Time Format Please", Toast.LENGTH_SHORT)
                            .show();
                    e.printStackTrace();
                  /*  Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();*/
                }
hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                hidepDialog();

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder(acti);
                    adb.setMessage(getString(R.string.timeout_error_lastsync));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();

                    Log.v("refreshed data", "FALSE");
                   // lastUpdated.setText("Database Last Sync: "+tgl);
                    dba=new DatabaseHandlerAgendaDetails(getApplicationContext());
                    ArrayList<AgendaDetails> adlocal=dba.getAgendaDetails(id);
                    listView=(ListView) findViewById(R.id.AgendaDetailsListView);
                    agendaDetailsAdapter=new AgendaDetailsAdapter(AgendaEventDetails.this,R.layout.agenda_details_adapter,adlocal);
                    listView.setAdapter(agendaDetailsAdapter );
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            int myPosition = position;

                            String itemClickedId = listView.getItemAtPosition(myPosition).toString();

                        }

                    });

                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }

            }
        });
        hidepDialog();
        jsObjRequest.setRetryPolicy(Constants.RETRY_POLICY);
        requestQueue.add(jsObjRequest);

    }

    public void addAgendaDetails(){
    Intent i=new Intent(this, SearchVSforAgendaDetail.class);
    i.putExtra("agenda_id",id);
    i.putExtra("keyword","");
    i.putExtra("agendaStart",startdate);
    i.putExtra("agendaEnd",enddate);
    startActivity(i);


}

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
