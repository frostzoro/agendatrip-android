package com.example.android.AgendaTrip;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import model.HotelSearch;
import utils.Util;

public class HotelCheckout extends AppCompatActivity {
    TextView checkoutName,hotelName,roomName,priceDetail,totalPrice;
    Button continueButton;
    HotelSearch hs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_checkout);
        receiveDataFromIntent();
        bindViewToVariables();
        setDataToView();
        setListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.hotel_booking);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void receiveDataFromIntent(){
        hs = (HotelSearch) getIntent().getSerializableExtra("hotelsearch");
    }

    protected void bindViewToVariables(){
        checkoutName = (TextView) findViewById(R.id.hotelCheckoutOrderName);
        hotelName = (TextView) findViewById(R.id.hotelCheckoutHotelName);
        roomName = (TextView) findViewById(R.id.hotelCheckoutRoomName);
        priceDetail = (TextView) findViewById(R.id.hotelCheckoutPriceDetail);
        totalPrice = (TextView) findViewById(R.id.hotelCheckoutTotalPrice);
        continueButton = (Button) findViewById(R.id.hotelCheckoutContinueButton);
    }

    protected void setDataToView(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        String orderString = sdf.format(hs.getCheckIn().getTime()) + " - " + sdf.format(hs.getCheckOut().getTime());
        checkoutName.setText(orderString);
        hotelName.setText(hs.getH().breadCrumb.getName());
        roomName.setText(hs.getR().getName());
        long daysdiff = (hs.getCheckOut().getTimeInMillis() - hs.getCheckIn().getTimeInMillis()) /
                1000/24/60/60;
        String priceString = hs.getGuest() + " room(s) x " + daysdiff + " night(s) x " + Util
                .convertDoubleToIDRString(hs.getR().getPrice());
        priceDetail.setText(priceString);
        totalPrice.setText(Util.convertDoubleToIDRString(daysdiff * hs.getR().getPrice()));
    }

    protected void setListener(){
        continueButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HotelCheckout.this,HotelGuestInformation.class);
                Bundle b = new Bundle();
                b.putSerializable("hotelsearch",hs);
                i.putExtras(b);
                startActivity(i);
            }
        });
    }
}
