package com.example.android.AgendaTrip;

import android.content.Intent;
import android.graphics.LinearGradient;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gc.materialdesign.views.Button;

import java.text.NumberFormat;
import java.util.Locale;

import model.FlightOrder;
import model.FlightSearch;

public class FlightCheckout extends AppCompatActivity {
    LinearLayout departureLayout,returnLayout;
    FlightOrder departureOrder,returnOrder;
    TextView totalText;
    Double totalPrice;
    Button continueButton;
    FlightSearch fs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_checkout);
        totalText = (TextView) findViewById(R.id.flightCheckoutTotal);

        departureLayout = (LinearLayout) findViewById(R.id.flightCheckoutDepart);
        returnLayout = (LinearLayout) findViewById(R.id.flightCheckoutReturn);
        fs = (FlightSearch) getIntent().getSerializableExtra("flightsearch");

        departureOrder = new FlightOrder(fs.departure_flight,fs,"Departure",this);
        departureLayout.addView(departureOrder.getView());
        if(fs.return_flight!=null){
            returnOrder = new FlightOrder(fs.return_flight,fs,"Return",this);
            returnLayout.addView(returnOrder.getView());
        }

        totalPrice = departureOrder.getSubTotal();
        if(returnOrder !=null) totalPrice += returnOrder.getSubTotal();
        totalText.setText(NumberFormat.getCurrencyInstance(new
                Locale("EN", "ID")).format(totalPrice));

        continueButton = (Button) findViewById(R.id.flightCheckoutContinueButton);
        continueButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),FlightPassengerInformation.class);
                Bundle b= new Bundle();
                b.putSerializable("flightsearch",fs);
                i.putExtras(b);
                startActivity(i);
            }
        });
    }

}
