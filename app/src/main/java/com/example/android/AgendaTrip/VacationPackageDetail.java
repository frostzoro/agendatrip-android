package com.example.android.AgendaTrip;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;

import model.VacationPackage;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class VacationPackageDetail extends AppCompatActivity {
    TextView header,includes,excludes,itinerary,name,price,dates,notes;
    ImageView image;
    String vpid,vpname;
    VacationPackage vp;
    RelativeLayout loading;
    Button buyButton;
    NotificationManager nm ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacation_package_detail);

        vpname=getIntent().getStringExtra("vacation_package_name");
        vpid=getIntent().getStringExtra("vacation_package");
        Log.v("OIII", vpname+" "+vpid);
        setView();
        getData();
        nm = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE) ;
        nm.cancel(-1);
    }

    @Override
      public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(vpname);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void updatePreferences(){
        if(Util.isLoggedIn()){
            String url = Util.BASE_URL + Util.API_GATEWAY + Util.TAG_URL + Util.VACATION_PACKAGE_RESPONSE_URL;
            HashMap<String,String> param = new HashMap<>();
            param.putAll(Util.getUserParam());
            param.put("vacation_package_id",String.valueOf(vp.getId()));
            param.put("response",1+"");
            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, new JSONObject
                    (param), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.v("updatepackpref","success!");
                }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("updatepackpref","failure!");
                    error.printStackTrace();
                }
            });

            AppController.getInstance().addToRequestQueue(jor);

        }

    }

    void setView(){
        header = (TextView) findViewById(R.id.vacationPackageHeaderText);
        includes = (TextView) findViewById(R.id.vacationPackageDetailIncludesText);
        excludes = (TextView) findViewById(R.id.vacationPackageDetailExcludesText);
        itinerary = (TextView) findViewById(R.id.vacationPackageDetailItineraryText);
        name = (TextView) findViewById(R.id.vacationPackageName);
        price = (TextView) findViewById(R.id.vacationPackagePrice);
        dates = (TextView) findViewById(R.id.vacationPackageDateText);
        notes = (TextView) findViewById(R.id.vacationPackageDetailNoteText);
        image = (ImageView) findViewById(R.id.vacationPackageDetailImageHeader);
        loading = (RelativeLayout) findViewById(R.id.vacationPackageDetailLoading);
        buyButton = (Button) findViewById(R.id.vacationPackageDetailBuyButton);
        if(Util.isLoggedIn() == false) buyButton.setVisibility(View.GONE);
        else buyButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), VacationPackagePurchaseConfirmation.class);
                Bundle b = new Bundle();
                b.putSerializable("vacation_package",vp);
                i.putExtras(b);
                startActivityForResult(i,1);
            }
        });
    }

    void setDataToView(){
        header.setText(vp.getHeader());
        includes.setText(vp.getFacilityInclusion());
        excludes.setText(vp.getFacilityExclusion());
        itinerary.setText(vp.getItinerary().replace("\\n","\n"));
        name.setText(vp.getName());
        price.setText("IDR " + String.valueOf(vp.getPrice()));
        dates.setText(vp.getCompleteDateAsString());
        notes.setText(vp.getNotes());
        try{
            Picasso.with(getApplicationContext()).load(Util.BASE_URL + Util.VACATION_PACKAGE_IMAGE_URL + vp
                    .getImage())
                    .placeholder(R.drawable.placeholder_vacation_package).into(this.image);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    void getData(){
        String requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util.VACATION_PACKAGE_PATH +
                "/show";
        requestUrl += "?vacation_package_id="+vpid;
        JsonObjectRequest vpRequest = new JsonObjectRequest(Request.Method.POST, requestUrl,
                (JSONObject) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loading.setVisibility(View.GONE);
                try {
                    JSONObject vacation_package = response.getJSONObject("payload");
                    vp = new VacationPackage(vacation_package);
                    Log.v("vacationpackageid",String.valueOf(vp.getId()));
                    setDataToView();
                    updatePreferences();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (VacationPackageDetail.this);
                    adb.setMessage(getString(R.string.timeout_error));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), "Auth Error", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"Server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });
        vpRequest.setRetryPolicy(Constants.RETRY_POLICY);
        vpRequest.setTag("VacationPackageDetail");
        AppController.getInstance().addToRequestQueue(vpRequest);

    }

}
