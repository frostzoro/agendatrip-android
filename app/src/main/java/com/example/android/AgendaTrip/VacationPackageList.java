package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import adapters.VacationPackageListAdapter;
import model.VacationPackage;
import pl.droidsonroids.gif.GifImageView;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class VacationPackageList extends AppCompatActivity {
    ListView listView;
    TextView caption;
    RelativeLayout back,refresh;
    GifImageView loading;
    Boolean idle;
    ArrayList<VacationPackage> data;
    VacationPackageListAdapter vpls;
    Button sort,filter;
    SearchManager sM;
    SearchView sV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacation_package);
        idle=true;
        setView();
        handleIntent(getIntent());
        getVacationPackages(0, "");
    }

    protected void setView(){
        listView = (ListView) findViewById(R.id.vacationPackageListView);
        loading = (GifImageView) findViewById(R.id.vacationPackageLoading);
        data = new ArrayList<>();
        vpls = new VacationPackageListAdapter(VacationPackageList.this,R.layout
                .vacation_package_partial,data);
        sort = (Button) findViewById(R.id.vacationPackageListSort);
        filter = (Button) findViewById(R.id.vacationPackageListFilter);
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int[] selection = new int[1];
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(VacationPackageList.this);
                alertDialog.setTitle(R.string.sort_package_text).setSingleChoiceItems(R.array
                        .sort_package_array, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v("which is ",String.valueOf(which));
                        selection[0] = which;
                    }
                }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v("which is ",String.valueOf(selection[0]));
                        switch(selection[0]) {
                            case 0: vpls.sortItem(VacationPackageListAdapter.SORT_BY_PREFERENCE);
                                break;
                            case 1:
                                vpls.sortItem(VacationPackageListAdapter.SORT_BY_PRICE);
                                break;
                            case 2: vpls.sortItem(VacationPackageListAdapter.SORT_BY_PURCHASE);
                                break;
                            default : Toast.makeText(getApplicationContext(),"youre fukad m8",Toast.LENGTH_LONG);
                        }
                    }
                });
                AlertDialog d = alertDialog.create();
                d.show();
                Log.v("flightsearchresultsort","clicked");
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        listView.setAdapter(vpls);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        sM = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        sV = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        sV.setSearchableInfo(sM.getSearchableInfo(getComponentName()));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.v("query is",query);
            if(query.length()>0){
                getVacationPackages(1,query);
            }
            else{
                getVacationPackages(0,query);
            }
        }
    }

    public void getVacationPackages(int type,String keyword){
        if(idle){
            idle = false;
            data.clear();
            loading.setVisibility(View.VISIBLE);
            String requestUrl;
            HashMap<String,String> param = new HashMap<>();
            if(type==1){
                requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util
                        .VACATION_PACKAGE_PATH+"/search"+"?keyword="+keyword;
            }
            else{
                requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util
                        .VACATION_PACKAGE_PATH+"/hot";
            }

            if(Util.isLoggedIn())
                param.putAll(Util.getUserParam());
            Log.v("vacationpackagel1ist", requestUrl);
            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, requestUrl,
                    new JSONObject(param), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.setVisibility(View.GONE);
                    try {
                        JSONArray datas = response.getJSONArray("payload");
                        int rsize = datas.length();
                        Log.v("rsize",datas.toString());
                        for(int i=0;i<rsize;i++){
                            JSONObject jsonObj = datas.getJSONObject(i);
                            VacationPackage vp = new VacationPackage(jsonObj);
                            data.add(vp);
                            Log.v("hypis",String.valueOf(vp.getHypotheses()));
                        }
                        vpls.notifyDataSetChanged();
                        idle=true;
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.setVisibility(View.GONE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                        android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                                (VacationPackageList.this);
                        adb.setMessage(getString(R.string.timeout_error));
                        adb.setTitle(getString(R.string.error));
                        adb.setPositiveButton(R.string.ok, null);
                        adb.create().show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), "Auth Error", Toast.LENGTH_LONG).show();
                        //TODO
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(),"Server Error",Toast.LENGTH_LONG).show();
                        //TODO
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                        //TODO
                    }
                }
            });
            jor.setRetryPolicy(Constants.RETRY_POLICY);
            jor.setTag("VacationPackageList");
            AppController.getInstance().addToRequestQueue(jor);
        }
    }
}
