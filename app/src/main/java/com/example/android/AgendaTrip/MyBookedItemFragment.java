package com.example.android.AgendaTrip;


import android.app.Fragment;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.BookedItemAdapter;
import model.BookedItem;
import singleton.AppController;
import utils.Util;

/**
 * Created by Vin on 10/3/2015.
 */
public class MyBookedItemFragment extends Fragment
{
    ListView lv;
    BookedItemAdapter bad;
    ArrayList<BookedItem> abi;
    ProgressBar loading;
    View rootview;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootview=inflater.inflate(R.layout.my_booked, container, false);

        abi = new ArrayList<BookedItem>();
        bad = new BookedItemAdapter(getActivity(),R.layout
                .booked_item_partial,abi);
        if(!Util.isLoggedIn()){
            Util.forceLogInFragment(getActivity());
        }
        lv = (ListView) rootview.findViewById(R.id.myBookedListView);
        lv.setAdapter(bad);
        loading = (ProgressBar) rootview.findViewById(R.id.myBookedLoading);
        getData();
        return rootview;
    }



    protected void getData(){
        String url = Util.BASE_URL + Util.API_GATEWAY + "/bookings/showOrders?";
        url+= Util.paramToString(Util.getUserParam());

        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loading.setVisibility(View.GONE);
                        JSONArray payload = null;
                        try {
                            payload = response.getJSONArray("payload");
                            int sz = payload.length();
                            for(int i=0;i<sz;i++){
                                abi.add(new BookedItem(payload.getJSONObject(i)));
                            }
                            bad.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.setVisibility(View.GONE);
                        if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                            android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                                    (getActivity());
                            adb.setMessage(getString(R.string.timeout_error_polite));
                            adb.setTitle(getString(R.string.error));
                            adb.setPositiveButton(R.string.ok, null);
                            adb.create().show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getActivity().getApplicationContext(), "Auth Error",
                                    Toast
                                    .LENGTH_LONG).show();
                            Util.LogOut();
                            Util.forceLogInFragment(getActivity());
                            //TODO
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getActivity().getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ParseError) {
                            Toast.makeText(getActivity().getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                            //TODO
                        }
                    }
                });
        jor.setTag("MyBookedItemFragment");
        AppController.getInstance().addToRequestQueue(jor);
    }


}
