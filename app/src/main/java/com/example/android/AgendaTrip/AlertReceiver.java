package com.example.android.AgendaTrip;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.DebugUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import singleton.AppController;
import utils.Receivers.VacationPackageRejectReceiver;
import utils.Util;

/**
 * Created by Vin on 1/7/2016.
 */
public class AlertReceiver extends BroadcastReceiver {
    String token,id,key;
    int intentNotifID;
    Context dummyCtx;
    Intent classIntent;
    @Override
    public void onReceive(Context context,Intent intent){
        Log.v("receive nih", "RECEIVED");

        IntentFilter filter=new IntentFilter();
        filter.addAction("YES");
        filter.addAction("NO");
        String action=intent.getAction();
        if("YES".equals(action)){Log.v("baiklah","baiklahyes");}
        else{Log.v("baiklah","baiklahno");}

        classIntent=intent;
         dummyCtx=context;
        id=intent.getStringExtra("loggedID");
        key=intent.getStringExtra("loggedKey");

        if(intent.getIntExtra("notifnumber",-1)==0){
        getDeals();}

        else if(intent.getIntExtra("notifnumber",-1)==1){
    intentNotifID=intent.getIntExtra("notifid",1);
    createNotificationAgenda(dummyCtx, "Agenda Reminder!", "Your Agenda " + intent.getStringExtra("agendaname") + " is starting!", "Agenda is starting!", intentNotifID);

}

    }

    public void createNotificationAgenda(Context context, String msg, String msgText, String msgAlert, int notifId){

      Intent passedIntent=new Intent(context, MainActivity.class);

        PendingIntent pInt= PendingIntent.getActivity(context,notifId,passedIntent,0);

        NotificationCompat.Builder mBuilder=new NotificationCompat.Builder(context)
                .setContentTitle(msg)
                .setContentText(msgText)
                .setTicker(msgAlert)
                .setSmallIcon(R.drawable.agenda_logo);

        mBuilder.setContentIntent(pInt);
        mBuilder.setDefaults(NotificationCompat.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(intentNotifID, mBuilder.build()); //untuk agenda id berdasarkan agenda id, sehingga engga nimpa

    }

    public static class NotificationActionService extends IntentService {
        public NotificationActionService() {
            super(NotificationActionService.class.getSimpleName());
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            String action = intent.getAction();
            Log.v("action",action);
            if ("NO".equals(action)) {
                // TODO: handle action 1.
                // If you want to cancel the notification: NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);
                Log.v("apalah","apalah");
                Toast.makeText(NotificationActionService.this, "ok lah", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void createNotificationforDeals(Context context, String msg, String msgText, String msgAlert, int notifId, String vpname, int vpid){


        Intent Interested=new Intent(context, VacationPackageDetail.class);
        Interested.setAction("YES");
        Interested.addFlags(Notification.FLAG_AUTO_CANCEL);

        Intent notInterested=new Intent(context, VacationPackageRejectReceiver.class);
        notInterested.setAction("NO");


        Bundle mBundle = new Bundle();
        mBundle.putString("vacation_package", String.valueOf(vpid));
        mBundle.putString("vacation_package_name", String.valueOf(vpname));
        Interested.putExtras(mBundle);
        notInterested.putExtras(mBundle);



        PendingIntent pInt = PendingIntent.getActivity(context, notifId, Interested, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent pIntNo= PendingIntent.getBroadcast(context, notifId, notInterested, PendingIntent.FLAG_CANCEL_CURRENT);


        NotificationCompat.Builder mBuilder=new NotificationCompat.Builder(context)
                .setContentTitle(msg)
                .setContentText(msgText)
                .setTicker(msgAlert)
                .setSmallIcon(R.drawable.agenda_logo);

      //  mBuilder.setContentIntent(pInt);
        mBuilder.setContentIntent(PendingIntent.getActivity(context,0,new Intent(),PendingIntent.FLAG_UPDATE_CURRENT));
        mBuilder.addAction(0, "Let's see it!", pInt);
        mBuilder.addAction(0, "Not Interested", pIntNo);
        mBuilder.setDefaults(NotificationCompat.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);

        Notification nt = mBuilder.build();
        nt.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(-1, nt); //untuk daily deals id selalu -1

    }

    protected void getDeals(){
        String url = "http://www.agendatrip.com/api/v1/deals/recommended";
        Map<String, String> params = new HashMap<String, String>();
        params.put("userID", id);
        params.put("key", key);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray payload = response.getJSONArray("payload");
                    JSONObject takenDeal=payload.getJSONObject(0);

                    createNotificationforDeals(dummyCtx,
                            takenDeal.getString("title"),
                            takenDeal.getString("header"),
                            "Deal of the day for you!!",-1,takenDeal.getString("title"),takenDeal.getInt("id"));


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
/*
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (dummyCtx);
                    adb.setMessage(dummyCtx.getString(R.string.timeout_error));
                    adb.setTitle(dummyCtx.getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(dummyCtx, "auth error", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(dummyCtx,"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(dummyCtx,"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
                */
            }
        });

        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }


}
