package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import adapters.UserIdentityAdapter;
import model.FlightSearch;
import helper.PassengerInformationHolder;
import model.UserIdentity;
import singleton.AppController;
import utils.Util;

public class FlightPassengerInformation extends AppCompatActivity implements
    CalendarDatePickerDialogFragment.OnDateSetListener {
    AlertDialog dialog;
    ArrayList<PassengerInformationHolder> apih;
    ArrayList<String> nationalityList,nationalityCode,titleAdult,titleChild;
    ArrayList<UserIdentity> userIdentityArrayList;
    ArrayList<String> identityList;
    ArrayAdapter<String> titleAdapter, identityAdapter;
    FlightSearch fs;
    Intent paymentInfoIntent;
    LinearLayout detailField,passengerForm;
    RelativeLayout loading;
    Spinner contactTitleSpinner,contactIdentitySpinner;
    EditText contactFirstName,contactLastName,contactPhoneNumber,contactEmail;
    HashMap<String,Boolean> params;
    Button continueButton;
    CalendarDatePickerDialogFragment dpb;
    PassengerInformationHolder currentDateSelectionObject;
    Boolean doneCountry,doneIdentity,doneFlight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_passenger_information);
        fs  = (FlightSearch) getIntent().getSerializableExtra("flightsearch");
        initializeSupportVariables();
        buildAlertDialog();
        detailField = (LinearLayout) findViewById(R.id.flightPassengerInformationDetailLayout);
        passengerForm = (LinearLayout) findViewById(R.id.flightPassengerForm);
        passengerForm.setVisibility(View.INVISIBLE);
        params.put("round_trip",fs.getRound_trip());
        titleAdult = new ArrayList<>(Arrays.asList("Mr","Mrs","Ms"));
        titleChild = new ArrayList<>(Arrays.asList("Mstr","Miss"));
        continueButton = (Button) findViewById(R.id.passengerInformationContinueButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order();
            }
        });
        loading = (RelativeLayout) findViewById(R.id.flightPassengerInformationLoading);
        getCountries();
        getFlightData();
        if(Util.isLoggedIn())
            getUserIdentity();
        else {
            doneIdentity = true;
            userIdentityArrayList = new ArrayList<>();
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.flight_booking);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        Calendar temp = Calendar.getInstance();
        temp.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        temp.set(Calendar.MONTH,monthOfYear);
        temp.set(Calendar.YEAR, year);
        currentDateSelectionObject.setBirthDate(temp.getTime());
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setPositiveButton(getString(R.string.ok), null);
        dialog = adb.create();
    }

    protected void initializeSupportVariables(){
        apih = new ArrayList<PassengerInformationHolder>();
        // Params is used to send custom required infos from the passenger.
        params = new HashMap<>();
        paymentInfoIntent = new Intent(getApplicationContext(),PaymentInformationBank.class);
        doneCountry=false;
        doneIdentity=false;
        doneFlight=false;
    }

    @Override
    public void onDestroy(){
        //releases all requests that were made in this activity
        AppController.getInstance().cancelPendingRequests("FlightPassengerInformation");
        super.onDestroy();
    }

    protected void setForms(){
        if(doneCountry && doneFlight && doneIdentity) {
            passengerForm.setVisibility(View.VISIBLE);
            loading.setVisibility(View.INVISIBLE);
            contactIdentitySpinner = (Spinner) findViewById(R.id.flightPassengerInformationIdentity);
            contactTitleSpinner = (Spinner) findViewById(R.id.flightPassengerInformationTitle);
            contactFirstName = (EditText) findViewById(R.id
                    .flightPassengerInformationContactFirstName);
            contactLastName = (EditText) findViewById(R.id
                    .flightPassengerInformationContactLastName);
            contactPhoneNumber = (EditText) findViewById(R.id
                    .flightPassengerInformationContactPhoneNumber);
            contactEmail = (EditText) findViewById(R.id.flightPassengerInformationContactEmailAddress);

            int sz = userIdentityArrayList.size();
            identityList = new ArrayList<>();
            for(int i=0;i<sz;i++){
                identityList.add(userIdentityArrayList.get(i).getFirst_name() + " " + userIdentityArrayList.get(i).getLast_name());
            }
            identityAdapter = new ArrayAdapter<String>(FlightPassengerInformation.this,R.layout.support_simple_spinner_dropdown_item,identityList);
            contactIdentitySpinner.setAdapter(identityAdapter);
            contactIdentitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    UserIdentity selectedUI = userIdentityArrayList.get(position);
                    contactFirstName.setText(selectedUI.getFirst_name());
                    contactLastName.setText(selectedUI.getLast_name());
                    contactEmail.setText(selectedUI.getEmail().equals("null") ? "" : selectedUI.getEmail());
                    contactPhoneNumber.setText(selectedUI.getPhone_number());
                    contactTitleSpinner.setSelection(titleAdult.indexOf(selectedUI.getTitle()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            titleAdapter = new ArrayAdapter<String>(FlightPassengerInformation.this, R.layout
                    .support_simple_spinner_dropdown_item, titleAdult);
            contactTitleSpinner.setAdapter(titleAdapter);
            for (int i = 0; i < fs.getAdult(); i++) {
                PassengerInformationHolder pih = new PassengerInformationHolder(params, FlightPassengerInformation.this, getApplicationContext(), "Adult", i + 1,
                        nationalityList,
                        nationalityCode,userIdentityArrayList);
                apih.add(pih);
                detailField.addView(pih.getView());
                Log.v("flightpasi", "added Adlt" + i);
            }
            for (int i = 0; i < fs.getChild(); i++) {
                PassengerInformationHolder pih = new PassengerInformationHolder(params,
                        FlightPassengerInformation.this, getApplicationContext(), "Child", i + 1,
                        nationalityList, nationalityCode,userIdentityArrayList);
                apih.add(pih);
                detailField.addView(pih.getView());
                Log.v("flightpasi", "added Child" + i);
            }
            for (int i = 0; i < fs.getInfant(); i++) {
                PassengerInformationHolder pih = new PassengerInformationHolder(params,
                        FlightPassengerInformation.this, getApplicationContext(), "Infant", i + 1,
                        nationalityList, nationalityCode,userIdentityArrayList);
                apih.add(pih);
                detailField.addView(pih.getView());
                Log.v("flightpasi", "added Infant" + i);
            }
        }
    }

    protected void getCountries(){
        String url = "http://www.agendatrip.com/api/v1/bookings/getCountries?";
        HashMap<String,String> param = new HashMap<>();
        param.put("token", fs.getToken());
        param.putAll(Util.getUserParam());
        url += Util.paramToString(param);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray listCountries = null;
                        nationalityCode = new ArrayList<>();
                        nationalityList = new ArrayList<>();
                        try {
                            listCountries = response.getJSONArray("listCountry");
                            int sz= listCountries.length();
                            for(int i=0;i<sz;i++){
                                JSONObject country = listCountries.getJSONObject(i);
                                nationalityList.add(country.getString("country_name"));
                                nationalityCode.add(country.getString("country_id"));
                            }
                            doneCountry = true;
                            setForms();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        jor.setTag("FlightPassengerInformation");
        jor.setRetryPolicy(new DefaultRetryPolicy(
                305000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected void getUserIdentity(){
        userIdentityArrayList = new ArrayList<>();
        String url = Util.BASE_URL + Util.API_GATEWAY + Util.USER_IDENTITY_PATH;
        url += "?" + Util.paramToString(Util.getUserParam());
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray payload = null;
                        try {
                            payload = response.getJSONArray("payload");
                            int sz = payload.length();
                            for(int i=0;i<sz;i++){
                                userIdentityArrayList.add(new UserIdentity(payload.getJSONObject(i)));
                            }
                            doneIdentity=true;
                            setForms();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        jor.setTag("FlightPassengerInformation");
        jor.setRetryPolicy(new DefaultRetryPolicy(
                305000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jor);
    }

    public void showCalendarDatePicker(PassengerInformationHolder p){
        dpb = CalendarDatePickerDialogFragment
                .newInstance(FlightPassengerInformation.this, Calendar.getInstance().get(Calendar
                                .YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get
                                (Calendar
                                        .DAY_OF_MONTH));
        dpb.show(getSupportFragmentManager(), "title");
        currentDateSelectionObject = p;
    }

    protected void getFlightData(){
        String url = "http://www.agendatrip.com/api/v1/bookings/getFlightData?"+"flight_id="+fs
                .departure_flight.getFlight_id()+"&date="+fs.departure_flight.getDate()
                +"&token="+fs.getToken();
        if(fs.getRound_trip()){
            url+="&ret_flight_id="+fs.return_flight.getFlight_id()+"&ret_date="+fs.return_flight
                    .getDate();
        }
        Log.v("urlflightdata",url);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //checks what fields are required
                try {
                    JSONObject required_params = response.getJSONObject("required");
                    if(required_params.has("passportnationalitya1"))
                    {
                        params.put("Adult_nationality",true);
                    }

                    if(required_params.has("birthdatea1")){
                        params.put("Adult_birthdate",true);
                    }

                    if(required_params.has("dcheckinbaggagea11")){
                        params.put("Adult_baggage_d",true);
                    }

                    if(required_params.has("rcheckinbaggagea11")){
                        params.put("Adult_baggage_r",true);
                    }

                    if(required_params.has("passportnationalityc1")){
                        params.put("Child_nationality",true);
                    }

                    if(required_params.has("birthdatec1")){
                        params.put("Child_birthdate",true);
                    }

                    if(required_params.has("dcheckinbaggagec11")){
                        params.put("Child_baggage_d",true);
                    }

                    if(required_params.has("rcheckinbaggagec11")){
                        params.put("Child_baggage_r",true);
                    }

                    if(required_params.has("birthdatei1")){
                        params.put("Infant_birthdate",true);
                    }

                    if(required_params.has("passportnationalityi1")){
                        params.put("Infant_nationality",true);
                    }

                    if(required_params.has("ida1")){
                        params.put("Adult_id",true);
                    }
                    doneFlight = true;
                    setForms();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                        (FlightPassengerInformation.this);
                    adb.setMessage(getString(R.string.timeout_error_polite));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), "Auth Error", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG)
                            .show();
                    //TODO
                }
            }
        });
        jor.setTag("FlightPassengerInformation");
        jor.setRetryPolicy(new DefaultRetryPolicy(305000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected Boolean checkContactInformation(){
        Boolean can = true;

        if(contactFirstName.getText().length() <=0){
            can = false;
            contactFirstName.setBackground(getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("conPIH","confirstname");
        }

        if(contactLastName.getText().length() <=0){
            can = false;
            contactLastName.setBackground(getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("conPIH", "conlastname");
        }

        if(contactTitleSpinner.getSelectedItemPosition() <0){
            can = false;
            contactTitleSpinner.setBackground(getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("conPIH", "contitle");
        }

        if(contactEmail.getText().length() <=0){
            can = false;
            contactEmail.setBackground(getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("conPIH", "conemail");
        }

        if(contactPhoneNumber.getText().length() <=0){
            can = false;
            contactPhoneNumber.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("conPIH", "conphonnum");
        }

        return can;
    }

    protected void order(){
        Boolean valid = true;
        Boolean contactValid = checkContactInformation();
        Boolean emailValid = true;
        String error = "";
        String identityParam = "";
        String contactParam= "";
        String flightParam = "";
        //Checks contact person information

        //Checks all passenger information to be filled


        for(PassengerInformationHolder pih : apih){
            Log.v("checking x","test");
            valid = valid && pih.checkData();
            if(!valid)
                error = pih.getErrorMsg();
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(contactEmail.getEditableText()).matches()){
            emailValid=false;
            error = getString(R.string.email_address_format_error);
            Log.v("fpih","emailformatinvalid");
        }

        if(contactValid && valid) {
            Boolean first = true;
            for (PassengerInformationHolder pih : apih) {
                if (!first) identityParam += "&";
                identityParam += pih.attributesToParam();
                first = false;
            }
            contactParam+="&conSalutation=" + contactTitleSpinner.getSelectedItem();
            contactParam+="&conFirstName=" + contactFirstName.getText();
            contactParam+="&conLastName=" + contactLastName.getText();
            contactParam+="&conPhone=" +  contactPhoneNumber.getText();
            contactParam+="&conEmailAddress=" + contactEmail.getText();

             Log.v("completeparam", identityParam);
        }
        else{
            valid = false;
            if(error.length()==0)
            error = getString(R.string.form_not_complete_error);
        }

        flightParam += "&flight_id=" + fs.departure_flight.getFlight_id();
        flightParam += "&date=" + fs.departure_flight.getDate();

        if(fs.getRound_trip()){
            flightParam += "&ret_flight_id=" + fs.return_flight.getFlight_id();
            flightParam += "&ret_date=" + fs.return_flight.getDate();
        }

        // Extra checks for passenger information :
        // Contact person email address format

        if(valid && contactValid && emailValid){
            loading.setVisibility(View.VISIBLE);
            String url = "http://www.agendatrip.com/api/v1/bookings/addOrder?";
            url += identityParam;
            url += flightParam;
            url += contactParam;
            url += "&token=" + fs.getToken();
            if(Util.isLoggedIn()){
                url+="&" + Util.paramToString(Util.getUserParam());
            }
            if(fs.getAgenda_id()!=-1){
                url+= "&agenda_id=" + String.valueOf(fs.getAgenda_id());
            }
            Log.v("urlOrder", url);

            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject)
                    null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.setVisibility(View.INVISIBLE);
                    try {
                        JSONObject resp = response.getJSONObject("diagnostic");
                        if (resp.has("confirm")) {
                            Bundle b = new Bundle();
                            b.putString("payment_information", response.toString());
                            b.putSerializable("flightsearch", fs);
                            paymentInfoIntent.putExtras(b);
                            paymentInfoIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(paymentInfoIntent);
                        }
                        else {
                            dialog.setMessage(resp.getString("error_msgs"));
                            dialog.show();
                        }
                    }
                    catch (JSONException e) {
                        loading.setVisibility(View.INVISIBLE);
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.setVisibility(View.GONE);
                }
            });
            jor.setRetryPolicy(new DefaultRetryPolicy(
                    305000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            jor.setTag("FlightPassengerInformation");
            AppController.getInstance().

                addToRequestQueue(jor);
        }
        else{
            dialog.setMessage(getString(R.string.form_not_complete_error));
            dialog.show();
        }
    }

}
