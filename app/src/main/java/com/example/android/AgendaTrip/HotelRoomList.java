package com.example.android.AgendaTrip;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import adapters.RoomAdapter;
import model.Hotel;
import model.HotelSearch;

public class HotelRoomList extends AppCompatActivity {
    TextView hotelName,hotelAddress,bookingInfo;
    ListView roomListView;
    RoomAdapter roomAdapter;
    Hotel h;
    HotelSearch hs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_room_list);
        receiveDataFromIntent();
        bindViewToVariables();
        initializeSupportVariables();
        setDataToView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.hotel_booking);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void receiveDataFromIntent(){
        h = (Hotel) getIntent().getSerializableExtra("hotel");
        hs = (HotelSearch) getIntent().getSerializableExtra("hotelsearch");
        hs.setH(h);
    }

    protected void bindViewToVariables(){
        hotelName = (TextView) findViewById(R.id.hotelRoomListHotelName);
        hotelAddress = (TextView) findViewById(R.id.hotelRoomListHotelAddress);
        bookingInfo = (TextView) findViewById(R.id.hotelRoomListBookingInfo);
        roomListView = (ListView) findViewById(R.id.hotelRoomListListView);
    }

    protected void setDataToView(){
        hotelName.setText(h.breadCrumb.getName());
        hotelAddress.setText(h.getAddress());
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String bookingDate = sdf.format(hs.getCheckIn().getTime()) + " - " + sdf.format(hs.getCheckOut().getTime());
        bookingInfo.setText(bookingDate);
    }

    protected void initializeSupportVariables(){
        roomAdapter = new RoomAdapter(this,R.layout.partial_room,h.getAvailableRooms());
        roomListView.setAdapter(roomAdapter);
    }

    public void showRoomData(Hotel.Room r){
        Intent i = new Intent(this,RoomDetail.class);
        Bundle b = new Bundle();
        hs.setR(r);
        b.putSerializable("hotelsearch",hs);
        b.putSerializable("hotel",h);
        i.putExtras(b);
        startActivity(i);
    }

    public void goToCheckOut(Hotel.Room r){
        Intent i = new Intent(this,HotelCheckout.class);
        Bundle b = new Bundle();
        hs.setR(r);
        b.putSerializable("hotelsearch",hs);
        b.putSerializable("hotel",h);
        i.putExtras(b);
        startActivity(i);
    }
}
