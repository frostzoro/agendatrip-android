package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import adapters.FlightAdapter;
import model.Flight;
import model.FlightSearch;
import pl.droidsonroids.gif.GifImageView;
import singleton.AppController;
import utils.Util;

public class FlightSearchResult extends AppCompatActivity {
    AlertDialog airlinesFilterDialog;
    AlertDialog alert;
    ArrayList<Flight> data,data_return;
    ArrayList<String> airlines;
    boolean[] selected = {true,true,true,true};
    FlightSearch fs;
    Boolean isDeparture;
    FlightAdapter fa;
    ListView flightListView;
    TextView destinationTextView,dateAndPassengerTextView;
    Button sortButton,filterButton;
    GifImageView loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_search);
        fs = (FlightSearch) getIntent().getSerializableExtra
                ("flightsearch");
        isDeparture = (Boolean) getIntent().getBooleanExtra("isDeparture", true);
        loading = (GifImageView) findViewById(R.id.loading);
        loading.setVisibility(View.GONE);
        destinationTextView = (TextView) findViewById(R.id.flightSearchDestination);
        dateAndPassengerTextView = (TextView) findViewById(R.id.flightSearchDateAndPassenger);
        destinationTextView.setText(fs.getA_complete().substring(0,fs.getA_complete().indexOf(
                "(")) + " -> " +fs.getD_complete().substring(0,fs.getD_complete().indexOf("(")));
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MM yyyy ");
        String dateForText = sdf.format(fs.getDate().getTime());
        dateAndPassengerTextView.setText(dateForText + (fs.getAdult() + fs.getChild() + fs
                .getInfant()) + " Passenger(s)");
        data = new ArrayList<>();
        data_return = new ArrayList<>();
        fa = new FlightAdapter(this,R.layout.flight_partial,data);
        flightListView = (ListView) findViewById(R.id.flightSearchListView);
        flightListView.setAdapter(fa);
        airlines = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.filter_airlines)));
        buildFilterDialog();
        buildAlertDialog();
        sortButton = (Button) findViewById(R.id.flightSearchSort);
        sortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int[] selection = new int[1];
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(FlightSearchResult.this);
                alertDialog.setTitle(R.string.sort_flight_text).setSingleChoiceItems(R.array
                        .sort_flight_array, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v("which is ",String.valueOf(which));
                        selection[0] = which;
                    }
                }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v("which is ",String.valueOf(which));
                        switch(selection[0]) {
                            case 0: fa.sortItem(FlightAdapter.SORT_BY_PRICE);Log.v
                                    ("flightsearchres","SORT BY PRICE") ;
                                break;
                            case 1: fa.sortItem(FlightAdapter.SORT_BY_DEPARTURE);Log.v
                                    ("flightsearchres","SORT BY DEPARTURE TIME");
                        }
                    }
                });
                AlertDialog d = alertDialog.create();
                d.show();
                Log.v("flightsearchresultsort","clicked");
            }
        });

        filterButton = (Button) findViewById(R.id.flightSearchFilter);
        filterButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                airlinesFilterDialog.show();
            }
        });
        if(fs.getRound_trip()&&!isDeparture){
            data_return = getIntent().getParcelableArrayListExtra("flightreturn");
            getFlightsFromIntent();
        }else{
            getFlights();
        }

    }

    protected void buildFilterDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(FlightSearchResult.this);
        adb.setMultiChoiceItems(R.array.filter_airlines, selected, new DialogInterface.OnMultiChoiceClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                String airline = getResources().getStringArray(R.array.filter_airlines)[which];
                if(isChecked && airlines.indexOf(airline) == -1){
                    airlines.add(airline);
                }
                else if (!isChecked && airlines.indexOf(airline) != -1){
                    airlines.remove(airline);
                }
            }
        });
        adb.setTitle(getString(R.string.title_dialog_filter_airlines));
        adb.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.v("airlines is", airlines.toString());
                fa.setAirlineFilter(airlines);
                fa.filter();
            }
        });
        airlinesFilterDialog = adb.create();
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(FlightSearchResult.this);
        adb.setPositiveButton(R.string.ok, null);
        alert = adb.create();
    }

    protected void getFlights() {
        String url = "http://www.agendatrip.com/api/v1/bookings/getFlights?";
        url += fs.toGetParam() + "&" + Util.paramToString(Util.getUserParam());
        loading.setVisibility(View.VISIBLE);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.setVisibility(View.GONE);
                            data.clear();
                            JSONArray flights = response.getJSONObject("departures").getJSONArray("result");
                            Log.v("departs",flights.toString());
                            int sz = flights.length();
                            if(sz>0){
                                for(int i=0;i<sz;i++){
                                    Flight f = new Flight(flights.getJSONObject(i));
                                    f.setDate(response.getJSONObject("search_queries").getString("date"));
                                    data.add(f);
                                }
                                fa.notifyDataLoaded();
                                if(fs.getRound_trip()){
                                    JSONArray flight_return = response.getJSONObject("returns")
                                            .getJSONArray("result");
                                    Log.v("returns",flight_return.toString());
                                    int szr = flight_return.length();
                                    for(int i=0;i<szr;i++){
                                        Flight f = new Flight(flight_return.getJSONObject(i));
                                        f.setDate(response.getJSONObject("search_queries").getString("date"));
                                        data_return.add(f);
                                    }
                                }
                            }
                            else{
                                alert.setMessage(getString(R.string.no_flight_error));
                                alert.show();
                            }
                        } catch (JSONException e) {
                            //this means no flights for that day
                            alert.setMessage(getString(R.string.no_flight_error));
                            alert.show();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.setVisibility(View.GONE);
            }
        });

        jor.setTag("flightsearchresult");
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected void getFlightsFromIntent(){
        data.clear();
        int szr = data_return.size();
        for(int i=0;i<szr;i++){
            data.add(data_return.get(i));
        }
        fa.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy(){
        AppController.getInstance().cancelPendingRequests("flightsearchresult");
        super.onDestroy();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (isDeparture)
            getSupportActionBar().setTitle("Departure");
        else
            getSupportActionBar().setTitle("Return");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void goToNextStep(Flight f) {
        Log.v("ayy","gotonexttrip");
        //=========================================================================================
        //Determines whether to go to checkout (if single trip OR if already selecting return trip)
        //=========================================================================================
        Log.v("roundtrip",fs.getRound_trip().toString());
        Log.v("isdepar",isDeparture.toString());
        if ((fs.getRound_trip() && !isDeparture) || (!fs.getRound_trip() && isDeparture)) {
            //=============================================================
            //if already selecting the return trip or if its a single trip
            //=============================================================
            Intent i = new Intent(this,FlightCheckout.class);
            Bundle b = new Bundle();
            if(isDeparture){
                fs.departure_flight =f;
            }
            else
                fs.return_flight = f;
            b.putSerializable("flightsearch",fs);
            i.putExtras(b);
            startActivity(i);
            Log.v("ayy","gotonexttrip1");
        } else {
            //=====================================================================================
            //if its a round trip but we still have to select the return trip
            //=====================================================================================
            Log.v("ayy","gotonexttrip");
            Intent i = new Intent(this, FlightSearchResult.class);
            Bundle b = new Bundle();
            fs.departure_flight = f;
            b.putSerializable("flightsearch", fs);
            b.putSerializable("isDeparture", false);
            b.putParcelableArrayList("flightreturn",data_return);
            i.putExtras(b);
            startActivity(i);
        }
    }

}
