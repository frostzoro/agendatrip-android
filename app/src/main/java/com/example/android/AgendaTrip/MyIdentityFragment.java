package com.example.android.AgendaTrip;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.UserIdentityAdapter;
import model.UserIdentity;
import pl.droidsonroids.gif.GifImageView;
import singleton.AppController;
import utils.Util;


public class MyIdentityFragment extends Fragment {
    ListView identityListView;
    ArrayList<UserIdentity> identityArrayList;
    ArrayAdapter<UserIdentity> identityAdapter;
    GifImageView loading;

    static final int CREATE_IDENTITY = 0;
    static final int EDIT_IDENTITY = 1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout =  inflater.inflate(R.layout.fragment_my_identity, container, false);
        identityListView = (ListView) layout.findViewById(R.id.myIdentityListView);
        identityArrayList = new ArrayList<>();
        identityAdapter = new UserIdentityAdapter(getActivity(),R.layout
                .partial_my_identity,identityArrayList);
        identityListView.setAdapter(identityAdapter);
        loading = (GifImageView) layout.findViewById(R.id.myIdentityLoading);
        getData();
        return layout;
    }

    @Override
     public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_identity, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menu_add_identity){
            Intent i = new Intent(this.getActivity(),UserIdentityDetail.class);
            Bundle b = new Bundle();
            b.putString("mode","new");
            i.putExtras(b);
            startActivityForResult(i, CREATE_IDENTITY);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("onactres","walked");
        getData();
    }

    protected void getData(){
        Log.v("getdata","dataget init");
        String url = Util.BASE_URL + Util.API_GATEWAY + Util.USER_IDENTITY_PATH;
        url += "?" + Util.paramToString(Util.getUserParam());
        Log.v("myidenfrag", loading.toString());
        loading.setVisibility(View.VISIBLE);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray payload = null;
                try {
                    payload = response.getJSONArray("payload");
                    Log.v("mifpay",payload.toString());
                    int sz=  payload.length();
                    identityArrayList.clear();
                for(int i=0;i<sz;i++){
                    UserIdentity ui = new UserIdentity(payload.getJSONObject(i));
                    identityArrayList.add(ui);
                }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.v("mifarsz",identityArrayList.size() + "");
                identityAdapter.notifyDataSetChanged();
                loading.setVisibility(View.GONE);
                Log.v("mifonresp", "Data get");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.setVisibility(View.GONE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (getActivity());
                    adb.setMessage(getString(R.string.timeout_error_polite));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Util.forceLogInFragment(getActivity());
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getActivity().getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getActivity().getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });
        jor.setTag("MyIdentityFragment");
        AppController.getInstance().addToRequestQueue(jor);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests("MyIdentityFragment");
    }
}
