package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import adapters.VacationSiteImageListAdapter;
import model.VacationSite;
import pl.droidsonroids.gif.GifImageView;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class VacationSiteDetail extends AppCompatActivity {
    ArrayList<String> dummy;
    AlertDialog dialog;
    Bundle b;
    Double latitude, longitude;
    GifImageView loading;
    int vsID;
    ImageView niv;
    Intent imageBigIntent;
    TextView map, wish,add  ;
    RatingBar rating;
    RecyclerView rv;
    String placename;
    String vsname;
    TextView name, tag, description, address;
    VacationSite vs;
    VacationSiteImageListAdapter vsil;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        pDialog=new ProgressDialog(this);
        pDialog.setMessage("Please wait");
        pDialog.setCancelable(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacation_site_detail);
        placename = getIntent().getExtras().getString("name");
        latitude = getIntent().getExtras().getDouble("latitude");
        longitude = getIntent().getExtras().getDouble("longitude");
        buildAlertDialog();
        bindViewToVariables();
        initializeSupportVariables();
        setOnClickListener();
        getData();
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(getResources().getString(R.string.error));
        adb.setPositiveButton(getResources().getString(R.string.ok),null);
        dialog = adb.create();
    }

    protected void bindViewToVariables(){
        loading = (GifImageView) findViewById(R.id.vacationSiteDetailLoading);
        rv = (RecyclerView) findViewById(R.id.vacationSiteDetailRecyclerView);
        name = (TextView) findViewById(R.id.vacationSiteHeaderInfoName);
        rating = (RatingBar) findViewById(R.id.vacationSiteHeaderInfoRating);
        tag = (TextView) findViewById(R.id.vacationSiteHeaderInfoTags);
        description = (TextView) findViewById(R.id.vacationSiteDetailDescription);
        address = (TextView) findViewById(R.id.vacationSiteDetailAddress);
        niv = (ImageView) findViewById(R.id.vacationSiteDetailHeaderImage);
        wish = (TextView) findViewById(R.id.vacationSiteDetailsAddToWishlistIcon);
        map = (TextView) findViewById(R.id.vacationSiteDetailsSeeOnMapIcon);
    }

    protected void initializeSupportVariables(){
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv.setLayoutManager(llm);
        dummy = new ArrayList<String>();
        imageBigIntent = new Intent(VacationSiteDetail.this,ImageFullView.class);
        vsil = new VacationSiteImageListAdapter(dummy, VacationSiteDetail.this);
        rv.setAdapter(vsil);
        b = new Bundle();
    }

    protected void getData() {
        vsID = Integer.parseInt(getIntent().getExtras().getString("vacation_site"));
        String requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util.VACATION_SITE_PATH +
                "/show";
        JSONObject params = new JSONObject();
        try {
            params.put("vacation_site_id", vsID);
        } catch (JSONException e) {
            Log.v("vacationsitedetail", "error on putting params");
        }
        JsonObjectRequest vsRequest = new JsonObjectRequest(Request.Method.POST, requestUrl,
                (JSONObject) params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loading.setVisibility(View.GONE);
                try {
                    JSONObject vacation_site = response.getJSONObject("payload");
                    vs = new VacationSite(vacation_site);
                    vs.loadImages(vacation_site.getJSONArray("photos"));
                    vs.loadTags(vacation_site.getJSONArray("tags"));
                    vsil.setImages(vs.getImages());
                    Log.v("success !", "nearby data get");
                    setDataToView();
                    updatePreferences();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.setVisibility(View.GONE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    dialog.setMessage(getString(R.string.timeout_error));
                    dialog.show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), "Auth Error", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(), "server Error", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(), "parse Error", Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });

        AppController.getInstance().cancelPendingRequests("VacationSiteDetail");
        vsRequest.setTag("VacationSiteDetail");
        vsRequest.setRetryPolicy(new DefaultRetryPolicy(
                1000*60,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(vsRequest);
    }

    protected void updatePreferences() {
        if (Util.isLoggedIn()) {
            String url = Util.BASE_URL + Util.API_GATEWAY + Util.TAG_URL + Util.VACATION_SITE_RESPONSE_URL;
            HashMap<String, String> param = new HashMap<>();
            param.putAll(Util.getUserParam());
            param.put("vacation_site_id", String.valueOf(vs.getID()));
            param.put("response",1+"");
            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, new JSONObject
                    (param), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.v("updatesitepref", "success!");
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("updatesitepref", "failure!");
                }
            });
            AppController.getInstance().addToRequestQueue(jor);
        }

    }

    protected void setOnClickListener() {
        add=(TextView) findViewById(R.id.vacationSiteDetailsAddToAgendaIcon);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Util.isLoggedIn()){
             Intent i=new Intent(getApplicationContext(),AddAgendaFromDetails.class);
                i.putExtra("vsid",vsID);
                i.putExtra("vsname",vsname);
                startActivity(i);}

                else{
                    Toast.makeText(VacationSiteDetail.this, "You need to be logged in to insert plan into an agenda", Toast.LENGTH_SHORT).show();
                }
            }
        });
        wish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Util.isLoggedIn()){
                AddWish(vsID);}
                else{
                    Toast.makeText(VacationSiteDetail.this, "You need to be logged in to insert plan into your wishlist", Toast.LENGTH_SHORT).show();
                }
            }
        });

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MapShowing.class);
                i.putExtra("longitude", longitude);
                i.putExtra("latitude", latitude);
                i.putExtra("name", placename);
                startActivity(i);
            }
        });
        vsil.setOnItemClickListener(new VacationSiteImageListAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int position) {
                b.clear();
                b.putString("image_url",vsil.getItem(position));
                imageBigIntent.putExtras(b);
                startActivity(imageBigIntent);
            }
        });
    }

    protected void setDataToView() {
        name.setText(vs.getName());
        rating.setRating((float)vs.getRating());
        tag.setText(vs.getTagsAsString());
        description.setText(vs.getDescription());
        address.setText(vs.getAddress());
        Picasso.with(getApplicationContext()).load(Util.BASE_URL + Util.COVER_URL + vs.getImage
                ()).fit().centerCrop()
                .placeholder(R.drawable
                        .agenda_placeholder_1).into(niv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        vsname = getIntent().getStringExtra("vacation_site_name");
        getSupportActionBar().setTitle(vsname);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void AddWish(int vsID) {
        pDialog.show();
        Context context = getApplicationContext();
        String url = "Http://www.agendatrip.com/api/v1/user/wish";
        Map<String, String> params = new HashMap<String, String>();
        params.put("userID", Util.loggedID);
        params.put("key", Util.loggedKey);
        params.put("vacation_site_id", vsID + "");
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if(pDialog.isShowing()){
                    pDialog.hide();

                }
                Log.v("dataresp", response.toString());


                Log.v("asd", response.toString());
                Toast.makeText(VacationSiteDetail.this, "Added to Wishlist!", Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if(pDialog.isShowing()){
                    pDialog.hide();

                }
                NetworkResponse nResponse = error.networkResponse;
                Log.v("data", error.toString());
                Log.v("error is", error.toString());
                VolleyLog.d("asd", "Error: " + error.getMessage());
                Toast.makeText(VacationSiteDetail.this, "Failed to add to Wishlist!", Toast.LENGTH_SHORT).show();

            }
        });

        AppController.getInstance().cancelPendingRequests("VacationSiteDetail");
        jsObjRequest.setTag("VacationSiteDetail");
        jsObjRequest.setRetryPolicy(Constants.RETRY_POLICY);
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

}

