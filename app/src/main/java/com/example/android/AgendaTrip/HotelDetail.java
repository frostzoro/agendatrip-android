package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;

import model.Hotel;
import model.HotelSearch;
import singleton.AppController;
import utils.Util;

public class HotelDetail extends AppCompatActivity  {
    HotelSearch hs;
    Hotel h;
    TextView hotelStartsFrom;
    ImageView hotelCoverImage;
    AlertDialog dialog;
    RelativeLayout loading;
    LinearLayout mainLayout;
    ViewPager mViewPager;
    PagerTabStrip  mPagerTabStrip;
    HotelDetailPagerAdapter mHotelDetailPagerAdapter;
    Fragment f;
    Bundle args;
    Button bookRoomButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detai);
        receiveDataFromIntent();
        buildAlertDialog();
        bindViewToVariables();
        initializeSupportVariables();
        setListeners();
        getData();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests("HotelDetail");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(h.breadCrumb.getName());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void buildAlertDialog() {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setPositiveButton("OK", null).setCancelable(false);
        dialog = b.create();
    }

    protected void receiveDataFromIntent(){
        hs = (HotelSearch) getIntent().getSerializableExtra("hotelsearch");
        h = (Hotel) getIntent().getSerializableExtra("hotel");
    }

    protected void bindViewToVariables(){
        hotelStartsFrom = (TextView) findViewById(R.id.hotelDetailRoomStartsFrom);
        hotelCoverImage = (ImageView) findViewById(R.id.hotelDetailCoverImage);
        loading = (RelativeLayout) findViewById(R.id.loading);
        mainLayout = (LinearLayout) findViewById(R.id.hotelDetailMainLayout);
        mViewPager = (ViewPager) findViewById(R.id.hotelDetailPager);
        mPagerTabStrip = (PagerTabStrip) findViewById(R.id.hotelDetailPagerTabStrip);
        bookRoomButton = (Button) findViewById(R.id.hotelDetailBookRoomButton);
    }

    protected void initializeSupportVariables(){
        mainLayout.setVisibility(View.GONE);
        mHotelDetailPagerAdapter = new HotelDetailPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mHotelDetailPagerAdapter);
    }

    protected void setListeners(){
        bookRoomButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HotelDetail.this,HotelRoomList.class);
                Bundle b = new Bundle();
                b.putSerializable("hotelsearch",hs);
                b.putSerializable("hotel",h);
                i.putExtras(b);
                startActivity(i);
            }
        });
    }

    protected void getData(){
        String url = h.breadCrumb.getBusiness_url();
        url+="&token="+hs.getToken()+"&output=json";
        Log.v("urlhoteldetail", url);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        h.setHotelData(response);
                        loadDataToView();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.setVisibility(View.GONE);
                        if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                            dialog.setMessage(getString(R.string.timeout_error));
                            dialog.show();
                        }
                        else{
                            dialog.setMessage(getString(R.string.error_unknown));
                            dialog.show();
                        }
                    }
                });
        jor.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jor.setTag("HotelDetail");
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected void loadDataToView(){
        Log.v("loading data to view", "loading hotel details");
        loading.setVisibility(View.GONE);
        mainLayout.setVisibility(View.VISIBLE);
        hotelStartsFrom.setText("Room starts from " + Util.convertDoubleToIDRString(Double.valueOf(h.breadCrumb.getPrice())));
        Picasso.with(this).load(h.getPrimaryphoto()).placeholder(R.drawable.agenda_placeholder_narrow_1).fit().centerCrop()
                .into(hotelCoverImage);
    };

    public class HotelDetailPagerAdapter extends FragmentPagerAdapter{
        ArrayList<String> pageTitles;
        public HotelDetailPagerAdapter(FragmentManager fm) {
            super(fm);
            pageTitles = new ArrayList<>();
            pageTitles.add("General");
            pageTitles.add("Description");
            pageTitles.add("Photos");
        }

        @Override
        public Fragment getItem(int position) {
            Log.v("getting item at",position+"");
            switch(position){
                case 0 :
                    f = new HotelDetailGeneral();
                    args = new Bundle();
                    args.putSerializable("hotelsearch",hs);
                    args.putSerializable("hotel", h);
                    f.setArguments(args);
                    return f;
                case 1 :
                    f = new HotelDetailDescription();
                    args = new Bundle();
                    args.putSerializable("hotelsearch",hs);
                    args.putSerializable("hotel", h);
                    f.setArguments(args);
                    return f;
                case 2 :
                    f = new PhotoCollectionFragment();
                    args = new Bundle();
                    args.putSerializable("photos",h.getPhotos());
                    f.setArguments(args);
                    return f;
                default :
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pageTitles.get(position);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
