package com.example.android.AgendaTrip;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.gc.materialdesign.views.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import adapters.HashMapTextAdapter;
import model.FlightSearch;
import pl.droidsonroids.gif.GifImageView;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class FlightBooking extends AppCompatActivity implements CalendarDatePickerDialogFragment
        .OnDateSetListener{
    AutoCompleteTextView source,destination;
    String token;
    ArrayList<Pair<String,String>> airports,airports2;
    HashMapTextAdapter hmsource,hmdest;
    Calendar departureDate, returnDate;
    TextView departureText, returnText;
    Boolean forDeparture,twoway;
    CalendarDatePickerDialogFragment dpb;
    LinearLayout returnDateContainer;
    CheckBox roundTripCheckBox;
    FlightSearch flightdata;
    Button searchButton;
    Spinner adult,child,infant;
    ArrayAdapter<String> adulta,childa,infanta;
    Integer agendaID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_booking);
        token="";
        twoway=false;
        agendaID = getIntent().getIntExtra("agenda_id",-1);
        setView();
        getToken();
    }

    protected void setView(){
        adult = (Spinner) findViewById(R.id.flightBookingSpinnerAdult);
        child = (Spinner) findViewById(R.id.flightBookingSpinnerChild);
        infant = (Spinner) findViewById(R.id.flightBookingSpinnerInfant);
        String[] spinnerContent = new String[] {"0","1","2","3","4","5","6"};
        String[] spinnerContentAdult = new String[] {"1","2","3","4","5","6"};
        adulta = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,
                spinnerContentAdult);
        childa = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,
                spinnerContent);
        infanta = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,
                spinnerContent);
        adult.setAdapter(adulta);
        child.setAdapter(childa);
        infant.setAdapter(infanta);
        departureDate = Calendar.getInstance();
        returnDate = Calendar.getInstance();
        returnDate.add(Calendar.DATE, 1);
        source = (AutoCompleteTextView) findViewById(R.id.flightBookingSourceTextView);
        source.setOnItemClickListener(new AdapterView.OnItemClickListener() {
              @Override
              public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hmsource.setSelectedIndex(position);
              }
          }
        );
        destination = (AutoCompleteTextView) findViewById(R.id.flightBookingDestinationTextView);
        destination.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hmdest.setSelectedIndex(position);
            }
        });
        airports = new ArrayList<>();
        airports2 = new ArrayList<>();
        hmsource = new HashMapTextAdapter(this,android.R.layout
                .simple_dropdown_item_1line,airports);
        hmdest = new HashMapTextAdapter(this,android.R.layout.simple_dropdown_item_1line,airports2);
        source.setAdapter(hmsource);
        source.setThreshold(1);
        destination.setAdapter(hmdest);
        destination.setThreshold(1);
        returnDateContainer = (LinearLayout) findViewById(R.id.flightBookingReturnDateContainer);
        departureText = (TextView) findViewById(R.id.flightBookingDepartureDate);
        departureText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDepartureDate();
            }
        });
        SimpleDateFormat sdf2 = new SimpleDateFormat("E, dd MMM yyyy");
        departureText.setText(sdf2.format(departureDate.getTime()));
        returnText = (TextView) findViewById(R.id.flightBookingReturnDate);
        returnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setReturnDate();
            }
        });
        returnText.setText(sdf2.format(returnDate.getTime()));
        returnDateContainer.setVisibility(View.GONE);
        roundTripCheckBox = (CheckBox) findViewById(R.id.flightBookingTwoWayCheckbox);
        roundTripCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    returnDateContainer.setVisibility(View.VISIBLE);
                    twoway=true;
                }
                else{
                    returnDateContainer.setVisibility(View.GONE);
                    twoway=false;
                }
            }
        });
        searchButton = (Button) findViewById(R.id.flightBookingSearchButton);
        searchButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(check_fields()){
                    Intent i = new Intent(getApplicationContext(),FlightSearchResult.class);
                    Bundle b = new Bundle();
                    sendFlightSearchRequest();
                    b.putSerializable("flightsearch",flightdata);
                    b.putBoolean("isDeparture",true);
                    i.putExtras(b);
                    startActivity(i);
                }
            }
        });
    }

    protected Boolean check_fields(){
        android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                (FlightBooking.this);
        adb.setTitle(getString(R.string.error));
        adb.setPositiveButton(R.string.ok, null);
        adb.create();
        Boolean result = true;
        if(hmsource.getSelectedIndex()==-1){
            adb.setMessage(R.string.empty_departure_error);
            adb.show();
            result = false;
        }
        else if(hmdest.getSelectedIndex()==-1){
            adb.setMessage(R.string.empty_destination_error);
            adb.show();
            result = false;
        }
        else if((hmsource.getItem(hmsource.getSelectedIndex()).first.equals(hmdest.getItem(hmdest
                .getSelectedIndex()).first))&&twoway ==  true){
            adb.setMessage(R.string.same_destination_error);
            adb.show();
            result = false;
        }
        else if(departureDate==null){
            adb.setMessage(R.string.departure_date_error);
            adb.show();
            result = false;
        }
        else if(returnDate==null){
            adb.setMessage(R.string.return_date_error);
            adb.show();
            result = false;
        }
        else if(departureDate.compareTo(returnDate)>0 && twoway){
            adb.setMessage(R.string.departure_return_date_error);
            adb.show();
            result = false;
        }
        return result;
    }

    protected void sendFlightSearchRequest(){
        flightdata = new FlightSearch();
        flightdata.setA(hmsource.getItem(hmsource.getSelectedIndex()).first);
        flightdata.setD(hmdest.getItem(hmdest.getSelectedIndex()).first);
        flightdata.setA_complete(hmsource.getItem(hmsource.getSelectedIndex()).second);
        flightdata.setD_complete(hmdest.getItem(hmdest.getSelectedIndex()).second);
        flightdata.setAdult(Integer.parseInt(adult.getSelectedItem().toString()));
        flightdata.setInfant(Integer.parseInt(infant.getSelectedItem().toString()));
        flightdata.setChild(Integer.parseInt(child.getSelectedItem().toString()));
        flightdata.setToken(token);
        flightdata.setDate(departureDate);
        flightdata.setRound_trip(twoway);
        if(twoway)flightdata.setRet_date(returnDate);
        if(agendaID!=-1)flightdata.setAgenda_id(agendaID);
    }

    protected void setDepartureDate(){
        forDeparture=true;
        dpb = null;
        dpb = CalendarDatePickerDialogFragment
                .newInstance(FlightBooking.this, Calendar.getInstance().get(Calendar.YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get
                                (Calendar
                                .DAY_OF_MONTH));
        MonthAdapter.CalendarDay c = new MonthAdapter.CalendarDay(Calendar.getInstance());
        dpb.setDateRange(c, null);
        dpb.show(getSupportFragmentManager(),"title");
        Log.v("set depart date","today");
    }

    protected void setReturnDate(){
        forDeparture=false;
        dpb = null;
        dpb = CalendarDatePickerDialogFragment
                .newInstance(FlightBooking.this, returnDate.get(Calendar.YEAR), returnDate
                        .get(Calendar.MONTH),returnDate.get(Calendar.DAY_OF_MONTH));
        MonthAdapter.CalendarDay c = new MonthAdapter.CalendarDay(departureDate);
        dpb.setDateRange(c, null);
        dpb.show(getSupportFragmentManager(), "title");
    }

    protected void getToken(){
        String url = "http://www.agendatrip.com/api/v1/bookings/getToken";
        url+= "?" +  Util.paramToString(Util.getUserParam());
        Log.v("flightbooking", url);
        JsonObjectRequest jor1 = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    token = response.getString("token").toString();
                    Log.v("token is ",token);
                    getAirports();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (FlightBooking.this);
                    adb.setMessage(getString(R.string.timeout_error));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), "Auth Error", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });

        AppController.getInstance().addToRequestQueue(jor1);
    }

    protected void getAirports(){
        String url = "http://www.agendatrip.com/api/v1/bookings/getAirports";
        url+="?" + Util.paramToString(Util.getUserParam())+"&token="+token;
        JsonObjectRequest jor2 = new JsonObjectRequest(Request.Method.GET, url,
                (JSONObject) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray airportsjson = response.getJSONObject("all_airport").getJSONArray
                            ("airport");
                    int sz = airportsjson.length();
                    JSONObject j;
                    for(int i=0;i<sz;i++){
                        j = airportsjson.getJSONObject(i);
                        if(j.getString("country_id").equals("id")){
                        String code = j.getString("airport_code");
                        String caption = j.getString("location_name") + " (" + j.getString
                                ("airport_code") + "), " + j.getString("airport_name");
                        airports.add(Pair.create(code,caption));
                        }
                    }
                    airports2.addAll(airports);
                    Log.v("flightbooking", "data get, sz = " + airports.size());
                    Log.v("flightbooking2","data get, sz = " + airports2.size());
                    hmsource.notifyDataSetChanged();
                    hmsource.notifyDataLoaded();
                    hmdest.notifyDataSetChanged();
                    hmdest.notifyDataLoaded();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        jor2.setRetryPolicy(Constants.RETRY_POLICY);
        jor2.setTag("FlightBooking");
        AppController.getInstance().addToRequestQueue(jor2);
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth)
    {
        monthOfYear = monthOfYear+1;
        Log.v("year",String.valueOf(year));
        Log.v("month",String.valueOf(monthOfYear));
        Log.v("dayOfMonth",String.valueOf(dayOfMonth));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("E, dd MMM yyyy");

            if(forDeparture) {
                departureDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                departureDate.set(Calendar.MONTH,monthOfYear-1);
                departureDate.set(Calendar.YEAR,year);
                departureText.setText(sdf2.format(departureDate.getTime()));
                if(twoway){
                    setReturnDate();
                }
            }
            else{
                returnDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                returnDate.set(Calendar.MONTH,monthOfYear-1);
                returnDate.set(Calendar.YEAR,year);
                returnText.setText(sdf2.format(returnDate.getTime()));
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.flight_booking);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
