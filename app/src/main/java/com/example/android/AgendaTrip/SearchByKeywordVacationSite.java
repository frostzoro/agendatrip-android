package com.example.android.AgendaTrip;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import adapters.VacationPackageListAdapter;
import adapters.VacationSiteListAdapter;
import data.JSONServer;
import model.VacationSite;
import pl.droidsonroids.gif.GifImageView;
import singleton.AppController;
import utils.Constants;
import utils.Util;


public class SearchByKeywordVacationSite extends Activity {

    RelativeLayout back,search;
    GifImageView loading;
    EditText keywordEditText;
    ListView vacationSiteList;
    ArrayList<VacationSite> vacationSiteArrayList;
    Intent i;
    String keyword;
    JSONServer jsonServer;
    VacationSiteListAdapter vacationSiteListAdapter;
    Boolean idle;
    Button filter,sort;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_keyword_vacation_site);
        setViews();
        getIntentData();

        keywordEditText.setText(keyword);
        Log.v("data is", keyword);
        idle=true;
        getVacationSites();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    private void setViews(){
        filter = (Button) findViewById(R.id.searchByKeyWordVacationSiteFilter);
        sort = (Button) findViewById(R.id.searchByKeyWordVacationSiteSort);
        sort.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final int[] selection = new int[1];
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SearchByKeywordVacationSite.this);
                alertDialog.setTitle(R.string.sort_package_text).setSingleChoiceItems(R.array
                        .sort_site_array, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v("which is ",String.valueOf(which));
                        selection[0] = which;
                    }
                }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch(selection[0]) {
                            case 0: vacationSiteListAdapter.sortItem(vacationSiteListAdapter
                                    .SORT_BY_PREFERENCE);
                                break;
                            case 1:
                                vacationSiteListAdapter.sortItem(vacationSiteListAdapter
                                        .SORT_BY_POPULARITY);
                                break;
                            case 2: vacationSiteListAdapter.sortItem(vacationSiteListAdapter
                                    .SORT_BY_RELEVANCE);
                                break;
                        }
                    }
                });
                AlertDialog d = alertDialog.create();
                d.show();
                Log.v("flightsearchresultsort","clicked");
            }
        });
        keywordEditText = (EditText) findViewById(R.id.searchByKeyWordVacationSiteEditText);
        back = (RelativeLayout) findViewById(R.id.searchByKeyWordVacationSiteBack);
        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom);
            }
        });
        search = (RelativeLayout) findViewById(R.id.searchByKeyWordVacationSiteSearch);
        loading = (GifImageView) findViewById(R.id.searchByKeyWordVacationSiteLoading);
        search.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getVacationSites();
            }
        });
        vacationSiteList = (ListView) findViewById(R.id.searchByKeyWordVacationSiteListView);
        vacationSiteArrayList = new ArrayList<>();
        jsonServer = new JSONServer();
        vacationSiteListAdapter = new VacationSiteListAdapter(SearchByKeywordVacationSite.this,R
                .layout
                .vacation_site_partial,vacationSiteArrayList);
        vacationSiteList.setAdapter(vacationSiteListAdapter);
    }

    private void getIntentData(){
        i = getIntent();
        keyword = i.getStringExtra("keyword");
    }

    public void getVacationSites() {
        if(idle){
            idle=false;
            loading.setVisibility(View.VISIBLE);
            keyword = keywordEditText.getText().toString();
            if (keyword.length() <= 0) {
                Toast.makeText(getApplicationContext(), "Please Input a Keyword", Toast.LENGTH_LONG);
            } else {
                vacationSiteArrayList.clear();
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("keyword", keyword);
                String requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util.VACATION_SITE_PATH +
                        "/keyword";

                if(Util.isLoggedIn())
                    param.putAll(Util.getUserParam());

                JsonObjectRequest vsRequest = new JsonObjectRequest(Request.Method.POST, requestUrl,
                        new JSONObject(param), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loading.setVisibility(View.GONE);
                        idle=true;
                        try {
                            //Log.v("datass", response.toString());
                            JSONArray array = response.getJSONArray("payload");
                            //Log.v("datass", array.toString());
                            int resultSize = array.length();

                            for (int i = 0; i < resultSize; i++) {
                                JSONObject vs = array.getJSONObject(i);
                                vacationSiteArrayList.add(new VacationSite(vs));
                            }

                            vacationSiteListAdapter.notifyDataSetChanged();

                            if(resultSize==0){
                                android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                                        (SearchByKeywordVacationSite.this);
                                adb.setMessage(getString(R.string.search_no_place));
                                adb.setTitle(getString(R.string.error));
                                adb.setPositiveButton(R.string.ok, null);
                                adb.create().show();
                            }
                        } catch (Exception e) {
                            //Log.v("datass", e.getMessage());
                            Log.v("getVacationSite", e.getStackTrace().toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.setVisibility(View.GONE);
                        idle=true;
                        if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                            android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                                    (SearchByKeywordVacationSite.this);
                            adb.setMessage(getString(R.string.timeout_error));
                            adb.setTitle(getString(R.string.error));
                            adb.setPositiveButton(R.string.ok, null);
                            adb.create().show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ParseError) {
                            Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                            //TODO
                        }
                    }
                });
                vsRequest.setRetryPolicy(Constants.RETRY_POLICY);
                vsRequest.setTag("SearchByKeywordVacationSite");
                AppController.getInstance().addToRequestQueue(vsRequest);
            }
        }
    }

}
