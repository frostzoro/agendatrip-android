package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import model.BookedItem;
import singleton.AppController;
import utils.Util;

public class MyOrderDetail extends AppCompatActivity {
    AlertDialog dialog;
    BookedItem bi;
    Button payButton;
    SwipeRefreshLayout srl;
    LinearLayout ll,contentLayout;
    RelativeLayout loading;
    Intent i;
    TextView orderStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order_detail);
        receiveDataFromIntent();
        bindViewToVariables();
        buildAlertDialog();
        i = new Intent(MyOrderDetail.this,WebviewActivity.class);
        refreshView();
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshOrderDetail();
            }
        });
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(getResources().getString(R.string.error));
        adb.setPositiveButton(R.string.ok,null);
        dialog = adb.create();
    }

    protected void refreshView(){
        loading.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);
        srl.setRefreshing(false);
        ll.removeAllViews();
        ll.addView(bi.getBid().getView(this));
        orderStatus.setText(bi.getStatus());
        if(bi.getStatus().equals("paid")){
            payButton.setVisibility(View.GONE);
        }
        else{
            payButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    i.putExtra("url",Util.PAYMENT_URL + "?order_id="+bi.getId()+"&order_type=booking");
                    startActivityForResult(i, 0);
                }
            });
        }
    }

    protected void refreshOrderDetail(){
        Log.v("refreshing", "orderdetail");
        loading.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);
        String url = Util.BASE_URL + Util.API_GATEWAY + Util.BOOKING_URL + "/showOrderDetail?user_booking_id=" + bi.getId() + "&" + Util.paramToString(Util.getUserParam());
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
            new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    bi = new BookedItem(response.getJSONObject("payload"));
                    loading.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshView();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.setMessage(getString(R.string.timeout_error_polite));
                dialog.show();
            }
        });
        jor.setTag("MyOrderDetail");
        jor.setRetryPolicy(new DefaultRetryPolicy(305000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected void receiveDataFromIntent(){
        bi = (BookedItem) getIntent().getSerializableExtra("booked_item");
    }

    protected void bindViewToVariables(){
        srl = (SwipeRefreshLayout) findViewById(R.id.myOrderDetailSwipeRefreshLayout);
        loading = (RelativeLayout) findViewById(R.id.loading);
        ll = (LinearLayout) findViewById(R.id.fragmentMyOrderDetailLayout);
        contentLayout = (LinearLayout) findViewById(R.id.myOrderDetailContentLayout);
        payButton = (Button) findViewById(R.id.fragmentMyOrderPayButton);
        orderStatus = (TextView) findViewById(R.id.fragmentMyOrderDetailOrderStatus);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Order #" + bi.getOrder_id());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        refreshOrderDetail();
    }
}
