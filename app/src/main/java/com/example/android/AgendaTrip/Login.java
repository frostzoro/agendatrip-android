package com.example.android.AgendaTrip;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import data.DatabaseHandler;
import data.DatabaseHandlerAgendaDetails;
import singleton.AppController;
import utils.Util;

/**
 * Created by Vin on 10/3/2015.
 */
public class Login extends Fragment
{
    View rootview;
    private ProgressDialog pDialog;
    private static String TAG = MainActivity.class.getSimpleName();
    private String urlJsonObj = "http://agendatrip.com/api/v1/login";
    private String jsonResponse;
    private TextView txtResponse;
    Button loginButton;
    EditText txtEmail,txtPassword;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.login, container, false);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        loginButton = (Button) rootview.findViewById(R.id.login_button);

        txtEmail = (EditText) rootview.findViewById(R.id.email);
        txtPassword = (EditText) rootview.findViewById(R.id.password);
        loginButton.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TryLogin(v);

            }
        });
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        return rootview;
    }

    @Override
    public void onDestroyView() {
        if(pDialog!=null)
        pDialog.dismiss();
        pDialog=null;
        super.onDestroyView();
    }

    private boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static final Pattern EMAIL_ADDRESS
            = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    public void TryLogin(View view) {
       final String email = ((EditText) rootview.findViewById(R.id.email)).getText().toString();
        String password = ((EditText) rootview.findViewById(R.id.password)).getText().toString();


        if (!isValidEmail(email)) {
            Context context = getActivity().getApplicationContext();
            Toast toast = Toast.makeText(context, "Invalid E-mail format"+email, Toast.LENGTH_SHORT);
            toast.show();
        } else {
            final Context context = getActivity().getApplicationContext();

            if(password.length()<9 || password.length()>14){
                Toast toast=Toast.makeText(context, "Password should be 9 - 14 characters", Toast.LENGTH_SHORT);
                toast.show();
            }
            else{
                makeJsonObjectRequest(email, password);
                pDialog.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(Util.loggedKey!="" && Util.loggedID!="") {
                        pDialog.hide();
                            SharedPreferences sp=context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                            SharedPreferences.Editor ed = sp.edit();


                            String newid=sp.getString("newid","");
                            Log.v("APA", newid+" | "+email);
                            Intent i;

                            if(email.equals(newid)){
                                 i=new Intent(context, TagRegister.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            }

                            else{

                        i = new Intent(context, MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        }

                            startActivity(i);
                        }

                        else{
                            pDialog.hide();
                            Toast.makeText(context, "Invalid E-mail or Password", Toast.LENGTH_LONG).show();}
                    }
                }, 15000);


                pDialog.show();


                }


                //lempar data login ke JSON
            }
        }

    public void setAlarm(){

        Log.v("masukk", "masukk");
        Calendar c= Calendar.getInstance();

        Long alertTime=c.getTimeInMillis()+1*30; //setelah login, user bakalan dapet notif 30 menit berikutnya
//        Long alertTime=c.getTimeInMillis()+60000*30; //setelah login, user bakalan dapet notif 30 menit berikutnya
        Intent alertIntent=new Intent(getActivity(), AlertReceiver.class);
        alertIntent.putExtra("loggedID", Util.loggedID+"");
        alertIntent.putExtra("loggedKey", Util.loggedKey);
        alertIntent.putExtra("notifnumber",0);
        alertIntent.putExtra("notifid",0);

        AlarmManager alarmManager= (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alertTime,60000 * 60 * 24, PendingIntent.getBroadcast(getActivity(), -1, alertIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        //notif bakalan repeat every 24 hours
    }


    private void makeJsonObjectRequest(String email, String password) {

        final Context context = getActivity().getApplicationContext();
        String url = "http://agendatrip.com/api/v1/login";
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                Log.v("dataresp", response.toString());


                Log.v(TAG, response.toString());

                try {

                    JSONObject payload = response.getJSONObject("payload");
                    String key = payload.getString("key");
                    String userID = payload.getString("userID");
                    JSONObject user=payload.getJSONObject("user");
                    String username=user.getString("firstname")+" "+user.getString("lastname");
                    jsonResponse = "";

                    jsonResponse += "Key: " + key + "\n\n";
                    jsonResponse += "userID: " + userID + "\n\n";

                    if(key!="" && userID!=""){
                    Util.loggedID=userID;
                    Util.loggedKey=key;
                    Util.loggedUserName=username;

                        setAlarm();
                        Context context= getActivity().getApplicationContext();
                        SharedPreferences sharedPreference=context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreference.edit();
                        String uid=sharedPreference.getString("dbid","");
                        editor.putString("username",username);
                        editor.putString("key", key);
                        editor.putString("userid", userID);
                        editor.apply();
                        Log.v("userID vs uid", userID + " " + uid);
                        if(!userID.equals(uid)){
                            DatabaseHandler dba=new DatabaseHandler(context);
                            DatabaseHandlerAgendaDetails dbad=new DatabaseHandlerAgendaDetails(context);
                            dba.clearDB();
                            dbad.clearDB();
                            dba.close();
                            dbad.close();
                            Log.v("Logged ID","DIFFERENT");
                        }

                        else{
                            Log.v("Logged ID","SAME");

                        }

                        Toast.makeText(context, "Logged in and Saved", Toast.LENGTH_SHORT).show();

                       // hidepDialog();
                    }

                   /* Toast.makeText(getActivity().getApplicationContext(),
                            jsonResponse,
                            Toast.LENGTH_LONG).show();
*/

                } catch (JSONException e) {
                    e.printStackTrace();
                  /*  Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hidepDialog();
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (getActivity());
                    adb.setMessage(getString(R.string.timeout_error));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(context,"Auth Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(context,"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(context,"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });
        AppController.getInstance().addToRequestQueue(jsObjRequest);
//        requestQueue.add(jsObjRequest);
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
