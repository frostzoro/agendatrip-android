package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import model.UserIdentity;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class UserIdentityDetail extends AppCompatActivity implements
        CalendarDatePickerDialogFragment.OnDateSetListener {
    CalendarDatePickerDialogFragment dpb;
    UserIdentity ui;
    String mode;
    EditText first_name,last_name,birthdate,phone_number,email,identity_number,passport_number;
    Calendar birthdateCalendar;
    Spinner title;
    ArrayAdapter<String> titleAdapter;
    ArrayList<String> titles;
    RelativeLayout loading;
    Button save,delete;
    String errorMessage;
    AlertDialog deleteDialog, backDialog;
    // constants

    public static int IDENTITY_MODIFIED = 1 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_identity_detail);
        ui = (UserIdentity) getIntent().getSerializableExtra("user_identity");
        mode = getIntent().getStringExtra("mode");
        titles = new ArrayList<String>(Arrays.asList("Mr", "Mrs", "Ms"));
        titleAdapter = new ArrayAdapter<String>(UserIdentityDetail.this,R.layout
                .support_simple_spinner_dropdown_item,titles);
        birthdateCalendar = Calendar.getInstance();
        buildAlertDialog();
        setView();
        if(mode.equals("edit")){
            setDataToView();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_user_identity_detail);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                backDialog.show();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(UserIdentityDetail.this);
        builder.setMessage("Are you sure?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteData();
            }
        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
        deleteDialog = builder.create();
        builder.setMessage(getString(R.string.prompt_unsaved_change)).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
        backDialog = builder.create();
    }

    protected void setView(){
        loading=(RelativeLayout) findViewById(R.id.userIdentityDetailLoading
        );
        loading.setVisibility(View.GONE);
        title = (Spinner) findViewById(R.id.userIdentityDetailTitleSpinner);
        title.setAdapter(titleAdapter);
        first_name = (EditText) findViewById(R.id.userIdentityDetailFirstName);
        last_name = (EditText) findViewById(R.id.userIdentityDetailLastName);
        email = (EditText) findViewById(R.id.userIdentityDetailEmail);
        phone_number = (EditText) findViewById(R.id.userIdentityDetailPhoneNumber);
        birthdate = (EditText) findViewById(R.id.userIdentityDetailBirthDate);
        birthdate.setKeyListener(null);
        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendarDatePicker();
            }
        });
        identity_number = (EditText) findViewById(R.id.userIdentityDetailIdentityNumber);
        passport_number = (EditText) findViewById(R.id.userIdentityDetailPassportNumber);
        save = (Button) findViewById(R.id.userIdentityDetailSaveButton);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkData()) {
                    sendData();
                } else {
                    promptError(errorMessage);
                }
            }
        });
        delete = (Button) findViewById(R.id.userIdentityDetailDeleteButton);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.show();
            }
        });
        if(!mode.equals("edit")){
            delete.setVisibility(View.GONE);
        }
    }

    protected void setDataToView(){
        title.setSelection(titleAdapter.getPosition(ui.getTitle()));
        first_name.setText(ui.getFirst_name());
        last_name.setText(ui.getLast_name());
        phone_number.setText(ui.getPhone_number());
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        birthdate.setText(sdf.format(ui.getBirthdate()));
        identity_number.setText(ui.getId_number().equals("null") ? "" : ui.getId_number() );
        passport_number.setText(ui.getPassport_number().equals("null") ? "" : ui.getPassport_number() );
        email.setText(ui.getEmail().equals("null") ? "" : ui.getEmail());
    }

    protected boolean checkData(){
        Boolean clear = true;
        first_name.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        last_name.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        birthdate.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        phone_number.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        email.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        if(first_name.getText().length()<=0){
            first_name.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
            clear = false;
            errorMessage = getString(R.string.form_not_complete_error);
        }

        if(last_name.getText().length()<=0){
            last_name.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
            clear = false;
            errorMessage = getString(R.string.form_not_complete_error);
        }

        if(phone_number.getText().length()<=0){
            phone_number.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
            clear=false;
            errorMessage = getString(R.string.form_not_complete_error);
        }

        if(birthdate.getText().length()<=0){
            birthdate.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
            clear = false;
            errorMessage = getString(R.string.form_not_complete_error);
        };

        if(email.getText().length()>0 && !android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText()).matches()){
            email.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
            clear = false;
            errorMessage = getString(R.string.email_address_format_error);
        }

        if(identity_number.getText().length()<=0){
            identity_number.setBackground(getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            clear=false;
            errorMessage = getString(R.string.form_not_complete_error);
        }

        return clear;
    }

    protected void sendData(){
        String url = "";
        loading.setVisibility(View.VISIBLE);
        if(mode.equals("new")){
            url =  Util.BASE_URL+Util.API_GATEWAY+Util.USER_IDENTITY_PATH+"/";
        }
        else{
            url =  Util.BASE_URL+Util.API_GATEWAY+Util.USER_IDENTITY_PATH+"/edit";
        }
        HashMap<String,String> param = new HashMap<>();
        param.putAll(Util.getUserParam());
        param.put("title",title.getSelectedItem().toString());
        param.put("first_name", first_name.getText().toString());
        param.put("last_name", last_name.getText().toString());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        param.put("birthdate", sdf.format(birthdateCalendar.getTime()));
        param.put("phone_number", phone_number.getText().toString());
        param.put("email", email.getText().toString());
        param.put("id_number",identity_number.getText().toString());
        param.put("passport_number", passport_number.getText().toString());
        if(mode.equals("edit")){
            param.put("user_identity_id",String.valueOf(ui.getId()));
        }
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, new JSONObject
                (param), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.setVisibility(View.GONE);
                try {
                    Log.v("UserIdenError", new String(error.networkResponse.data,"UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                   promptError(getString(R.string.timeout_error_polite));
                } else if (error instanceof AuthFailureError) {
                    Util.forceLogInFragment(UserIdentityDetail.this);
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("X-Requested-With","XMLHttpRequest");
                return map;
            }
        };
        jor.setRetryPolicy(Constants.RETRY_POLICY);
        jor.setTag("UserIdentityDetail");
        AppController.getInstance().addToRequestQueue(jor);

    }

    protected void deleteData(){
        HashMap<String,String> param = new HashMap<>();
        param.putAll(Util.getUserParam());
        param.put("user_identity_id", String.valueOf(ui.getId()));
        String url = Util.BASE_URL + Util.API_GATEWAY + Util.USER_IDENTITY_PATH + "/delete";
        Log.v("deleteurl", url);
        Log.v("deleteparam", param.toString());
        loading.setVisibility(View.VISIBLE);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, new JSONObject
                (param), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loading.setVisibility(View.GONE);
                setResult(1);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("UserIdenError",error.getMessage());
            }
        });
        jor.setRetryPolicy(Constants.RETRY_POLICY);
        jor.setTag("UserIdentityDetail");
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected void promptError(String error){
        android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                (UserIdentityDetail.this);
        adb.setMessage(error);
        adb.setTitle(getString(R.string.error));
        adb.setPositiveButton(R.string.ok, null);
        adb.create().show();
    }


    public void showCalendarDatePicker(){
        dpb = CalendarDatePickerDialogFragment
                .newInstance(UserIdentityDetail.this, birthdateCalendar.get(Calendar
                                .YEAR),
                        birthdateCalendar.get(Calendar.MONTH), birthdateCalendar.get
                                (Calendar
                                        .DAY_OF_MONTH));
        dpb.show(getSupportFragmentManager(), "title");
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        birthdateCalendar.set(Calendar.YEAR,year);
        birthdateCalendar.set(Calendar.MONTH,monthOfYear);
        birthdateCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        birthdate.setText(sdf.format(birthdateCalendar.getTime()));
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests("UserIdentityDetail");
    }
}
