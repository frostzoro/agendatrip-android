package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import adapters.HotelAdapter;
import helper.RangeSeekBarDialogFragment;
import model.Hotel;
import model.HotelSearch;
import singleton.AppController;
import utils.Util;

public class HotelSearchResult extends AppCompatActivity {
    ArrayList<Hotel> hotelArrayList;
    AlertDialog dialog,sortDialog;
    RangeSeekBarDialogFragment filterDialog;
    Button sortButton,filterButton;
    HotelSearch hs;
    int currentPage,maxPage,dialogSelection;
    TextView loadMoreButton,searchQuery,searchDate;
    ListView hotelListView;
    HotelAdapter hotelAdapter;
    String extraParam;
    RelativeLayout loading;
    double maxPrice,minPrice;
    Boolean filtering;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_search_result);
        receiveDataFromIntent();
        buildAlertDialog();
        bindViewToVariables();
        initializeSupportVariables();
        setListeners();
        sendDataRequest(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.hotel_search_result);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void receiveDataFromIntent(){
        hs = (HotelSearch) getIntent().getSerializableExtra("hotelsearch");
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setPositiveButton("OK",null).setCancelable(false);
        dialog = b.create();
        //below code is for sorting the hotels list
        AlertDialog.Builder sortBuilder = new AlertDialog.Builder(this);
        sortBuilder.setTitle(getString(R.string.sort_hotel_by)).setSingleChoiceItems(R.array
                .sort_hotel_array, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogSelection = which;
            }
        }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch(dialogSelection){
                    case 0 : extraParam = "sort=popular"; sendDataRequest(false);
                        break;
                    case 1 : extraParam = "sort=priceasc"; sendDataRequest(false);
                        break;
                    case 2 : extraParam = "sort=pricedesc"; sendDataRequest(false);
                        break;
                    case 3 : extraParam = "sort=starasc"; sendDataRequest(false);
                        break;
                    case 4 : extraParam = "sort=stardesc"; sendDataRequest(false);
                        break;
                }
            }
        });
        sortDialog = sortBuilder.create();
        filterDialog = new RangeSeekBarDialogFragment();

    }

    protected void bindViewToVariables(){
        hotelListView = (ListView) findViewById(R.id.hotelSearchResultListView);
        loading = (RelativeLayout) findViewById(R.id.loading);
        sortButton = (Button) findViewById(R.id.extraSortButton);
        filterButton = (Button) findViewById(R.id.extraFilterButton);
        searchQuery = (TextView) findViewById(R.id.hotelSearchResultQuery);
        searchDate = (TextView) findViewById(R.id.hotelSearchResultDate);
    }

    protected void initializeSupportVariables(){
        filtering=false;
        extraParam="";
        minPrice = 0.0;
        maxPrice = 100000000.0;
        LayoutInflater inflater =  LayoutInflater.from(this);
        loadMoreButton = (TextView) inflater.inflate(R.layout.partial_load_more,null);
        hotelArrayList = new ArrayList<>();
        hotelAdapter = new HotelAdapter(HotelSearchResult.this,R.layout.partial_hotel,
                hotelArrayList);
        hotelListView.setAdapter(hotelAdapter);
        searchQuery.setText(hs.getQuery());
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MM-yyyy");
        searchDate.setText(sdf.format(hs.getCheckIn().getTime()) + " - " + sdf
                .format(hs
                .getCheckOut().getTime()));
    }

    protected void setListeners(){
        loadMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDataRequest(true);
            }
        });
        sortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortDialog.show();
            }
        });
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.show(getSupportFragmentManager(), "filterdialog");
                filterDialog.setRange(minPrice, maxPrice);
            }
        });
        filterDialog.setOnDialogDismissalListener(new RangeSeekBarDialogFragment.OnDialogDismissalListener() {
            @Override
            public void onDialogDismissal(Number min, Number max) {
                Log.v("inact", "hotelsearch");
                minPrice = min.doubleValue();
                maxPrice = max.doubleValue();
                filtering = true;
                sendDataRequest(false);
            }
        });
        hotelListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToNextStep(hotelAdapter.getItem(position));
            }
        });
    }
    protected void sendDataRequest(final Boolean append){

        hotelListView.removeFooterView(loadMoreButton);
        loading.setVisibility(View.VISIBLE);
        if(!append)
            currentPage = 0 ;
        String url = Util.BASE_URL + Util.API_GATEWAY + Util.BOOKING_URL + "/hotels/searchHotel?";
        if(filtering){
            url+="&minprice="+Math.floor(minPrice)+"&maxprice="+Math.floor(maxPrice)+"&";
        }
        url += hs.toGetParam()+"&page="+(currentPage+1)+"&"+extraParam;
        Log.v("url is", url);
        Log.v("hotelsearchresult", "request sent");
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.v("resp",response.getJSONObject("search_queries").toString());
                    JSONArray hotels = response.getJSONObject("results").getJSONArray("result");
                    int sz = hotels.length();
                    if(!append){
                        hotelArrayList.clear();
                        currentPage=1;
                    }
                    if(!filtering){
                        minPrice = response.getJSONObject("search_queries").getDouble("minprice");
                        maxPrice = response.getJSONObject("search_queries").getDouble("maxprice");
                    }
                    for(int i=0;i<sz;i++){
                        Hotel h = new Hotel();
                        h.setbreadCrumb(hotels.getJSONObject(i));
                        hotelArrayList.add(h);
                    }
                    hotelAdapter.notifyDataSetChanged();
                    // check pagination
                    currentPage =  response.getJSONObject("pagination").getInt
                            ("current_page");
                    maxPage = response.getJSONObject("pagination").getInt("lastPage");
                    if(currentPage>=maxPage) {

                    }
                    else
                        hotelListView.removeFooterView(loadMoreButton);
                } catch (JSONException e) {
                    dialog.setMessage(getString(R.string.error_no_hotel));
                    dialog.show();
                }
                loading.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                loading.setVisibility(View.GONE);
                error.printStackTrace();
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    dialog.setMessage(getString(R.string.timeout_error));
                    dialog.show();
                }
                else{
                    dialog.setMessage(getString(R.string.error_unknown));
                    dialog.show();
                }
            }
        });
        jor.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jor.setTag("HotelSearchResult");
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected void goToNextStep(Hotel h){
        Intent i = new Intent(HotelSearchResult.this,HotelDetail.class);
        Bundle mBundle = new Bundle();
        mBundle.putSerializable("hotelsearch",hs);
        mBundle.putSerializable("hotel",h);
        i.putExtras(mBundle);
        startActivity(i);
    }
}
