package com.example.android.AgendaTrip;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gc.materialdesign.views.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.FlightSearch;
import model.TrainSearch;
import utils.Util;

public class TrainPaymentInformationBank extends AppCompatActivity {
    TextView destination,flightname,date,time,subTotal,total,orderID,adultDetail,childDetail,
            infantDetail;
    LinearLayout paymentSteps;
    JSONObject paymentInformation;
    JSONArray steps;
    TrainSearch fs;
    Double totals;
    Button continueButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_information_bank);
        try {
             paymentInformation = new JSONObject(getIntent().getStringExtra
                    ("payment_information"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        fs = (TrainSearch) getIntent().getSerializableExtra("trainsearch");
        totals=0.0;
        bindViews();
        bindDataToView();

    }

    protected void bindViews(){
        destination = (TextView) findViewById(R.id.paymentInformationBankFlightDestination);
        flightname = (TextView) findViewById(R.id.paymentInformationBankFlightNumber);
        date = (TextView) findViewById(R.id.paymentInformationBankFlightDate);
        time = (TextView) findViewById(R.id.paymentInformationBankFlightTime);
        subTotal = (TextView) findViewById(R.id.paymentInformationBankSubtotal);
        total = (TextView) findViewById(R.id.paymentInformationBankTotal);
        paymentSteps = (LinearLayout) findViewById(R.id.paymentInformationBankSteps);
        adultDetail = (TextView) findViewById(R.id.paymentInformationBankPriceAdultDetail);
        childDetail = (TextView) findViewById(R.id.paymentInformationBankPriceChildDetail);
        infantDetail = (TextView) findViewById(R.id.paymentInformationBankPriceInfantDetail);
        orderID = (TextView) findViewById(R.id.paymentInformationBankOrderID);
        continueButton = (Button) findViewById(R.id.paymentInformationBankContinueButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
    }

    protected void bindDataToView(){
        try {
            Log.v("jsonobjekpayment", paymentInformation.toString());
            JSONArray orders = paymentInformation.getJSONObject("result").getJSONArray("orders");
            JSONObject order = orders.getJSONObject(0);
            // if hotel_icon
            if(paymentInformation.getJSONObject("result").getJSONArray("order_types").getString(0)
                    .equals("train")){
                destination.setText(order.getString("order_name"));
                flightname.setText(order.getString("order_name_detail"));
                date.setText("tanggal");
                SimpleDateFormat in = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                SimpleDateFormat out = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
                Date dep,arr;
                dep = in.parse(order.getString("departure_datetime"));
                arr = in.parse(order.getString("arrival_datetime"));
                time.setText(out.format(dep) + " - " + out.format(arr));
                childDetail.setVisibility(View.GONE);
                infantDetail.setVisibility(View.GONE);

                adultDetail.setText(fs.getAdult()+" x Adult x " + Util
                       .convertDoubleToIDRString(fs.departure_flight.getPrice_adult()));

                if(fs.getChild()!=0){
                    childDetail.setText(fs.getChild() +" x Child x " + Util
                            .convertDoubleToIDRString(fs.departure_flight.getPrice_child()));
                }
                else{
                    childDetail.setVisibility(View.GONE);
                }

                if(fs.getInfant()!=0){
                    infantDetail.setText(fs.getInfant() +" x Infant x " + Util
                            .convertDoubleToIDRString(fs.departure_flight.getPrice_child()));
                }
                else{
                    infantDetail.setVisibility(View.GONE);
                }
                Double totalDouble = 0.0;
                totalDouble += fs.getAdult() * fs.departure_flight
                        .getPrice_adult();
                totalDouble += fs.getChild() * fs.departure_flight
                        .getPrice_child();
                totalDouble += fs.getInfant() * fs.departure_flight
                        .getPrice_infant();
                subTotal.setText(Util.convertDoubleToIDRString(totalDouble));
                totals+=totalDouble;
                orderID.setText(paymentInformation.getJSONObject("result").getString("order_id"));
            }
            // if hotel_icon
            else if(true) {

            }
            total.setText(totals+"");
            steps = paymentInformation.getJSONArray("steps");
            int sz = steps.length();
            Log.v("sizeofstepscentral", String.valueOf(sz));
            for(int i=0;i<sz;i++){
                LinearLayout ll = new LinearLayout(this);
                ll.setPadding(Math.round(getResources().getDimension(R.dimen.padding)),Math.round(getResources().getDimension(R.dimen.padding)),Math.round(getResources().getDimension(R.dimen.padding)),Math.round(getResources().getDimension(R.dimen.padding)));
                ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
                ll.setOrientation(LinearLayout.VERTICAL);
                JSONArray stepStrings = steps.getJSONObject(i).getJSONArray("step");
                int sza = stepStrings.length();
                Log.v("sizeofstepscentral1", String.valueOf(sza));
                TextView cap = new TextView(this);
                cap.setText(steps.getJSONObject(i).getString("name"));
                cap.setTextColor(getResources().getColor(R.color.black));
                cap.setTextSize(getResources().getDimension(R.dimen.header_s));
                ll.addView(cap);
                for(int j=0;j<sza;j++){
                    TextView t = new TextView(this);
                    t.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
                    t.setText(stepStrings.getString(j));
                    t.setTextColor(getResources().getColor(R.color.black));
                    t.setTextSize(getResources().getDimensionPixelSize(R.dimen.font_s));
                    //Log.v("text = ",stepStrings.getString(j));
                    ll.addView(t);
                }
                paymentSteps.addView(ll);
            }
            Log.v("childcountpaystep", String.valueOf(paymentSteps.getChildCount()));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
