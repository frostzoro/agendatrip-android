package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import model.VacationPackagePurchase;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class VacationPackagePurchaseDetail extends AppCompatActivity {
    AlertDialog dialog;
    Button payButton,VPDetailButton;
    ImageView VPImage;
    int vppid;
    Intent vpDetail,paymentIntent;
    RelativeLayout loading, content;
    TextView orderHeader,VPName,VPHeader,orderAmount,orderStatus,orderDate,referenceCode;
    VacationPackagePurchase vpp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacation_package_purchase_detail);
        receiveDataFromIntent();
        buildAlertDialog();
        bindViewToVariables();
        initializeSupportVariables();
        bindListeners();
        getData();
        Log.v("vppid is", String.valueOf(vppid));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests("VacationPackagePurchaseDetail");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Checkout");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getData();
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(getString(R.string.error));
        adb.setPositiveButton(getString(R.string.ok),null);
        dialog = adb.create();
    }

    protected void receiveDataFromIntent(){
        vppid = getIntent().getIntExtra("vppid",-1);
    }

    protected void bindViewToVariables(){
        orderHeader = (TextView) findViewById(R.id.vacationPackagePurchaseDetailOrderHeader);
        VPName = (TextView) findViewById(R.id.vacationPackagePurchaseDetailVPName);
        VPHeader = (TextView) findViewById(R.id.vacationPackagePurchaseDetailVPHeader);
        orderDate = (TextView) findViewById(R.id.vacationPackagePurchaseDetailOrderDate);
        orderAmount = (TextView) findViewById(R.id.vacationPackagePurchaseDetailOrderAmount);
        orderStatus = (TextView) findViewById(R.id.vacationPackagePurchaseDetailOrderStatus);
        referenceCode = (TextView) findViewById(R.id.vacationPackagePurchaseDetailReferenceCode);
        VPImage = (ImageView) findViewById(R.id.vacationPackagePurchaseDetailVPImage);
        payButton = (Button) findViewById(R.id.vacationPackagePurchaseDetailPayButton);
        VPDetailButton = (Button) findViewById(R.id.vacationPackagePurchaseDetailVPDetailButton);
        loading = (RelativeLayout) findViewById(R.id.loading);
        content = (RelativeLayout) findViewById(R.id.vacationPackagePurchaseDetailContent);
    }

    protected void setData(){
        orderHeader.setText("Order #"+vpp.getId());
        VPName.setText(vpp.getVp().getName());
        VPHeader.setText(vpp.getVp().getHeader());
        orderDate.setText(vpp.getPurchaseDateAsString());
        orderAmount.setText(Util.convertDoubleToIDRString(vpp.getPrice()));
        orderStatus.setText(vpp.getStatus());
        referenceCode.setText(vpp.getReference_code());
        VPImage.setLayoutParams(new LinearLayout.LayoutParams(VPImage.getWidth(),VPImage.getWidth()*9/16));
        Picasso.with(this).load(Util.BASE_URL + Util.VACATION_PACKAGE_IMAGE_URL + vpp.getVp().getImage()).placeholder(getResources().getDrawable(R.drawable.agenda_placeholder_1)).into(VPImage);
        Log.v("vpimagesize", VPImage.getWidth() + " & " + VPImage.getHeight());
    }

    protected void initializeSupportVariables(){
        vpDetail = new Intent(VacationPackagePurchaseDetail.this,VacationPackageDetail.class);
        vpDetail.putExtra("vacation_package",String.valueOf(vppid));
        paymentIntent = new Intent(VacationPackagePurchaseDetail.this,WebviewActivity.class);
        paymentIntent.putExtra("url", Util.PAYMENT_URL + "?order_id=" + vppid + "&order_type=package");
    }

    protected void bindListeners(){
        payButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivityForResult(paymentIntent, 0);
            }
        });
        VPDetailButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(vpDetail);
            }
        });
    }

    protected void getData(){
        String url = Util.VACATION_PACKAGE_PURCHASE_DETAIL_URL + "?" + "vacation_package_purchase_id=" + vppid + "&" + Util.paramToString(Util.getUserParam());
        content.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    vpp = new VacationPackagePurchase(response.getJSONObject("payload"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setData();
                Log.v("vppid", String.valueOf(vpp.getId()));
                content.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.setMessage(getString(R.string.timeout_error_polite));
                loading.setVisibility(View.GONE);
            }
        });
        jor.setRetryPolicy(Constants.RETRY_POLICY_BOOKING);
        jor.setTag("VacationPackagePurchaseDetail");
        AppController.getInstance().addToRequestQueue(jor);
    }



}
