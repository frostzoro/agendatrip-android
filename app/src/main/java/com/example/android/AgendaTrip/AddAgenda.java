package com.example.android.AgendaTrip;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

//import utils.RegAgendaDatePickerDialog;
import singleton.AppController;
import utils.Constants;
import utils.RegAgendaHeaderTimePickerDialog;
import utils.Util;

/**
 * Created by Vin on 10/27/2015.
 */
public class AddAgenda extends AppCompatActivity implements CalendarDatePickerDialogFragment.OnDateSetListener,android.app.TimePickerDialog.OnTimeSetListener {
    CalendarDatePickerDialogFragment dpb;
    EditText namaAgenda;
    Button tombolSubmit;
    Context con;
    boolean startFlag=false;
    TextView startDate,endDate,hiddenStart,hiddenEnd;
    String dateTemp,timeTemp,sentTemp,sentTimeTemp;
    ProgressDialog pDialog;
    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

        Calendar temp = Calendar.getInstance();
        dateTemp="";
        temp.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        temp.set(Calendar.MONTH,monthOfYear);
        temp.set(Calendar.YEAR, year);

        SimpleDateFormat destFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());


        sentTemp=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        Log.v("tesbulan",sentTemp+" - "+monthOfYear);
        dateTemp=destFormat.format(temp.getTime());
        Log.v("formattedmonth",dateTemp);


    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute){
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        Calendar temp=Calendar.getInstance();
        temp.set(Calendar.HOUR_OF_DAY, hour);
        temp.set(Calendar.MINUTE,minute);
        timeTemp="";
        SimpleDateFormat sentsdf=new SimpleDateFormat("hh:mm:ss");
        sentTimeTemp=sentsdf.format(temp.getTime());
        timeTemp=sdf.format(temp.getTime());

        if(startFlag){
            startDate.setText(dateTemp+" "+timeTemp);
            hiddenStart.setText(sentTemp+" "+sentTimeTemp);

        }
        else{endDate.setText(dateTemp+" "+timeTemp);
            hiddenEnd.setText(sentTemp+" "+sentTimeTemp);
        }


    }
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_agenda);

        pDialog=new ProgressDialog(this);
        pDialog.setMessage("Please wait");
        pDialog.setCancelable(false);

        con=this.getApplicationContext();
        namaAgenda=(EditText) findViewById(R.id.AgendaName);
        tombolSubmit=(Button) findViewById(R.id.addAgendaSubmitBtn);
        startDate=(TextView) findViewById(R.id.selectedAgendaStart);
        endDate=(TextView) findViewById(R.id.selectedAgendaEnd);
        hiddenStart=(TextView) findViewById(R.id.hiddenStart);
        hiddenEnd=(TextView)findViewById(R.id.hiddenEnd);
        tombolSubmit.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                RetrieveData();
            }
        });

    }


    public void RetrieveData()    {
        Calendar tempcall1=Calendar.getInstance(),tempcall2=Calendar.getInstance();
        final SimpleDateFormat sdfin=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        final SimpleDateFormat sdfout=new SimpleDateFormat("MMM dd, yyyy hh:mm aa");
        Date tempcal1=null, tempcal2=null;


        try {
            tempcal1 = sdfin.parse(hiddenStart.getText().toString());
            tempcal2=sdfin.parse(hiddenEnd.getText().toString());
            tempcall1.setTime(tempcal1);
            tempcall2.setTime(tempcal2);
        }
        catch (Exception e){
            e.printStackTrace();

        }

        
        if(!namaAgenda.equals("") && !startDate.equals("") && !endDate.equals("")) {
if(tempcall2.after(tempcall1)){

            String nameParam = ((EditText) findViewById(R.id.AgendaName)).getText().toString();
            Log.v("coba isi hidden", hiddenStart.getText().toString() + " " + hiddenEnd.getText().toString());
            CreateAgenda(nameParam, hiddenStart.getText().toString(), hiddenEnd.getText().toString());}
            else{
    Toast.makeText(AddAgenda.this, "Your End Date can't be earlier than your Start Date", Toast.LENGTH_SHORT).show();
}
        }
   
        else{
            Toast.makeText(AddAgenda.this, "Please fill all fields", Toast.LENGTH_SHORT).show();
        }
    }

    public void setStartDate(View v){
        dpb = CalendarDatePickerDialogFragment
                .newInstance(AddAgenda.this, Calendar.getInstance().get(Calendar
                                .YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get
                                (Calendar
                                        .DAY_OF_MONTH));
        MonthAdapter.CalendarDay c = new MonthAdapter.CalendarDay(Calendar.getInstance());
        dpb.setDateRange(c, null);
        dpb.show(getSupportFragmentManager(), "title");


    }

    public void setStartTime(View v){
        startFlag=true;
        RegAgendaHeaderTimePickerDialog agendaTimePickerDialog=new RegAgendaHeaderTimePickerDialog();
        agendaTimePickerDialog.show(getFragmentManager(), "tagtime");
        setStartDate(v);
    }

    public void setEndDate(View v){

        dpb = CalendarDatePickerDialogFragment
                .newInstance(AddAgenda.this, Calendar.getInstance().get(Calendar
                                .YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get
                                (Calendar
                                        .DAY_OF_MONTH));
        MonthAdapter.CalendarDay c = new MonthAdapter.CalendarDay(Calendar.getInstance());
        dpb.setDateRange(c, null);
        dpb.show(getSupportFragmentManager(), "title");

    }
    public void setEndTime(View v){
        startFlag=false;
        RegAgendaHeaderTimePickerDialog agendaTimePickerDialog=new RegAgendaHeaderTimePickerDialog();
        agendaTimePickerDialog.show(getFragmentManager(), "tagtime");
        setEndDate(v);
    }

    public void CreateAgenda(final String name, String start, String end){
//jalanin json buat create agenda ke server
        pDialog.show();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date c=new Date();

        try{
        c= sdf.parse(start);}
        catch(Exception e){
           Log.v("asd",e.getMessage());
        }
        final Calendar cal=Calendar.getInstance();
        cal.setTime(c);

        String url="http://agendatrip.com/api/v1/agenda/create";
        Map<String,String> params= new HashMap<String,String>();
        params.put("userID", Util.loggedID);
        params.put("key", Util.loggedKey);
        params.put("name", name);
        params.put("startdate", start);
        params.put("enddate",end);
        Log.v("cobapostman", Util.loggedID+" "+Util.loggedKey);
        Log.v("cstart",start+" "+end);
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>(){
            @Override
        public void onResponse(JSONObject response){
                pDialog.dismiss();
            Log.v("dataresp", response.toString());
                try{

                    JSONObject payload=response.getJSONObject("payload");
                    String createdAgendaID=payload.getString("agenda_id");
                    Log.v("AGENDA ID CREATED",createdAgendaID);
                    setAlarm(name, Integer.parseInt(createdAgendaID), cal);

                    Toast.makeText(getApplicationContext(), "New Agenda Created!", Toast.LENGTH_SHORT).show();
                    Intent i =new Intent(getApplicationContext(),MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
                catch(Exception e){ e.printStackTrace();}
            }


        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                            android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                                    (AddAgenda.this);
                            adb.setMessage(getString(R.string.timeout_error));
                            adb.setTitle(getString(R.string.error));
                            adb.setPositiveButton(R.string.ok, null);
                            adb.create().show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ParseError) {
                            Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                            //TODO
                        }

                    }
                });
                    jsObjRequest.setRetryPolicy(Constants.RETRY_POLICY);
                    AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    public void setAlarm(String agendaname,int notifid, Calendar cal ){

        Log.v("masukk", "masukk");

//        c.set(Calendar.MINUTE,c.get(Calendar.MINUTE)+1);
        //REAL YANG HARUS DIIMPLEMENTASIKAN
        Long alertTime=cal.getTimeInMillis();

        //WAKTU CONTOH UNTUK SIMULASI AJA
        //Long  alertTime=Calendar.getInstance().getTimeInMillis()+5*1000;
        Intent alertIntent=new Intent(getApplicationContext(), AlertReceiver.class);

        alertIntent.putExtra("loggedID", Util.loggedID + "");
        alertIntent.putExtra("loggedKey", Util.loggedKey);
        alertIntent.putExtra("notifnumber",1);
        alertIntent.putExtra("notifid",notifid);
        alertIntent.putExtra("agendaname",agendaname);
        AlarmManager alarmManager= (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alertTime, PendingIntent.getBroadcast(getApplicationContext(), notifid, alertIntent, PendingIntent.FLAG_UPDATE_CURRENT));

    }
}
