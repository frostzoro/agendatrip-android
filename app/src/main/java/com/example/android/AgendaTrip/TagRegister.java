package com.example.android.AgendaTrip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import adapters.TagsAdapter;
import model.VacationSiteTag;
import utils.Util;

/**
 * Created by Vin on 12/13/2015.
 */
public class TagRegister extends Activity {
    JSONArray arraytag;
    JSONObject tags;
static boolean[] statusarray=new boolean[300];
    public static ArrayList<String> selectedTags;
    Button finishBtn;
    ProgressDialog pDialog;
    ArrayList<VacationSiteTag> vst;
    String jsonResponse;
    Activity a;
    ListView lv;
    TagsAdapter tagsAdapter;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        selectedTags=new ArrayList<String>();

        setContentView(R.layout.tagregister);

        pDialog=new ProgressDialog(TagRegister.this);
        vst=new ArrayList<>();
        lv=(ListView) findViewById(R.id.TagListView);
        RequestTags();
    }


    public void RequestTags(){
     final Context context=getApplicationContext();
        String url="http://www.agendatrip.com/api/v1/tags/get?userID="+Util.loggedID+"&key="+Util.loggedKey;
        showpDialog();

        RequestQueue requestQueue= Volley.newRequestQueue(context);

        CustomRequest jsObjRequest=new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            try{
                hidepDialog();
                JSONArray payload=new JSONArray(response.getString("payload"));
                jsonResponse="";
                for(int i=0;i<payload.length();i++){
                    JSONObject satuanpayload=payload.getJSONObject(i);
                    VacationSiteTag a=new VacationSiteTag(satuanpayload);
                    a.setName(satuanpayload.getString("name"));
                    a.setId(satuanpayload.getInt("id"));
                    vst.add(a);
                }

                tagsAdapter=new TagsAdapter(TagRegister.this,R.layout.tags_adapter,vst);
                lv.setAdapter(tagsAdapter);

            }

            catch(Exception e){
e.printStackTrace();
            }

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            hidepDialog();
            }
        });
        requestQueue.add(jsObjRequest);
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void ClickFinish(View v){

        if(selectedTags.size()==0){
            Toast.makeText(TagRegister.this, "Choose Tags First", Toast.LENGTH_SHORT).show();
        }
        else{
        GatherData();}
    }

    public void GatherData(){
 /*       CheckBox cb;
        TextView idtag;
        for(int i=0;i<lv.getChildCount();i++){
            cb=(CheckBox) lv.getChildAt(i).findViewById(R.id.tagsbox);
            idtag=(TextView) lv.getChildAt(i).findViewById(R.id.tagsid);
            if(cb.isChecked()){
                selectedTags.add(idtag.getText().toString());
            }
            else{selectedTags.remove(idtag.getText().toString());}
        }*/
        Log.v("Size ST",selectedTags.size()+"");
        arraytag=new JSONArray();
        arraytag.put(selectedTags);
        Map<String, String> params = new HashMap<String, String>();
        params.put("key",Util.loggedKey);
        params.put("userID",Util.loggedID);
        for(int i=0;i<selectedTags.size();i++){
        params.put("tags["+i+"]",selectedTags.get(i));
        Log.v("isinya ya", selectedTags.get(i));
        }
        final String url="http://www.agendatrip.com/api/v1/tags/new";
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        CustomRequest customRequest=new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.v("SUKSES", response.toString());
                SharedPreferences sp=getApplicationContext().getSharedPreferences("userInfo",Context.MODE_PRIVATE);
                SharedPreferences.Editor ed=sp.edit();
                ed.putString("newid","");
                ed.commit();
                Toast.makeText(TagRegister.this, "Done! You are ready to go!", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (getApplicationContext());
                    adb.setMessage(getString(R.string.timeout_error_lastsync));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();}

                else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext().getApplicationContext(), "Auth Error", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext().getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext().getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });
requestQueue.add(customRequest);


}

    }
