package com.example.android.AgendaTrip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import helper.APIinterface.GooglePlace;
import helper.model.GoogleLocation;
import singleton.AppController;

/**
 * Created by Vin on 12/1/2015.
 */
public class MapShowing extends AppCompatActivity {
    double longitude, latitude;
    String placename;
    GoogleMap googleMap;
    TextView tvDistanceDuration;
    ProgressDialog pd;
    PlaceAutocompleteFragment autoComplete;
    GoogleLocation origin,destination;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapshowing);
        tvDistanceDuration = (TextView) findViewById(R.id.tv_distance_time);
        autoComplete= (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autoComplete.setHint(getString(R.string.googlemap_autocomplete_hint));
        autoComplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                origin = new GoogleLocation(place.getName().toString(),place.getLatLng());
                drawOriginOnMap();
            }

            @Override
            public void onError(Status status) {
                Log.v("MapShowing", "Autocomplete error !");
            }
        });

        placename=getIntent().getExtras().getString("name");
        longitude=getIntent().getExtras().getDouble("longitude");
        latitude=getIntent().getExtras().getDouble("latitude");
        destination = new GoogleLocation(placename,new LatLng(latitude,longitude));

        pd=new ProgressDialog(MapShowing.this);

        pd.setMessage("Getting Location");
        pd.setCancelable(false);

        googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setMyLocationEnabled(true);

        Marker TP = googleMap.addMarker(new MarkerOptions().position(destination.getLatlon()).title(placename));
        TP.showInfoWindow();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(destination.getLatlon(), 11.0f));


        pd.hide();


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return true;
         }

    private String getDirectionsUrl(LatLng dest,LatLng origin){

        String str_origin = "origin="+origin.latitude+","+origin.longitude;
        String str_dest = "destination="+dest.latitude+","+dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin+"&"+str_dest+"&"+sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    public void drawOriginOnMap(){
        if(origin!=null){
            googleMap.clear();

            Marker TP = googleMap.addMarker(new MarkerOptions().position(destination.getLatlon()).title(placename));
            TP.showInfoWindow();
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(destination.getLatlon(), 11.0f));

            Marker TP2 = googleMap.addMarker(new MarkerOptions().position(origin.getLatlon()).title(origin.getName()));
            TP2.showInfoWindow();
            final String url = getDirectionsUrl(origin.getLatlon(),destination.getLatlon());
            StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    new ParserTask().execute(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error){

                }
            });
            AppController.getInstance().addToRequestQueue(sr);
        }
        else{
            Toast.makeText(MapShowing.this, "Invalid Origin", Toast.LENGTH_SHORT).show();
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
        public ParserTask() {
            super();
        }
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionJSONParser parser = new DirectionJSONParser();

                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Ambil distance dari list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Ambil duration dari list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                    Log.v("number points",points.size()+"");
                }

                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(getResources().getColor(R.color.bootstrapblue));
            }
            tvDistanceDuration.setText("Distance:" + distance + ", Duration:" + duration);

            googleMap.addPolyline(lineOptions);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(destination.getLatlon(), 11.0f));
        }
    }

}

