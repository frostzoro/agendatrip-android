/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.AgendaTrip;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import adapters.NavDrawerListAdapter;
import model.NavDrawerItem;
import singleton.AppController;
import utils.Util;


public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] navMenuTitles;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    int paramfragid;
    private TypedArray navMenuIcons;
    private  LinearLayout mDrawerLinear;
    private TextView UName;

    private ImageView dPicture;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.gpsHandler.startUpdates();
        setContentView(R.layout.activity_main);

        checkLoggedIn();


        paramfragid=getIntent().getIntExtra("fragmentid",0);
        mTitle = mDrawerTitle = getTitle();

        UName=(TextView) findViewById(R.id.username);
        dPicture=(ImageView) findViewById(R.id.displayPicture);
        if(Util.loggedKey!="" || Util.loggedID!=""){
            navMenuIcons=getResources().obtainTypedArray(R.array.nav_drawer_icons_loggedin);
            navMenuTitles = getResources().getStringArray(R.array.menu_array_loggedin);
            UName.setText("Welcome " + String.valueOf(Util.loggedUserName));

        }
        else{
            navMenuIcons=getResources().obtainTypedArray(R.array.nav_drawer_icons);
            navMenuTitles =getResources().getStringArray(R.array.menu_array);
            UName.setText("Welcome Guest!");
            dPicture.setImageResource(R.drawable.edp);
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLinear= (LinearLayout) findViewById(R.id.drawer);

        navDrawerItems=new ArrayList<NavDrawerItem>();
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
    if(Util.isLoggedIn()) {
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
    }
        navMenuIcons.recycle();

        mDrawerList.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.yellow)));
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        adapter=new NavDrawerListAdapter(getApplicationContext(),navDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close
        ) {

            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("User Menu");
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        if (savedInstanceState == null) {
            selectItem(0);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.gpsHandler.stop();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        AppController.gpsHandler.startUpdates();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerLinear);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch(item.getItemId()) {

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        Intent i;

        Fragment fragment = null;
        if(paramfragid==3){fragment=new MyWishlistFragment();paramfragid=-1;}
        else if(paramfragid==1){fragment=new MyAgendaFragment();paramfragid=-1;}
        else {
            switch (position) {
                case 0:
                    fragment = new HomeFragment();
                    break;
                case 1:
                    if(!Util.isLoggedIn()){fragment=new Login();}
                    else{fragment = new MyAgendaFragment();}
                    break;
                case 2:
                    if(!Util.isLoggedIn()){fragment=new Login();}
                    else{fragment = new MyBookedItemFragment();}
                    break;
                case 3:
                    fragment = new MyVacationPackagesFragment();
                    break;
                case 4:
                    fragment = new MyWishlistFragment();
                    break;
                case 5:
                    fragment = new MyIdentityFragment();
                    break;
                case 6:

                    if (Util.loggedID != "" && Util.loggedKey != "") {
                        fragment = new Logout();
                        break;
                    } else {
                        fragment = new Login();
                        break;
                    }
            }
        }

        Bundle args = new Bundle();
        args.putInt(myFragment.ARG_NUMBER, position);
        fragment.setArguments(args);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        mDrawerList.setItemChecked(position, true);
        setTitle(navMenuTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerLinear);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    public static class myFragment extends Fragment {
        public static final String ARG_NUMBER = "arg";

        public myFragment() {
        }


    }

    void checkLoggedIn(){
        Context context=getApplicationContext();
        SharedPreferences sharedPreference=context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        Util.loggedKey=sharedPreference.getString("key","");
        Util.loggedID=sharedPreference.getString("userid","");
        Util.loggedUserName=sharedPreference.getString("username","");

    }

    public void IntentToRegister(View v){
        Log.v("asd", "asd");
        Intent i = new Intent(this,Register.class);
        startActivity(i);

    }

}
