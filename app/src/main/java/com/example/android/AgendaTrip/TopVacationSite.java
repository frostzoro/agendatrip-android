package com.example.android.AgendaTrip;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import adapters.TopVacationSiteListAdapter;
import model.VacationSite;
import pl.droidsonroids.gif.GifImageView;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class TopVacationSite extends AppCompatActivity {
    ListView listView;
    GifImageView loading;
    ArrayList<VacationSite> vacationSiteArrayList;
    TopVacationSiteListAdapter  vacationSiteAdapter;
    Boolean idle;
    Button filter,sort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_vacation_site);
        vacationSiteArrayList = new ArrayList<VacationSite>();
        vacationSiteAdapter = new TopVacationSiteListAdapter(TopVacationSite.this,R.layout
                .top_vacation_site_partial,vacationSiteArrayList);
        setContentView(R.layout.activity_top_vacation_site);
        //Toolbar t = (Toolbar)findViewById(R.id.menuBackActionBar);
        // setActionbars();
        setView();
        // registerBackActivity();
        idle=true;
        getVacationSites(0);
    }

    public void setView(){
        listView = (ListView) findViewById(R.id.topVacationSiteListView);
        listView.setAdapter(vacationSiteAdapter);
        loading = (GifImageView) findViewById(R.id.topVacationSiteLoading);
        filter = (Button) findViewById(R.id.topVacationSiteFilter);
        filter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

            }
        });
        sort = (Button) findViewById(R.id.topVacationSiteSort);
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int[] selection = new int[1];
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(TopVacationSite.this);
                alertDialog.setTitle(R.string.sort_package_text).setSingleChoiceItems(R.array
                        .sort_top_site_array, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v("which is ",String.valueOf(which));
                        selection[0] = which;
                    }
                }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch(selection[0]) {
                            case 0: vacationSiteAdapter.sortItem(vacationSiteAdapter
                                    .SORT_BY_PREFERENCE);
                                break;
                            case 1:
                                vacationSiteAdapter.sortItem(vacationSiteAdapter
                                        .SORT_BY_POPULARITY);
                                break;
                        }
                    }
                });
                AlertDialog d = alertDialog.create();
                d.show();
                Log.v("topvacationsite","clicked");
            }
        });
    }
/*
    public void setActionbars(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.menu_back);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
    }
*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.top_vacation_sites);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getVacationSites(int type){
        if(idle){
            idle=false;
            vacationSiteArrayList.clear();
            loading.setVisibility(View.VISIBLE);
            String requestUrl;
            HashMap<String,String> param = new HashMap<>();
            if(type==0)
                requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util.VACATION_SITE_PATH +
                        "/top";
            else if(type==1){
                requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util.VACATION_SITE_PATH +
                        "/recommended";
                }
            else{
                Toast.makeText(getApplicationContext(),"You need to be logged in.",Toast.LENGTH_LONG).show();
                idle=true;
                loading.setVisibility(View.GONE);
                return;
            }
            if(Util.isLoggedIn())
                param.putAll(Util.getUserParam());
            JsonObjectRequest vsRequest = new JsonObjectRequest(Request.Method.POST, requestUrl,
                    new JSONObject(param), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    idle=true;
                    loading.setVisibility(View.GONE);
                    try {
                        //Log.v("datass", response.toString());
                        JSONArray array = response.getJSONArray("payload");
                        //Log.v("datass", array.toString());
                        int resultSize = array.length();
                        Log.v("arris",array.toString());
                        for (int i = 0; i < resultSize; i++) {
                            JSONObject vs = array.getJSONObject(i);
                            VacationSite vacationSite = new VacationSite(vs);
                            vacationSiteArrayList.add(vacationSite);

                            Log.v("hypis", String.valueOf(vacationSite.getHypotheses()));
                        }

                        vacationSiteAdapter.notifyDataSetChanged();
                        Log.v("success !", "nearby data get");
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.v("getVacationSite", e.getStackTrace().toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    idle=true;
                    loading.setVisibility(View.GONE);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                        android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                                (TopVacationSite.this);
                        adb.setMessage(getString(R.string.timeout_error_polite));
                        adb.setTitle(getString(R.string.error));
                        adb.setPositiveButton(R.string.ok, null);
                        adb.create().show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_LONG).show();
                        //TODO
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                        //TODO
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                        //TODO
                    }
                }
            });
            vsRequest.setRetryPolicy(Constants.RETRY_POLICY);
            vsRequest.setTag("TopVacationSite");
            AppController.getInstance().addToRequestQueue(vsRequest);
        }
    }
}