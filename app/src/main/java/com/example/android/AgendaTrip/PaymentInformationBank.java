package com.example.android.AgendaTrip;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.FlightSearch;
import model.HotelSearch;
import model.TrainSearch;
import utils.Util;

public class PaymentInformationBank extends AppCompatActivity {
    TextView destination,flightname,date,time,subTotal,total,orderID,adultDetail,childDetail,
            infantDetail;

    TextView hotelOrderName,hotelName,roomName,hotelOrderDetail,trainname,trainnumber,traindate,traintime,trainadultDetail,trainchildDetail,traininfantDetail;
    LinearLayout paymentSteps;
    LinearLayout flight,hotel,train;
    JSONObject paymentInformation;
    JSONArray steps;
    FlightSearch fs;
    HotelSearch hs;
    TrainSearch ts;
    Double totals;
    Button continueButton,veritransPayButton;
    Intent paymentIntent;
    int bookingID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_information_bank);
        try {
             paymentInformation = new JSONObject(getIntent().getStringExtra
                    ("payment_information"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        fs = (FlightSearch) getIntent().getSerializableExtra("flightsearch");
        hs = (HotelSearch) getIntent().getSerializableExtra("hotelsearch");
        ts = (TrainSearch) getIntent().getSerializableExtra("trainsearch");
        totals=0.0;
        bookingID = 0 ;
        bindViews();
        bindDataToView();
    }

    protected void bindViews(){
        // FLIGHTS
        flight = (LinearLayout) findViewById(R.id.paymentInformationBankOrderDetailFlight);
        destination = (TextView) findViewById(R.id.paymentInformationBankFlightDestination);
        flightname = (TextView) findViewById(R.id.paymentInformationBankFlightNumber);
        date = (TextView) findViewById(R.id.paymentInformationBankFlightDate);
        time = (TextView) findViewById(R.id.paymentInformationBankFlightTime);
        adultDetail = (TextView) findViewById(R.id.paymentInformationBankPriceAdultDetail);
        childDetail = (TextView) findViewById(R.id.paymentInformationBankPriceChildDetail);
        infantDetail = (TextView) findViewById(R.id.paymentInformationBankPriceInfantDetail);

        // HOTEL
        hotel = (LinearLayout) findViewById(R.id.paymentInformationBankOrderDetailHotel);
        hotelOrderName = (TextView) findViewById(R.id.paymentInformationBankHotelOrderName);
        hotelName = (TextView) findViewById(R.id.paymentInformationBankHotelName);
        roomName = (TextView) findViewById(R.id.paymentInformationBankRoomName);
        hotelOrderDetail = (TextView) findViewById(R.id.paymentInformationBankHotelOrderDetail);

        // TRAINZ
        train = (LinearLayout) findViewById(R.id.paymentInformationBankHotelOrderDetailTrain);
        trainname=(TextView) findViewById(R.id.paymentInformationBankTrainName2);
        trainnumber=(TextView) findViewById(R.id.paymentInformationBankTrainNumber);
        traindate=(TextView) findViewById(R.id.paymentInformationBankTrainDate);
        traintime=(TextView)findViewById(R.id.paymentInformationBankTrainTime);
        trainadultDetail=(TextView) findViewById(R.id.paymentInformationBankPriceTrainAdultDetail);
        trainchildDetail=(TextView) findViewById(R.id.paymentInformationBankPriceTrainChildDetail);
        traininfantDetail=(TextView) findViewById(R.id.paymentInformationBankPriceTrainInfantDetail);
        subTotal = (TextView) findViewById(R.id.paymentInformationBankSubtotal);
        total = (TextView) findViewById(R.id.paymentInformationBankTotal);
        paymentSteps = (LinearLayout) findViewById(R.id.paymentInformationBankSteps);
        orderID = (TextView) findViewById(R.id.paymentInformationBankOrderID);

        //Buttons
        continueButton = (Button) findViewById(R.id.paymentInformationBankContinueButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        veritransPayButton = (Button) findViewById(R.id.veritransButton);
        veritransPayButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                paymentIntent = new Intent(PaymentInformationBank.this,WebviewActivity.class);
                paymentIntent.putExtra("url", Util.PAYMENT_URL + "?order_id=" + bookingID + "&order_type=booking");
                startActivity(paymentIntent);
            }
        });
    }

    protected void bindDataToView(){
        try {
            Log.v("jsonobjekpayment", paymentInformation.toString());
            JSONArray orders = paymentInformation.getJSONObject("result").getJSONArray("orders");
            JSONObject order = orders.getJSONObject(0);
            //<editor-fold desc="if flight">
            // if hotel_icon
            bookingID = paymentInformation.getInt("booking_id");
            if(paymentInformation.getJSONObject("result").getJSONArray("order_types").getString(0)
                    .equals("flight")){
                flight.setVisibility(View.VISIBLE);
                hotel.setVisibility(View.GONE);
                train.setVisibility(View.GONE);
                Log.v("in flight","fff");
                destination.setText(order.getString("order_name"));
                flightname.setText(order.getString("order_name_detail"));
                date.setText(order.getString("flight_date"));
                SimpleDateFormat in = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                SimpleDateFormat out = new SimpleDateFormat("hh:mm");
                Date dep,arr;
                dep = in.parse(order.getString("departure_time"));
                arr = in.parse(order.getString("arrival_time"));
                time.setText(out.format(dep) + " - " + out.format(arr));
                adultDetail.setText(order.getString("adult") + " x Adult x " + Util
                        .convertDoubleToIDRString(fs.departure_flight.getPrice_adult()));
                if(!order.getString("child").equals("0")){
                    childDetail.setText(order.getString("child") +" x Child x " + Util
                            .convertDoubleToIDRString(fs.departure_flight.getPrice_child()));
                }
                else{
                    childDetail.setVisibility(View.GONE);
                }

                if(!order.getString("infant").equals("0")){
                    infantDetail.setText(order.getString("infant") +" x Infant x " + Util
                            .convertDoubleToIDRString(fs.departure_flight.getPrice_child()));
                }
                else{
                    infantDetail.setVisibility(View.GONE);
                }
                Double totalDouble = 0.0;
                totalDouble += Integer.parseInt(order.getString("adult")) * fs.departure_flight
                        .getPrice_adult();
                totalDouble += Integer.parseInt(order.getString("child")) * fs.departure_flight
                        .getPrice_child();
                totalDouble += Integer.parseInt(order.getString("infant")) * fs.departure_flight
                        .getPrice_infant();
                subTotal.setText(Util.convertDoubleToIDRString(totalDouble));
                totals+=totalDouble;
                Log.v("out flight","fff");
            }
            //</editor-fold>

            // if order is hotel
            else if(paymentInformation.getJSONObject("result").getJSONArray("order_types").getString(0)
                    .equals("hotel")) {
                flight.setVisibility(View.GONE);
                hotel.setVisibility(View.VISIBLE);
                train.setVisibility(View.GONE);
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                Log.v("pib",hs.toString());
                Log.v("pib",hs.getGuest());
                Log.v("pib", hs.getRoom());
                Log.v("pds",hotelOrderName.toString());
                Log.v("pds",hs.getCheckIn().getTime().toString());
                Log.v("pds",hs.getCheckOut().getTime().toString());
                hotelOrderName.setText(sdf.format(hs.getCheckIn().getTime()) + " - " + sdf.format(hs.getCheckOut().getTime()) + ", " + hs.getRoom() + " Room(s), " + hs.getGuest() + " Guest(s)");
                hotelName.setText(hs.getH().breadCrumb.getName());
                roomName.setText(hs.getR().getName());
                hotelOrderDetail.setText(hs.getRoom() + "Room(s) x " + hs.getStayLength() + " Night(s) x" +Util.convertDoubleToIDRString(hs.getR().getPrice()));
                Double totalDouble = 0.0;
                totalDouble = hs.getStayLength() * hs.getR().getPrice();
                subTotal.setText(Util.convertDoubleToIDRString(totalDouble));
                totals = totalDouble;
            }
            else if(paymentInformation.getJSONObject("result").getJSONArray("order_types").getString(0)
                    .equals("train")) {
                SimpleDateFormat in = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                SimpleDateFormat out = new SimpleDateFormat("MMM dd, yyyy - hh:mm");
                Date dep=in.parse(order.getString("departure_datetime"));
                Date arr=in.parse(order.getString("arrival_datetime"));
                String sDep=out.format(dep);
                String sArr=out.format(arr);
                Log.v("sDep",sDep);
                flight.setVisibility(View.GONE);
                hotel.setVisibility(View.GONE);
                train.setVisibility(View.VISIBLE);
                trainname.setText(order.getString("order_name"));
                trainnumber.setText(order.getString("order_name_detail"));
                traindate.setText("Departure: "+ sDep+" - Arrival: "+sArr);

                trainadultDetail.setText(ts.getAdult() + " x Adult x " + Util
                        .convertDoubleToIDRString(ts.departure_flight.getPrice_adult()));
                Double totalDouble=0.0;
                totalDouble=ts.getAdult()*ts.departure_flight.getPrice_adult();
                subTotal.setText(Util.convertDoubleToIDRString(totalDouble));
                totals=totalDouble;
            }
            orderID.setText(paymentInformation.getJSONObject("result").getString("order_id"));
            total.setText(Util.convertDoubleToIDRString(totals));
            /*
            steps = paymentInformation.getJSONArray("steps");
            int sz = steps.length();
            Log.v("sizeofstepscentral", String.valueOf(sz));
            for(int i=0;i<sz;i++){
                LinearLayout ll = new LinearLayout(this);
                ll.setPadding(Math.round(getResources().getDimension(R.dimen.padding)),Math.round(getResources().getDimension(R.dimen.padding)),Math.round(getResources().getDimension(R.dimen.padding)),Math.round(getResources().getDimension(R.dimen.padding)));
                ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
                ll.setOrientation(LinearLayout.VERTICAL);
                JSONArray stepStrings = steps.getJSONObject(i).getJSONArray("step");
                int sza = stepStrings.length();
                Log.v("sizeofstepscentral1", String.valueOf(sza));
                TextView cap = new TextView(this);
                cap.setText(steps.getJSONObject(i).getString("name"));
                cap.setTextColor(getResources().getColor(R.color.black));
                cap.setTextSize(getResources().getDimension(R.dimen.header_s));
                ll.addView(cap);
                for(int j=0;j<sza;j++){
                    TextView t = new TextView(this);
                    t.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
                    t.setText(stepStrings.getString(j));
                    t.setTextColor(getResources().getColor(R.color.black));
                    t.setTextSize(getResources().getDimensionPixelSize(R.dimen.font_s));
                    //Log.v("text = ",stepStrings.getString(j));
                    ll.addView(t);
                }
                paymentSteps.addView(ll);
            }
            Log.v("childcountpaystep", String.valueOf(paymentSteps.getChildCount()));*/

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
