package com.example.android.AgendaTrip;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import model.Hotel;
import model.HotelSearch;


public class HotelDetailGeneral extends Fragment {
    TextView hotelName,hotelAddress;
    RatingBar hotelRatingBar;
    HotelSearch hs;
    Hotel h;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiveDataFromIntent();
    }

    protected void receiveDataFromIntent(){
        Bundle b = this.getArguments();
        hs = (HotelSearch) b.getSerializable("hotelsearch");
        h = (Hotel) b.getSerializable("hotel");
    }

    protected void setDataFromIntent(){
        hotelName.setText(h.breadCrumb.getName());
        hotelAddress.setText(h.getAddress());
        hotelRatingBar.setRating(Float.valueOf(h.breadCrumb.getStar()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_hotel_detail_general, container, false);
        hotelName = (TextView) v.findViewById(R.id.hotelDetailName);
        hotelAddress = (TextView) v.findViewById(R.id.hotelDetailAddress);
        hotelRatingBar = (RatingBar) v.findViewById(R.id.hotelDetailRatingBar);
        setDataFromIntent();
        return v;
    }




}
