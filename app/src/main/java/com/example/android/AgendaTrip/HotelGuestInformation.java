package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.cast.Cast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;

import model.HotelSearch;
import model.UserIdentity;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class HotelGuestInformation extends AppCompatActivity {
    AlertDialog dialog;
    ArrayAdapter<String> conAdapter, guestAdapter, identityAdapter;
    ArrayList<String> titles, userIdentityString;
    ArrayList<UserIdentity> userIdentityArrayList;
    Button continueButton;
    CheckBox forOtherPerson;
    EditText conFirstName,conLastName,conEmail,conPhone,guestFirstName,guestLastName,
            guestPhone;
    HotelSearch hs;
    LinearLayout guestForm;
    RelativeLayout loading;
    ScrollView mainLayout;
    Spinner conTitle,guestTitle,conIdentity;
    TextView identityLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_guest_information);
        receiveDataFromIntent();
        bindViewToVariables();
        buildAlertDialog();
        initializeSupportVariables();
        setListeners();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests("HotelGuestInformation");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.hotel_booking);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void receiveDataFromIntent(){
        hs = (HotelSearch) getIntent().getSerializableExtra("hotelsearch");
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setPositiveButton("OK", null);
        dialog = ad.create();
    }

    protected void bindViewToVariables(){
        identityLabel = (TextView) findViewById(R.id.contactIdentityLabel);
        conIdentity = (Spinner) findViewById(R.id.contactIdentity);
        conFirstName = (EditText) findViewById(R.id.contactFirstName);
        conLastName = (EditText) findViewById(R.id.contactLastName);
        conEmail = (EditText) findViewById(R.id.contactEmailAddress);
        conPhone = (EditText) findViewById(R.id.contactPhoneNumber);
        guestFirstName = (EditText) findViewById(R.id.guestFirstName);
        guestLastName = (EditText) findViewById(R.id.guestLastName);
        guestPhone = (EditText) findViewById(R.id.guestPhoneNumber);
        conTitle = (Spinner) findViewById(R.id.contactTitle);
        guestTitle = (Spinner) findViewById(R.id.guestTitle);
        continueButton = (Button) findViewById(R.id.continueButton);
        forOtherPerson = (CheckBox) findViewById(R.id.bookForOtherPerson);
        guestForm = (LinearLayout) findViewById(R.id.hotelGuestInformationGuestForm);
        loading = (RelativeLayout) findViewById(R.id.loading);
        mainLayout = (ScrollView) findViewById(R.id.userFormLayout);
    }

    protected void initializeSupportVariables(){
        loading.setVisibility(View.VISIBLE);
        mainLayout.setVisibility(View.GONE);
        titles = new ArrayList<>(Arrays.asList("Mr","Mrs","Ms"));
        conAdapter = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,
                titles);
        guestAdapter = new ArrayAdapter<String>(this,R.layout
                .support_simple_spinner_dropdown_item,titles);
        conTitle.setAdapter(conAdapter);
        guestTitle.setAdapter(guestAdapter);
        guestForm.setVisibility(View.GONE);
        if(Util.isLoggedIn())
            getUserIdentities();
        else{
            conIdentity.setVisibility(View.GONE);
            loading.setVisibility(View.GONE);
            mainLayout.setVisibility(View.VISIBLE);
            identityLabel.setVisibility(View.GONE);
        }

    }

    protected void getUserIdentities(){
        userIdentityArrayList = new ArrayList<>();
        userIdentityString = new ArrayList<>();
        String url = Util.BASE_URL + Util.API_GATEWAY + Util.USER_IDENTITY_PATH;
        url += "?" + Util.paramToString(Util.getUserParam());
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray payload = null;
                try {
                    payload = response.getJSONArray("payload");
                    int sz = payload.length();
                    for(int i=0; i<sz; i++){
                        userIdentityArrayList.add(new UserIdentity(payload.getJSONObject(i)));
                        userIdentityString.add(userIdentityArrayList.get(i).getFirst_name() + " " + userIdentityArrayList.get(i).getLast_name());
                    }
                    loading.setVisibility(View.GONE);
                    mainLayout.setVisibility(View.VISIBLE);
                    initializeIdentityAdapter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        jor.setRetryPolicy(Constants.RETRY_POLICY);
        jor.setTag("HotelGuestInformation");
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected void initializeIdentityAdapter(){
        identityAdapter = new ArrayAdapter<String>(HotelGuestInformation.this,R.layout.support_simple_spinner_dropdown_item,userIdentityString);
        conIdentity.setAdapter(identityAdapter);
        conIdentity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                UserIdentity selectedIdentity = userIdentityArrayList.get(position);
                conFirstName.setText(selectedIdentity.getFirst_name());
                conLastName.setText(selectedIdentity.getLast_name());
                conEmail.setText(selectedIdentity.getEmail().equals("null") ? "" : selectedIdentity.getEmail());
                conPhone.setText(selectedIdentity.getPhone_number());
                conTitle.setSelection(titles.indexOf(selectedIdentity.getTitle()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    protected void setListeners(){
        forOtherPerson.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    guestForm.setVisibility(View.VISIBLE);
                else
                    guestForm.setVisibility(View.GONE);
            }
        });
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFormDatas()) {
                    sendOrder();
                }
            }
        });
    }

    protected Boolean checkFormDatas(){
        Boolean clear = true;

        conFirstName.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        conLastName.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        conEmail.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        conPhone.setBackground(getResources().getDrawable(R.drawable.rectangle_border));

        guestFirstName.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        guestLastName.setBackground(getResources().getDrawable(R.drawable.rectangle_border));
        guestPhone.setBackground(getResources().getDrawable(R.drawable.rectangle_border));

        if(conFirstName.getText().length()<=0){
            clear=false;
            dialog.setMessage(getString(R.string.form_not_complete_error));
            conFirstName.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
        }
        if(conLastName.getText().length()<=0){
            clear=false;
            dialog.setMessage(getString(R.string.form_not_complete_error));
            conLastName.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
        }
        if(conEmail.getText().length()<=0){
            clear=false;
            dialog.setMessage(getString(R.string.form_not_complete_error));
            conEmail.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
        }

        if(conPhone.getText().length()<=0){
            clear=false;
            dialog.setMessage(getString(R.string.form_not_complete_error));
            conPhone.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
        }

        if(guestFirstName.getText().length()<=0 && forOtherPerson.isChecked()){
            clear=false;
            dialog.setMessage(getString(R.string.form_not_complete_error));
            guestFirstName.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
        }
        if(guestLastName.getText().length()<=0 && forOtherPerson.isChecked()){
            clear=false;
            dialog.setMessage(getString(R.string.form_not_complete_error));
            guestLastName.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
        }
        if(guestPhone.getText().length()<=0 && forOtherPerson.isChecked()){
            clear=false;
            dialog.setMessage(getString(R.string.form_not_complete_error));
            guestPhone.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
        }

        if(!clear) dialog.show();
        return clear;
    }

    protected String getUserInfo(){
        String param = "";
        Boolean forOther = forOtherPerson.isChecked();
        param+= "conSalutation=" + conTitle.getSelectedItem();
        param+= "&conFirstName=" + conFirstName.getText();
        param+= "&conLastName=" + conLastName.getText();
        param+= "&conEmailAddress=" + conEmail.getText();
        param+= "&conPhone=" + conPhone.getText();
        param+= "&salutation=" + (forOther ? guestTitle.getSelectedItem() : conTitle.getSelectedItem());
        param+= "&firstName=" + (forOther ? guestFirstName.getText() : conFirstName.getText());
        param+= "&lastName=" + (forOther ? guestLastName.getText() : conLastName.getText());
        param+= "&phone=" +(forOther ? guestPhone.getText() : conPhone.getText());
        param+= "&country=ID";
        return param;
    }

    protected void sendOrder(){
        loading.setVisibility(View.VISIBLE);
        String url = Util.BASE_URL + Util.API_GATEWAY + Util.BOOKING_URL + "/hotels/addOrder?";
        url+= "bookUri=" + URLEncoder.encode(hs.getR().getBookUri());
        url+= "&"+getUserInfo();
        url+= "&token="+hs.getToken();
        Log.v("hotelguest",url);
        if(Util.isLoggedIn()){
            url+="&"+Util.paramToString(Util.getUserParam());
        }
        if(hs.getAgenda_id()!=-1){
            url+="&agenda_id="+hs.getAgenda_id();
        }
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject resp = response.getJSONObject("diagnostic");
                        loading.setVisibility(View.GONE);
                            if(resp.has("confirm")&&resp.getString("confirm").equals("success")){
                                Intent i = new Intent(getApplicationContext(),PaymentInformationBank.class);
                                Bundle b = new Bundle();
                                b.putString("payment_information",response.toString());
                                b.putSerializable("hotelsearch", hs);
                                i.putExtras(b);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                            }
                            else
                            {
                                if(resp.has("error_msgs")){
                                    dialog.setMessage(resp.getString("error_msgs"));
                                    dialog.show();
                                }
                                else{
                                    Log.v("unknown error",resp.toString());
                                    dialog.setMessage(getString(R.string.error_unknown));
                                    dialog.show();
                                }
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        loading.setVisibility(View.GONE);
                        if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {

                            dialog.setMessage(getString(R.string.timeout_error));
                            dialog.show();
                        }
                        else{
                            dialog.setMessage(getString(R.string.error_unknown));
                            dialog.show();
                        }
                    }
                });
        jor.setRetryPolicy(new DefaultRetryPolicy(
                305000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jor.setTag("HotelGuestInformation");
        AppController.getInstance().addToRequestQueue(jor);
    }
}
