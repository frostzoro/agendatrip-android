package com.example.android.AgendaTrip;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.net.URLDecoder;

import adapters.IconTextViewAdapter;
import ext.ExpandableHeightGridView;
import model.Hotel;
import model.HotelSearch;


public class HotelDetailDescription extends Fragment {
    WebView hotelDescriptionHeader,hotelDescription;
    TextView hotelFacHeader,roomFacHeader,sportFacHeader;
    HotelSearch hs;
    Hotel h;
    Bundle b;
    ExpandableHeightGridView hotelFac,roomFac,sportFac;
    IconTextViewAdapter hotelAd,roomAd,sportAd;
    public HotelDetailDescription() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = this.getArguments();
        hs = (HotelSearch) b.getSerializable("hotelsearch");
        h = (Hotel) b.getSerializable("hotel");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_hotel_detail_description, container, false);
        hotelDescription = (WebView) v.findViewById(R.id.hotelDetailDescription);
        hotelDescriptionHeader = (WebView) v.findViewById(R.id.hotelDetailDescriptionHeader);
        hotelFacHeader = (TextView) v.findViewById(R.id.hotelDetailDescriptionHotelFacilityHeader);
        roomFacHeader = (TextView) v.findViewById(R.id.hotelDetailDescriptionRoomFacilityHeader);
        sportFacHeader = (TextView) v.findViewById(R.id.hotelDetailDescriptionSportFacilityHeader);
        hotelFac = (ExpandableHeightGridView) v.findViewById(R.id.hotelDetailDescriptionHotelFacilityGridView);
        roomFac = (ExpandableHeightGridView) v.findViewById(R.id.hotelDetailDescriptionRoomFacilityGridView);
        sportFac = (ExpandableHeightGridView) v.findViewById(R.id.hotelDetailDescriptionSportFacilityGridView);
        setViewFromVariables();
        return v;
    }
    protected void setViewFromVariables(){
        hotelFac.setExpanded(true);
        roomFac.setExpanded(true);
        sportFac.setExpanded(true);
        hotelDescriptionHeader.loadData("About " + h.breadCrumb.getName() + " :","text/html","utf-8");
        hotelDescription.loadData(URLDecoder.decode(h.getDescription()),"text/html","utf-8");
        hotelAd = new IconTextViewAdapter(getActivity(),R.layout.partial_facility_item,R.drawable
                .check_circle_black_hdpi,h
                .getHotelFacilities());
        hotelFac.setAdapter(hotelAd);
        roomAd = new IconTextViewAdapter(getActivity(),R.layout.partial_facility_item,R.drawable
                .check_circle_black_hdpi,h
                .getRoomFacilities());
        roomFac.setAdapter(roomAd);
        if(h.getSportFacilities().size()>0){
            sportAd = new IconTextViewAdapter(getActivity(),R.layout.partial_facility_item,R.drawable
                    .check_circle_black_hdpi,h
                    .getSportFacilities());
        }
        else {
            sportFacHeader.setVisibility(View.GONE);
            sportFac.setVisibility(View.GONE);
        }
    }

}
