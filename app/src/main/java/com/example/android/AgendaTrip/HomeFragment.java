package com.example.android.AgendaTrip;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import singleton.AppController;

/**
 * Created by Vin on 10/3/2015.
 */
public class HomeFragment extends Fragment
{
    ImageButton search,nearby,top,vacationpackage,booking;
    EditText searchText;
    View rootview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getInstance().gpsHandler.startUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().gpsHandler.startUpdates();
    }

    @Override
    public void onPause() {
        super.onPause();
        AppController.getInstance().gpsHandler.stop();
    }

    @Override
    public void onStop() {
        super.onStop();
        AppController.getInstance().gpsHandler.stop();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootview=inflater.inflate(R.layout.home, container, false);
        setViews();
        return rootview;
    }


    void setViews(){
        searchText = (EditText) rootview.findViewById(R.id.homeFragmentSearchText);
        search = (ImageButton) rootview.findViewById(R.id.homeFragmentSearchButton);
        search.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String keyword = searchText.getText().toString();
                if(keyword.length()<=0){
                    Toast.makeText(getActivity(),"Please enter a keyword",Toast.LENGTH_LONG).show();
                }
                else {
                    Intent i = new Intent(getActivity(), SearchByKeywordVacationSite.class);
                    i.putExtra("keyword", keyword);
                    getActivity().startActivity(i);
                    getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);
                }
            }
        });

        nearby = (ImageButton) rootview.findViewById(R.id.homeFragmentNearbyButton);
        nearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AppController.getInstance().gpsHandler.getCurrentLocation() == null){
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (getActivity());
                    adb.setMessage(getString(R.string.gps_off_error));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok,null);
                    adb.create().show();
                }
                else {
                    Intent i = new Intent(getActivity(), NearbyVacationSite.class);
                    getActivity().startActivity(i);
                    getActivity().overridePendingTransition(R.anim.abc_slide_in_top, R.anim
                            .abc_slide_out_bottom);
                }
            }
        });

        top = (ImageButton) rootview.findViewById(R.id.homeFragmentTopButton);
        top.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),TopVacationSite.class);
                getActivity().startActivity(i);
                getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top);
            }
        });

        vacationpackage = (ImageButton) rootview.findViewById(R.id.homeFragmentVacationPackageButton);
        vacationpackage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getActivity(),VacationPackageList.class);
                getActivity().startActivity(i);
                getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom,R.anim
                        .abc_slide_out_top);
            }
        });

        booking = (ImageButton) rootview.findViewById(R.id.homeFragmentBookingButton);
        booking.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),BookingSelection.class);
                getActivity().startActivity(i);
                getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom,R.anim
                        .abc_slide_out_top);
            }
        });
    }

}