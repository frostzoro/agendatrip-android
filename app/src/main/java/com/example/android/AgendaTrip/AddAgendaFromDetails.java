package com.example.android.AgendaTrip;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;


import adapters.AgendaFromVSDetailsAdapter;
import data.DatabaseHandler;
import model.Agenda;
import utils.Constants;
import utils.Util;

/**
 * Created by Vin on 10/3/2015.
 */
public class AddAgendaFromDetails extends AppCompatActivity {
    TextView lastUpdated;
    ListView listView;
    AgendaFromVSDetailsAdapter agendaAdapter;

    private ProgressDialog pDialog;
    private static String TAG = MainActivity.class.getSimpleName();
    private String urlJsonArry = "http://api.androidhive.info/volley/person_array.json";
    private String jsonResponse,lastUpdateString;
    private TextView txtResponse;
    private DatabaseHandler dba;
    String tgl,vsname;
    ImageButton btnCreateAgenda, btnRefreshAgenda;
    int vsid;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_agenda);
        vsid=getIntent().getIntExtra("vsid",-1);
        vsname=getIntent().getStringExtra("vsname");
        SharedPreferences sharedPreference=getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        tgl=sharedPreference.getString("agendaDBSyncDate", "");
        Context context=getApplicationContext();


        lastUpdated=(TextView) findViewById(R.id.lastUpdated);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        agendaRequest();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_agenda, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

      if( id == R.id.menu_refresh_agenda){
            RefreshData();
        }
        return super.onOptionsItemSelected(item);
    }

    private void agendaRequest() {
        final Context context=getApplicationContext();
        String url="http://agendatrip.com/api/v1/agenda?userID="+Util.loggedID+"&key="+Util.loggedKey;
        showpDialog();
        RequestQueue requestQueue = Volley.newRequestQueue(context);


        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                try {
                   JSONArray payload = new JSONArray(response.getString("payload"));
                    ArrayList<Agenda> agendaList=new ArrayList<>();

                  dba=new DatabaseHandler(context);
                  dba.clearDB();
                  dba.close();

                    jsonResponse="";
                    for(int i=0;i<payload.length();i++) {
                        JSONObject satuanPayload=payload.getJSONObject(i);
                        int agendaid = satuanPayload.getInt("id");
                        String userid = satuanPayload.getString("user_id");
                        String agendaname = satuanPayload.getString("name");
                        String startTime = satuanPayload.getString("start");
                        String endTime = satuanPayload.getString("end");

                        Agenda a=new Agenda(satuanPayload);
                        a.setUserid(userid);
                        a.setId(agendaid);
                        a.setName(agendaname);
                        a.setStartDate(startTime);
                        a.setEndDate(endTime);
                       // agendaList.add(a);
                        dba=new DatabaseHandler(context);
                        dba.addAgenda(a);
                        dba.close();



                    }

                    tgl = DateFormat.getDateTimeInstance().format(new Date());
                    SharedPreferences sharedPreference=getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreference.edit();
                    editor.putString("agendaDBSyncDate", tgl);
                    editor.apply();
                    lastUpdated.setText("Database Last Sync: "+tgl);

                    dba=new DatabaseHandler(context);
                    ArrayList<Agenda> agendasLocal=dba.getAgenda();
                    Log.v("SIZE Agenda Local di DB", " "+agendasLocal.size());
                    listView=(ListView) findViewById(R.id.AgendaListView);
                    agendaAdapter=new AgendaFromVSDetailsAdapter(AddAgendaFromDetails.this,R.layout.agenda_adapter,agendasLocal,vsid,vsname);
                    Log.v("vsid+ vsname di AAFD",vsid+" "+vsname);
                    listView.setAdapter(agendaAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Log.v("penandaonItemClick","BERHASILLLLLLLLL");
                            int myPosition = position;

                            String itemClickedId = listView.getItemAtPosition(myPosition).toString();
                            Toast.makeText(context, "Id Clicked" + itemClickedId, Toast.LENGTH_LONG).show();
                        }

                    });
                    jsonResponse+="payload";




                    hidepDialog();
                  } catch (JSONException e) {
                    e.printStackTrace();
                  /*  Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

              if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (context);
                    adb.setMessage(getString(R.string.timeout_error_lastsync));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                  Log.v("refreshed data", "FALSE");
                  lastUpdated.setText("Database Last Sync: "+tgl);
                  dba=new DatabaseHandler(context);
                  ArrayList<Agenda> agendasLocal=dba.getAgenda();
                  listView=(ListView) findViewById(R.id.AgendaListView);
                  agendaAdapter=new AgendaFromVSDetailsAdapter(AddAgendaFromDetails.this,R.layout.agenda_adapter,agendasLocal,vsid,vsname);
                  listView.setAdapter(agendaAdapter);
                  listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                      @Override
                      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                          Log.v("penandaonItemClick", "BERHASILLLLLLLLL");
                          int myPosition = position;

                          String itemClickedId = listView.getItemAtPosition(myPosition).toString();
                          Toast.makeText(context, "Id Clicked" + itemClickedId, Toast.LENGTH_LONG).show();
                      }

                  });

                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(context,"Session is over, please log in again",Toast.LENGTH_LONG).show();
                  Util.LogOut();
                  Util.forceLogInFragment(AddAgendaFromDetails.this);
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(context,"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(context,"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }


                hidepDialog();
                //Toast.makeText(getActivity().getApplicationContext(),
                //      error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                // hidepDialog();
            }
        });

        requestQueue.add(jsObjRequest);

    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void addAgenda(){
        Intent i=new Intent(this,AddAgenda.class);
        startActivity(i);
    }


    public void RefreshData(){

        Context context=getApplicationContext();
        String url="http://agendatrip.com/api/v1/agenda?userID="+Util.loggedID+"&key="+Util.loggedKey;
        showpDialog();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                //Log.v("dataresp", response.toString());
                Log.v(TAG, response.toString());
                Log.v("dataresp", response.toString() + Util.loggedKey);

                try {
                    //kode ini harus dipindahin ke fungsi baru khusus refresh DB ntar
                    JSONArray payload = new JSONArray(response.getString("payload"));
                    ArrayList<Agenda> agendaList=new ArrayList<>();
                    if(payload.length()>0){
                        dba=new DatabaseHandler(AddAgendaFromDetails.this);
                        dba.clearDB();
                        dba.close();}

                    jsonResponse="";
                    for(int i=0;i<payload.length();i++) {
                        JSONObject satuanPayload=payload.getJSONObject(i);
                        int agendaid = satuanPayload.getInt("id");
                        String userid = satuanPayload.getString("user_id");
                        String agendaname = satuanPayload.getString("name");
                        String startTime = satuanPayload.getString("start");
                        String endTime = satuanPayload.getString("end");

                        Agenda a=new Agenda(satuanPayload);
                        a.setUserid(userid);
                        a.setId(agendaid);
                        a.setName(agendaname);
                        a.setStartDate(startTime);
                        a.setEndDate(endTime);

                        agendaList.add(a);
                        dba=new DatabaseHandler(AddAgendaFromDetails.this);
                        dba.addAgenda(a);
                        dba.close();

                       /* jsonResponse += "AgendaID: " + agendaid + "\n\n";
                        jsonResponse += "AgendaName: " + agendaname + "\n\n";
                        jsonResponse += "UserID: " + userid + "\n\n";
                        jsonResponse += "Start Time: " + startTime + "\n\n";
                        jsonResponse += "End Time: " + endTime + "\n\n\n";*/

                    }


                    tgl = DateFormat.getDateTimeInstance().format(new Date());

                    SharedPreferences sharedPreference=getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreference.edit();
                    editor.putString("agendaDBSyncDate", tgl);
                    editor.apply();
                    lastUpdated.setText("Database Last Sync: "+tgl);
                    listView=(ListView) findViewById(R.id.AgendaListView);
                    agendaAdapter=new AgendaFromVSDetailsAdapter(AddAgendaFromDetails.this,R.layout.agenda_adapter,agendaList,vsid,vsname);
                    listView.setAdapter(agendaAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Log.v("penandaonItemClick","BERHASILLLLLLLLL");
                            int myPosition = position;

                            String itemClickedId = listView.getItemAtPosition(myPosition).toString();
                            Toast.makeText(getApplicationContext(), "Id Clicked" + itemClickedId, Toast.LENGTH_LONG).show();
                        }

                    });
                    jsonResponse+="payload";




                    hidepDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                  /*  Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

              if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (AddAgendaFromDetails.this);
                    adb.setMessage(getString(R.string.timeout_error));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
                hidepDialog();
                //Toast.makeText(getActivity().getApplicationContext(),
                //      error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                // hidepDialog();
            }
        });
        jsObjRequest.setRetryPolicy(Constants.RETRY_POLICY);
        requestQueue.add(jsObjRequest);

    }


}