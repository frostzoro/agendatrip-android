package com.example.android.AgendaTrip;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.WishListAdapter;
import model.VacationSite;
import utils.Util;

/**
 * Created by Vin on 10/3/2015.
 */
public class MyWishlistFragment extends Fragment
{
    View rootview;
    String jsonResponse;
    ListView listView;
    WishListAdapter vsadapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootview=inflater.inflate(R.layout.my_wishlist, container, false);

        WishlistRequest();

        return rootview;


    }

    public void WishlistRequest(){
        String url="http://agendatrip.com/api/v1/user/wish?userID="+ Util.loggedID+"&key="+Util.loggedKey;
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        Log.v("WishList","foo1");
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                try {
                    JSONArray payload = new JSONArray(response.getString("payload"));
                    ArrayList<VacationSite> vslist=new ArrayList<>();

                    Log.v("WishList","foo2");

                    jsonResponse="";
                    for(int i=0;i<payload.length();i++) {
                        Log.v("WishList","foorepeat");
                        JSONObject satuanPayload=payload.getJSONObject(i);
                        JSONObject satuanvs=satuanPayload.getJSONObject("vacation_site");
                        int wishid=satuanPayload.getInt("id");

                        VacationSite singleWish= new VacationSite(satuanvs);

                        vslist.add(new VacationSite(satuanvs));
                      /*  VacationSite vs=new VacationSite();
                        vs.setID(vsid);
                        vs.setAddress(vsaddress);
                        vs.setName(vsname);
                        vs.setLatitude(latitude);
                        vs.setLongitude(longitude);
                        vslist.add(vs);
                        Log.v("NAMA DAN ID TEMPAT",vsid+" | "+vsname);*/


                    }


                    listView=(ListView) getActivity().findViewById(R.id.WishListView);
                    vsadapter=new WishListAdapter(getActivity(),R.layout.wishlist_partial,vslist);

                    listView.setAdapter(vsadapter);

                    Log.v("WishList", "foo3");
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Log.v("penandaonItemClick","BERHASILLLLLLLLL");
                            int myPosition = position;

                            String itemClickedId = listView.getItemAtPosition(myPosition).toString();
                            Toast.makeText(getActivity().getApplicationContext(), "Id Clicked" + itemClickedId, Toast.LENGTH_LONG).show();
                        }

                    });
                    jsonResponse+="payload";




                } catch (JSONException e) {
                    e.printStackTrace();
                  /*  Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();*/
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (getActivity());
                    adb.setMessage(getString(R.string.timeout_error_lastsync));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                    Log.v("refreshed data", "FALSE");



                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getActivity().getApplicationContext(),"Session is over, please log in again",Toast.LENGTH_LONG).show();
                    Util.LogOut();
                    Util.forceLogInFragment(getActivity());
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getActivity().getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getActivity().getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }



                //Toast.makeText(getActivity().getApplicationContext(),
                //      error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                // hidepDialog();
            }
        });
        requestQueue.add(jsObjRequest);

    }

}
