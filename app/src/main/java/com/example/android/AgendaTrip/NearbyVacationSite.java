package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import adapters.NearbyVacationSiteListAdapter;
import model.VacationSite;
import pl.droidsonroids.gif.GifImageView;
import singleton.AppController;
import utils.Util;

public class NearbyVacationSite extends AppCompatActivity {
    AlertDialog dialog,sortDialog,filterDialog;
    ArrayList<VacationSite> vacationSiteArrayList;
    Boolean idle;
    Button sort,filter;
    HashMap<String,String> param;
    double distanceParam;
    int currentPage,maxPage;
    int sortSelection,filterSelection;
    ListView listView;
    Location userLocation;
    NearbyVacationSiteListAdapter vacationSiteAdapter;
    RelativeLayout loading;
    String requestUrl;
    SwipeRefreshLayout swipeContainer;
    TextView loadMoreButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setActionbar();
        vacationSiteArrayList = new ArrayList<VacationSite>();
        vacationSiteAdapter = new NearbyVacationSiteListAdapter(NearbyVacationSite.this,R.layout
                .nearby_vacation_site_partial,vacationSiteArrayList);
        setContentView(R.layout.activity_nearby_vacation_site);
        AppController.getInstance().gpsHandler.startUpdates();
        setView();
        buildAlertDialog();
        initializeSupportVariables();
        setListener();
        getVacationSites(false);
    }

    protected void buildAlertDialog(){
        //for regular dialog
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setPositiveButton(getResources().getString(R.string.ok),null);
        adb.setTitle(getResources().getString(R.string.error));
        dialog = adb.create();
        //for sort dialog
        AlertDialog.Builder sortDialogBuilder = new AlertDialog.Builder(NearbyVacationSite.this);
        sortDialogBuilder.setTitle(R.string.sort_site_text).setSingleChoiceItems(R.array
                .sort_nearby_site_array, sortSelection, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sortSelection = which;
            }
        }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (sortSelection) {
                    case 0:
                        vacationSiteAdapter.sortItem(vacationSiteAdapter.SORT_BY_PREFERENCE);
                        break;
                    case 1:
                        vacationSiteAdapter.sortItem(vacationSiteAdapter.SORT_BY_DISTANCE);
                        break;
                    case 2:
                        vacationSiteAdapter.sortItem(vacationSiteAdapter.SORT_BY_POPULARITY);
                        break;
                    default:
                        break;
                }
            }
        });
        sortDialog = sortDialogBuilder.create();
        // for filter dialog
        AlertDialog.Builder filterDialogBuilder = new AlertDialog.Builder(NearbyVacationSite.this);
        filterDialogBuilder.setTitle(R.string.sort_site_text).setSingleChoiceItems(R.array.filter_nearby_site_array, filterSelection, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                filterSelection = which;
            }
        }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (filterSelection) {
                    case 0:
                        distanceParam = 0.5f;
                        getVacationSites(false);
                        break;
                    case 1:
                        distanceParam = 1;
                        getVacationSites(false);
                        break;
                    case 2:
                        distanceParam = 3;
                        getVacationSites(false);
                        break;
                    case 3:
                        distanceParam = 5;
                        getVacationSites(false);
                        break;
                }
            }
        });
        filterDialog = filterDialogBuilder.create();
    }

    private void initializeSupportVariables(){
        param = new HashMap<>();
        idle=true;
        distanceParam = 10;
        currentPage=0;
        maxPage = 999;
        filterSelection = 1;
        sortSelection = 0;
        loadMoreButton = (TextView) getLayoutInflater().inflate(R.layout.partial_load_more, null);
    }

    public void setListener(){
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getVacationSites(false);
                swipeContainer.setRefreshing(false);
            }
        });
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortDialog.show();
            }
        });
        filter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                swipeContainer.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
        loadMoreButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getVacationSites(true);
            }
        });
    }

    public void setView(){
        listView = (ListView) findViewById(R.id.nearbyVacationSiteListView);
        listView.setAdapter(vacationSiteAdapter);
        loading = (RelativeLayout) findViewById(R.id.agendaLoading);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.nearbyVacationSiteSwipeContainer);
        sort = (Button) findViewById(R.id.extraSortButton);
        filter = (Button) findViewById(R.id.extraFilterButton);
    }

    public void getVacationSites(final Boolean append){
        if(idle){
            idle=false;
            loading.setVisibility(View.VISIBLE);
            loadMoreButton.setVisibility(View.GONE);
            if ((AppController.getInstance().gpsHandler.getCurrentLocation())==null) {
              dialog.setMessage(getResources().getString(R.string.gps_off_error));
            }
            else {
                userLocation = AppController.getInstance().gpsHandler
                        .getCurrentLocation();
                if(!append) {
                    vacationSiteArrayList.clear();
                    currentPage = 0;
                }

                // put param. just to make sure everything gonna be right clear it
                param.clear();
                param.put("longitude",String.valueOf(userLocation.getLongitude()));
                param.put("latitude", String.valueOf(userLocation.getLatitude()));
                param.put("treshold",String.valueOf(distanceParam));
                param.put("page",String.valueOf(currentPage+1));
                Log.v("distparais",String.valueOf(distanceParam));
                if(Util.isLoggedIn())
                    param.putAll(Util.getUserParam());
                // define the request url
                requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util.VACATION_SITE_PATH +
                        "/nearby";
                final Location finalUserLocation = userLocation;
                JsonObjectRequest vsRequest = new JsonObjectRequest(Request.Method.POST, requestUrl,
                        new JSONObject(param), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        idle=true;
                        loading.setVisibility(View.GONE);
                        swipeContainer.setRefreshing(false);
                        try {
                            JSONArray array = response.getJSONArray("payload");
                            int resultSize = array.length();
                            for (int i = 0; i < resultSize; i++) {
                                JSONObject vs = array.getJSONObject(i);
                                Log.v("hasil",vs.getString("name") + " hyp = " + vs.getString("hypotheses"));
                                VacationSite vacationSite = new VacationSite(vs);
                                vacationSite.calculateDistance(finalUserLocation);
                                vacationSiteArrayList.add(vacationSite);
                            }

                            vacationSiteAdapter.notifyDataSetChanged();
                            if(resultSize==0 && !append){
                                dialog.setMessage(getString(R.string.nearby_no_place));
                                dialog.show();
                                return;
                            }

                            currentPage = 1;
                            maxPage = 1;
                            if(response.has("pagination")){
                                currentPage = response.getJSONObject("pagination").getInt("currentPage");
                                maxPage = response.getJSONObject("pagination").getInt("maxPage");
                            }
                            Log.v("paginationresp",response.getJSONObject("pagination").toString());
                            if(!(currentPage>=maxPage)){
                                loadMoreButton.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        idle = true;
                        loading.setVisibility(View.GONE);
                        if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                            dialog.setMessage(getString(R.string.timeout_error));
                            dialog.show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ParseError) {
                            Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                            //TODO
                        }
                    }
                });

                AppController.getInstance().cancelPendingRequests("NearByVacationSite");
                vsRequest.setRetryPolicy(new DefaultRetryPolicy(
                                1000*60,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                vsRequest.setTag("NearbyVacationSite");
                AppController.getInstance().addToRequestQueue(vsRequest);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Nearby Vacation Sites");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests("NearbyVacationSite");
    }
}
