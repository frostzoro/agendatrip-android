package com.example.android.AgendaTrip;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Vin on 10/20/2015.
 */
public class Logout extends Fragment {
    View rootview;
Button buttonLogout;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview=inflater.inflate(R.layout.logout, container, false);
        buttonLogout= (Button) rootview.findViewById(R.id.logoutButton);
        buttonLogout.setOnClickListener(logoutClickListener);
        return rootview;
    }
    private View.OnClickListener logoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            TryLogout(v);
        }
    };

    void TryLogout(View view){
        Context context=getActivity().getApplicationContext();
        SharedPreferences sharedPreference=context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String uid=sharedPreference.getString("userid", "");
        Log.v("uid ", uid);
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString("key", "");
        editor.putString("dbid",uid);
        editor.putString("userid", "");

        editor.apply();

        Toast.makeText(context, "Logged out", Toast.LENGTH_SHORT).show();
        AlarmManager alarmManager=(AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        Intent alertIntent=new Intent(getActivity(), AlertReceiver.class);
        alarmManager.cancel(PendingIntent.getBroadcast(getActivity(), -1, alertIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        Intent i=new Intent(context,MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);

    }

}