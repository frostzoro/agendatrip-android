package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.BookedItemAdapter;
import model.BookedItem;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class AgendaBookingDetail extends AppCompatActivity {
    AlertDialog dialog;
    ArrayList<BookedItem> bookedItemArrayList;
    BookedItemAdapter bookedItemAdapter;
    Double totalAmount;
    Intent newBookingIntent;
    ListView bookedItemListView;
    TextView bookingAmountTotal;
    int agenda_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda_booking_detail);
        buildAlertDialog();
        receiveDataFromIntent();
        bindViewToVariables();
        initializeSupportVariables();
        getData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests("AgendaBookingDetail");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_agenda_booking, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menu_id = item.getItemId();
        if(menu_id== R.id.menu_add){
            newBookingIntent.putExtra("agenda_id",agenda_id);
            startActivity(newBookingIntent);
        }
        else if(menu_id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setPositiveButton(getString(R.string.ok), null);
        dialog = adb.create();
    }

    protected void initializeSupportVariables(){
        bookedItemArrayList = new ArrayList<>();
        bookedItemAdapter = new BookedItemAdapter(AgendaBookingDetail.this, R.layout.booked_item_partial, bookedItemArrayList);
        bookedItemListView.setAdapter(bookedItemAdapter);
        newBookingIntent = new Intent(AgendaBookingDetail.this,BookingSelection.class);
    }

    protected void receiveDataFromIntent(){
        agenda_id = getIntent().getIntExtra("agenda_id",-1);
        Log.v("aid", String.valueOf(agenda_id));
    }

    protected void bindViewToVariables(){
        bookedItemListView = (ListView) findViewById(R.id.agendaBookingDetailListView);
        bookingAmountTotal = (TextView) findViewById(R.id.agendaBookingDetailAmountTotal);
    }

    protected void countTotal(){
        totalAmount = 0.0 ;
        int sz = bookedItemArrayList.size();
        for(int i=0; i<sz; i++){
            totalAmount += bookedItemArrayList.get(i).getOrder_amount();
        }
        bookingAmountTotal.setText(Util.convertDoubleToIDRString(totalAmount));
    }

    protected void getData(){
        String url = Util.GET_AGENDA_BOOKINGS_URL + "?agenda_id=" + agenda_id + "&" + Util.paramToString(Util.getUserParam());
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
        new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                bookedItemArrayList.clear();
                try {
                    JSONArray bookedItems = response.getJSONArray("payload");
                    Log.v("items",bookedItems.toString());
                    int sz = bookedItems.length();
                    for(int i=0; i<sz; i++){
                        BookedItem bi = new BookedItem(bookedItems.getJSONObject(i));
                        bookedItemArrayList.add(bi);
                    }
                    bookedItemAdapter.notifyDataSetChanged();
                    if(sz<=0){
                        dialog.setMessage(getString(R.string.error_no_agenda_booking));
                        dialog.show();
                    }
                    countTotal();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.setMessage(getString(R.string.timeout_error_polite));
                dialog.show();
            }
        });
        jor.setRetryPolicy(Constants.RETRY_POLICY);
        jor.setTag("AgendaBookingDetail");
        AppController.getInstance().addToRequestQueue(jor);
    }

}
