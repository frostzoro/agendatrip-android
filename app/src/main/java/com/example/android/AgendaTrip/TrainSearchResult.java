package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import adapters.TrainAdapter;


import model.Train;
import model.TrainSearch;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class TrainSearchResult extends AppCompatActivity {
    AlertDialog filterDialog;
    boolean[] selected = {true,true,true};
    TrainSearch fs;
    Boolean isDeparture;
    ArrayList<String> trainClassCode;
    ArrayList<Train> data,data_return;
    TrainAdapter fa;
    ListView trainListView;
    TextView destinationTextView,dateAndPassengerTextView;
    Button sortButton,filterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_search);
        fs = (TrainSearch) getIntent().getSerializableExtra
                ("trainsearch");
        isDeparture = (Boolean) getIntent().getBooleanExtra("isDeparture", true);
        destinationTextView = (TextView) findViewById(R.id.trainSearchDestination);
        dateAndPassengerTextView = (TextView) findViewById(R.id.trainSearchDateAndPassenger);
        destinationTextView.setText(fs.getA_complete().substring(0,fs.getA_complete().indexOf(
                "(")) + " -> " +fs.getD_complete().substring(0,fs.getD_complete().indexOf("(")));
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MM yyyy ");
        String dateForText = sdf.format(fs.getDate().getTime());
        dateAndPassengerTextView.setText(dateForText + (fs.getAdult() + fs.getChild() + fs
                .getInfant()) + " Passenger(s)");
        data = new ArrayList<>();
        data_return = new ArrayList<>();
        fa = new TrainAdapter(this,R.layout.flight_partial,data);
        trainListView = (ListView) findViewById(R.id.trainSearchListView);
        trainListView.setAdapter(fa);

        sortButton = (Button) findViewById(R.id.trainSearchSort);
        sortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int[] selection = new int[1];
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(TrainSearchResult.this);
                alertDialog.setTitle("Sort Trains By").setSingleChoiceItems(R.array
                        .sort_flight_array, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v("which is ",String.valueOf(which));
                        selection[0] = which;
                    }
                }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v("which is ",String.valueOf(which));
                        switch(selection[0]) {
                            case 0: fa.sortItem(TrainAdapter.SORT_BY_PRICE);Log.v
                                    ("trainsearchres","SORT BY PRICE") ;
                                break;
                            case 1: fa.sortItem(TrainAdapter.SORT_BY_DEPARTURE);Log.v
                                    ("trainsearchres","SORT BY DEPARTURE TIME");
                        }
                    }
                });
                AlertDialog d = alertDialog.create();
                d.show();
                Log.v("trainresultsort","clicked");
            }
        });

        trainClassCode = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.filter_trainclasscode)));
        buildFilterDialog();
        filterButton = (Button) findViewById(R.id.trainSearchFilter);
        filterButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                filterDialog.show();
            }
        });
        if(fs.getRound_trip()&&!isDeparture){
            data_return = getIntent().getParcelableArrayListExtra("flightreturn");
            getTrainsFromIntent();
        }else{
            getTrains();
        }

    }

    protected void buildFilterDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(TrainSearchResult.this);
        adb.setMultiChoiceItems(getResources().getStringArray(R.array.filter_trainclass), selected, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                String selectedCode = getResources().getStringArray(R.array.filter_trainclasscode)[which];
                if(isChecked && trainClassCode.indexOf(selectedCode) == -1){
                    trainClassCode.add(selectedCode);
                }
                else if(!isChecked && trainClassCode.indexOf(selectedCode) != -1){
                    trainClassCode.remove(selectedCode);
                }
            }
        });
        adb.setTitle(getString(R.string.title_dialog_filter_class));
        adb.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fa.setTrainClassFilter(trainClassCode);
                fa.filter();
                fa.notifyDataSetChanged();
            }
        });
        filterDialog = adb.create();
    }

    protected void getTrains() {
        String url = "http://www.agendatrip.com/api/v1/bookings/trains/getTrains?";
        url += fs.toGetParam();
             Log.v("trainsearchresult", url);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            data.clear();
                            JSONArray trains = response.getJSONObject("departures").getJSONArray("result");
                            Log.v("departs",trains.toString());
                            int sz = trains.length();
                            for(int i=0;i<sz;i++){
                                Train f = new Train(trains.getJSONObject(i));
                                Log.v("test waktu",f.getDeparture_time() + " - "+f.getArrival_time());
                                f.setDate(response.getJSONObject("search_queries").getString("date"));
                                data.add(f);
                            }
                            fa.notifyDataLoaded();
                            if(fs.getRound_trip()){
                                JSONArray train_return = response.getJSONObject("returns")
                                        .getJSONArray("result");
                                Log.v("returns",train_return.toString());
                                int szr = train_return.length();
                                for(int i=0;i<szr;i++){
                                    Train f = new Train(train_return.getJSONObject(i));
                                    f.setDate(response.getJSONObject("search_queries").getString("date"));
                                    data_return.add(f);
                                }
                            }
                        } catch (JSONException e) {
                            //this means no trains for that day
                            Log.v("Trainserres",response.toString());
                             AlertDialog.Builder adb = new AlertDialog.Builder
                            (TrainSearchResult.this);
                            adb.setMessage(getString(R.string.no_train_error));
                            adb.setTitle(getString(R.string.error));
                            adb.setPositiveButton(R.string.ok, null);
                            adb.create().show();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        jor.setRetryPolicy(Constants.RETRY_POLICY_LONG);
        jor.setTag("trainsearchresult");
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected void getTrainsFromIntent(){
        data.clear();
        int szr = data_return.size();
        for(int i=0;i<szr;i++){
            data.add(data_return.get(i));
        }
        fa.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy(){
        AppController.getInstance().cancelPendingRequests("trainsearchresult");
        super.onDestroy();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (isDeparture)
            getSupportActionBar().setTitle("Departure");
        else
            getSupportActionBar().setTitle("Return");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void goToNextStep(Train f) {
        Log.v("ayy","gotonexttrip");
        //=========================================================================================
        //Determines whether to go to checkout (if single trip OR if already selecting return trip)
        //=========================================================================================
        Log.v("roundtrip",fs.getRound_trip().toString());
        Log.v("isdepar",isDeparture.toString());
        if ((fs.getRound_trip() && !isDeparture) || (!fs.getRound_trip() && isDeparture)) {
            //=============================================================
            //if already selecting the return trip or if its a single trip
            //=============================================================
            Intent i = new Intent(this,TrainCheckout.class);
            Bundle b = new Bundle();
            if(isDeparture){
                fs.departure_flight =f;
            }
            else
                fs.return_flight = f;
            b.putSerializable("trainsearch",fs);
            i.putExtras(b);
            startActivity(i);
            Log.v("ayy","gotonexttrip1");
        } else {
            //=====================================================================================
            //if its a round trip but we still have to select the return trip
            //=====================================================================================
            Log.v("ayy","gotonexttrip");
            Intent i = new Intent(this, TrainSearchResult.class);
            Bundle b = new Bundle();
            fs.departure_flight = f;
            b.putSerializable("trainsearch", fs);
            b.putSerializable("isDeparture", false);
            b.putParcelableArrayList("trainreturn",data_return);
            i.putExtras(b);
            startActivity(i);
        }
    }

}
