package com.example.android.AgendaTrip;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.gc.materialdesign.views.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import helper.TrainPassengerInformationHolder;
import model.TrainSearch;
import model.UserIdentity;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class TrainPassengerInformation extends AppCompatActivity implements
    CalendarDatePickerDialogFragment.OnDateSetListener {
    ArrayList<TrainPassengerInformationHolder> apih;
    ArrayList<String> userIdentityString,titleAdult,titleChild;
    ArrayList<UserIdentity> userIdentityArrayList;
    ArrayAdapter<String> titleAdapter,identityAdapter;
    TrainSearch fs;
    LinearLayout detailField,passengerForm,identitylayout;
    TextView contactIdentityLabel;
    Spinner contactTitleSpinner,contactIdentitySpinner;
    EditText contactFirstName,contactLastName,contactPhoneNumber,contactEmail;
    HashMap<String,Boolean> params;
    Button continueButton;
    CalendarDatePickerDialogFragment dpb;
    TrainPassengerInformationHolder currentDateSelectionObject;
    Integer agendaID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_passenger_information);
        identitylayout=(LinearLayout) findViewById(R.id.trainPassengerIdentityLayout);
        if(!Util.isLoggedIn()){identitylayout.setVisibility(View.GONE);}
        else{identitylayout.setVisibility(View.VISIBLE);}


        fs  = (TrainSearch) getIntent().getSerializableExtra("trainsearch");
        detailField = (LinearLayout) findViewById(R.id
                .flightPassengerInformationDetailLayout);
        passengerForm = (LinearLayout) findViewById(R.id.flightPassengerForm);
        passengerForm.setVisibility(View.VISIBLE);
        apih = new ArrayList<TrainPassengerInformationHolder>();
        // Params is used to send custom required infos from the passenger.
        params = new HashMap<>();
        params.put("round_trip",fs.getRound_trip());
        titleAdult = new ArrayList<>(Arrays.asList("Mr","Mrs","Ms"));
        titleChild = new ArrayList<>(Arrays.asList("Mstr","Miss"));
        continueButton = (Button) findViewById(R.id.passengerInformationContinueButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order();
            }
        });
        //loading = (RelativeLayout) findViewById(R.id.flightPassengerInformationLoading);
       // loading.setVisibility(View.GONE);
       // getCountries();
getUserIdentity();

        agendaID = getIntent().getIntExtra("agenda_id",-1);
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        Calendar temp = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        temp.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        temp.set(Calendar.MONTH,monthOfYear);
        temp.set(Calendar.YEAR, year);
        currentDateSelectionObject.setBirthDate(sdf.format(temp.getTime()));
    }

    @Override
    public void onDestroy(){
        //releases all requests that were made in this activity
        AppController.getInstance().cancelPendingRequests("TrainPassengerInformation");
        super.onDestroy();
    }

    protected void setForms(){;
        passengerForm.setVisibility(View.VISIBLE);
       // loading.setVisibility(View.GONE);
      if(userIdentityArrayList.size()==0){
          identitylayout.setVisibility(View.GONE);

      }

        if(Util.isLoggedIn()){
        contactIdentityLabel = (TextView) findViewById(R.id.flightPassengerInformationIdentityLabel);
        contactIdentitySpinner = (Spinner) findViewById(R.id.flightPassengerInformationIdentity);
        identityAdapter = new ArrayAdapter<String>(TrainPassengerInformation.this,R.layout.support_simple_spinner_dropdown_item,userIdentityString);
        contactIdentitySpinner.setAdapter(identityAdapter);
        contactIdentitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                UserIdentity selectedIdentity = userIdentityArrayList.get(position);
                contactFirstName.setText(selectedIdentity.getFirst_name());
                contactLastName.setText(selectedIdentity.getLast_name());
                contactEmail.setText(selectedIdentity.getEmail().equals("null") ? "" : selectedIdentity.getEmail());
                contactPhoneNumber.setText(selectedIdentity.getPhone_number().equals("null") ? "" : selectedIdentity.getPhone_number());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });}

        contactTitleSpinner = (Spinner) findViewById(R.id.flightPassengerInformationTitle);
        contactFirstName  = (EditText) findViewById(R.id
                .flightPassengerInformationContactFirstName);
        contactLastName  = (EditText) findViewById(R.id
                .flightPassengerInformationContactLastName);
        contactPhoneNumber = (EditText) findViewById(R.id
                .flightPassengerInformationContactPhoneNumber);
        contactEmail = (EditText) findViewById(R.id. flightPassengerInformationContactEmailAddress);
        titleAdapter = new ArrayAdapter<String>(TrainPassengerInformation.this,R.layout
                .support_simple_spinner_dropdown_item,titleAdult);
        contactTitleSpinner.setAdapter(titleAdapter);
        for(int i=0;i<fs.getAdult();i++){
            TrainPassengerInformationHolder pih = new TrainPassengerInformationHolder(params,TrainPassengerInformation.this,getApplicationContext(),"Adult",i+1,userIdentityArrayList);
            apih.add(pih);
            detailField.addView(pih.getView());
            Log.v("flightpasi","added Adlt" +i );
        }
        for(int i=0;i<fs.getChild();i++){
            TrainPassengerInformationHolder pih = new TrainPassengerInformationHolder(params,
                    TrainPassengerInformation.this,getApplicationContext(),"Child",i+1,userIdentityArrayList);
            apih.add(pih);
            detailField.addView(pih.getView());
            Log.v("flightpasi", "added Child" + i);
        }
        for(int i=0;i<fs.getInfant();i++){
            TrainPassengerInformationHolder pih = new TrainPassengerInformationHolder(params,
                    TrainPassengerInformation.this,getApplicationContext(),"Infant",i+1,userIdentityArrayList);
            apih.add(pih);
            detailField.addView(pih.getView());
            Log.v("flightpasi", "added Infant" + i);
        }

    }

    public void showCalendarDatePicker(TrainPassengerInformationHolder p){
        dpb = CalendarDatePickerDialogFragment
                .newInstance(TrainPassengerInformation.this, Calendar.getInstance().get(Calendar
                                .YEAR),
                        Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get
                                (Calendar
                                        .DAY_OF_MONTH));
        dpb.show(getSupportFragmentManager(), "title");
        currentDateSelectionObject = p;
    }

    protected void getUserIdentity(){

        userIdentityArrayList = new ArrayList<>();
        userIdentityString = new ArrayList<>();
        if(!Util.isLoggedIn()){setForms();}
        else {
            String url = Util.BASE_URL + Util.API_GATEWAY + Util.USER_IDENTITY_PATH;
            url += "?" + Util.paramToString(Util.getUserParam());
            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONArray payload = null;
                            try {
                                payload = response.getJSONArray("payload");
                                int sz = payload.length();
                                for (int i = 0; i < sz; i++) {
                                    userIdentityArrayList.add(new UserIdentity(payload.getJSONObject(i)));
                                    userIdentityString.add(userIdentityArrayList.get(i).getFirst_name() + " " + userIdentityArrayList.get(i).getLast_name());
                                }
                                setForms();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
            AppController.getInstance().addToRequestQueue(jor);
        }
    }


    protected Boolean checkContactInformation(){
        Boolean can = true;

        if(contactFirstName.getText().length() <=0){
            can = false;
            contactFirstName.setBackground(getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("conPIH","confirstname");
        }

        if(contactLastName.getText().length() <=0){
            can = false;
            contactLastName.setBackground(getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("conPIH", "conlastname");
        }

        if(contactTitleSpinner.getSelectedItemPosition() <0){
            can = false;
            contactTitleSpinner.setBackground(getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("conPIH", "contitle");
        }

        if(contactEmail.getText().length() <=0){
            can = false;
            contactEmail.setBackground(getResources().getDrawable(R.drawable
                    .rectangle_border_red));
            Log.v("conPIH", "conemail");
        }

        if(contactPhoneNumber.getText().length() <=0){
            can = false;
            contactPhoneNumber.setBackground(getResources().getDrawable(R.drawable.rectangle_border_red));
            Log.v("conPIH", "conphonnum");
        }

        return can;
    }

    protected void order(){
        Boolean valid = true;
        Boolean contactValid = checkContactInformation();
        Boolean emailValid = true;
        String error = "";
        String identityParam = "";
        String contactParam= "";
        String trainParam = "";
        //Checks contact person information

        //Checks all passenger information to be filled


        for(TrainPassengerInformationHolder pih : apih){
            valid = valid && pih.checkData();
            if(!valid)
                error = pih.getErrorMsg();
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(contactEmail.getEditableText()).matches()){
            emailValid=false;
            error = getString(R.string.email_address_format_error);
            Log.v("fpih","emailformatinvalid");
        }

        if(contactValid && valid) {
            Boolean first = true;
            for (TrainPassengerInformationHolder pih : apih) {
                if (!first) identityParam += "&";
                identityParam += pih.attributesToParam();
                first = false;
            }
            contactParam+="&conSalutation=" + contactTitleSpinner.getSelectedItem();
            contactParam+="&conFirstName=" + contactFirstName.getText();
            contactParam+="&conLastName=" + contactLastName.getText();
            contactParam+="&conPhone=" +  contactPhoneNumber.getText();
            contactParam+="&conEmailAddress=" + contactEmail.getText();

             Log.v("completeparam", identityParam);
        }
        else{
            valid = false;
            if(error.length()==0)
            error = getString(R.string.form_not_complete_error);
        }

        trainParam += "&train_id=" + fs.departure_flight.getTrain_id();
        trainParam += "&date=" + fs.departure_flight.getDate();
        trainParam += "&d=" + fs.getD();
        trainParam +="&a="+fs.getA();
        trainParam+="&subclass=A";
        trainParam+="&adult="+fs.getAdult();
        trainParam+="&child="+fs.getChild();
        trainParam+="&infant="+fs.getInfant();
        if(fs.getRound_trip()){
            trainParam += "&ret_flight_id=" + fs.return_flight.getTrain_id();
            trainParam += "&ret_date=" + fs.return_flight.getDate();
        }

        // Extra checks for passenger information :
        // Contact person email address format

        if(valid && contactValid && emailValid){
//           loading.setVisibility(View.GONE);
            //String url="https://api-sandbox.tiket.com/order/add/train?output=json&";
            String url = "http://www.agendatrip.com/api/v1/bookings/trains/addOrder?";
            url += identityParam;
            url += trainParam;
            url += contactParam;
            url += "&token=" + fs.getToken();
            if(Util.isLoggedIn()){
                url+="&" + Util.paramToString(Util.getUserParam());
            }
            if(agendaID!=-1){
                url+= "&agenda_id=" + String.valueOf(agendaID);
            }
            Log.v("urlOrder", url);

            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject)
                    null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                      //  loading.setVisibility(View.GONE);
                        Intent i = new Intent(getApplicationContext(),PaymentInformationBank.class);
                        Bundle b = new Bundle();
                        b.putString("payment_information",response.toString());
                        Log.v("asd",response.toString());
                        b.putSerializable("trainsearch", fs);
                        i.putExtras(b);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                  //  loading.setVisibility(View.GONE);
                    Log.v("error",error.toString());
                }
            });
            jor.setRetryPolicy(Constants.RETRY_POLICY_BOOKING_LONG);
            jor.setTag("FlightPassengerInformation");
            AppController.getInstance().addToRequestQueue(jor);
        }
        else{
            android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                    (TrainPassengerInformation.this);
            adb.setMessage(error);
            adb.setTitle(getString(R.string.error));
            adb.setPositiveButton(R.string.ok, null);
            adb.create().show();
        }
    }

}
