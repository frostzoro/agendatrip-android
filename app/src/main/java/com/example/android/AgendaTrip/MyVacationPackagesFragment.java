package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.VacationPackagePurchaseAdapter;
import model.VacationPackagePurchase;
import singleton.AppController;
import utils.Util;

public class MyVacationPackagesFragment extends Fragment {
    AlertDialog dialog;
    ArrayList<VacationPackagePurchase> vacationPackagePurchaseArrayList;
    Intent i;
    ListView vacationPackageListView;
    RelativeLayout loading;
    VacationPackagePurchaseAdapter vppa;

    public MyVacationPackagesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildAlertDialog();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_my_vacation_packages, container, false);
        vacationPackageListView = (ListView) v.findViewById(R.id.myVacationPackagesListView);
        loading = (RelativeLayout) v.findViewById(R.id.loading);
        loading.setVisibility(View.GONE);
        initializeSupportVariables();
        bindListeners();
        getData();
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests("MyVacationPackagesFragment");
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle(getResources().getString(R.string.error));
        adb.setPositiveButton(getResources().getString(R.string.ok), null);
        dialog = adb.create();
    }

    protected void bindListeners(){
        vacationPackageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v("positionclicked", String.valueOf(position));
                i.putExtra("vppid",vppa.getItem(position).getId());
                startActivity(i);
            }
        });
    }

    protected void initializeSupportVariables(){
        vacationPackagePurchaseArrayList = new ArrayList<>();
        vppa = new VacationPackagePurchaseAdapter(getActivity(),R.layout.partial_vacation_package_purchase,vacationPackagePurchaseArrayList);
        vacationPackageListView.setAdapter(vppa);
        i = new Intent(getActivity(),VacationPackagePurchaseDetail.class);
    }

    protected void getData(){
        loading.setVisibility(View.VISIBLE);
        String url = Util.BASE_URL + Util.API_GATEWAY  +Util.VACATION_PACKAGE_PURCHASE_PATH + "?" +Util.paramToString(Util.getUserParam()) ;
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loading.setVisibility(View.GONE);
                vacationPackagePurchaseArrayList.clear();
                JSONArray purchases;
                try {
                    purchases = response.getJSONArray("payload");
                    int sz = purchases.length();
                    for(int i=0;i<sz;i++){
                        VacationPackagePurchase newvp = new VacationPackagePurchase(purchases.getJSONObject(i));
                        vacationPackagePurchaseArrayList.add(newvp);
                    }
                    vppa.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.setVisibility(View.GONE);
                dialog.setMessage(getString(R.string.timeout_error_polite));
                dialog.show();
            }
        });
        jor.setTag("MyVacationPackagesFragment");
        AppController.getInstance().addToRequestQueue(jor);

    }
}
