package com.example.android.AgendaTrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import adapters.HotelSearchResultAdapter;
import model.FlightSearch;
import model.HotelSearch;
import singleton.AppController;
import utils.Constants;
import utils.Util;

public class HotelBooking extends AppCompatActivity implements CalendarDatePickerDialogFragment
        .OnDateSetListener{
    AutoCompleteTextView hotelSearchQuery;
    HotelSearchResultAdapter hotelSearchResultAdapter;
    ArrayList<String> hotelNames,guestsArrayList,roomsArrayList;
    ArrayAdapter<String> roomAdapter,guestAdapter;
    LinearLayout checkInDateBox,checkOutDateBox;
    TextView checkInDay,checkInMonthYear,checkInDayName,checkOutDay,checkOutDayName,
            checkOutMonthYear;
    Calendar checkInCalendar, checkOutCalendar;
    Spinner guestNumberSpinner,roomNumberSpinner;
    Button searchButton;
    AlertDialog dialog;
    String token;
    CalendarDatePickerDialogFragment calendarPicker;
    Boolean pickDateCheckIn;
    HotelSearch hs;
    int agenda_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_booking);
        receiveDataFromIntent();
        buildAlertDialog();
        buildCalendarDialog();
        bindViewToVariables();
        initializeSupportVariables();
        bindListeners();
        getToken();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests("HotelBooking");
        if(dialog!=null)
            dialog.dismiss();
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        if(pickDateCheckIn){
            checkInCalendar.set(Calendar.YEAR, year);
            checkInCalendar.set(Calendar.MONTH ,monthOfYear);
            checkInCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            changeViewDate();
            showCheckOutCalendarPicker();
        }
        else{
            checkOutCalendar.set(Calendar.YEAR,year);
            checkOutCalendar.set(Calendar.MONTH,monthOfYear);
            checkOutCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            changeViewDate();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.hotel_booking);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    protected void receiveDataFromIntent(){
        agenda_id = getIntent().getIntExtra("agenda_id",-1);
    }

    protected void changeViewDate(){
        if(pickDateCheckIn==true){
            checkInDay.setText(String.valueOf(checkInCalendar.get(Calendar.DAY_OF_MONTH)));
            checkInMonthYear.setText(new SimpleDateFormat("MMM yyyy").format(checkInCalendar
                    .getTime()));
        }
        else{
            checkOutDay.setText(String.valueOf(checkOutCalendar.get(Calendar.DAY_OF_MONTH)));
            checkOutMonthYear.setText(new SimpleDateFormat("MMM yyyy").format(checkOutCalendar
                    .getTime()));
        }
    }

    protected void showCheckInCalendarPicker(){
        Log.v("testing","insideshowcheckin");
        pickDateCheckIn = true;
        MonthAdapter.CalendarDay calendarDay = new MonthAdapter.CalendarDay(Calendar.getInstance());
        Calendar calendarRangeLimit = Calendar.getInstance();
        calendarRangeLimit.add(Calendar.DAY_OF_MONTH, 547);
        MonthAdapter.CalendarDay calendarDayLimit = new MonthAdapter.CalendarDay
                (calendarRangeLimit);
        calendarPicker = new CalendarDatePickerDialogFragment();
        calendarPicker.setDateRange(calendarDay,calendarDayLimit);
        calendarPicker.setOnDateSetListener(this);
        calendarPicker.setThemeDark(true);
        calendarPicker.show(getSupportFragmentManager(),"HotelBookingCalendarPicker");
    }

    protected void showCheckOutCalendarPicker(){
        Log.v("hotel booking", "changing viewdate for checkout ");
        pickDateCheckIn = false;
        MonthAdapter.CalendarDay calendarDay = new MonthAdapter.CalendarDay(checkInCalendar);
        Calendar calendarRangeLimit = (Calendar) checkInCalendar.clone();
        calendarRangeLimit.add(Calendar.DAY_OF_MONTH, 15);
        MonthAdapter.CalendarDay calendarDayLimit = new MonthAdapter.CalendarDay
                (calendarRangeLimit);
        calendarPicker = new CalendarDatePickerDialogFragment();
        calendarPicker.setDateRange(calendarDay, calendarDayLimit);
        calendarPicker.setOnDateSetListener(this);
        calendarPicker.setThemeDark(true);
        calendarPicker.show(getSupportFragmentManager(), "HotelBookingCalendarPicker");
    }

    protected void buildAlertDialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setPositiveButton("OK",null).setCancelable(false);
        dialog = b.create();
    }

    protected void buildCalendarDialog(){
        calendarPicker = new CalendarDatePickerDialogFragment();
        calendarPicker.setOnDateSetListener(this);
    }

    protected void bindViewToVariables(){
        hotelSearchQuery = (AutoCompleteTextView) findViewById(R.id.hotelBookingSearchTextBox);
        checkInDay = (TextView) findViewById(R.id.hotelBookingCheckInDay);
        checkInMonthYear = (TextView) findViewById(R.id.hotelBookingCheckInMonthYear);
        checkInDayName = (TextView) findViewById(R.id.hotelBookingCheckInDayName);
        checkOutDay = (TextView) findViewById(R.id.hotelBookingCheckOutDay);
        checkOutDayName = (TextView) findViewById(R.id.hotelBookingCheckOutDayName);
        checkOutMonthYear = (TextView) findViewById(R.id.hotelBookingCheckoutMonthYear);
        guestNumberSpinner = (Spinner) findViewById(R.id.hotelBookingGuestSpinner);
        roomNumberSpinner = (Spinner) findViewById(R.id.hotelBookingRoomSpinner);
        searchButton = (Button) findViewById(R.id.hotelBookingSearchButton);
        checkInDateBox = (LinearLayout) findViewById(R.id.hotelBookingCheckInBox);
        checkOutDateBox = (LinearLayout) findViewById(R.id.hotelBookingCheckOutBox);
    }

    protected void initializeSupportVariables(){
        checkInCalendar = Calendar.getInstance();
        pickDateCheckIn = true;
        changeViewDate();
        checkOutCalendar = Calendar.getInstance();
        checkOutCalendar.add(Calendar.DAY_OF_MONTH, 1);
        pickDateCheckIn = false;
        changeViewDate();
        hotelNames = new ArrayList<>();
        hotelSearchResultAdapter = new HotelSearchResultAdapter(this,R.layout
                .support_simple_spinner_dropdown_item,hotelNames);
        hotelSearchQuery.setAdapter(hotelSearchResultAdapter);
        guestsArrayList = new ArrayList<>(Arrays.asList("1","2","3","4","5","6"));
        guestAdapter = new ArrayAdapter<String>(HotelBooking.this,R.layout
                .support_simple_spinner_dropdown_item,guestsArrayList);
        guestNumberSpinner.setAdapter(guestAdapter);
        roomsArrayList = new ArrayList<>(Arrays.asList("1","2","3","4","5","6","7","8"));
        roomAdapter = new ArrayAdapter<String>(HotelBooking.this,R.layout
                .support_simple_spinner_dropdown_item,roomsArrayList);
        roomNumberSpinner.setAdapter(roomAdapter);
    }

    protected void getToken(){
        String url = "http://www.agendatrip.com/api/v1/bookings/getToken";
        JsonObjectRequest jor1 = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    token = response.getString("token").toString();
                    hotelSearchResultAdapter.setToken(token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    dialog.setMessage(getString(R.string.timeout_error_polite));
                    dialog.show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), "Auth Error", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });
        jor1.setRetryPolicy(Constants.RETRY_POLICY);
        jor1.setTag("HotelBooking");
        AppController.getInstance().addToRequestQueue(jor1);
    }

    protected void bindListeners(){
        checkInDateBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckInCalendarPicker();
            }
        });
        checkOutDateBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckOutCalendarPicker();
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInputs();
            }
        });
    }

    protected void validateInputs(){
        if(hotelSearchQuery.getText().length()<=0){
            Log.v("foobar","queryinput");
            dialog.setMessage(getString(R.string.error_search_query_empty));
            dialog.show();
            return;
        }

        if(checkInCalendar.compareTo(checkOutCalendar)>0){
            dialog.setMessage(getString(R.string.error_checkin_date_later));
            dialog.show();
            return;
        }

        if((checkInCalendar.getTimeInMillis()-Calendar.getInstance().getTimeInMillis()) / (24 * 60
                * 60 * 1000) > 547){
            dialog.setMessage(getString(R.string.error_checkin_date_547));
            dialog.show();
            return;
        }

        if((checkOutCalendar.getTimeInMillis()-checkInCalendar.getTimeInMillis()) /
        (24 * 60 * 60 * 1000) > 15){
            dialog.setMessage(getString(R.string.error_stay_length_15));
            dialog.show();
            return;
        }

        Intent i = new Intent(HotelBooking.this,HotelSearchResult.class);
        hs = new HotelSearch(hotelSearchQuery.getText().toString(),
                roomNumberSpinner.getSelectedItem().toString(),guestNumberSpinner
                .getSelectedItem().toString(),checkInCalendar,checkOutCalendar,token);
        Bundle mBundle = new Bundle();
        hs.setAgenda_id(agenda_id);
        mBundle.putSerializable("hotelsearch",hs);
        i.putExtras(mBundle);
        startActivity(i);
    }


}
