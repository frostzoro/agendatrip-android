package com.example.android.AgendaTrip;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;

import singleton.AppController;
import utils.Util;

/**
 * Created by Vin on 2/25/2016.
 */
public class DecreasePreferences extends Activity {
    NotificationManager nm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(DecreasePreferences.this, "putarrrrrrrrrrrrrrr", Toast.LENGTH_LONG).show();
        Log.v("pref", "preferences decreased");
        updatePreferences();
        nm = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE) ;
        nm.cancel(-1);
    }


    protected void updatePreferences(){
        if(Util.isLoggedIn()){
            String url = Util.BASE_URL + Util.API_GATEWAY + Util.TAG_URL + Util.VACATION_PACKAGE_RESPONSE_URL;
            HashMap<String,String> param = new HashMap<>();
            param.putAll(Util.getUserParam());
            param.put("vacation_package_id",String.valueOf(getIntent().getBundleExtra("vacation_package")));
            param.put("response",0+"");
            Log.v("params",param.toString());
            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, new JSONObject
                    (param), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.v("updatepackpref", "success!");
                }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("updatepackpref","failure!");
                    error.printStackTrace();
                }
            });

            AppController.getInstance().addToRequestQueue(jor);

        }

    }

}
