package com.example.android.AgendaTrip;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gc.materialdesign.views.Button;

import java.text.NumberFormat;
import java.util.Locale;

import model.FlightOrder;
import model.FlightSearch;
import model.TrainOrder;
import model.TrainSearch;

public class TrainCheckout extends AppCompatActivity {
    LinearLayout departureLayout,returnLayout;
    TrainOrder departureOrder,returnOrder;
    TextView totalText;
    Double totalPrice;
    Button continueButton;
    TrainSearch fs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_checkout);
        totalText = (TextView) findViewById(R.id.flightCheckoutTotal);

        departureLayout = (LinearLayout) findViewById(R.id.flightCheckoutDepart);
        returnLayout = (LinearLayout) findViewById(R.id.flightCheckoutReturn);
        fs = (TrainSearch) getIntent().getSerializableExtra("trainsearch");

        departureOrder = new TrainOrder(fs.departure_flight,fs,"Departure",this);
        departureLayout.addView(departureOrder.getView());
        if(fs.return_flight!=null){
            returnOrder = new TrainOrder(fs.return_flight,fs,"Return",this);
            returnLayout.addView(returnOrder.getView());
        }

        totalPrice = departureOrder.getSubTotal();
        if(returnOrder !=null) totalPrice += returnOrder.getSubTotal();
        totalText.setText(NumberFormat.getCurrencyInstance(new
                Locale("EN", "ID")).format(totalPrice));

        continueButton = (Button) findViewById(R.id.flightCheckoutContinueButton);
        continueButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),TrainPassengerInformation.class);
                Bundle b= new Bundle();
                b.putSerializable("trainsearch",fs);
                i.putExtras(b);
                startActivity(i);
            }
        });
    }

}
