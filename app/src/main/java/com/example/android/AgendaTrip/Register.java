package com.example.android.AgendaTrip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import utils.Constants;


public class Register extends Activity {
EditText fName,lName,pass1,pass2,email;

    Button regBtn;
ProgressDialog pDialog;
    protected void onCreate(Bundle savedInstanceState){
        pDialog=new ProgressDialog(this);
        pDialog.setMessage("Please wait");
        pDialog.setCancelable(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        fName=(EditText) findViewById(R.id.firstname);
        lName=(EditText) findViewById(R.id.lastname);
        pass1=(EditText) findViewById(R.id.password);
        pass2=(EditText) findViewById(R.id.confirmpassword);
        email=(EditText) findViewById(R.id.email);
        regBtn=(Button) findViewById(R.id.register_button);

        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryRegister();
            }


        });

    }


    private void tryRegister(){
        //lempar isi ke API

        if(fName.getText().toString().equals("") || lName.getText().toString().equals("") || pass1.getText().toString().equals("")||pass2.getText().toString().equals("")||email.getText().toString().equals("")){
        Toast.makeText(Register.this, "All field must be filled!", Toast.LENGTH_SHORT).show();
                        }
        else if(!isValidEmail(email.getText().toString())){
            Toast.makeText(Register.this, "Invalid E-mail format", Toast.LENGTH_SHORT).show();
        }

        else if (!pass1.getText().toString().equals(pass2.getText().toString())){
    Toast.makeText(Register.this, "Your Password and Confirm Password is different!", Toast.LENGTH_SHORT).show();

        }

        else if(pass1.getText().toString().length()<9 || pass1.getText().toString().length()>14){
            Toast.makeText(Register.this, "Password and Confirm Password length must be betweem 9 - 14 characters", Toast.LENGTH_SHORT).show();

        }

        else{
            //makerequest
            pDialog.show();
makeJsonObjectRequest(email.getText().toString(), fName.getText().toString(), lName.getText().toString(), pass1.getText().toString());
        }

    }


    private void makeJsonObjectRequest(final String email, String fname, String lname, String password){
        String url="http://agendatrip.com/api/v1/user/";
        Context context=getApplicationContext();
        Map<String, String> params = new HashMap<String, String>();
        params.put("email",email);
        params.put("firstname",fname);
        params.put("lastname", lname);
        params.put("password", password);
        Log.v("isi",email+" "+fname+" "+lname+" "+password);

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response){
                try{
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "New ID Registered", Toast.LENGTH_SHORT).show();
                    SharedPreferences sharedPreference=getApplicationContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreference.edit();
                    editor.putString("newid",email);
                    editor.commit();
                    finish();
                }
                catch(Exception e){ e.printStackTrace();}
            }


        },
                new Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        NetworkResponse nResponse = error.networkResponse;
  //                      String json = new String(nResponse.data);
                       Log.v("data", error.toString());
      //                  Log.v("diti", (String) json);
                        VolleyLog.d("errorMessage", "Error: " + error.getMessage());
                       /* if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                            android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                                    (Register.this);
                            adb.setMessage(getString(R.string.timeout_error));
                            adb.setTitle(getString(R.string.error));
                            adb.setPositiveButton(R.string.ok, null);
                            adb.create().show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                            //TODO
                        }
                        else if (error instanceof ParseError) {
                            Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                            //TODO
                        }*/
                    }
                });
        jsObjRequest.setRetryPolicy(Constants.RETRY_POLICY);
        jsObjRequest.setTag("Register");
        requestQueue.add(jsObjRequest);
    }



    private boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static final Pattern EMAIL_ADDRESS
            = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

}
