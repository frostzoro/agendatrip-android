package com.example.android.AgendaTrip;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

public class BookingSelection extends AppCompatActivity {
    LinearLayout flight,hotel,train;
    int agenda_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_selection);

        flight = (LinearLayout) findViewById(R.id.bookingSelectionFlight);
        hotel = (LinearLayout) findViewById(R.id.bookingSelectionHotel);
        train = (LinearLayout) findViewById(R.id.bookingSelectionTrain);

        flight.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                bookFlight();
            }
        });

        hotel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                bookHotel();
            }
        });

        train.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                bookTrain();
            }
        });

        agenda_id = getIntent().getIntExtra("agenda_id",-1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_booking_selection));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void bookFlight(){
        Intent i = new Intent(this,FlightBooking.class);
        i.putExtra("agenda_id",agenda_id);
        startActivity(i);
    }

    protected void bookHotel(){
        Intent i = new Intent(this,HotelBooking.class);
        i.putExtra("agenda_id",agenda_id);
        startActivity(i);
    }

    protected void bookTrain(){
        Intent i = new Intent(this,TrainBooking.class);
        i.putExtra("agenda_id",agenda_id);
        startActivity(i);
    }
}
