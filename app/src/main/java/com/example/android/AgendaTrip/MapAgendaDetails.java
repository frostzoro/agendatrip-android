package com.example.android.AgendaTrip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import model.VacationSite;
import singleton.AppController;
import utils.Util;

/**
 * Created by Vin on 12/1/2015.
 */
public class MapAgendaDetails extends Activity {
    double longitude, latitude;
    VacationSite vs;
    String placename;
    LatLng src2;
    Button showBtn;
    DownloadTask downloadTask;
    ArrayList<LatLng> markerPoints;
    GoogleMap googleMap;
    ArrayAdapter<String> smsearch;
    TextView tvDistanceDuration,pathName;
    AutoCompleteTextView tv;
    ProgressDialog pd;
    ArrayList<String > searchresults;
    Double LatSrc, LongSrc, LatDest, LongDest;
    String sourcename,destname;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapagendadetails);
        int vsidsrc, vsiddest;
        pathName=(TextView) findViewById(R.id.pathName);

        sourcename=getIntent().getStringExtra("sourcename");
        destname=getIntent().getStringExtra("destname");
        pathName.setText("Estimated path for: "+sourcename+" - "+destname);
        vsidsrc=getIntent().getIntExtra("sourcevsid",-1);
        vsiddest=getIntent().getIntExtra("destvsid",-1);
        Log.v("ya",vsidsrc+" + "+vsiddest);
        getData(vsidsrc, 1);
        getData(vsiddest,2);


    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        String str_origin = "origin="+origin.latitude+","+origin.longitude;
        String str_dest = "destination="+dest.latitude+","+dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin+"&"+str_dest+"&"+sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /**Get JSON from URL */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);


            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class DownloadTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    public void ShowPath(){
        final LatLng source=new LatLng(LatSrc,LongSrc);
        final LatLng dest=new LatLng(LatDest,LongDest);
        pd=new ProgressDialog(MapAgendaDetails.this);
        pd.setMessage("Getting Location");
        pd.setCancelable(false);
        tvDistanceDuration=(TextView) findViewById(R.id.tv_distance_time);
        markerPoints=new ArrayList<LatLng>();

        googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setMyLocationEnabled(true);

        //getLatLongFromAddress("binus");
        pd.hide();
        Marker TP = googleMap.addMarker(new MarkerOptions().position(source).title(sourcename+"(Origin)"));
        Marker TP2 = googleMap.addMarker(new MarkerOptions().position(dest).title(destname+"(Destination)"));
        TP.showInfoWindow();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(source, 14.0f));
        markerPoints.add(source);
        markerPoints.add(dest);

        LatLng origin = markerPoints.get(0);
        LatLng desti = markerPoints.get(1);

        // Getting URL to the Google Directions API
        final String url = getDirectionsUrl(origin, desti);
        downloadTask=new DownloadTask();
        downloadTask.execute(url);
    }

    /** Fungsi parsing Google Places ke JSON format*/
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionJSONParser parser = new DirectionJSONParser();
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if(result.size()<1){
                Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Ambil distance dari list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Ambil duration dari list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.BLUE);
            }

            tvDistanceDuration.setText("Distance:" + distance + ", Duration:" + duration);

            googleMap.addPolyline(lineOptions);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    protected void getData(int idvs, final int code) {
//code untuk tentuin apakah dia src atau dest
        Log.v("idvs + code", idvs+" | "+code);

        String requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util.VACATION_SITE_PATH +
                "/show";
        JSONObject params = new JSONObject();
        try {
            params.put("vacation_site_id",idvs);
        } catch (JSONException e) {
            Log.v("vacationsitedetail","error on putting params");
        }
        JsonObjectRequest vsRequest = new JsonObjectRequest(Request.Method.POST, requestUrl,
                (JSONObject) params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.v("kalo respon", 1+"");

                try {
                    JSONObject vacation_site = response.getJSONObject("payload");
                    vs = new VacationSite(vacation_site);

                    if(code==1){
                       LatSrc=vs.getLatitude(); LongSrc=vs.getLongitude();
                    Log.v("Sukses untuk source", LatSrc+" "+LongSrc);
                    }
                    else{LatDest=vs.getLatitude(); LongDest=vs.getLongitude();
                    Log.v("Sukses untuk dest", +LatDest+" "+LongDest);
                        ShowPath();
                    };


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pd.hide();
                Log.v("kalo ga", 0 + "");
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (MapAgendaDetails.this);
                    adb.setMessage(getString(R.string.timeout_error));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), "Auth Error", Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });

        AppController.getInstance().addToRequestQueue(vsRequest);
    }



}

