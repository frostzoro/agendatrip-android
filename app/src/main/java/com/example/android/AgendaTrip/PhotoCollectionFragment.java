package com.example.android.AgendaTrip;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;

import adapters.PhotoAdapter;


public class PhotoCollectionFragment extends Fragment {
    ArrayList<String> photos;
    PhotoAdapter pa;
    GridView photoGridView;
    Bundle b;
    public PhotoCollectionFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = this.getArguments();
        photos = b.getStringArrayList("photos");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_photo_collection, container, false);
        photoGridView = (GridView) v.findViewById(R.id.photoCollectionGridView);
        pa = new PhotoAdapter(getActivity(),R.layout.partial_photo,photos);
        photoGridView.setAdapter(pa);
        return v;
    }
}
