package com.example.android.AgendaTrip;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import adapters.AgendaAdapter;
import utils.Constants;
import utils.Util;

/**
 * Created by Vin on 11/5/2015.
 */
public class DeleteAgenda extends Activity {


    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delete_agenda_details);
        Intent i=getIntent();
        String namaAgenda=i.getStringExtra("agName");
        final int agID=i.getIntExtra("agID",9999);
        TextView AgendaName=(TextView) findViewById(R.id.AgendaName);
        AgendaName.setText(namaAgenda);
        Button delBtn=(Button) findViewById(R.id.delConfirm);

        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteAgenda(String.valueOf(agID));
            }
        });
    }
    private void DeleteAgenda(String agID) {

        Context context = getApplicationContext();
        String url = "http://agendatrip.com/api/v1/agenda/delete";
        Map<String, String> params = new HashMap<String, String>();
        Log.v("url",url+" "+Util.loggedID+Util.loggedKey+" "+agID);

        params.put("userID", Util.loggedID);
        params.put("key", Util.loggedKey);
        params.put("agenda_id", agID);


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                Log.v("dataresp", response.toString());


                Log.v("asd", response.toString());
                Intent i=new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("fragmentid",1);
                startActivity(i);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                    android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                            (DeleteAgenda.this);
                    adb.setMessage(getString(R.string.timeout_error));
                    adb.setTitle(getString(R.string.error));
                    adb.setPositiveButton(R.string.ok, null);
                    adb.create().show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(),"Auth Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ServerError) {
                    Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                    //TODO
                } else if (error instanceof ParseError) {
                    Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                    //TODO
                }
            }
        });
        jsObjRequest.setRetryPolicy(Constants.RETRY_POLICY);
        requestQueue.add(jsObjRequest);
    }

}
