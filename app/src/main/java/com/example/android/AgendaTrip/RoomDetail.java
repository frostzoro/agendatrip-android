package com.example.android.AgendaTrip;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import adapters.IconTextViewAdapter;
import model.Hotel;
import model.HotelSearch;
import utils.Util;

public class RoomDetail extends AppCompatActivity {
    TextView roomPolicy,roomPrice,roomName,roomPolicyHeader;
    ImageView roomPhoto;
    GridView roomFacility;
    Button bookButton;
    IconTextViewAdapter facAdapter;
    HotelSearch hs;
    Hotel.Room r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);
        receiveDataFromIntent();
        bindViewToVariables();
        setDataToView();
        setListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.hotel_booking);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void receiveDataFromIntent(){
        hs = (HotelSearch) getIntent().getSerializableExtra("hotelsearch");
        r = hs.getR();
    }

    protected void bindViewToVariables(){
        roomName = (TextView)findViewById(R.id.roomDetailName);
        roomPolicy = (TextView) findViewById(R.id.roomDetailRoomPolicy);
        roomPolicyHeader = (TextView) findViewById(R.id.roomDetailRoomPolicyHeader);
        roomPrice = (TextView) findViewById(R.id.roomDetailPrice);
        roomPhoto = (ImageView) findViewById(R.id.roomDetailRoomCover);
        roomFacility = (GridView) findViewById(R.id.roomDetailFacilityGrid);
        bookButton = (Button) findViewById(R.id.roomDetailBookButton);
    }

    protected void setDataToView(){
        roomName.setText(r.getName());
        ArrayList<String> policies = hs.getH().getPolicies().get(hs.getR().getName());
        if(policies!=null){
        int sz = policies.size();
        String s = "";
            for(int i=0; i<sz; i++){
                s += policies.get(i);
                s += "\r\n";
            }
        roomPolicy.setText(s);
        }
        else{
            roomPolicyHeader.setVisibility(View.GONE);
            roomPolicy.setVisibility(View.GONE);
        }
        roomPrice.setText(Util.convertDoubleToIDRString(r.getPrice())+" /room /night");
        Picasso.with(this).load(hs.getR().getPhoto()).placeholder(R.drawable
                .agenda_placeholder_narrow_1)
                .fit()
                .centerCrop()
                .into(roomPhoto);
        facAdapter = new IconTextViewAdapter(this,R.layout.partial_facility_item,R.drawable.check_circle_black_hdpi,r
                .getRoomFacilities());
        roomFacility.setAdapter(facAdapter);

    }

    protected void setListeners(){
        bookButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RoomDetail.this,HotelCheckout.class);
                Bundle b = new Bundle();
                b.putSerializable("hotelsearch",hs);
                i.putExtras(b);
                startActivity(i);
            }
        });
    }

}
