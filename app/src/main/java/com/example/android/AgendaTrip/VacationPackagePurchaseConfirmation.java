package com.example.android.AgendaTrip;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.gc.materialdesign.views.ButtonRectangle;
import com.gc.materialdesign.widgets.ProgressDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import model.VacationPackage;
import model.VacationPackagePurchase;
import singleton.AppController;
import utils.Constants;
import utils.Util;


public class VacationPackagePurchaseConfirmation extends AppCompatActivity {
    ButtonRectangle buyButton;
    Button backButton, veriTransButton;
    VacationPackage vp;
    VacationPackagePurchase vpc;
    ProgressDialog pd;
    Boolean showingPaymentInfo;
    TextView purchasenumber,purchasedate,purchaseamount,purchasestatus,vpname,vpdate,vpprice,vpenddate;
    ImageView vpimage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacation_package_purchase_confirmation);
        vp = (VacationPackage) getIntent().getSerializableExtra("vacation_package");
        showingPaymentInfo = false;
        setView();
    }

    protected void setView(){
        buyButton = (ButtonRectangle) findViewById(R.id
                .vacationPackagePurchaseConfirmationBuyButton);
        vpname = (TextView) findViewById(R.id.vacationPackagePurchaseConfirmationVacationPackageName);
        vpprice = (TextView) findViewById(R.id.vacationPackagePurchaseConfirmationVacationPackagePrice);
        vpdate = (TextView) findViewById(R.id.vacationPackagePurchaseConfirmationDateStart);
        vpenddate = (TextView) findViewById(R.id.vacationPackagePurchaseConfirmationDateEnd);
        vpname.setText(vp.getName());
        vpprice.setText(Util.convertDoubleToIDRString(vp.getPrice()));
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        vpdate.setText(sdf.format(vp.getDateStart()));
        vpenddate.setText(sdf.format(vp.getDateEnd()));

        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("vploading", "beforeloadshow");
                pd = new ProgressDialog(VacationPackagePurchaseConfirmation.this, getResources().getString(R.string.processing_request));
                pd.show();
                Log.v("vploading", "afterloadshow");
                sendBuyRequest();
            }
        });
    }

    protected void sendBuyRequest(){
        String requestUrl = Util.BASE_URL + Util.API_GATEWAY + Util
                .VACATION_PACKAGE_PURCHASE_PATH + "/new";
        HashMap<String,String> param = new HashMap<>();
        param.putAll(Util.getUserParam());
        Log.v("vpid", String.valueOf(vp.getId()));
        param.put("vacation_package_id", String.valueOf(vp.getId()));
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST,requestUrl,
                new JSONObject(param),new
                Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        pd.dismiss();
                        Log.v("Vpresponse",response.toString());
                        try {
                            vpc = new VacationPackagePurchase(response.getJSONObject("payload"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        onPurchaseSuccess();
                    }
                }, new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError || error instanceof NetworkError) {
                            android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder
                                    (VacationPackagePurchaseConfirmation.this);
                            adb.setMessage(getString(R.string.timeout_error));
                            adb.setTitle(getString(R.string.error));
                            adb.setPositiveButton(R.string.ok, null);
                            adb.create().show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(), "Auth Error", Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getApplicationContext(),"server Error",Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof ParseError) {
                            Toast.makeText(getApplicationContext(),"parse Error",Toast.LENGTH_LONG).show();
                            //TODO
                        }
                    }
        });

        jor.setRetryPolicy(Constants.RETRY_POLICY);
        jor.setTag("VacationPackagePurchaseConfirmation");
        AppController.getInstance().addToRequestQueue(jor);
    }

    protected void onPurchaseSuccess(){
        showingPaymentInfo = true;
        buyButton.setOnClickListener(null);
        setContentView(R.layout.vacation_package_purchase_thank_you);
        bindPaymentInfoPage();
        setPaymentInfoPage();
    }

    protected void bindPaymentInfoPage(){
        purchasenumber = (TextView)findViewById(R.id.vacationPackagePurchaseThankYouPurchaseNumber);
        purchasedate = (TextView) findViewById(R.id.vacationPackagePurchaseThankYouPurchaseDate);
        purchaseamount = (TextView) findViewById(R.id
                .vacationPackagePurchasethankYouPurchaseAmount);
        purchasestatus = (TextView) findViewById(R.id
                .vacationPackagePurchaseThankYouPurchaseStatus);
        vpname = (TextView) findViewById(R.id.vacationPackagePurchaseThankYouVacationPackageName);
        vpdate = (TextView) findViewById(R.id.vacationPackagePurchaseThankYouVacationPackageDate);
        vpimage = (ImageView) findViewById(R.id
                .vacationPackagePurchaseThankYouVacationPackageImage);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("Thank You");
        backButton = (Button) findViewById(R.id.vacationPackagePurchaseThankYouBackButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity();
            }
        });


        veriTransButton = (Button) findViewById(R.id.veritransButton);

        veriTransButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent paymentIntent = new Intent(VacationPackagePurchaseConfirmation.this,WebviewActivity.class);
                paymentIntent.putExtra("url",Util.PAYMENT_URL + "?order_id=" + vpc.getId() + "&order_type=package");
                startActivity(paymentIntent);
            }
        });
    }

    protected void finishActivity(){
        Log.v("isshhowingpay",showingPaymentInfo.toString());
        if(showingPaymentInfo){
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        finish();
    }

    @Override
    protected void onDestroy(){
        if(pd != null)
            pd.dismiss();
        super.onDestroy();
    }

    public void onBackPressed(){
        finishActivity();
    }

    protected void setPaymentInfoPage(){
        purchasenumber.setText(getResources().getText(R.string.purchase_number) +" "+ String
                .valueOf
                        (vpc
                                .getId()));
        purchasedate.setText(getResources().getText(R.string.purchase_date)+" " + vpc
                .getPurchaseDateAsString());
        purchaseamount.setText(getResources().getText(R.string.purchase_amount) +" IDR "+ String
                .valueOf
                        (vpc.getPrice()));
        purchasestatus.setText(getResources().getText(R.string.purchase_status)+" " + vpc.getStatus());
        vpname.setText(vp.getName());
        vpdate.setText("Departure Date: " + vp.getDepartureDateAsString());
        Picasso.with(getApplicationContext()).load(Util.BASE_URL + Util.VACATION_PACKAGE_IMAGE_URL + vp
                .getImage())
                .placeholder(R.drawable.placeholder_vacation_package).into(vpimage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Checkout");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
