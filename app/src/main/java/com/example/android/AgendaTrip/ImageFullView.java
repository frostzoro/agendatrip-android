package com.example.android.AgendaTrip;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import utils.Util;

public class ImageFullView extends AppCompatActivity {
    String image;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_full_view);


        image = getIntent().getStringExtra("image_url");
        Log.v("image is ", image);
        imageView = (ImageView) findViewById(R.id.ImageFullViewImageView);

        Picasso.with(this).load(Util.BASE_URL + Util.COVER_URL + image).fit().into(imageView);
    }

}
