package model;

import android.util.Log;

import java.io.Serializable;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by STEFAN on 12/25/2015.
 */
public class HotelSearch implements Serializable {
    String query,room,guest,token;
    Calendar checkIn,checkOut;
    int agenda_id;
    Hotel h;
    Hotel.Room r;

    public HotelSearch(){

    }

    public HotelSearch(String query,String room, String guest, Calendar checkIn, Calendar
            checkOut,String token){
        this.query = query;
        this.room = room;
        this.guest = guest;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.token = token;
    }

    public String toGetParam(){
        String param = "";
        param+="token="+token;
        param+="&q="+ URLEncoder.encode(query);
        param+="&room="+room;
        param+="&guest="+guest;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        param+="&startdate="+sdf.format(checkIn.getTime());
        param+="&enddate="+sdf.format(checkOut.getTime());
        Log.v("checkoutdate", sdf.format(checkOut.getTime()));
        return param;
    }

    public String toGetAddOrderParam(){
        String param ="";
        param+="token="+token;
        param+="&room="+room;
        param+="&room_id="+r.getRoom_id();
        param+="&adult="+guest;
        param+="&child=0";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        param+="&startdate="+sdf.format(checkIn.getTime());
        param+="&enddate="+sdf.format(checkOut.getTime());
        param+="&night="+getStayLength();
        return param;
    }

    public long getStayLength(){
        return (checkOut.getTimeInMillis() - checkIn.getTimeInMillis())/24/60/60/1000;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getGuest() {
        return guest;
    }

    public void setGuest(String guest) {
        this.guest = guest;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Calendar getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Calendar checkIn) {
        this.checkIn = checkIn;
    }

    public Calendar getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Calendar checkOut) {
        this.checkOut = checkOut;
    }

    public int getAgenda_id() {
        return agenda_id;
    }

    public void setAgenda_id(int agenda_id) {
        this.agenda_id = agenda_id;
    }

    public Hotel.Room getR() {
        return r;
    }

    public void setR(Hotel.Room r) {
        this.r = r;
    }

    public Hotel getH() {
        return h;
    }

    public void setH(Hotel h) {
        this.h = h;
    }

}
