package model;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by STEFAN on 2015/10/17.
 */
public class VacationSiteTag {
    private int id;
    private String name;
    private double value;
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public  VacationSiteTag(JSONObject src){
        try {
            //this.setId(src.getInt("id"));
            this.setName(src.getJSONObject("tag").getString("name"));
            this.setValue(src.getDouble("value"));
        }
        catch(Exception e){
            Log.v("Init VacationSiteTag",e.getMessage());
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
