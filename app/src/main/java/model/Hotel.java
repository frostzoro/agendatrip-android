package model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by STEFAN on 12/17/2015.
 */
public class Hotel implements Serializable{
    public HotelBreadcrumb breadCrumb;
    String description,address,primaryphoto;
    ArrayList<String> hotelFacilities;
    ArrayList<String> roomFacilities;
    ArrayList<String> sportFacilities;
    ArrayList<String> photos;
    ArrayList<Room> availableRooms;
     HashMap<String,ArrayList<String>> policies;
    public Hotel(){
    }

    public void setbreadCrumb(JSONObject source){
        breadCrumb = new HotelBreadcrumb(source);
    }

    public void setHotelData(JSONObject source){
        Log.v("setting hotel data","sethoteldata");
        try {
            this.description = source.getJSONObject("general").getString("description");
            this.address = source.getJSONObject("general").getString("address");
            this.primaryphoto = source.getString("primaryPhotos");
            this.primaryphoto = this.primaryphoto.substring(0,this.primaryphoto.indexOf(".s")) +
                    ".jpg";
            Log.v("hoprim",this.primaryphoto);
            JSONArray results = source.getJSONObject("results").getJSONArray("result");
            availableRooms = new ArrayList<>();
            int sz = results.length();
            for(int i=0; i<sz; i++){
                Room r = new Room(results.getJSONObject(i));
                availableRooms.add(r);
            }
            results = source.getJSONObject("all_photo").getJSONArray("photo");
            sz = results.length();
            photos = new ArrayList<>();
            for(int i=0; i<sz; i++){
                photos.add(results.getJSONObject(i).getString("file_name"));
            }
            results = source.getJSONObject("avail_facilities").getJSONArray("avail_facilitiy");
            sz = results.length();
            hotelFacilities = new ArrayList<>();
            roomFacilities = new ArrayList<>();
            sportFacilities = new ArrayList<>();
            for(int i=0; i<sz; i++){
                switch(results.getJSONObject(i).getString("facility_type")){
                    case "hotel" : hotelFacilities.add(results.getJSONObject(i).getString
                            ("facility_name")); break;
                    case "room" : roomFacilities.add(results.getJSONObject(i).getString
                            ("facility_name")); break;
                    case "sport" : sportFacilities.add(results.getJSONObject(i).getString
                            ("facility_name")); break;
                    default : break;
                }
            }
            policies = new HashMap<>();
            if(source.has("policy")){
                results = source.getJSONArray("policy");
                sz = results.length();
                for(int i=0; i<sz; i++){
                    ArrayList<String> policy = new ArrayList<>();
                    policy.add(results.getJSONObject(i).getString("tier_one"));
                    policy.add(results.getJSONObject(i).getString("tier_two"));
                    policies.put(results.getJSONObject(i).getString("name"),policy);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HotelBreadcrumb getBreadCrumb() {
        return breadCrumb;
    }

    public void setBreadCrumb(HotelBreadcrumb breadCrumb) {
        this.breadCrumb = breadCrumb;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPrimaryphoto() {
        return primaryphoto;
    }

    public void setPrimaryphoto(String primaryphoto) {
        this.primaryphoto = primaryphoto;
    }

    public ArrayList<String> getHotelFacilities() {
        return hotelFacilities;
    }

    public void setHotelFacilities(ArrayList<String> hotelFacilities) {
        this.hotelFacilities = hotelFacilities;
    }

    public ArrayList<String> getRoomFacilities() {
        return roomFacilities;
    }

    public void setRoomFacilities(ArrayList<String> roomFacilities) {
        this.roomFacilities = roomFacilities;
    }

    public ArrayList<String> getSportFacilities() {
        return sportFacilities;
    }

    public void setSportFacilities(ArrayList<String> sportFacilities) {
        this.sportFacilities = sportFacilities;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<String> photos) {
        this.photos = photos;
    }

    public ArrayList<Room> getAvailableRooms() {
        return availableRooms;
    }

    public void setAvailableRooms(ArrayList<Room> availableRooms) {
        this.availableRooms = availableRooms;
    }

    public HashMap<String, ArrayList<String>> getPolicies() {
        return policies;
    }

    public void setPolicies(HashMap<String, ArrayList<String>> policies) {
        this.policies = policies;
    }


    public class HotelBreadcrumb implements Serializable{
        String room_available,lat,lon,star,business_url,province,kecamatan,photo;
        String oldprice,price,name,regional,id,promo_name;

        public HotelBreadcrumb(){

        }

        public HotelBreadcrumb(JSONObject source){
            try {
                this.room_available = source.getString("room_available");
            this.lat = source.getString("latitude");
            this.lon = source.getString("longitude");
            this.star = source.getString("star_rating");
            this.business_url = source.getString("business_uri");
            this.province = source.getString("province_name");
            this.kecamatan = source.getString("kecamatan_name");
            this.photo = source.getString("photo_primary");
            if(source.getString("price").length()==0){
                this.price=source.getString("oldprice");
                this.oldprice=null;
            }
            else{
                this.price=source.getString("price");
                if(source.getString("oldprice").length()==0)
                    this.oldprice=null;
                else
                    this.oldprice=source.getString("oldprice");
            }
            this.promo_name = source.getString("promo_name");
            this.regional = source.getString("regional");
            this.name = source.getString("name");
            this.id = source.getString("hotel_id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public String getRoom_available() {
            return room_available;
        }

        public void setRoom_available(String room_available) {
            this.room_available = room_available;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public String getStar() {
            return star;
        }

        public void setStar(String star) {
            this.star = star;
        }

        public String getBusiness_url() {
            return business_url;
        }

        public void setBusiness_url(String business_url) {
            this.business_url = business_url;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getKecamatan() {
            return kecamatan;
        }

        public void setKecamatan(String kecamatan) {
            this.kecamatan = kecamatan;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getOldprice() {
            return oldprice;
        }

        public void setOldprice(String oldprice) {
            this.oldprice = oldprice;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRegional() {
            return regional;
        }

        public void setRegional(String regional) {
            this.regional = regional;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPromo_name() {
            return promo_name;
        }

        public void setPromo_name(String promo_name) {
            this.promo_name = promo_name;
        }


    }

    public class Room implements Serializable{
        String id;
        String room_id;
        String photo;
        String name;
        String bookUri;
        int room_available,minimum_stay ;
        Boolean with_breakfast;
        Double price,oldprice;

        ArrayList<String> roomFacilities;
        public Room(){

        }

        public Room(JSONObject source){
            try {
                id = source.getString("id");
                room_id = source.getString("room_id");
                photo = source.getString("photo_url");
                name = source.getString("room_name");
                price = source.getDouble("price");
                bookUri = source.getString("bookUri");
                if(source.has("oldprice"))
                    oldprice = source.getDouble("oldprice");
                else
                    oldprice = null;
                room_available = source.getInt("room_available");
                minimum_stay = source.getInt("minimum_stays");
                if(source.has("with_breakfast"))
                    with_breakfast = source.getInt("with_breakfast") == 1 ? true : false ;
                else
                    with_breakfast = false;
                roomFacilities = new ArrayList<>();
                if(source.has("room_facility")){
                    JSONArray facs = source.getJSONArray("room_facility");
                    int sz = facs.length();
                    for(int i=0; i<sz; i++){
                        roomFacilities.add(facs.getString(i));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public Boolean getWith_breakfast() {
            return with_breakfast;
        }

        public void setWith_breakfast(Boolean with_breakfast) {
            this.with_breakfast = with_breakfast;
        }

        public String getBookUri() {
            return bookUri;
        }

        public void setBookUri(String bookUri) {
            this.bookUri = bookUri;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getRoom_available() {
            return room_available;
        }

        public void setRoom_available(int room_available) {
            this.room_available = room_available;
        }

        public int getMinimum_stay() {
            return minimum_stay;
        }

        public void setMinimum_stay(int minimum_stay) {
            this.minimum_stay = minimum_stay;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Double getOldprice() {
            return oldprice;
        }

        public void setOldprice(Double oldprice) {
            this.oldprice = oldprice;
        }

        public ArrayList<String> getRoomFacilities() {
            return roomFacilities;
        }

        public void setRoomFacilities(ArrayList<String> roomFacilities) {
            this.roomFacilities = roomFacilities;
        }
    }
}
