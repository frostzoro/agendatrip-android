package model;

/**
 * Created by STEFAN on 11/29/2015.
 */
public class User {
    private String key;
    private String id;

    public User(){

    }

    public User(String key, String id){
        this.key = key;
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
