package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by STEFAN on 12/22/2015.
 */
public class UserIdentity implements Serializable {

    int id;
    String title;
    String first_name;
    String last_name;
    String phone_number;


    String email;
    String id_number;
    String passport_number;
    Calendar birthdate;

    public UserIdentity(){

    }

    public UserIdentity(JSONObject source){
        try {
            this.id = Integer.parseInt(source.getString("id"));
            this.title = source.getString("title");
            this.first_name = source.getString("first_name");
            this.last_name = source.getString("last_name");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            this.birthdate = Calendar.getInstance();
            this.birthdate.setTime(sdf.parse(source.getString("birthdate")));
            this.email = source.getString("email");
            this.phone_number = source.getString("phone_number");
            this.id_number = source.getString("id_number");
            this.passport_number = source.getString("passport_number");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthdate(Calendar birthdate) {
        this.birthdate = birthdate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getPassport_number() {
        return passport_number;
    }

    public void setPassport_number(String passport_number) {
        this.passport_number = passport_number;
    }

    public Date getBirthdate() {
        return birthdate.getTime();
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate.setTime(birthdate);
    }

}
