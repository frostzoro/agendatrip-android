package model;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.AgendaTrip.R;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by STEFAN on 12/9/2015.
 */
public class TrainOrder {

    Train train;
    int adultNum,infantNum,childNum;
    String type;
    ArrayList<PassengerDetail> p;
    Activity act;
    LayoutInflater inflater;
    int partial_resource_id = R.layout.flight_order_detail_partial;

    public TrainOrder(Train train, TrainSearch fs, String type, Activity act){
        this.train = train;
        this.act = act;
        this.adultNum = fs.getAdult();
        this.childNum = fs.getChild();
        this.infantNum = fs.getInfant();
        this.type = type;
        p = new ArrayList<PassengerDetail>();
    }

    public View getView(){
        TextView destination,airlineName,flightNumber,baggageIcon,taxIcon,foodIcon,departDate,
                time,detail,adult,child,infant,subtotal,caption;
        ImageView airlineLogo;
        View view;
        inflater = LayoutInflater.from(act);
        view = inflater.inflate(partial_resource_id, null);
        caption = (TextView) view.findViewById(R.id.flightOrderPartialCaption);
        destination = (TextView) view.findViewById(R.id.flightOrderDetailPartialDestination);
        airlineName = (TextView) view.findViewById(R.id.flightOrderDetailPartialAirlinename);
        flightNumber = (TextView) view.findViewById(R.id.flightOrderDetailPartialFlightNumber);
        baggageIcon = (TextView) view.findViewById(R.id.flightOrderDetailPartialBaggageIcon);
        taxIcon = (TextView) view.findViewById(R.id.flightOrderDetailPartialTaxIcon);
        foodIcon = (TextView) view.findViewById(R.id.flightOrderDetailPartialFoodIcon);
        departDate = (TextView) view.findViewById(R.id.flightOrderDetailPartialDepartDate);
        time = (TextView) view.findViewById(R.id.flightOrderDetailPartialTime);
        detail = (TextView) view.findViewById(R.id.flightOrderDetailPartialDetail);
        adult = (TextView) view.findViewById(R.id.flightOrderDetailPartialAdult);
        child = (TextView) view.findViewById(R.id.flightOrderDetailPartialChild);
        infant = (TextView) view.findViewById(R.id.flightOrderDetailPartialInfant);
        subtotal = (TextView) view.findViewById(R.id.flightOrderDetailPartialSubtotal);
        airlineLogo = (ImageView) view.findViewById(R.id.flightOrderDetailPartialAirlineLogo);
        caption.setText(type);
        Log.v("flightorder", train.getDeparture_station() + train.getArrival_station());
        if(type.equals("Departure"))
            destination.setText(train.getDeparture_station() + " -> " + train.getArrival_station());
        else
            destination.setText(train.getArrival_station() + " -> " + train.getDeparture_station());
        airlineName.setText(train.getTrain_name());
        flightNumber.setText(train.getTrain_id());
        baggageIcon.setVisibility(View.GONE);
        foodIcon.setVisibility(View.GONE);
        taxIcon.setVisibility(View.GONE);
        departDate.setText(train.getDate());
        time.setText(train.getDeparture_time() + " - " +train.getArrival_time());
        detail.setText(train.getDuration());
        adult.setText(this.adultNum + " x Adult x " + NumberFormat.getCurrencyInstance(new Locale
                ("EN", "ID")).format
                (train.getPrice_adult()));
        if(childNum>0){
            child.setText(this.childNum + " x Children x " + NumberFormat.getCurrencyInstance(new
            Locale("EN","ID")).format(train.getPrice_child()));
        }
        else{
            child.setVisibility(View.GONE);
        }
        if(infantNum>0){
            child.setText(this.infantNum + " x Infant x " + NumberFormat.getCurrencyInstance(new
                    Locale("EN","ID")).format(train.getPrice_infant()));
        }
        else{
            infant.setVisibility(View.GONE);
        }

        subtotal.setText(NumberFormat.getCurrencyInstance(new
                Locale("EN","ID")).format((getSubTotal())));

        //Picasso.with(act).load(train.getImage()).placeholder(R.drawable.description).into
          //      (airlineLogo);
        return view;
    }

    public double getSubTotal(){
        return (childNum * train.getPrice_infant() + adultNum * train
                .getPrice_adult
                        () + infantNum * train.getPrice_infant());
    }

    public Train getFlight() {
        return train;
    }

    public void setFlight(Train train) {
        this.train = train;
    }

    public ArrayList<PassengerDetail> getP() {
        return p;
    }

    public void setP(ArrayList<PassengerDetail> p) {
        this.p = p;
    }

    public void addPassengerDetail(PassengerDetail pd){
        this.p.add(pd);
    }
}
