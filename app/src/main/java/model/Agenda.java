package model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Vin on 10/22/2015.
 */
public class Agenda implements Serializable {
    private int id;
    private String userid;
    private String name;
    private String startDate;
    private String endDate;


    public Agenda() {
    }

    public Agenda(JSONObject src) {
        try {
            this.id = src.getInt("id");
            this.userid = src.getString("user_id");
            this.name = src.getString("name");
            this.startDate = src.getString("start");
            this.endDate = src.getString("end");
        } catch (Exception e) {
            Log.v("Agenda Json Error", e.getStackTrace().toString());

        }
    }

    public Agenda (Agenda src){
        this.id = src.id;
        this.userid = src.userid;
        this.name = src.name;
        this.startDate = src.startDate;
        this.endDate = src.endDate;


    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }


}
