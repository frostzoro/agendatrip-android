package model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by STEFAN on 11/24/2015.
 */
public class VacationPackagePurchase implements Serializable {
    int id, user_id;
    double price;
    VacationPackage vp;
    String status,reference_code;
    Calendar purchase_date;

    public VacationPackagePurchase(){
    }

    public VacationPackagePurchase(JSONObject src){
        Log.v("jsonobjectvpkg", src.toString());
        try {
            setId(src.getInt("id"));
            setUser_id(src.getInt("user_id"));
            mapVP(src.getJSONObject("vacation_package"));
            setPrice(src.getDouble("order_amount"));
            setStatus(src.getString("status"));
            setReference_code(src.getString("reference_code"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            purchase_date = Calendar.getInstance();
            purchase_date.setTime(sdf.parse(src.getString("created_at")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void mapVP(JSONObject src) throws JSONException, ParseException {
        vp = new VacationPackage();
        vp.setId(src.getInt("id"));
        vp.setName(src.getString("title"));
        vp.setImage(src.getString("image"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        vp.setDateStart(sdf.parse(src.getString("date_start")));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public VacationPackage getVp() {
        return vp;
    }

    public void setVp(VacationPackage vp) {
        this.vp = vp;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReference_code() {
        return reference_code;
    }

    public void setReference_code(String reference_code) {
        this.reference_code = reference_code;
    }

    public Calendar getPurchase_date() {
        return purchase_date;
    }

    public String getPurchaseDateAsString(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
        String dateString = sdf.format(this.getPurchase_date().getTime());
        return dateString;
    }

    public void setPurchase_date(Calendar purchase_date) {
        this.purchase_date = purchase_date;
    }

}
