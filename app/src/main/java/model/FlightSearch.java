package model;

import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by STEFAN on 12/3/2015.
 */
public class FlightSearch implements Serializable, Cloneable {
    String d;
    String a;
    String d_complete;

    String a_complete;
    Calendar date,ret_date;
    int adult,child,infant;
    int v;

    int agenda_id;
    String token;

    Boolean round_trip;

    public Flight departure_flight,return_flight;

    public FlightSearch(){
        d = "";
        a = "";
        date = Calendar.getInstance();
        ret_date = Calendar.getInstance();
        adult=0;
        child=0;
        infant=0;
        v=0;
        token="";
        round_trip=false;
    }

    public HashMap<String,String> toParam(){
        HashMap<String,String> param = new HashMap<>();
        param.put("d",d);
        param.put("a",a);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        param.put("date",sdf.format(date.getTime()));
        if(round_trip)
            param.put("ret_date",sdf.format(date.getTime()));
        param.put("adult",String.valueOf(adult));
        param.put("child",String.valueOf(child));
        param.put("infant",String.valueOf(infant));
        param.put("token",token);
        return param;
    }

    public Object clone() {
        try {
            return (FlightSearch)super.clone();
        }
        catch (CloneNotSupportedException e) {
            // This should never happen
        }
        return null;
    }

    public String getA_complete() {
        return a_complete;
    }

    public void setA_complete(String a_complete) {
        this.a_complete = a_complete;
    }

    public String getD_complete() {
        return d_complete;
    }

    public void setD_complete(String d_complete) {
        this.d_complete = d_complete;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public Calendar getRet_date() {
        return ret_date;
    }

    public void setRet_date(Calendar ret_date) {
        this.ret_date = ret_date;
    }

    public int getAdult() {
        return adult;
    }

    public void setAdult(int adult) {
        this.adult = adult;
    }

    public int getChild() {
        return child;
    }

    public void setChild(int child) {
        this.child = child;
    }

    public int getInfant() {
        return infant;
    }

    public void setInfant(int infant) {
        this.infant = infant;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getRound_trip() {
        return round_trip;
    }

    public void setRound_trip(Boolean round_trip) {
        this.round_trip = round_trip;
    }


    public int getAgenda_id() {
        return agenda_id;
    }

    public void setAgenda_id(int agenda_id) {
        this.agenda_id = agenda_id;
    }

    public HashMap<String,String> toPostParam(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        HashMap<String,String> h = new HashMap<>();
        h.put("a",this.a);
        h.put("d",this.d);
        h.put("token",this.token);
        h.put("date",sdf.format(this.date.getTime()));
        h.put("round_trip",this.round_trip.toString());
        if(round_trip) h.put("ret_date",sdf.format(this.ret_date.getTime()));
        h.put("adult",String.valueOf(this.adult));
        h.put("infant",String.valueOf(this.infant));
        h.put("child",String.valueOf(this.child));
        return h;
    }

    public String toGetParam(){
        HashMap<String,String> h = toPostParam();
        String param="";
        Boolean isFirst = true;
        for(Map.Entry<String,String> en : h.entrySet()){
            if(!isFirst)param+="&";
            param+= en.getKey() + "="+en.getValue();
            isFirst=false;
        }
        return param;
    }
}
