package model;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.AgendaTrip.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import utils.Util;

/**
 * Created by STEFAN on 12/17/2015.
 */
public class BookedItem implements Serializable {
    Integer id,user_id,agenda_id;
    Double order_amount;
    String order_id,order_detail_id,order_type,token;
    String status;
    String contact_name,contact_title,contact_phone,contact_email;

    String order_date;
    BookedItemDetail bid;

    public BookedItem(JSONObject source){
        try {
            JSONArray order_detail = source.getJSONArray("order");
            this.id = source.getInt("id");
            if(!source.isNull("user_id"))
                this.user_id = source.getInt("user_id");
            if(!source.isNull("agenda_id"))
                this.agenda_id = source.getInt("agenda_id");
            this.order_id = source.getString("order_id");
            this.token = source.getString("token");
            this.order_amount = source.getDouble("order_amount");

            this.order_detail_id = source.getString("order_detail_id");
            this.order_date = source.getString("created_at");
            this.contact_email = source.getString("contact_email");
            this.status = source.getString("status");
            this.order_type = order_detail.getJSONObject(0).getString("order_type");
            bid = new BookedItemDetail(order_detail.getJSONObject(0));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    //<editor-fold desc="BookedItem Setter Getter">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getAgenda_id() {
        return agenda_id;
    }

    public void setAgenda_id(Integer agenda_id) {
        this.agenda_id = agenda_id;
    }

    public Double getOrder_amount() {
        return order_amount;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public void setOrder_amount(Double order_amount) {
        this.order_amount = order_amount;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_detail_id() {
        return order_detail_id;
    }

    public void setOrder_detail_id(String order_detail_id) {
        this.order_detail_id = order_detail_id;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_title() {
        return contact_title;
    }

    public void setContact_title(String contact_title) {
        this.contact_title = contact_title;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public String getContact_email() {
        return contact_email;
    }

    public void setContact_email(String contact_email) {
        this.contact_email = contact_email;
    }

    public BookedItemDetail getBid() {
        return bid;
    }

    public void setBid(BookedItemDetail bid) {
        this.bid = bid;
    }
    //</editor-fold>

    public class BookedItemDetail implements Serializable{
        String order_name,order_detail_id,order_type,order_name_detail;
        BookedItemDetailInfo bidi;
        View bookedDetailView;

        public BookedItemDetail(JSONObject source){
            try {
                this.order_name = source.getString("order_name");
                this.order_type = source.getString("order_type");
                this.order_name_detail = source.getString("order_name_detail");
                this.order_detail_id = source.getString("order_detail_id");
                switch(this.order_type){
                    case "flight":setFlightDetailInfo(source.getJSONObject("detail"));
                        break;
                    case "hotel":setHotelDetailInfo(source);
                        break;
                    case "train" :setTrainDetailInfo(source);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void setFlightDetailInfo(JSONObject source){
            bidi = new BookedItemDetailFlight(source);
        }

        public void setHotelDetailInfo(JSONObject source){
            bidi = new BookedItemDetailHotel(source);
        }

        public void setTrainDetailInfo(JSONObject source){
            bidi = new BookedItemDetailTrain(source);
        }

        public View getView(Activity act){
            View mView;
            switch(order_type){
                case "flight" : mView =  getFlightView(act); break;
                case "hotel" : mView =  getHotelView(act); break;
                case "train" : mView = getTrainView(act); break;
                default :  mView = null; break;
            }
            return mView;
        }

        protected View getFlightView(Activity act){
            TextView name,name_detail,date,time,adult,child,infant,subtotal;
            ImageView airline_logo;
            if(bookedDetailView == null){
                LayoutInflater inflater = LayoutInflater.from(act);
                bookedDetailView = inflater.inflate(R.layout.partial_booked_item_detail_flight,
                        null);
                name = (TextView) bookedDetailView.findViewById(R.id
                        .partialBookedIitemDetailFlightName);
                name_detail = (TextView) bookedDetailView.findViewById(R.id
                        .partialBookedItemDetailFlightNameDetail);
                date = (TextView) bookedDetailView.findViewById(R.id
                        .partialBookedItemDetailFlightDate);
                time = (TextView) bookedDetailView.findViewById(R.id
                        .partialBookedItemDetailFlightTime);
                adult = (TextView) bookedDetailView.findViewById(R.id
                        .partialBookedItemDetailFlightAdult);
                child = (TextView) bookedDetailView.findViewById(R.id
                        .partialBookedItemDetailFlightChild);
                infant = (TextView) bookedDetailView.findViewById(R.id
                        .partialBookedItemDetailFlightInfant);
                subtotal = (TextView) bookedDetailView.findViewById(R.id
                        .partialBookedItemDetailFlightSubtotal);
                airline_logo = (ImageView) bookedDetailView.findViewById(R.id
                        .partialBookedItemDetailFlightAirlineLogo);
                BookedItemDetailFlight bidf = (BookedItemDetailFlight) bidi;
                name.setText(getOrder_name());
                name_detail.setText(getOrder_name_detail());
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                date.setText(sdf.format(bidf.getDeparture().getTime()));
                SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm");
                time.setText(sdfTime.format(bidf.getDeparture().getTime()) + " - " + sdfTime
                        .format(bidf.getArrival().getTime()));
                adult.setText(bidf.getAdult() + " x Adult");
                if(bidf.getChild()>0){
                    child.setText(bidf.getChild() + " x Child");
                }
                else{
                    child.setVisibility(View.GONE);
                }
                if(bidf.getInfant()>0){
                    infant.setText(bidf.getInfant() + " x Infant");
                    infant.setVisibility(View.GONE);
                }
                else{
                    child.setVisibility(View.GONE);
                }
                subtotal.setText(Util.convertDoubleToIDRString(getOrder_amount()));
            }


            return bookedDetailView;
        }

        protected View getHotelView(Activity act){
            TextView orderName,hotelName,roomName,contactName,contactPhone,totalPrice,checkInDate,nights;
            if(bookedDetailView == null ){
                LayoutInflater inflater = LayoutInflater.from(act);
                bookedDetailView = inflater.inflate(R.layout.partial_booked_item_detail_hotel,
                        null);
                orderName = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailHotelOrderName);
                hotelName = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailHotelHotelName);
                roomName = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailHotelRoomName);
                contactName = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailHotelContactName);
                contactPhone = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailHotelContactPhone);
                totalPrice = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailHotelSubtotal);
                checkInDate = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailHotelCheckInDate);
                nights = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailHotelStayLength);
                BookedItemDetailHotel bidh = (BookedItemDetailHotel) bidi;
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                String mOrderName = sdf.format(bidh.getCheckIn().getTime()) + " - " +sdf.format(bidh.getCheckOut().getTime()) + ", " + bidh.getRooms() + " room(s), " + bidh.getNights() + " night(s)";
                orderName.setText(mOrderName);
                hotelName.setText(bidh.getHotelName());
                roomName.setText(bidh.getRoomName());
                contactName.setText(bidh.getContactName());
                contactPhone.setText(bidh.getContactPhone());
                totalPrice.setText(Util.convertDoubleToIDRString(bidh.getTotalprice()));
                checkInDate.setText(sdf.format(bidh.getCheckIn().getTime()));
                nights.setText(bidh.getNights() + " Night(s)");
            }
            return bookedDetailView;
        }

        protected View getTrainView(Activity act){
            TextView orderName,trainName,trainClass,trainDeparture,trainArrival,departStation,arriveStation,adult,child,infant,subTotal;
            ImageView trainLogo;
            if(bookedDetailView == null){
                LayoutInflater inflater = LayoutInflater.from(act);
                bookedDetailView = inflater.inflate(R.layout.partial_booked_item_detail_train,
                        null);
                orderName = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainOrderName);
                trainName = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainName);
                trainClass = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainClass);
                trainDeparture = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainDeparture);
                trainArrival = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainArrival);
                departStation = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainDepartureStation);
                arriveStation = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainArrivalStation);
                adult = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainAdult);
                child = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainChild);
                infant = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainInfant);
                subTotal = (TextView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainSubtotal);
                trainLogo = (ImageView) bookedDetailView.findViewById(R.id.partialBookedItemDetailTrainLogo);
                BookedItemDetailTrain bidt = (BookedItemDetailTrain) bidi;
                orderName.setText(bidt.getOrderName());
                trainName.setText(bidt.getTrainName());
                trainClass.setText(bidt.getTrainClass());
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm");
                trainDeparture.setText(sdf.format(bidt.getDeparture().getTime()));
                departStation.setText(bidt.getDepartStation());
                trainArrival.setText(sdf.format(bidt.getArrival().getTime()));
                arriveStation.setText(bidt.getArriveStation());
                adult.setText(bidt.getAdult() + " Adult(s)");
                child.setText(bidt.getChild() + " Child(s)");
                infant.setText(bidt.getInfant() + " Infant(s)");
                subTotal.setText(Util.convertDoubleToIDRString(bidt.getSubtotal()));
                Picasso.with(act).load(R.drawable.agenda_placeholder_square_1).fit().into(trainLogo);
            }
            return bookedDetailView;
        }

        //<editor-fold desc="BookedItemDetail Setter Getter">
        public String getOrder_name() {
            return order_name;
        }

        public void setOrder_name(String order_name) {
            this.order_name = order_name;
        }

        public String getOrder_detail_id() {
            return order_detail_id;
        }

        public void setOrder_detail_id(String order_detail_id) {
            this.order_detail_id = order_detail_id;
        }

        public String getOrder_type() {
            return order_type;
        }

        public void setOrder_type(String order_type) {
            this.order_type = order_type;
        }

        public String getOrder_name_detail() {
            return order_name_detail;
        }

        public void setOrder_name_detail(String order_name_detail) {
            this.order_name_detail = order_name_detail;
        }
        //</editor-fold>

        public class BookedItemDetailInfo implements Serializable{
        }

        public class BookedItemDetailFlight extends BookedItemDetailInfo{
            String contact_name,contact_title,contact_phone;
            int adult, child, infant;
            String flight_number,airline_name;
            Calendar departure,arrival;

            public BookedItemDetailFlight(){

            }

            public BookedItemDetailFlight(JSONObject source){
                try {
                    this.contact_name = source.getString("contact_first_name");
                    this.contact_title = source.getString("contact_title");
                    this.contact_phone = source.getString("contact_phone");
                    this.adult = source.getInt("count_adult");
                    this.child = source.getInt("count_child");
                    this.infant = source.getInt("count_infant");
                    this.flight_number = source.getString("flight_number");
                    this.airline_name = source.getString("airlines_name");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    this.departure = Calendar.getInstance();
                    this.departure.setTime(sdf.parse(source.getString("departure_time")));
                    this.arrival = Calendar.getInstance();
                    this.arrival.setTime(sdf.parse(source.getString("arrival_time")));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

            //<editor-fold desc="BookedItemDetailFlight Setter Getter">
            public String getContact_name() {
                return contact_name;
            }

            public void setContact_name(String contact_name) {
                this.contact_name = contact_name;
            }

            public String getContact_title() {
                return contact_title;
            }

            public void setContact_title(String contact_title) {
                this.contact_title = contact_title;
            }

            public String getContact_phone() {
                return contact_phone;
            }

            public void setContact_phone(String contact_phone) {
                this.contact_phone = contact_phone;
            }

            public int getAdult() {
                return adult;
            }

            public void setAdult(int adult) {
                this.adult = adult;
            }

            public int getChild() {
                return child;
            }

            public void setChild(int child) {
                this.child = child;
            }

            public int getInfant() {
                return infant;
            }

            public void setInfant(int infant) {
                this.infant = infant;
            }

            public String getFlight_number() {
                return flight_number;
            }

            public void setFlight_number(String flight_number) {
                this.flight_number = flight_number;
            }

            public String getAirline_name() {
                return airline_name;
            }

            public void setAirline_name(String airline_name) {
                this.airline_name = airline_name;
            }

            public Calendar getDeparture() {
                return departure;
            }

            public void setDeparture(Calendar departure) {
                this.departure = departure;
            }

            public Calendar getArrival() {
                return arrival;
            }

            public void setArrival(Calendar arrival) {
                this.arrival = arrival;
            }
            //</editor-fold>

        }

        public class BookedItemDetailHotel extends BookedItemDetailInfo{
            String hotelName,roomName,contactName,contactPhone;
            int rooms;
            int nights;
            Double totalprice;
            Calendar checkIn,checkOut;

            public BookedItemDetailHotel(JSONObject source){
                try {
                    JSONObject detail = source;
                    this.hotelName = detail.getString("order_name");
                    this.roomName = detail.getString("order_name_detail");
                    JSONObject passObj = detail.getJSONArray("passanger").getJSONObject(0);
                    this.contactName =passObj.getString("account_salutation_name") + " " + passObj.getString("account_first_name") + " " +passObj.getString("account_last_name");
                    this.contactPhone = passObj.getString("account_phone");
                    this.rooms = detail.getJSONObject("detail").getInt("rooms");
                    this.totalprice = detail.getDouble("customer_price");
                    this.nights = detail.getInt("total_ticket");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    this.checkIn = Calendar.getInstance();
                    checkIn.setTime(sdf.parse(detail.getJSONObject("detail").getString("checkin")));
                    checkOut = (Calendar) checkIn.clone();
                    checkOut.add(Calendar.DAY_OF_MONTH,nights);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            public int getNights() {
                return nights;
            }

            public void setNights(int nights) {
                this.nights = nights;
            }

            public Calendar getCheckOut() {
                return checkOut;
            }

            public void setCheckOut(Calendar checkOut) {
                this.checkOut = checkOut;
            }

            public Calendar getCheckIn() {
                return checkIn;
            }

            public void setCheckIn(Calendar checkIn) {
                this.checkIn = checkIn;
            }

            public String getHotelName() {
                return hotelName;
            }

            public void setHotelName(String hotelName) {
                this.hotelName = hotelName;
            }

            public String getRoomName() {
                return roomName;
            }

            public void setRoomName(String roomName) {
                this.roomName = roomName;
            }

            public String getContactName() {
                return contactName;
            }

            public void setContactName(String contactName) {
                this.contactName = contactName;
            }

            public String getContactPhone() {
                return contactPhone;
            }

            public void setContactPhone(String contactPhone) {
                this.contactPhone = contactPhone;
            }

            public int getRooms() {
                return rooms;
            }

            public void setRooms(int rooms) {
                this.rooms = rooms;
            }

            public Double getTotalprice() {
                return totalprice;
            }

            public void setTotalprice(Double totalprice) {
                this.totalprice = totalprice;
            }

        }

        public class BookedItemDetailTrain extends BookedItemDetailInfo{
            String orderName;
            String trainName;
            String trainClass;
            String seatNumber;
            String departStation;

            public Calendar getDeparture() {
                return departure;
            }

            public void setDeparture(Calendar departure) {
                this.departure = departure;
            }

            public Calendar getArrival() {
                return arrival;
            }

            public void setArrival(Calendar arrival) {
                this.arrival = arrival;
            }

            String arriveStation;
            int adult,child,infant;
            Double subtotal;
            ArrayList<Passenger> passengerArrayList;
            Calendar departure,arrival;

            public BookedItemDetailTrain(){

            }

            public BookedItemDetailTrain(JSONObject source){
                JSONObject cartDetail = null;
                try {
                    Log.v("booekditemsource",source.toString());
                    JSONObject cartDetailDetail =source.getJSONObject("detail");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    this.orderName = source.getString("order_name");
                    this.trainName = cartDetailDetail.getString("train_name");
                    this.trainClass = cartDetailDetail.getString("train_class");
                    this.seatNumber = cartDetailDetail.getString("tiket_seating");
                    this.adult = cartDetailDetail.getInt("count_adult");
                    this.child = cartDetailDetail.getInt("count_child");
                    this.infant = cartDetailDetail.getInt("count_infant");
                    this.subtotal = source.getDouble("customer_price");
                    this.departure = Calendar.getInstance();
                    departure.setTime(sdf.parse(cartDetailDetail.getString("departure_datetime")));
                    this.arrival = Calendar.getInstance();
                    arrival.setTime(sdf.parse(cartDetailDetail.getString("arrival_datetime")));
                    this.departStation = cartDetailDetail.getString("train_from");
                    this.arriveStation = cartDetailDetail.getString("train_to");
                    JSONArray passenger = source.getJSONArray("passanger");
                    int sz = passenger.length();
                    passengerArrayList = new ArrayList<>();
                    for(int i=0; i<sz; i++ ){
                        Passenger p = new Passenger(passenger.getJSONObject(i));
                        passengerArrayList.add(p);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            public String getOrderName() {
                return orderName;
            }

            public void setOrderName(String orderName) {
                this.orderName = orderName;
            }

            public String getTrainName() {
                return trainName;
            }

            public void setTrainName(String trainName) {
                this.trainName = trainName;
            }

            public String getTrainClass() {
                return trainClass;
            }

            public void setTrainClass(String trainClass) {
                this.trainClass = trainClass;
            }

            public String getSeatNumber() {
                return seatNumber;
            }

            public void setSeatNumber(String seatNumber) {
                this.seatNumber = seatNumber;
            }

            public int getAdult() {
                return adult;
            }

            public void setAdult(int adult) {
                this.adult = adult;
            }

            public int getChild() {
                return child;
            }

            public void setChild(int child) {
                this.child = child;
            }

            public int getInfant() {
                return infant;
            }

            public void setInfant(int infant) {
                this.infant = infant;
            }

            public Double getSubtotal() {
                return subtotal;
            }

            public void setSubtotal(Double subtotal) {
                this.subtotal = subtotal;
            }

            public ArrayList<Passenger> getPassengerArrayList() {
                return passengerArrayList;
            }

            public void setPassengerArrayList(ArrayList<Passenger> passengerArrayList) {
                this.passengerArrayList = passengerArrayList;
            }

            public String getArriveStation() {
                return arriveStation;
            }

            public void setArriveStation(String arriveStation) {
                this.arriveStation = arriveStation;
            }

            public String getDepartStation() {
                return departStation;
            }

            public void setDepartStation(String departStation) {
                this.departStation = departStation;
            }
        }

    }

}
