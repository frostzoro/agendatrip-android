package model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by STEFAN on 12/9/2015.
 */
public class PassengerDetail {
    String first_name,last_name,id_number,passport_number,birth_date,phone_number_1,
            phone_number_2,title;

    public PassengerDetail(JSONObject source){
        try {
            this.first_name=source.getString("first_name");
            this.last_name=source.getString("last_name");
            this.id_number=source.getString("id_number");
            this.passport_number=source.getString("passport_number");
            this.birth_date=source.getString("birth_date");
            this.phone_number_1=source.getString("phone_number_1");
            this.phone_number_2=source.getString("phone_number_2");
            this.title=source.getString("title");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }
}
