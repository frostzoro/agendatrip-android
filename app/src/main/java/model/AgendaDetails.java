package model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Vin on 10/25/2015.
 */
public class AgendaDetails implements Serializable {
    private String vacation_site_name;

    public String getVacation_site_name() {
        return vacation_site_name;
    }

    public void setVacation_site_name(String vacation_site_name) {
        this.vacation_site_name = vacation_site_name;
    }

    private int agenda_id;
    private int vacation_site_id;
    private String vacationSiteImage;
    private String startTime, endTime;
    private Double lat,lng;
    public AgendaDetails(){}

    public AgendaDetails(JSONObject src){
        try {
            this.agenda_id = src.getInt("id");
            this.vacation_site_id=src.getInt("vacation_site_id");
            this.startTime=src.getString("start");
            this.endTime=src.getString("end");
            this.vacationSiteImage = src.getJSONObject("vacation_site").getString("image");
            this.lat=src.getJSONObject("vacation_site").getDouble("latitude");
            this.lng=src.getJSONObject("vacation_site").getDouble("longitude");
            Log.v("ASDSA",this.vacationSiteImage);
            Log.v("OBJEK",src.toString());
        }
        catch(Exception e){
            Log.v("Agenda Json Error", e.getStackTrace().toString());

        }

    }

    public AgendaDetails(AgendaDetails src){
        this.agenda_id=src.agenda_id;
        this.vacation_site_id=src.vacation_site_id;
        this.startTime=src.startTime;
        this.endTime=src.endTime;
    }

    public int getAgenda_id() {
        return agenda_id;
    }

    public void setAgenda_id(int agenda_id) {
        this.agenda_id = agenda_id;
    }

    public int getVacation_site_id() {
        return vacation_site_id;
    }

    public void setVacation_site_id(int vacation_site_id) {
        this.vacation_site_id = vacation_site_id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getVacationSiteImage() {
        return vacationSiteImage;
    }

    public void setVacationSiteImage(String vacationSiteImage) {
        this.vacationSiteImage = vacationSiteImage;
    }


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
