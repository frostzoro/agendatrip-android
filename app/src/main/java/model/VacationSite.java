package model;

import android.location.Location;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by STEFAN on 2015/10/17.
 */
public class VacationSite implements Serializable {
    private int id;
    private String name;
    private String address;
    private String contactnumber;
    private String description;
    private String image;
    private ArrayList<String> images;
    private double latitude;
    private double longitude;
    private double rating;
    private double hypotheses;

    private double relevance;
    private ArrayList<VacationSiteTag> tags;

    //extras

    private double distance;

    public VacationSite(){
        this.images = new ArrayList<>();
        this.tags = new ArrayList<>();
    }

    public VacationSite(JSONObject src){
        try {
            this.id = src.getInt("id");
            this.name = src.getString("name");
            this.address = src.getString("address");
            this.contactnumber = src.getString("contactnumber");
            this.description = src.getString("description");
            this.latitude = src.getDouble("latitude");
            this.longitude = src.getDouble("longitude");
            this.rating = src.getDouble("rating");
            this.image = src.getString("image");
            this.images = new ArrayList<>();
            this.tags = new ArrayList<>();
            if(src.has("hypotheses"))
                this.hypotheses = src.getDouble("hypotheses");
            if(src.has("relevance"))
                this.relevance = src.getDouble("relevance");
        }
        catch(Exception e){
            Log.v("VacationSite Json Error", e.getMessage());
        }
    }

    public void loadTags(JSONArray src){
        if(src==null) return;
        int l = src.length();
        for(int i=0;i<l;i++){
            try {
                tags.add(new VacationSiteTag(src.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadImages(JSONArray src){

        int l = src.length();
        for(int i=0;i<l;i++){
            try {
                images.add(src.getJSONObject(i).getString("imageurl"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public String getTagsAsString(){
        int l = tags.size();
        String s="";
        for(int i=0;i<l;i++){
            s += tags.get(i).getName();
            if(s.length()<=20&&i!=l-1){
                s+=", ";
            }
            else if(s.length()>20 && i !=l-1){
                s+=",...";
                break;
            }
        }
        return s;
    }

    public VacationSite(VacationSite src){
        this.id = src.getID();
        this.name = src.getName();
        this.address = src.getAddress();
        this.contactnumber = src.getContactnumber();
        this.description = src.getDescription();
        this.latitude = src.getLatitude();
        this.longitude =src.getLongitude();
        this.rating = src.getRating();
        this.images = src.getImages();
        this.image = src.getImage();
        this.tags = src.getTags();
    }

    public int getID() {
        return id;
    }

    public void setID(int ID) {
        this.id = ID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<VacationSiteTag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<VacationSiteTag> tags) {
        this.tags = tags;
    }

    public double getHypotheses() {
        return hypotheses;
    }

    public void setHypotheses(double hypotheses) {
        this.hypotheses = hypotheses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void calculateDistance(Location target){
        Location currentLocation = new Location("current");
        currentLocation.setLatitude(this.latitude);
        currentLocation.setLongitude(this.longitude);
        this.distance = currentLocation.distanceTo(target);
    }

    public double getRelevance() {
        return relevance;
    }

    public void setRelevance(double relevance) {
        this.relevance = relevance;
    }
}
