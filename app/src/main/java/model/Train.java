package model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by STEFAN on 12/7/2015.
 */
public class Train implements Serializable, Parcelable {


    String id;
    int detail_availability;
    String train_id;
    String train_name;
    String departure_station;
    String departure_time;
    String departure_timestamp;
    String arrival_station;
    String arrival_time;
    String arrival_timestamp;
    String class_name;
    String class_name_lang;
    String detail_class_name;
    String subclass_name;
    Boolean is_promo,can_be_shown,is_closing;
    String formatted_schedule_date;
    Calendar timestamp;
    String time_diff;
    Double price_adult_clean, price_adult,price_child_clean,price_child,price_infant_clean,price_infant, price_total_clean;
    String price_total;
    String duration;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    String date;


    public Train(){

    }

    public Train(JSONObject flightdata){
        timestamp = Calendar.getInstance();
        try {
            this.detail_availability=flightdata.getInt("detail_availability");
            this.train_id=flightdata.getString("train_id");
            this.train_name=flightdata.getString("train_name");
            this.departure_station=flightdata.getString("departure_station");
            this.departure_timestamp=flightdata.getString("departure_timestamp");
            this.arrival_station=flightdata.getString("arrival_station");
            this.arrival_timestamp=flightdata.getString("arrival_timestamp");
            this.class_name=flightdata.getString("class_name");
            this.class_name_lang=flightdata.getString("class_name_lang");
            this.detail_class_name=flightdata.getString("detail_class_name");
            this.subclass_name=flightdata.getString("subclass_name");

            this.price_adult = flightdata.getDouble("price_adult");
            this.price_child = flightdata.getDouble("price_child");
            this.price_infant = flightdata.getDouble("price_infant");
            this.price_adult_clean=flightdata.getDouble("price_adult_clean");
            this.price_infant_clean=flightdata.getDouble("price_infant_clean");
            this.price_child_clean=flightdata.getDouble("price_child_clean");
            this.price_total=flightdata.getString("price_total");
            this.price_total_clean=flightdata.getDouble("price_total_clean");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            this.timestamp.setTime(sdf.parse(flightdata.getString("timestamp")));
           // this.is_promo = ( 1 == flightdata.getInt("has_food"));
            this.departure_time = flightdata.getString("departure_time");
            this.arrival_time = flightdata.getString("arrival_time");
            this.duration = flightdata.getString("duration");
            this.can_be_shown=true;
            this.formatted_schedule_date=flightdata.getString("formatted_schedule_date");
            this.time_diff=flightdata.getString("time_diff");
            this.duration=flightdata.getString("duration");

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }



    public Double getPrice_adult_clean() {
        return price_adult_clean;
    }

    public void setPrice_adult_clean(Double price_adult_clean) {
        this.price_adult_clean = price_adult_clean;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public String getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }


    public int getDetail_availability() {
        return detail_availability;
    }

    public void setDetail_availability(int detail_availability) {
        this.detail_availability = detail_availability;
    }

    public String getTrain_id() {
        return train_id;
    }

    public void setTrain_id(String train_id) {
        this.train_id = train_id;
    }

    public String getTrain_name() {
        return train_name;
    }

    public void setTrain_name(String train_name) {
        this.train_name = train_name;
    }

    public String getDeparture_station() {
        return departure_station;
    }

    public void setDeparture_station(String departure_station) {
        this.departure_station = departure_station;
    }

    public String getDeparture_timestamp() {
        return departure_timestamp;
    }

    public void setDeparture_timestamp(String departure_timestamp) {
        this.departure_timestamp = departure_timestamp;
    }

    public String getArrival_station() {
        return arrival_station;
    }

    public void setArrival_station(String arrival_station) {
        this.arrival_station = arrival_station;
    }

    public String getArrival_timestamp() {
        return arrival_timestamp;
    }

    public void setArrival_timestamp(String arrival_timestamp) {
        this.arrival_timestamp = arrival_timestamp;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getClass_name_lang() {
        return class_name_lang;
    }

    public void setClass_name_lang(String class_name_lang) {
        this.class_name_lang = class_name_lang;
    }

    public String getDetail_class_name() {
        return detail_class_name;
    }

    public void setDetail_class_name(String detail_class_name) {
        this.detail_class_name = detail_class_name;
    }

    public String getSubclass_name() {
        return subclass_name;
    }

    public void setSubclass_name(String subclass_name) {
        this.subclass_name = subclass_name;
    }

    public Boolean getCan_be_shown() {
        return can_be_shown;
    }

    public void setCan_be_shown(Boolean can_be_shown) {
        this.can_be_shown = can_be_shown;
    }

    public Boolean getIs_closing() {
        return is_closing;
    }

    public void setIs_closing(Boolean is_closing) {
        this.is_closing = is_closing;
    }

    public String getFormatted_schedule_date() {
        return formatted_schedule_date;
    }

    public void setFormatted_schedule_date(String formatted_schedule_date) {
        this.formatted_schedule_date = formatted_schedule_date;
    }

    public String getTime_diff() {
        return time_diff;
    }

    public void setTime_diff(String time_diff) {
        this.time_diff = time_diff;
    }

    public Double getPrice_child_clean() {
        return price_child_clean;
    }

    public void setPrice_child_clean(Double price_child_clean) {
        this.price_child_clean = price_child_clean;
    }

    public Double getPrice_infant_clean() {
        return price_infant_clean;
    }

    public void setPrice_infant_clean(Double price_infant_clean) {
        this.price_infant_clean = price_infant_clean;
    }

    public Double getPrice_total_clean() {
        return price_total_clean;
    }

    public void setPrice_total_clean(Double price_total_clean) {
        this.price_total_clean = price_total_clean;
    }

    public String getPrice_total() {
        return price_total;
    }

    public void setPrice_total(String price_total) {
        this.price_total = price_total;
    }

    public static Creator<Train> getCREATOR() {
        return CREATOR;
    }



    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }



    public Double getPrice_adult() {
        return price_adult;
    }

    public void setPrice_adult(Double price_adult) {
        this.price_adult = price_adult;
    }

    public Double getPrice_child() {
        return price_child;
    }

    public void setPrice_child(Double price_child) {
        this.price_child = price_child;
    }

    public Double getPrice_infant() {
        return price_infant;
    }

    public void setPrice_infant(Double price_infant) {
        this.price_infant = price_infant;
    }



    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }




    public Boolean getIs_promo() {
        return is_promo;
    }

    public void setIs_promo(Boolean is_promo) {
        this.is_promo = is_promo;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(departure_time);
        dest.writeString(arrival_time);
        dest.writeString(duration);
        dest.writeDouble(price_adult);
        dest.writeDouble(price_child);
        dest.writeDouble(price_infant);

        dest.writeByte((byte) (is_promo ? 1 : 0));

    }

    public static final Creator<Train> CREATOR
            = new Creator<Train>() {
        public Train createFromParcel(Parcel in) {
            return new Train(in);
        }

        public Train[] newArray(int size) {
            return new Train[size];
        }
    };

    private Train(Parcel in) {

        departure_time = in.readString();
        arrival_time = in.readString();
        duration = in.readString();
        price_adult = in.readDouble();
        price_child = in.readDouble();
        price_infant = in.readDouble();

    }


}
