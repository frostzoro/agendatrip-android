package model;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by STEFAN on 2015/10/31.
 */
public class VacationPackage implements Serializable {
    private int id;
    private double price;
    private int quota;
    private int purchaseCount;
    private double hypotheses;
    private String name,title,header,facilityInclusion,facilityExclusion,itinerary,notes,terms,
            image;
    private Date saleStart,saleEnd,dateStart,dateEnd;

    public VacationPackage(){

    }

    public VacationPackage(JSONObject src){
        try {
            this.setId(src.getInt("id"));
            this.setName(src.getString("title"));
            this.setPrice(src.getInt("price"));
            this.setQuota(src.getInt("quota"));
            this.setTitle(src.getString("title"));
            this.setHeader(src.getString("header"));
            this.setFacilityExclusion(src.getString("facility_inclusion"));
            this.setFacilityInclusion(src.getString("facility_exclusion"));
            this.setItinerary(src.getString("itinerary"));
            this.setNotes(src.getString("notes"));
            this.setTerms(src.getString("terms"));
            this.setImage(src.getString("image"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            this.setSaleStart(sdf.parse(src.getString("sale_start")));
            this.setSaleEnd(sdf.parse(src.getString("sale_end")));
            this.setDateStart(sdf.parse(src.getString("date_start")));
            this.setDateEnd(sdf.parse(src.getString("date_end")));
            if(src.has("purchase_count"))
                this.setPurchaseCount(src.getInt("purchase_count"));
            if(src.has("hypotheses"))
                this.setHypotheses(src.getDouble("hypotheses"));
        }
        catch(Exception e){
            e.printStackTrace();
            Log.v("vperrorjson","err");
        }
    }

    public VacationPackage(VacationPackage src){
        this.setId(src.getId());
        this.setPrice(src.getPrice());
        this.setQuota(src.getQuota());
        this.setTitle(src.getTitle());
        this.setHeader(src.getHeader());
        this.setFacilityInclusion(src.getFacilityInclusion());
        this.setFacilityExclusion(src.getFacilityExclusion());
        this.setItinerary(src.getItinerary());
        this.setNotes(src.getNotes());
        this.setTerms(src.getTerms());
        this.setImage(src.getImage());
        this.setSaleStart(src.getSaleStart());
        this.setSaleEnd(src.getSaleEnd());
        this.setDateStart(src.getDateStart());
        this.setDateEnd(src.getDateEnd());
        this.setPurchaseCount(src.getPurchaseCount());
    }

    public String getCompleteDateAsString(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String datestring = sdf.format(this.getDateStart()) + " - " + sdf.format(this.getDateEnd());
        return datestring;
    }

    public String getDepartureDateAsString(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String datestring = sdf.format(this.getDateStart());
        return datestring;
    }

    public String getEndDateAsString(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String datestring = sdf.format(this.getDateEnd());
        return datestring;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuota() {
        return quota;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public int getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(int purchaseCount) {
        this.purchaseCount = purchaseCount;
    }


    public double getHypotheses() {
        return hypotheses;
    }

    public void setHypotheses(double hypotheses) {
        this.hypotheses = hypotheses;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getFacilityInclusion() {
        return facilityInclusion;
    }

    public void setFacilityInclusion(String facilityInclusion) {
        this.facilityInclusion = facilityInclusion;
    }

    public String getFacilityExclusion() {
        return facilityExclusion;
    }

    public void setFacilityExclusion(String facilityExclusion) {
        this.facilityExclusion = facilityExclusion;
    }

    public String getItinerary() {
        return itinerary;
    }

    public void setItinerary(String itinerary) {
        this.itinerary = itinerary;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getSaleStart() {
        return saleStart;
    }

    public void setSaleStart(Date saleStart) {
        this.saleStart = saleStart;
    }

    public Date getSaleEnd() {
        return saleEnd;
    }

    public void setSaleEnd(Date saleEnd) {
        this.saleEnd = saleEnd;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }
}
