package model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by STEFAN on 12/7/2015.
 */
public class Flight implements Serializable, Parcelable {
    String flight_id;
    String airline_name;
    String flight_number;
    String departure_city;
    String arrival_city;
    String stop;
    String departure_time;
    String arrival_time;
    String long_via;
    String full_via;
    String markup_price_string;
    String image;
    String duration;
    String departure_city_name;
    String arrival_city_name;
    String date;
    Double price_value,price_adult,price_child,price_infant,markup_price;
    Calendar timestamp;
    Integer check_in_baggagge;
    Boolean has_food,best_deal,is_promo,need_baggage,airport_tax;

    public Flight(){

    }

    public Flight(JSONObject flightdata){
        timestamp = Calendar.getInstance();
        try {
            this.flight_id = flightdata.getString("flight_id");
            this.airline_name = flightdata.getString("airlines_name");
            this.flight_number = flightdata.getString("flight_number");
            this.departure_city = flightdata.getString("departure_city");
            this.arrival_city = flightdata.getString("arrival_city");
            this.stop = flightdata.getString("stop");
            this.price_value = flightdata.getDouble("price_value");
            this.price_adult = flightdata.getDouble("price_adult");
            this.price_child = flightdata.getDouble("price_child");
            this.price_infant = flightdata.getDouble("price_infant");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            this.timestamp.setTime(sdf.parse(flightdata.getString("timestamp")));
            this.has_food = ( 1 == flightdata.getInt("has_food"));
            this.check_in_baggagge = flightdata.getInt("check_in_baggage");
            this.is_promo = ( 1 == flightdata.getInt("has_food"));
            this.airport_tax = flightdata.getBoolean("airport_tax");
            this.departure_time = flightdata.getString("simple_departure_time");
            this.arrival_time = flightdata.getString("simple_arrival_time");
            this.long_via = flightdata.getString("long_via");
            this.full_via = flightdata.getString("full_via");
            this.markup_price_string = flightdata.getString("markup_price_string");
            if(this.markup_price_string.length()!=0)
                this.markup_price = flightdata.getDouble("markup_price");
            else this.markup_price = price_adult;
            this.need_baggage = (1 == flightdata.getInt("need_baggage"));
            this.duration = flightdata.getString("duration");
            this.image = flightdata.getString("image");
            this.departure_city_name= flightdata.getString("departure_city_name");
            this.arrival_city_name = flightdata.getString("arrival_city_name");
            this.best_deal = flightdata.getBoolean("best_deal");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getFlight_id() {
        return flight_id;
    }

    public void setFlight_id(String flight_id) {
        this.flight_id = flight_id;
    }

    public String getAirline_name() {
        return airline_name;
    }

    public void setAirline_name(String airline_name) {
        this.airline_name = airline_name;
    }

    public String getFlight_number() {
        return flight_number;
    }

    public void setFlight_number(String flight_number) {
        this.flight_number = flight_number;
    }

    public String getDeparture_city() {
        return departure_city;
    }

    public void setDeparture_city(String departure_city) {
        this.departure_city = departure_city;
    }

    public String getArrival_city() {
        return arrival_city;
    }

    public void setArrival_city(String arrival_city) {
        this.arrival_city = arrival_city;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public String getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }

    public String getLong_via() {
        return long_via;
    }

    public void setLong_via(String long_via) {
        this.long_via = long_via;
    }

    public String getFull_via() {
        return full_via;
    }

    public void setFull_via(String full_via) {
        this.full_via = full_via;
    }

    public String getMarkup_price_string() {
        return markup_price_string;
    }

    public void setMarkup_price_string(String markup_price_string) {
        this.markup_price_string = markup_price_string;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDeparture_city_name() {
        return departure_city_name;
    }

    public void setDeparture_city_name(String departure_city_name) {
        this.departure_city_name = departure_city_name;
    }

    public String getArrival_city_name() {
        return arrival_city_name;
    }

    public void setArrival_city_name(String arrival_city_name) {
        this.arrival_city_name = arrival_city_name;
    }

    public Double getPrice_value() {
        return price_value;
    }

    public void setPrice_value(Double price_value) {
        this.price_value = price_value;
    }

    public Double getPrice_adult() {
        return price_adult;
    }

    public void setPrice_adult(Double price_adult) {
        this.price_adult = price_adult;
    }

    public Double getPrice_child() {
        return price_child;
    }

    public void setPrice_child(Double price_child) {
        this.price_child = price_child;
    }

    public Double getPrice_infant() {
        return price_infant;
    }

    public void setPrice_infant(Double price_infant) {
        this.price_infant = price_infant;
    }

    public Double getMarkup_price() {
        return markup_price;
    }

    public void setMarkup_price(Double markup_price) {
        this.markup_price = markup_price;
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getCheck_in_baggagge() {
        return check_in_baggagge;
    }

    public void setCheck_in_baggagge(Integer check_in_baggagge) {
        this.check_in_baggagge = check_in_baggagge;
    }

    public Boolean getHas_food() {
        return has_food;
    }

    public void setHas_food(Boolean has_food) {
        this.has_food = has_food;
    }

    public Boolean getBest_deal() {
        return best_deal;
    }

    public void setBest_deal(Boolean best_deal) {
        this.best_deal = best_deal;
    }

    public Boolean getIs_promo() {
        return is_promo;
    }

    public void setIs_promo(Boolean is_promo) {
        this.is_promo = is_promo;
    }

    public Boolean getNeed_baggage() {
        return need_baggage;
    }

    public void setNeed_baggage(Boolean need_baggage) {
        this.need_baggage = need_baggage;
    }

    public Boolean getAirport_tax() {
        return airport_tax;
    }

    public void setAirport_tax(Boolean airport_tax) {
        this.airport_tax = airport_tax;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(flight_id);
        dest.writeString(airline_name);
        dest.writeString(flight_number);
        dest.writeString(departure_city);
        dest.writeString(arrival_city);
        dest.writeString(stop);
        dest.writeString(departure_time);
        dest.writeString(arrival_time);
        dest.writeString(long_via);
        dest.writeString(full_via);
        dest.writeString(markup_price_string);
        dest.writeString(image);
        dest.writeString(duration);
        dest.writeString(departure_city_name);
        dest.writeString(arrival_city_name);
        dest.writeString(date);
        dest.writeDouble(price_value);
        dest.writeDouble(price_adult);
        dest.writeDouble(price_child);
        dest.writeDouble(price_infant);
        dest.writeDouble(markup_price);
        dest.writeInt(check_in_baggagge);
        dest.writeByte((byte) (has_food ? 1 : 0));
        dest.writeByte((byte) (best_deal ? 1 : 0));
        dest.writeByte((byte) (is_promo ? 1 : 0));
        dest.writeByte((byte) (need_baggage ? 1 : 0));
        dest.writeByte((byte) (airport_tax ? 1 : 0));
    }

    public static final Parcelable.Creator<Flight> CREATOR
            = new Parcelable.Creator<Flight>() {
        public Flight createFromParcel(Parcel in) {
            return new Flight(in);
        }

        public Flight[] newArray(int size) {
            return new Flight[size];
        }
    };

    private Flight(Parcel in) {
        flight_id = in.readString();
        airline_name = in.readString();
        flight_number = in.readString();
        departure_city = in.readString();
        arrival_city = in.readString();
        stop = in.readString();
        departure_time = in.readString();
        arrival_time = in.readString();
        long_via = in.readString();
        full_via = in.readString();
        markup_price_string = in.readString();
        image = in.readString();
        duration = in.readString();
        departure_city_name = in.readString();
        arrival_city_name = in.readString();
        date = in.readString();
        price_value = in.readDouble();
        price_adult = in.readDouble();
        price_child = in.readDouble();
        price_infant = in.readDouble();
        markup_price = in .readDouble();
        check_in_baggagge = in.readInt();
        has_food = in.readByte() != 0;
        best_deal = in.readByte() != 0;
        is_promo = in.readByte() != 0;
        need_baggage = in.readByte() != 0;
        airport_tax = in.readByte() != 0;
    }


}
