package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by STEFAN on 1/3/2016.
 */
public class Passenger implements Serializable {
    String name;
    String phone;
    String ageGroup;
    String phoneNumber;
    String idNumber;
    String nationality;
    String birthdate;
    String ticketNumber,baggage;
    public Passenger(){

    }

    public Passenger(JSONObject source){
        try {
            if(source.has("passenger_name")){
                    this.name = source.getString("passenger_name");
            }
            if(source.has("passenger_phone")){
                this.phone = source.getString("passenger_phone");
            }
            if(source.has("passenger_age_group")){
                this.ageGroup = source.getString("passenger_age_group");
            }
            if(source.has("passenger_id_number")){
                this.idNumber = source.getString("passenger_id_number");
            }
            if(source.has("passenger_birth_date")){
                this.birthdate = source.getString(("passenger_birth_date"));
            }
            if(source.has("passenger_baggage")){
                this.baggage = source.getString("passenger_baggage");
            }
            if(source.has("passenger_ticket_number")){
                this.ticketNumber = source.getString("passenger_ticket_number");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }



}
