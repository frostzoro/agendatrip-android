package utils.Receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;

import singleton.AppController;
import utils.Util;

public class VacationPackageRejectReceiver extends BroadcastReceiver {

    NotificationManager nm;

    public VacationPackageRejectReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Util.isLoggedIn()){
            String url = Util.BASE_URL + Util.API_GATEWAY + Util.TAG_URL + Util.VACATION_PACKAGE_RESPONSE_URL;
            HashMap<String,String> param = new HashMap<>();
            param.putAll(Util.getUserParam());
            param.put("vacation_package_id", intent.getExtras().getString("vacation_package"));
            param.put("response",String.valueOf(0));
            Log.v("params", param.toString());
            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.POST, url, new JSONObject
                    (param), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.v("vprejectreceiver", "success!");
                }
            },new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.v("vprejectreceiver","failure!");
                    Log.v("vprejectreceiver", error.getMessage().toString());
                    error.printStackTrace();
                }
            });
            AppController.getInstance().addToRequestQueue(jor);
        }
        nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(-1);
        Log.v("vprejectreceiver","end of block");
    }
}
