package utils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RetryPolicy;

/**
 * Created by STEFAN on 2/18/2016.
 */
public class Constants {
    private static int TIMEOUT_600 = 600000;
    private static int TIMEOUT_300 = 300000;
    private static int TIMEOUT_60 = 60000 ;// in miliseconds
    private static int TIMEOUT_30 = 30000 ;
    private static int TIMEOUT_10 = 10000 ;
    private static int RETRY_ZERO = 0;

    public  static RetryPolicy RETRY_POLICY_LONG = new DefaultRetryPolicy(TIMEOUT_60, RETRY_ZERO, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ) {
    };
    public static RetryPolicy RETRY_POLICY = new DefaultRetryPolicy(TIMEOUT_30, RETRY_ZERO, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    public static RetryPolicy RETRY_POLICY_SHORT = new DefaultRetryPolicy(TIMEOUT_10, RETRY_ZERO,  DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    public static RetryPolicy RETRY_POLICY_BOOKING_LONG = new DefaultRetryPolicy(TIMEOUT_600, RETRY_ZERO, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT); // tiket.com api requires approx 5 mins to wait for transaction to be done
    public static RetryPolicy RETRY_POLICY_BOOKING = new DefaultRetryPolicy(TIMEOUT_300, RETRY_ZERO, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);



}
