package utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by STEFAN on 2015/10/20.
 */
public class GPSHandler{
    private static int FIVE_MINUTES = 1000*60*5;
    private static int TWO_MINUTES = 1000*60*5;
    private static int GPS_DELAY = 1000*10;
    private static int NETWORK_DELAY = 1000*10;
    private static int MEAN_COUNT_LIMIT = 3;
    private LocationManager locationManager;
    private Context mContext;
    private LocationListener gpsLocationListener,networkLocationListener;
    private Location currBestLocation;
    Boolean running;
    int meanCount;
    LocationProvider GPSProvider, NetworkProvider;
    public GPSHandler(final Context mContext){
        this.mContext=mContext;
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        gpsLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if(isBetterLocation(location,currBestLocation)){
                    currBestLocation = location;
                    Log.v("test", "GPS Location Updated");
                    meanCount++;
                }
                if(meanCount>=MEAN_COUNT_LIMIT){
                    stop();
                }
                Log.v("test", "GPS Location Get");
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        networkLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if(isBetterLocation(location,currBestLocation)){
                    currBestLocation = location;
                    Log.v("test", "Net Location Updated");
                    meanCount++;
                }
                if(meanCount>=MEAN_COUNT_LIMIT){
                    stop();
                }
                Log.v("test", "Net Location Get");
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        running = false;
        meanCount = 0;
        GPSProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);
        NetworkProvider = locationManager.getProvider(LocationManager.NETWORK_PROVIDER);
        NetworkProvider = NetworkProvider == null ? locationManager.getProvider(LocationManager.PASSIVE_PROVIDER) : NetworkProvider;
    }

    public void  startUpdates(){
        Log.v("GPSHandler","Starting Updates");
        if(!running) {
            for(String s : locationManager.getAllProviders()){
                Log.v("GPSHandler",s);
            }
            if(GPSProvider == null && NetworkProvider == null) {
                Log.v("GPSHandler", "No available location providers found");
                return;
            }
            if(GPSProvider != null)
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPS_DELAY, 0, gpsLocationListener);
            if(NetworkProvider != null)
                locationManager.requestLocationUpdates(NetworkProvider.getName(), NETWORK_DELAY, 0, networkLocationListener);
            running=true;
                Log.v("GPSHandler","Location Update Request Registered");
        }
    }

    public void stop(){
        Log.v("GPSHandler","Stopping Location Request");
        locationManager.removeUpdates(gpsLocationListener);
        locationManager.removeUpdates(networkLocationListener);
        running = false;
        meanCount=0;
    }

    public Location getCurrentLocation(){
        return currBestLocation;
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            return true;
        }

        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > FIVE_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -FIVE_MINUTES;
        boolean isNewer = timeDelta > 0;

        if (isSignificantlyNewer) {
            return true;
        } else if (isSignificantlyOlder) {
            return false;
        }

        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}

