package utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.widget.Toast;

import com.example.android.AgendaTrip.Login;
import com.example.android.AgendaTrip.MainActivity;
import com.example.android.AgendaTrip.R;

import java.util.Date;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

/**
 * Created by STEFAN on 2015/10/17.
 */
public class Util {
    public static int currentAgenda=0;
    public static String loggedID="";
    public static String loggedKey="";
    public static String loggedUserName="";
    public static String agendaDBSyncDate;
    public static final String BASE_URL = "http://www.agendatrip.com";
    public static final String API_GATEWAY = "/api/v1";
    public static final String USER_IDENTITY_PATH = "/user/identity";
    public static final String VACATION_SITE_PATH = "/destination";
    public static final String VACATION_PACKAGE_PATH ="/deals";
    public static final String VACATION_PACKAGE_PURCHASE_PATH ="/deals/purchases";
    public static final String COVER_URL = "/vacationimages/" +
            "/";
    public static final String THUMB_URL = "/small_vs_thumbs/ts_";
    public static final String VACATION_PACKAGE_IMAGE_URL = "/vacationpackages/";
    public static final String VACATION_PACKAGE_RESPONSE_URL = "/vp_response";
    public static final String VACATION_SITE_RESPONSE_URL = "/vs_response";
    public static final String TAG_URL = "/tags";
    public static final String BOOKING_URL = "/bookings";
    public static final String HOTEL_AUTOCOMPLETE_URL ="/hotels/autoComplete";
    public static final String PAYMENT_URL = "http://www.agendatrip.com/payment/pay";
    public static final String VACATION_PACKAGE_PURCHASE_DETAIL_URL = "http://www.agendatrip.com/api/v1/deals/purchases/show";
    public static final String GET_AGENDA_BOOKINGS_URL = "http://www.agendatrip.com/api/v1/bookings/showAgendaOrders";
    private static  Locale idLocale;

    public static String distanceToWord(double dist){
        if(dist > 1000){
            double distInKM = dist / 1000;
            return String.format("%.2f", distInKM) + " kilometres";
        }
        else{
            return String.format("%.2f", dist) + " metres";
        }
    }

    public static int getRatingColor(double rating){
        int R = (int)Math.round((double) 5 - (double)rating /(double) 5 *(double) 255);
        int G = (int)Math.round((double) rating /(double) 5 *(double) 255);

        return Color.rgb(R,G,0);
    }

    public static String paramToString(HashMap<String,String> param){
        String params = "";
        Boolean first = true;
        for( HashMap.Entry<String,String> entry : param.entrySet()){
            if(!first) params+="&";
            params += entry.getKey() + "=" + entry.getValue();
            first=false;
        }
        return params;
    }

    public static Boolean isLoggedIn(){
        return loggedID.length() > 0  && loggedKey.length() > 0;
    }

    public static HashMap<String,String> getUserParam(){
        HashMap<String,String> param = new HashMap<>();
        if(isLoggedIn()){
            param.put("userID",loggedID);
            param.put("key",loggedKey);
        }
        return param;
    }

    public static void LogOut(){
        loggedID="";
        loggedKey="";
    }

    public static void forceLogInFragment(Activity act){
        act.getFragmentManager().beginTransaction().replace(R.id.content_frame, new Login()).commit();
    }
    public static String convertDoubleToIDRString(double mony){
        if(idLocale==null) idLocale = new Locale("EN","ID");

        String money = NumberFormat.getCurrencyInstance(idLocale).format(mony);
        money = money.substring(0,3) + " " + money.substring(3,money.length());

        return money;
    }

}

