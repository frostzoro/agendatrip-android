package utils;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

import com.example.android.AgendaTrip.AddAgenda;
import com.example.android.AgendaTrip.RegisterAgendaDetail;

import java.util.Calendar;

/**
 * Created by Vin on 11/5/2015.
 */
public class RegAgendaHeaderTimePickerDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    //    DateSettings dateSettings=new DateSettings(getActivity());
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog dialog;
        dialog=new TimePickerDialog(getActivity(),(AddAgenda) getActivity(),hour,minute,false);
        return dialog;


    }

    public void onTimeSet(TimePicker view, int hour, int minute) {

    }


}
